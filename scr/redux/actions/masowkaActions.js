import {
  TOTAL_INCREASE,
  GREEN_INCREASE,
  GREEN_DECREASE,
  BLACK_INCREASE,
  YELLOW_INCREASE,
  RED_INCREASE,
  BLACK_DECREASE,
  YELLOW_DECREASE,
  RED_DECREASE,
  RESET_TOTAL,
  ITEM_PRESSED,
} from './types';

export const totalIncrease = () => ({
  type: TOTAL_INCREASE,
});

export const greenIncrease = () => ({
  type: GREEN_INCREASE,
});
export const greenDecrease = () => ({
  type: GREEN_DECREASE,
});
export const yellowDecrease = () => ({
  type: YELLOW_DECREASE,
});
export const yellowIncrease = () => ({
  type: YELLOW_INCREASE,
});
export const redIncrease = () => ({
  type: RED_INCREASE,
});
export const redDecrease = () => ({
  type: RED_DECREASE,
});
export const blackIncrease = () => ({
  type: BLACK_INCREASE,
});
export const blackDecrease = () => ({
  type: BLACK_DECREASE,
});

export const resetTotal = () => ({
  type: RESET_TOTAL,
});

export const itemPressed = myArray => ({
  type: ITEM_PRESSED,
  payload: myArray,
});
