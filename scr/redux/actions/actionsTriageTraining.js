import {
  STARTGAME,
  DIMINISH,
  RUN_OUT,
  ERROR_DIRECTION,
  INSTRUCTIONS,
  GREEN_ALLOWED,
  RED_ALLOWED,
  YELLOW_ALLOWED,
  BLACK_ALLOWED,
  BLACK_PLUS,
  RED_PLUS,
  YELLOW_PLUS,
  GREEN_PLUS,
  TOTAL_VICITMS,
} from './types';

export const startTheGame = () => ({
  type: STARTGAME,
});
export const diminishTime = () => ({
  type: DIMINISH,
});
export const runOutOfTime = () => ({
  type: RUN_OUT,
});
export const handleErrorBadDirection = error => ({
  type: ERROR_DIRECTION,
  payload: error,
});
export const setInstructionText = chodzi => ({
  type: INSTRUCTIONS,
  payload: chodzi,
});
export const setGreen = () => ({
  type: GREEN_ALLOWED,
});
export const setRed = () => ({
  type: RED_ALLOWED,
});
export const setYellow = () => ({
  type: YELLOW_ALLOWED,
});
export const setBlack = () => ({
  type: BLACK_ALLOWED,
});
export const blackPlusOne = () => ({
  type: BLACK_PLUS,
});
export const redPlusOne = () => ({
  type: RED_PLUS,
});
export const yellowPlusOne = () => ({
  type: YELLOW_PLUS,
});
export const greenPlusOne = () => ({
  type: GREEN_PLUS,
});
export const addVictim = () => ({
  type: TOTAL_VICITMS,
});
