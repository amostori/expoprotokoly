import {
  HEAD_PRESS,
  KLATA_PRESS,
  BELLY_PRESS,
  PLUS,
  MINUS,
  RAMIE_R_PRESS,
  RAMIE_L_PRESS,
  PRZEDRAMIE_R_PRESS,
  PRZEDRAMIE_L_PRESS,
  DLON_R,
  DLON_L,
  POSLADKI,
  STOPA_R,
  STOPA_L,
  UDO_R,
  UDO_L,
  P_UDO_L,
  P_UDO_R,
  GROINS,
  SZYJA,
  PLECY_G,
  PLECY_D,
  RESET_TOTAL,
  PARKLAND,
} from './types';

export const onPlusClick = () => ({
  type: PLUS,
});
export const onMinusClick = () => ({
  type: MINUS,
});
export const onResetClick = () => ({
  type: RESET_TOTAL,
});
export const onHeadPress = wiek => ({
  type: HEAD_PRESS,
  payload: wiek,
});

export const onKlataPress = () => ({
  type: KLATA_PRESS,
});
export const onBellyPress = () => ({
  type: BELLY_PRESS,
});
export const onRamieRPress = () => ({
  type: RAMIE_R_PRESS,
});
export const onRamieLPress = () => ({
  type: RAMIE_L_PRESS,
});
export const onPrzedRamieRPress = () => ({
  type: PRZEDRAMIE_R_PRESS,
});
export const onPrzedRamieLPress = () => ({
  type: PRZEDRAMIE_L_PRESS,
});
export const onDlonRPress = () => ({
  type: DLON_R,
});
export const onDlonLPress = () => ({
  type: DLON_L,
});
export const onPosladkiPress = () => ({
  type: POSLADKI,
});
export const onStopaRPress = () => ({
  type: STOPA_R,
});
export const onStopaLPress = () => ({
  type: STOPA_L,
});
export const onUdoRPress = wiek => ({
  type: UDO_R,
  payload: wiek,
});
export const onUdoLPress = wiek => ({
  type: UDO_L,
  payload: wiek,
});
export const onPodudzieRPress = wiek => ({
  type: P_UDO_R,
  payload: wiek,
});
export const onPodudzieLPress = wiek => ({
  type: P_UDO_L,
  payload: wiek,
});
export const onKroczePress = () => ({
  type: GROINS,
});
export const onSzyjaPressed = () => ({
  type: SZYJA,
});
export const onPlecyGoraPressed = () => ({
  type: PLECY_G,
});
export const onPlecyDolPressed = () => ({
  type: PLECY_D,
});
export const setParkland = weight => ({
  type: PARKLAND,
  payload: weight,
});
