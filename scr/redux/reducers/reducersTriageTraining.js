import {
  STARTGAME,
  DIMINISH,
  RUN_OUT,
  ERROR_DIRECTION,
  INSTRUCTIONS,
  GREEN_ALLOWED,
  RED_ALLOWED,
  YELLOW_ALLOWED,
  BLACK_ALLOWED,
  BLACK_PLUS,
  RED_PLUS,
  YELLOW_PLUS,
  GREEN_PLUS,
  TOTAL_VICITMS,
} from '../actions/types';

const INITIAL_STATE = {
  totalVictimNumber: 0,
  canGreenBePressed: false,
  canRedBePressed: false,
  canBlackBePressed: false,
  canYellowBePressed: false,
  nextQuestionAllowed: true,
  questionNumber: 0,
  tekst: '',
  disabled: false,
  blacks: 0,
  reds: 0,
  yellows: 0,
  greens: 0,
  instruction:
    'Wstępne badanie poszkodowanego w zdarzeniu masowym nie powinno trwać dłużej niż 20 sekund. Spróbuj w tym czasie przyznać odpowiedni kolor. Wybierz pierwsze pytanie:',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case STARTGAME:
      return {
        ...state,
        questionNumber: 0,
        instruction: '',
        seconds: 20,
        disabled: true,
        nextQuestionAllowed: true,
        canGreenBePressed: false,
        canRedBePressed: false,
        canBlackBePressed: false,
        canYellowBePressed: false,
      };
    case DIMINISH:
      return {
        ...state,
        seconds: state.seconds - 1,
        tekst: `${state.seconds} sek`,
      };
    case RUN_OUT:
      return {
        ...state,
        questionNumber: 0,
        instruction: 'Nie zdążyłeś. Brakło ci czasu. Spróbuj jeszcze raz.',
        canGreenBePressed: false,
        canRedBePressed: false,
        canBlackBePressed: false,
        canYellowBePressed: false,
        nextQuestionAllowed: true,
        disabled: false,
        blacks: 0,
        reds: 0,
        yellows: 0,
        greens: 0,
        totalVictimNumber: 0,
      };
    case ERROR_DIRECTION:
      return {
        ...state,
        instruction: action.payload,
        disabled: false,
        canGreenBePressed: false,
        canRedBePressed: false,
        canBlackBePressed: false,
        canYellowBePressed: false,
        questionNumber: 0,
        nextQuestionAllowed: true,
        blacks: 0,
        reds: 0,
        yellows: 0,
        greens: 0,
        totalVictimNumber: 0,
      };
    case INSTRUCTIONS:
      return {
        ...state,
        questionNumber: state.questionNumber + 1,
        instruction: action.payload,
      };
    case GREEN_ALLOWED:
      return {
        ...state,
        canGreenBePressed: true,
        nextQuestionAllowed: false,
      };
    case RED_ALLOWED:
      return {
        ...state,
        canRedBePressed: true,
        nextQuestionAllowed: false,
      };
    case YELLOW_ALLOWED:
      return {
        ...state,
        canYellowBePressed: true,
        nextQuestionAllowed: false,
      };
    case BLACK_ALLOWED:
      return {
        ...state,
        canBlackBePressed: true,
        nextQuestionAllowed: false,
      };
    case BLACK_PLUS:
      return {
        ...state,
        questionNumber: 0,
        instruction: 'Bardzo dobrze! Kolejny poszkodowany:',
        disabled: false,
        canGreenBePressed: false,
        canRedBePressed: false,
        canBlackBePressed: false,
        canYellowBePressed: false,
        nextQuestionAllowed: true,
        blacks: state.blacks + 1,
      };
    case RED_PLUS:
      return {
        ...state,
        questionNumber: 0,
        instruction: 'Bardzo dobrze! Kolejny poszkodowany:',
        disabled: false,
        canGreenBePressed: false,
        canRedBePressed: false,
        canBlackBePressed: false,
        canYellowBePressed: false,
        nextQuestionAllowed: true,
        reds: state.reds + 1,
      };
    case YELLOW_PLUS:
      return {
        ...state,
        questionNumber: 0,
        instruction: 'Bardzo dobrze! Kolejny poszkodowany:',
        disabled: false,
        canGreenBePressed: false,
        canRedBePressed: false,
        canBlackBePressed: false,
        canYellowBePressed: false,
        nextQuestionAllowed: true,
        yellows: state.yellows + 1,
      };
    case GREEN_PLUS:
      return {
        ...state,
        questionNumber: 0,
        instruction: 'Bardzo dobrze! Kolejny poszkodowany:',
        disabled: false,
        canGreenBePressed: false,
        canRedBePressed: false,
        canBlackBePressed: false,
        canYellowBePressed: false,
        greens: state.greens + 1,
        nextQuestionAllowed: true,
      };
    case TOTAL_VICITMS:
      return {
        ...state,
        totalVictimNumber: state.totalVictimNumber + 1,
        questionNumber: 0,
      };
    default:
      return INITIAL_STATE;
  }
};
