/* eslint-disable no-fallthrough */
import {
  HEAD_PRESS,
  KLATA_PRESS,
  BELLY_PRESS,
  PLUS,
  MINUS,
  RESET_TOTAL,
  RAMIE_R_PRESS,
  RAMIE_L_PRESS,
  PRZEDRAMIE_R_PRESS,
  PRZEDRAMIE_L_PRESS,
  DLON_R,
  DLON_L,
  POSLADKI,
  STOPA_R,
  STOPA_L,
  UDO_R,
  UDO_L,
  P_UDO_L,
  P_UDO_R,
  GROINS,
  SZYJA,
  PLECY_G,
  PLECY_D,
  PARKLAND,
} from '../actions/types';

const INITIAL_STATE = {
  total: 0,
  headPressed: false,
  klataPressed: false,
  bellyPressed: false,
  ramieRPressed: false,
  ramieLPressed: false,
  przedramieLPressed: false,
  przedramieRPressed: false,
  dlonRPressed: false,
  dlonLPressed: false,
  posladkiPressed: false,
  stopaRPressed: false,
  stopaLPressed: false,
  isUdoRPressed: false,
  isUdoLPressed: false,
  isPodUdoRPressed: false,
  isPodUdoLPressed: false,
  isGroinsPressed: false,
  isSzyjaPressed: false,
  isPlecyGoraPressed: false,
  isPlecyDolPressed: false,
  weight: 0,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PLUS:
      if (state.total < 100) {
        return {
          ...state,
          total: state.total + 1,
        };
      }
      return {
        ...state,
        total: 100,
      };
    case MINUS:
      if (state.total > 0) {
        return {
          ...state,
          total: state.total - 1,
        };
      }
      return {
        ...state,
        total: 0,
      };
    case RESET_TOTAL:
      return INITIAL_STATE;
    case PARKLAND:
      return {
        ...state,
        weight: action.payload,
      };
    case HEAD_PRESS:
      switch (action.payload) {
        // dziecko 0 - 1 HEAD_PRESS
        case 1:
          if (state.headPressed) {
            return {
              ...state,
              headPressed: !state.headPressed,
              total: state.total - 19,
            };
          }
          return {
            ...state,
            headPressed: !state.headPressed,
            total: state.total + 19,
          };
        // dziecko 1-4 HEAD_PRESS
        case 2:
          if (state.headPressed) {
            return {
              ...state,
              headPressed: !state.headPressed,
              total: state.total - 17,
            };
          }
          return {
            ...state,
            headPressed: !state.headPressed,
            total: state.total + 17,
          };
        // dziecko 5-9 HEAD_PRESS
        case 3:
          if (state.headPressed) {
            return {
              ...state,
              headPressed: !state.headPressed,
              total: state.total - 13,
            };
          }
          return {
            ...state,
            headPressed: !state.headPressed,
            total: state.total + 13,
          };
        // dziecko 10-14 HEAD_PRESS
        case 4:
          if (state.headPressed) {
            return {
              ...state,
              headPressed: !state.headPressed,
              total: state.total - 11,
            };
          }
          return {
            ...state,
            headPressed: !state.headPressed,
            total: state.total + 11,
          };
        // dorosly HEAD_PRESS
        case 5:
          if (state.headPressed) {
            return {
              ...state,
              headPressed: !state.headPressed,
              total: state.total - 8,
            };
          }
          return {
            ...state,
            headPressed: !state.headPressed,
            total: state.total + 8,
          };
        default:
          break;
      }
      break;
    case UDO_R:
      switch (action.payload) {
        case 1:
          if (state.isUdoRPressed) {
            return {
              ...state,
              isUdoRPressed: !state.isUdoRPressed,
              total: state.total - 5.5,
            };
          }
          return {
            ...state,
            isUdoRPressed: !state.isUdoRPressed,
            total: state.total + 5.5,
          };
        case 2:
          if (state.isUdoRPressed) {
            return {
              ...state,
              isUdoRPressed: !state.isUdoRPressed,
              total: state.total - 6.5,
            };
          }
          return {
            ...state,
            isUdoRPressed: !state.isUdoRPressed,
            total: state.total + 6.5,
          };
        case 3: // case 3 i 4 robią to samo, więc jeśli oceniany jest case 3 brak
        // break spowoduje przejście do case 4
        case 4:
          if (state.isUdoRPressed) {
            return {
              ...state,
              isUdoRPressed: !state.isUdoRPressed,
              total: state.total - 8.5,
            };
          }
          return {
            ...state,
            isUdoRPressed: !state.isUdoRPressed,
            total: state.total + 8.5,
          };
        case 5:
          if (state.isUdoRPressed) {
            return {
              ...state,
              isUdoRPressed: !state.isUdoRPressed,
              total: state.total - 9,
            };
          }
          return {
            ...state,
            isUdoRPressed: !state.isUdoRPressed,
            total: state.total + 9,
          };
        default:
          break;
      }
      break;
    case UDO_L:
      switch (action.payload) {
        case 1:
          if (state.isUdoLPressed) {
            return {
              ...state,
              isUdoLPressed: !state.isUdoLPressed,
              total: state.total - 5.5,
            };
          }
          return {
            ...state,
            isUdoLPressed: !state.isUdoLPressed,
            total: state.total + 5.5,
          };
        case 2:
          if (state.isUdoLPressed) {
            return {
              ...state,
              isUdoLPressed: !state.isUdoLPressed,
              total: state.total - 6.5,
            };
          }
          return {
            ...state,
            isUdoLPressed: !state.isUdoLPressed,
            total: state.total + 6.5,
          };
        case 3:
        case 4:
          if (state.isUdoLPressed) {
            return {
              ...state,
              isUdoLPressed: !state.isUdoLPressed,
              total: state.total - 8.5,
            };
          }
          return {
            ...state,
            isUdoLPressed: !state.isUdoLPressed,
            total: state.total + 8.5,
          };
        case 5:
          if (state.isUdoLPressed) {
            return {
              ...state,
              isUdoLPressed: !state.isUdoLPressed,
              total: state.total - 9,
            };
          }
          return {
            ...state,
            isUdoLPressed: !state.isUdoLPressed,
            total: state.total + 9,
          };
        default:
          break;
      }
      break;
    case P_UDO_R:
      switch (action.payload) {
        case 1:
        case 2:
        case 3:
          if (state.isPodUdoRPressed) {
            return {
              ...state,
              isPodUdoRPressed: !state.isPodUdoRPressed,
              total: state.total - 5,
            };
          }
          return {
            ...state,
            isPodUdoRPressed: !state.isPodUdoRPressed,
            total: state.total + 5,
          };
        case 4:
          if (state.isPodUdoRPressed) {
            return {
              ...state,
              isPodUdoRPressed: !state.isPodUdoRPressed,
              total: state.total - 6,
            };
          }
          return {
            ...state,
            isPodUdoRPressed: !state.isPodUdoRPressed,
            total: state.total + 6,
          };
        case 5:
          if (state.isPodUdoRPressed) {
            return {
              ...state,
              isPodUdoRPressed: !state.isPodUdoRPressed,
              total: state.total - 7,
            };
          }
          return {
            ...state,
            isPodUdoRPressed: !state.isPodUdoRPressed,
            total: state.total + 7,
          };
        default:
          break;
      }
      break;
    case P_UDO_L:
      switch (action.payload) {
        case 1:
        case 2:
        case 3:
          if (state.isPodUdoLPressed) {
            return {
              ...state,
              isPodUdoLPressed: !state.isPodUdoLPressed,
              total: state.total - 5,
            };
          }
          return {
            ...state,
            isPodUdoLPressed: !state.isPodUdoLPressed,
            total: state.total + 5,
          };
        case 4:
          if (state.isPodUdoLPressed) {
            return {
              ...state,
              isPodUdoLPressed: !state.isPodUdoLPressed,
              total: state.total - 6,
            };
          }
          return {
            ...state,
            isPodUdoLPressed: !state.isPodUdoLPressed,
            total: state.total + 6,
          };
        case 5:
          if (state.isPodUdoLPressed) {
            return {
              ...state,
              isPodUdoLPressed: !state.isPodUdoLPressed,
              total: state.total - 7,
            };
          }
          return {
            ...state,
            isPodUdoLPressed: !state.isPodUdoLPressed,
            total: state.total + 7,
          };
        default:
          break;
      }
      break;
    case KLATA_PRESS:
      if (state.klataPressed) {
        return {
          ...state,
          klataPressed: !state.klataPressed,
          total: state.total - 6.5,
        };
      }
      return {
        ...state,
        klataPressed: !state.klataPressed,
        total: state.total + 6.5,
      };
    case BELLY_PRESS:
      if (state.bellyPressed) {
        return {
          ...state,
          bellyPressed: !state.bellyPressed,
          total: state.total - 6.5,
        };
      }
      return {
        ...state,
        bellyPressed: !state.bellyPressed,
        total: state.total + 6.5,
      };
    case RAMIE_R_PRESS:
      if (state.ramieRPressed) {
        return {
          ...state,
          ramieRPressed: !state.ramieRPressed,
          total: state.total - 4,
        };
      }
      return {
        ...state,
        ramieRPressed: !state.ramieRPressed,
        total: state.total + 4,
      };
    case RAMIE_L_PRESS:
      if (state.ramieLPressed) {
        return {
          ...state,
          ramieLPressed: !state.ramieLPressed,
          total: state.total - 4,
        };
      }
      return {
        ...state,
        ramieLPressed: !state.ramieLPressed,
        total: state.total + 4,
      };
    case PRZEDRAMIE_L_PRESS:
      if (state.przedramieLPressed) {
        return {
          ...state,
          przedramieLPressed: !state.przedramieLPressed,
          total: state.total - 3,
        };
      }
      return {
        ...state,
        przedramieLPressed: !state.przedramieLPressed,
        total: state.total + 3,
      };
    case PRZEDRAMIE_R_PRESS:
      if (state.przedramieRPressed) {
        return {
          ...state,
          przedramieRPressed: !state.przedramieRPressed,
          total: state.total - 3,
        };
      }
      return {
        ...state,
        przedramieRPressed: !state.przedramieRPressed,
        total: state.total + 3,
      };
    case DLON_R:
      if (state.dlonRPressed) {
        return {
          ...state,
          dlonRPressed: !state.dlonRPressed,
          total: state.total - 2.5,
        };
      }
      return {
        ...state,
        dlonRPressed: !state.dlonRPressed,
        total: state.total + 2.5,
      };
    case DLON_L:
      if (state.dlonLPressed) {
        return {
          ...state,
          dlonLPressed: !state.dlonLPressed,
          total: state.total - 2.5,
        };
      }
      return {
        ...state,
        dlonLPressed: !state.dlonLPressed,
        total: state.total + 2.5,
      };
    case POSLADKI:
      if (state.posladkiPressed) {
        return {
          ...state,
          posladkiPressed: !state.posladkiPressed,
          total: state.total - 5,
        };
      }
      return {
        ...state,
        posladkiPressed: !state.posladkiPressed,
        total: state.total + 5,
      };
    case STOPA_R:
      if (state.stopaRPressed) {
        return {
          ...state,
          stopaRPressed: !state.stopaRPressed,
          total: state.total - 3.5,
        };
      }
      return {
        ...state,
        stopaRPressed: !state.stopaRPressed,
        total: state.total + 3.5,
      };
    case STOPA_L:
      if (state.stopaLPressed) {
        return {
          ...state,
          stopaLPressed: !state.stopaLPressed,
          total: state.total - 3.5,
        };
      }
      return {
        ...state,
        stopaLPressed: !state.stopaLPressed,
        total: state.total + 3.5,
      };
    case GROINS:
      if (state.isGroinsPressed) {
        return {
          ...state,
          isGroinsPressed: !state.isGroinsPressed,
          total: state.total - 1,
        };
      }
      return {
        ...state,
        isGroinsPressed: !state.isGroinsPressed,
        total: state.total + 1,
      };
    case SZYJA:
      if (state.isSzyjaPressed) {
        return {
          ...state,
          isSzyjaPressed: !state.isSzyjaPressed,
          total: state.total - 2,
        };
      }
      return {
        ...state,
        isSzyjaPressed: !state.isSzyjaPressed,
        total: state.total + 2,
      };
    case PLECY_G:
      if (state.isPlecyGoraPressed) {
        return {
          ...state,
          isPlecyGoraPressed: !state.isPlecyGoraPressed,
          total: state.total - 6.5,
        };
      }
      return {
        ...state,
        isPlecyGoraPressed: !state.isPlecyGoraPressed,
        total: state.total + 6.5,
      };
    case PLECY_D:
      if (state.isPlecyDolPressed) {
        return {
          ...state,
          isPlecyDolPressed: !state.isPlecyDolPressed,
          total: state.total - 6.5,
        };
      }
      return {
        ...state,
        isPlecyDolPressed: !state.isPlecyDolPressed,
        total: state.total + 6.5,
      };
    default:
      return state;
  }
};
