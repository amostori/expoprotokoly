import {
  TOTAL_INCREASE,
  GREEN_INCREASE,
  GREEN_DECREASE,
  BLACK_INCREASE,
  YELLOW_INCREASE,
  RED_INCREASE,
  BLACK_DECREASE,
  YELLOW_DECREASE,
  RED_DECREASE,
  RESET_TOTAL,
  ITEM_PRESSED,
} from '../actions/types';

const INITIAL_STATE = {
  green: 0,
  yellow: 0,
  red: 0,
  black: 0,
  total: 0,
  itemPressedId: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TOTAL_INCREASE:
      return {
        ...state,
        total: state.total + 1,
      };
    case GREEN_INCREASE:
      return {
        ...state,
        green: state.green + 1,
        total: state.total + 1,
      };
    case GREEN_DECREASE:
      if (state.green > 0) {
        return {
          ...state,
          green: state.green - 1,
          total: state.total - 1,
        };
      }
      return {
        ...state,
      };
    case YELLOW_DECREASE:
      if (state.yellow > 0) {
        return {
          ...state,
          yellow: state.yellow - 1,
          total: state.total - 1,
        };
      }
      return {
        ...state,
      };
    case RED_DECREASE:
      if (state.red > 0) {
        return {
          ...state,
          red: state.red - 1,
          total: state.total - 1,
        };
      }
      return {
        ...state,
      };
    case BLACK_DECREASE:
      if (state.black > 0) {
        return {
          ...state,
          black: state.black - 1,
          total: state.total - 1,
        };
      }
      return {
        ...state,
      };
    case BLACK_INCREASE:
      return {
        ...state,
        black: state.black + 1,
        total: state.total + 1,
      };
    case YELLOW_INCREASE:
      return {
        ...state,
        yellow: state.yellow + 1,
        total: state.total + 1,
      };
    case RED_INCREASE:
      return {
        ...state,
        red: state.red + 1,
        total: state.total + 1,
      };
    case RESET_TOTAL:
      return {
        ...state,
        green: 0,
        yellow: 0,
        red: 0,
        black: 0,
        total: 0,
        itemPressedId: [],
      };
    case ITEM_PRESSED:
      return {
        ...state,
        itemPressedId: action.payload,
      };
    default:
      return state;
  }
};
