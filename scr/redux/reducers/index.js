import { combineReducers } from 'redux';
import MasowkaReducer from './MasowkaReducer';
import BurnsReducer from './BurnsReducer';
import reducersTriageTraining from './reducersTriageTraining';

export default combineReducers({
  masowkaReducer: MasowkaReducer,
  burnsReducer: BurnsReducer,
  triageTrainingReducer: reducersTriageTraining,
});
