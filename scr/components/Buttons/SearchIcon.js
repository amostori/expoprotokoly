import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';
import styles from './styles';

const SearchIcon = ({ searchEverything }) => (
  <View style={styles.filmButton}>
    <TouchableOpacity onPress={searchEverything}>
      <Ionicons name="ios-search" size={32} color="white" />
    </TouchableOpacity>
  </View>
);

SearchIcon.propTypes = {
  searchEverything: PropTypes.func,
};

export default SearchIcon;
