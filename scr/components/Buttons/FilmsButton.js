import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';
import styles from './styles';

const FilmsButton = ({ showMovie }) => (
  <View style={styles.filmButton}>
    <TouchableOpacity onPress={showMovie}>
      <Ionicons name="md-film" size={32} color="white" />
    </TouchableOpacity>
  </View>
);

FilmsButton.propTypes = {
  showMovie: PropTypes.func,
};

export default FilmsButton;
