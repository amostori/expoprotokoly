import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const MyButton = ({ onPress, tekst }) => (
  <TouchableOpacity style={styles.container} onPress={onPress}>
    <View style={styles.wrapper}>
      <Text style={styles.tekst}>{tekst}</Text>
    </View>
  </TouchableOpacity>
);

MyButton.propTypes = {
  onPress: PropTypes.func,
  tekst: PropTypes.string,
};

export default MyButton;
