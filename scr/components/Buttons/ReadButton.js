import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { Entypo } from '@expo/vector-icons';
import styles from './styles';

const ReadButton = ({ openInfo }) => (
  <View style={styles.filmButton}>
    <TouchableOpacity onPress={openInfo}>
      <Entypo name="open-book" size={32} color="white" />
    </TouchableOpacity>
  </View>
);

ReadButton.propTypes = {
  openInfo: PropTypes.func,
};

export default ReadButton;
