import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const TriageColorButton = ({
  onPress, children, kolor, tekstKolor, onLongPress,
}) => (
  <TouchableOpacity
    style={[styles.triageContainer, { backgroundColor: kolor }]}
    onPress={onPress}
    onLongPress={onLongPress}
  >
    <View>
      <Text style={[styles.triageTekst, { color: tekstKolor }]}>{children}</Text>
    </View>
  </TouchableOpacity>
);

TriageColorButton.propTypes = {
  onPress: PropTypes.func,
  onLongPress: PropTypes.func,
  children: PropTypes.any,
  kolor: PropTypes.string,
  tekstKolor: PropTypes.string,
};

export default TriageColorButton;
