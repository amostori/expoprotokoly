import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import TriageColorButton from './TriageColorButton';

const TriageColorButtons = ({
  onPressGreen,
  onPressYellow,
  onPressRed,
  onPressBlack,
  greenTekst,
  yellowTekst,
  redTekst,
  blackTekst,
  onGreenLongPress,
  onYellowLongPress,
  onRedLongPress,
  onBlackLongPress,
}) => (
  <View style={styles.triageColorButtonsContainer}>
    <TriageColorButton
      kolor="green"
      tekstKolor="white"
      onPress={onPressGreen}
      onLongPress={onGreenLongPress}
    >
      {greenTekst}
    </TriageColorButton>
    <TriageColorButton
      kolor="yellow"
      tekstKolor="black"
      onPress={onPressYellow}
      onLongPress={onYellowLongPress}
    >
      {yellowTekst}
    </TriageColorButton>
    <TriageColorButton
      kolor="red"
      tekstKolor="white"
      onPress={onPressRed}
      onLongPress={onRedLongPress}
    >
      {redTekst}
    </TriageColorButton>
    <TriageColorButton
      kolor="black"
      tekstKolor="white"
      onPress={onPressBlack}
      onLongPress={onBlackLongPress}
    >
      {blackTekst}
    </TriageColorButton>
  </View>
);

TriageColorButtons.propTypes = {
  onPressGreen: PropTypes.func,
  onPressYellow: PropTypes.func,
  onPressRed: PropTypes.func,
  onPressBlack: PropTypes.func,
  onGreenLongPress: PropTypes.func,
  onYellowLongPress: PropTypes.func,
  onRedLongPress: PropTypes.func,
  onBlackLongPress: PropTypes.func,
  greenTekst: PropTypes.any,
  yellowTekst: PropTypes.any,
  redTekst: PropTypes.any,
  blackTekst: PropTypes.any,
};

export default TriageColorButtons;
