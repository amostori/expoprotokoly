import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import MyButton from './MyButton';
import styles from './styles';

const ButtonsYesNo = ({ onPressYes, onPressNo }) => (
  <View style={styles.containerYesNo}>
    <MyButton tekst="TAK" onPress={onPressYes} />
    <MyButton tekst="NIE" onPress={onPressNo} />
  </View>
);

ButtonsYesNo.propTypes = {
  onPressYes: PropTypes.func,
  onPressNo: PropTypes.func,
};
export default ButtonsYesNo;
