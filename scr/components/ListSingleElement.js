/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import {
  StyleSheet, View, Text, Image, TouchableOpacity,
} from 'react-native';

const styless = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: 10,
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#e9ecf0',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#acb0b7',
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: 70,
    alignSelf: 'stretch',
  },
  textContainer: {
    width: 0,
    flexGrow: 1,
    alignItems: 'center',
  },
  title: {
    color: '#FF5353',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    fontSize: 20,
    paddingLeft: 15,
  },
  text: {
    color: '#D4D5D4',
    fontSize: 16,
    paddingLeft: 15,
  },
  img: {
    height: 50,
    width: 50,
    marginLeft: 15,
    borderRadius: 25,
  },
});

export default class ListSingleElement extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPressMe}>
        <View style={styless.container}>
          <View>
            <Image source={this.props.myImageFunc} resizeMode="cover" style={styless.img} />
          </View>
          <View style={styless.textContainer}>
            <Text style={styless.title}>{this.props.title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
