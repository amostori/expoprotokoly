import EStyleSheet from 'react-native-extended-stylesheet';
import { StatusBar } from 'react-native';

export default EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e9ecf0',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    flexWrap: 'wrap',
    padding: 10,
    '@media ios': {
      paddingTop: 20,
    },
    '@media android': {
      paddingTop: StatusBar.currentHeight,
    },
  },
  image: {
    width: 100,
    height: 100,
    margin: 4,
  },
});

// justifyContent: 'space-between' oznacza, że między elementami będzie maksymalnie duzo
// przestrzeni.
// Rozdzieli to elementy na maksa od siebie.
