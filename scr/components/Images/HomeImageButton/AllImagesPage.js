import React, { Component } from 'react';
import { View } from 'react-native';
import { FloatingAction } from 'react-native-floating-action';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import ListSingleElement from '../../ListSingleElement';

export default class AllImagesPage extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  // Uwaga, żeby to zadziałało trzeba  przesłać "notification" w propsach z nadrzędnego
  // componentu, tu z Home.js
  touchEventOnTools = () => {
    const { navigation } = this.props;
    navigation.navigate('ToolsScreen');
  };

  touchEventOnAls = () => {
    const { navigation } = this.props;
    navigation.navigate('AlsScreen');
  };

  touchEventOnPAls = () => {
    const { navigation } = this.props;
    navigation.navigate('PalsScreen');
  };

  touchEventOnciaza = () => {
    const { navigation } = this.props;
    navigation.navigate('PoloznictwoScreen');
  };

  touchEventOncases = () => {
    const { navigation } = this.props;
    navigation.navigate('CasesScreen');
  };

  touchEventOnFarma = () => {
    const { navigation } = this.props;
    navigation.navigate('FarmaScreen');
  };

  touchEventOnUraz = () => {
    const { navigation } = this.props;
    navigation.navigate('UrazyScreen');
  };

  touchEventOnZatrucia = () => {
    const { navigation } = this.props;
    navigation.navigate('ZatruciaScreen');
  };

  touchEventOnLekiUDzieci = () => {
    const { navigation } = this.props;
    navigation.navigate('LekiUDzieciScreen');
  };

  startCPR = () => {
    const { navigation } = this.props;
    navigation.navigate('CPRAssistant');
  };

  render() {
    return (
      <View>
        <ListSingleElement
          title="Narzędzia"
          myImageFunc={require('./images/tools.png')}
          onPressMe={this.touchEventOnTools}
        />
        <ListSingleElement
          title="ALS"
          myImageFunc={require('./images/als.png')}
          onPressMe={this.touchEventOnAls}
        />
        <ListSingleElement
          title="PALS"
          myImageFunc={require('./images/pals.png')}
          onPressMe={this.touchEventOnPAls}
        />
        <ListSingleElement
          title="Ciąża"
          myImageFunc={require('./images/ciaza.png')}
          onPressMe={this.touchEventOnciaza}
        />
        <ListSingleElement
          title="Urazy"
          myImageFunc={require('./images/urazy.png')}
          onPressMe={this.touchEventOnUraz}
        />
        <ListSingleElement
          title="Zatrucia"
          myImageFunc={require('./images/zatrucia.png')}
          onPressMe={this.touchEventOnZatrucia}
        />
        <ListSingleElement
          title="Farmakologia"
          myImageFunc={require('./images/farma.png')}
          onPressMe={this.touchEventOnFarma}
        />

        <ListSingleElement
          title="Leki u dzieci"
          myImageFunc={require('./images/lekiUDzieci.png')}
          onPressMe={this.touchEventOnLekiUDzieci}
        />
        <ListSingleElement
          title="Przypadki"
          myImageFunc={require('./images/cases.png')}
          onPressMe={this.touchEventOncases}
        />
      </View>
    );
  }
}
