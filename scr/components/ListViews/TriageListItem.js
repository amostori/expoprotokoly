import React from 'react';
import { TouchableHighlight, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './ALSList/styles';

const TriageListItem = ({ text, onPress }) => (
  <TouchableHighlight onPress={onPress} underlayColor={styles.$underlayColor}>
    <View style={styles.container}>
      <Text style={styles.tekstStyle}>{text}</Text>
    </View>
  </TouchableHighlight>
);

TriageListItem.propTypes = {
  text: PropTypes.string,
  onPress: PropTypes.func,
};
export default TriageListItem;
