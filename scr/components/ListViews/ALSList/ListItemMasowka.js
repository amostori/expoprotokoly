import React from 'react';
import { TouchableHighlight, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const ListItemMasowka = ({ text, onPress, selected = false }) => (
  <TouchableHighlight onPress={onPress} underlayColor={styles.$underlayColor}>
    <View style={styles.container}>
      {selected ? (
        <Text style={styles.itemSelectedText}>{text}</Text>
      ) : (
        <Text style={styles.tekstStyle}>{text}</Text>
      )}
    </View>
  </TouchableHighlight>
);

ListItemMasowka.propTypes = {
  text: PropTypes.string,
  onPress: PropTypes.func,
  selected: PropTypes.bool,
};
export default ListItemMasowka;
