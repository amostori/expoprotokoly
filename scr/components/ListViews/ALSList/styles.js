import EStyleSheet from 'react-native-extended-stylesheet';
import { StyleSheet } from 'react-native';

const styles = EStyleSheet.create({
  $underlayColor: '$border',
  $textColor: '$darkText',
  container: {
    paddingHorizontal: 20,
    paddingVertical: 16,
    alignItems: 'center',
  },
  tekstStyle: {
    fontSize: 18,
    textAlign: 'center',
    color: '$darkText',
  },
  itemSelectedText: {
    fontSize: 18,
    textAlign: 'center',
    color: '$lightText',
  },
  separator: {
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: '$border',
    flex: 1,
    height: StyleSheet.hairlineWidth,
  },
});

export default styles;
