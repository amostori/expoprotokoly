import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const BadanieHipertekst = ({ onPress, children, onLongPress }) => (
  <TouchableOpacity onPress={onPress} onLongPress={onLongPress}>
    <View style={styles.badanieContainer}>
      <Text style={styles.badanieHypertekst} onPress={onPress}>
        {' '}
        {children}
        {' '}
      </Text>
    </View>
  </TouchableOpacity>
);

BadanieHipertekst.propTypes = {
  onPress: PropTypes.func,
  onLongPress: PropTypes.func,
  children: PropTypes.any,
};

export default BadanieHipertekst;
