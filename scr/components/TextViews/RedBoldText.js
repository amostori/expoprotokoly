import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const RedBoldText = ({ children }) => (
  <Text style={styles.redBoldText}>
    {' '}
    {children}
    {' '}
  </Text>
);

RedBoldText.propTypes = {
  children: PropTypes.any,
};

export default RedBoldText;
