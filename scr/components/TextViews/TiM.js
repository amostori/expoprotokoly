import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

const TiM = () => (
  <View style={styles.container}>
    <Text style={styles.mainTekst}>Pełen monitoring i transport na SOR.</Text>
  </View>
);

export default TiM;
