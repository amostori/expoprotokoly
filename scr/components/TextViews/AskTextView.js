import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const AskTextView = ({ children }) => <Text style={styles.askTekst}>{children}</Text>;

AskTextView.propTypes = {
  children: PropTypes.any,
};

export default AskTextView;
