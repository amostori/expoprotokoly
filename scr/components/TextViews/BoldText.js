import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const BoldText = ({ children, style }) => (
  <Text style={[styles.boldText, style]}>
    {' '}
    {children}
    {' '}
  </Text>
);

BoldText.propTypes = {
  children: PropTypes.any,
  style: PropTypes.any,
};

export default BoldText;
