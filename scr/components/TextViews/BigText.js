import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

const BigText = ({ children, color }) => (
  <Text style={{ color, fontSize: 22, fontWeight: 'bold' }}>{children}</Text>
);

BigText.propTypes = {
  children: PropTypes.string,
  color: PropTypes.string,
};

export default BigText;
