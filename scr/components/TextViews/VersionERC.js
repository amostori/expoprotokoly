import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

export const VersionERC = () => (
  <View style={styles.container}>
    <Text style={styles.versionTekst}>Na podstawie wytycznych ERC 2015</Text>
  </View>
);

export const VersionSzczeklikERC = () => (
  <View style={styles.container}>
    <Text style={styles.versionTekst}>
      Na podstawie wytycznych ERC 2015 i &quot;Interny Szczeklika&quot; pod redakcją Piotra
      Gajewskiego.
    </Text>
  </View>
);

export const VersionSzczeklikERCLeczenieBolu = () => (
  <View style={styles.container}>
    <Text style={styles.versionTekst}>
      Na podstawie wytycznych ERC 2015 i &quot;Interny Szczeklika&quot; pod redakcją Piotra
      Gajewskiego. Leczenie bólu na podstawie Dobrych Praktyk Leczenia Bólu Ministerstwa Zdrowia.
    </Text>
  </View>
);

export const VersionTkaczyk = () => (
  <View style={styles.container}>
    <Text style={styles.versionTekst}>
      Na podstawie Marcin Tkaczyk &quot;Stany nagłe pediatria&quot; Medical Tribune Polska Warszawa
      2015.
    </Text>
  </View>
); 

export const VersionTkaczykZwalczanieBolu = () => (
  <View style={styles.container}>
    <Text style={styles.versionTekst}>
      Na podstawie Marcin Tkaczyk &quot;Stany nagłe pediatria&quot; Medical Tribune Polska Warszawa
      2015. Leczenie bólu na podstawie Dobrych Praktyk Leczenia Bólu Ministerstwa Zdrowia.
    </Text>
  </View>
);

export const VersionSzczeklik = () => (
  <View style={styles.container}>
    <Text style={styles.versionTekst}>
      Na podstawie &quot;Interny Szczeklika&quot; pod redakcją Piotra Gajewskiego.
    </Text>
  </View>
);

export const VersionDrug = () => (
  <View style={styles.container}>
    <Text style={styles.versionTekst}>
      Na podstawie Davi&apos;s Drug Guide (www.drugguide.com) oraz charakterystyki produktu
      leczniczego.
    </Text>
  </View>
);

export const Sol = () => <Text style={styles.simpleTekst}> 0,9% NaCl </Text>;
