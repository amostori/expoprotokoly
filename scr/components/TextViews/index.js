import styles from './styles';
import VersionTextView from './VersionTextView';
import Hypertekst from './Hypertekst';
import MyTextView from './MyTextView';
import AskTextView from './AskTextView';
import BoldText from './BoldText';
import BadanieHipertekst from './BadanieHipertekst';
import ListaTextView from './ListaTextView';
import SmallText from './SmallText';
import BigText from './BigText';
import RedBoldText from './RedBoldText';
import {
  VersionERC,
  VersionSzczeklikERC,
  VersionSzczeklik,
  Sol,
  VersionTkaczyk,
  VersionDrug,
  VersionSzczeklikERCLeczenieBolu,
  VersionTkaczykZwalczanieBolu,
} from './VersionERC';
import TiM from './TiM';

export {
  styles,
  VersionTextView,
  Hypertekst,
  MyTextView,
  AskTextView,
  BoldText,
  BadanieHipertekst,
  ListaTextView,
  SmallText,
  BigText,
  VersionERC,
  VersionSzczeklikERC,
  VersionSzczeklik,
  TiM,
  Sol,
  VersionTkaczyk,
  RedBoldText,
  VersionDrug,
  VersionTkaczykZwalczanieBolu,
};
