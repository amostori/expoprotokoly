import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  versionTekst: {
    textAlign: 'center',
    fontSize: 15,
    marginTop: 50,
    marginBottom: 20,
    fontStyle: 'italic',
  },
  hypertekstContainer: {
    marginBottom: 20,
  },
  hypertekst: {
    color: '$primaryRed',
    fontSize: 18,
    fontWeight: 'bold',
    lineHeight: 30,
    textDecorationLine: 'underline',
  },
  mainTekst: {
    textAlign: 'center',
    fontSize: 18,
    marginBottom: 20,
    lineHeight: 30,
  },
  listaTextView: {
    textAlign: 'center',
  },
  smallTextView: {
    textAlign: 'center',
    fontSize: 15,
    fontStyle: 'italic',
  },
  askTekst: {
    textAlign: 'center',
    fontSize: 18,
    marginBottom: 20,
    fontWeight: 'bold',
  },
  boldText: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  redBoldText: {
    textAlign: 'center',
    color: '$primaryRed',
    fontSize: 20,
    fontWeight: 'bold',
  },
  simpleTekst: {
    textAlign: 'center',
    fontSize: 18,
  },
  badanieContainer: { marginBottom: 30, marginHorizontal: 20 },
  badanieHypertekst: {
    backgroundColor: '$primaryRed',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 5,
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    overflow: 'hidden',
    padding: 12,
    textAlign: 'center',
  },
  containermainmenu: {
    flex: 1,
    backgroundColor: '#e9ecf0',
    justifyContent: 'center',
  },
  containerbody: {
    flex: 1,
    backgroundColor: '#E9ECF0',
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 150,
  },
  bottomins: {
    position: 'absolute',
    bottom: 0,
  },
  flex: {
    flex: 1,
  },
});
