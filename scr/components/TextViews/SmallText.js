import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const SmallText = ({ children }) => <Text style={styles.smallTextView}>{children}</Text>;

SmallText.propTypes = {
  children: PropTypes.string,
};

export default SmallText;
