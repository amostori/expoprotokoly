import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const ListaTextView = ({ children }) => <Text style={styles.listaTextView}>{children}</Text>;

ListaTextView.propTypes = {
  children: PropTypes.string,
};

export default ListaTextView;
