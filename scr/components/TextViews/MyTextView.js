import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const MyTextView = ({ children }) => <Text style={styles.mainTekst}>{children}</Text>;

MyTextView.propTypes = {
  children: PropTypes.any,
};

export default MyTextView;
