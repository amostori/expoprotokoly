import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e9ecf0',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containermainmenu: {
    flex: 1,
    backgroundColor: '#e9ecf0',
    justifyContent: 'center',
  },
  scrollContainer: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: '#e9ecf0',
    paddingHorizontal: 10,
    // alignItems: center  powoduje wycentrowanie wszystkiego i połączenie guzik
  },
  wskazowkiContainer: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 10,
    backgroundColor: '#e9ecf0',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
