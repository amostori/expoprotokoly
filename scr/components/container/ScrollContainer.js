import React from 'react';
import { View, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const ScrollContainer = ({ children }) => (
  <View style={{ backgroundColor: '#e9ecf0', flex: 1 }}>
    <ScrollView>
      <View style={styles.scrollContainer}>{children}</View>
    </ScrollView>
  </View>
);
ScrollContainer.propTypes = {
  children: PropTypes.any,
};
export default ScrollContainer;
