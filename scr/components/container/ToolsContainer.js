import React from 'react';
import { View, ScrollView } from 'react-native';
import PropTypes from 'prop-types';

const styles = {
  scrollContainer: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: '#e9ecf0',
    paddingHorizontal: 10,
  },
};

const ToolsContainer = ({ children, style }) => (
  <View style={{ backgroundColor: '#e9ecf0', flex: 1 }}>
    <ScrollView>
      <View style={[styles.scrollContainer, style]}>{children}</View>
    </ScrollView>
  </View>
);
ToolsContainer.propTypes = {
  children: PropTypes.any,
  style: PropTypes.any,
};
export default ToolsContainer;
