import React from 'react';
import { View, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const WskazowkiContainer = ({ children }) => (
  <View style={{ backgroundColor: '#e9ecf0', flex: 1 }}>
    <ScrollView>
      <View style={styles.wskazowkiContainer}>{children}</View>
    </ScrollView>
  </View>
);
WskazowkiContainer.propTypes = {
  children: PropTypes.any,
};
export default WskazowkiContainer;
