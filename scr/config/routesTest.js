import { createStackNavigator } from 'react-navigation';
import React from 'react';
import { Linking } from 'react-native';
import NavigationService from './NavigationService';
import Home from '../screens/Home';
import SearchScreen from '../screens/SearchScreen';
import {
  AlsScreen,
  PalsScreen,
  PoloznictwoScreen,
  UrazyScreen,
  ZatruciaScreen,
  ToolsScreen,
  LekiUDzieciScreen,
  FarmaScreen,
  CasesScreen,
} from '../screens/ListScreens';
import {
  EnergiaDefibrylacji,
  Screen4H4T,
  Ogrzewanie,
  KlasyfikacjaHipotermii,
  Sedacja,
  Kardiowersja,
  AdrenalinaWskazowki,
  AmiodaronWskazowki,
  SaturacjaSwiezorodka,
  AtropinaWskazowki,
  SedacjaDzieci,
  NaloksonDzieci,
  MorfinaDzieci,
  FentanylDzieci,
  SkalaBolu,
  DiazepamDzieci,
  MidazolamDzieci,
  LeczenieGoraczki,
  Hiperwentylacja,
  Toksydrom,
  ObjawyBrzuszne,
  ParacetamolDzieci,
  KodCiazy,
  Vagal,
} from '../screens/wskazowkiScreens';
import {
  Adenozyna,
  Adrenalina,
  Amiodaron,
  Lignokaina,
  Mannitol,
  Atropina,
  Fentanyl,
  Flumazenil,
  Midazolam,
  Morfina,
  Diazepam,
  Magnez,
  Metoklopramid,
  Metoprolol,
  Nalokson,
  Nitrogliceryna,
  ASA,
  Tikagrelor,
  Ibuprofen,
  Klopidogrel,
  Heparyna,
  Urapidil,
  Hydroxyzyna,
  Furosemid,
  Torecan,
  Captopril,
  Hydrokortyzon,
  Dexaven,
  Izosorbid,
  Ketoprofen,
  Clemastin,
  Glukagon,
  Salbutamol,
  Natrii,
  Paracetamol,
  Pyralgina,
  Papaweryna,
  Drotaweryna,
  Glukoza,
  Clonazepam,
  Budezonid,
  Anestezja,
} from '../screens/farmascreens';
import {
  Wstep,
  FunkcjeZyciowe,
  Zlecenia,
  BadanieKliniczne,
  Podsumowanie,
} from '../screens/wskazowkiScreens/badanieSubScreens';
import {
  BadanieScreen,
  NzkAlsScreen,
  NzkHipotermia,
  Hipotermia,
  Bradykardia,
  Czestoskurcz,
  Ozw,
  Nadcisnienie,
  Dusznosc,
  Obrzek,
  Anafilaksja,
  Astma,
  BolBrzucha,
  Hipoglikemia,
  Hiperglikemia,
  Padaczka,
  Udar,
  BolPlecow,
  ZaburzeniaPsychiczne,
  BolGlowy,
} from '../screens/alsScreens';
import {
  NzkP,
  Swiezorodek,
  BradykardiaP,
  CzestoskurczP,
  AnafilaksjaP,
  AstmaP,
  ZapalenieKrtani,
  BolBrzuchaP,
  HipoglikemiaP,
  DrgawkiP,
  Goraczka,
  ZwalczanieBoluP,
} from '../screens/palsScreens';
import { FilmsButton, SearchIcon } from '../components/Buttons';
import {
  Porod,
  Pepowina,
  PepowinaNaSzyi,
  KrwawieniePorod,
  Rzucawka,
} from '../screens/poloznictwoScreens';
import {
  ZdarzenieMasowe,
  TriageAdult,
  TriageJump,
  CentrumUrazowe,
  Czaszka,
  Oparzenia,
  TestOparzen,
  BolUrazowy,

} from '../screens/urazyScreens';

import {
  Amfetamina,
  Benzodiazepiny,
  Betablokery,
  CO,
  Cyjanki,
  Digoksyna,
  Dopalacze,
  LSD,
  Metanol,
  Opiaty,
  Rtec,
  TLP,
  ZatrucieParacetamolem,
  Fosforany,
  Zelazo,
  OsrodkiOstrychZatruc,
} from '../screens/zatruciaScreens';

import {
  CPRAssistant,
  Glasgow,
  LifeParam,
  Apgar,
  Westley,
  Catheter,
  Pressure,
  TriageTrening,
} from '../screens/toolsScreens';

import {
  BetablokerCzestoskurcz,
  CzestoskurczKardiowersja,
  AdenozynaEfekt,
  PrzypadekOzw,
  MetoprololEfekt,
  ObrzekPlucCase,
  DzieckoSvt,
  ZawalCase,
  AnafilaksjaCase,
  AmiodaronCase,
  NiemowleCase,
  LewaRekaCase,
} from '../screens/cases';

export default createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        headerTitle: 'Ratownictwo medyczne',
        headerBackTitle: 'Menu',
        headerRight: (
          <SearchIcon searchEverything={() => NavigationService.navigate('SearchScreen')} />
        ),
      },
    },
    SearchScreen: {
      screen: SearchScreen,
      navigationOptions: {
        headerTitle: 'Wyszukaj...',
      },
    },
    TriageTrening: {
      screen: TriageTrening,
      navigationOptions: {
        headerTitle: 'Triage trainer',
      },
    },
    Vagal: {
      screen: Vagal,
      navigationOptions: {
        headerTitle: 'Stymulacja nerwu błędnego',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=8DIRiOA_OsA')}
          />
        ),
      },
    },
    TestOparzen: {
      screen: TestOparzen,
      navigationOptions: {
        headerBackTitle: 'Menu',
      },
    },
    BolUrazowy: {
      screen: BolUrazowy,
      navigationOptions: {
        headerTitle: 'Zwalczanie bólu urazowego',
      },
    },
    CPRAssistant: {
      screen: CPRAssistant,
      navigationOptions: {
        headerTitle: 'CPR Asystent',
      },
    },
    Oparzenia: {
      screen: Oparzenia,
      navigationOptions: {
        headerTitle: 'Oparzenia',
      },
    },
    BetablokerCzestoskurcz: {
      screen: BetablokerCzestoskurcz,
      navigationOptions: {
        headerTitle: 'Częstoskurcz',
      },
    },
    CzestoskurczKardiowersja: {
      screen: CzestoskurczKardiowersja,
      navigationOptions: {
        headerTitle: 'Kardiowersja',
      },
    },
    AdenozynaEfekt: {
      screen: AdenozynaEfekt,
      navigationOptions: {
        headerTitle: 'Efekt Adenozyny',
      },
    },
    PrzypadekOzw: {
      screen: PrzypadekOzw,
      navigationOptions: {
        headerTitle: 'Przypadek OZW',
      },
    },
    MetoprololEfekt: {
      screen: MetoprololEfekt,
      navigationOptions: {
        headerTitle: 'Efekt Metoprololu',
      },
    },
    FentanylDzieci: {
      screen: FentanylDzieci,
      navigationOptions: {
        headerTitle: 'Fentanyl u dzieci',
      },
    },
    BolGlowy: {
      screen: BolGlowy,
      navigationOptions: {
        headerTitle: 'Ból głowy',
      },
    },
    LewaRekaCase: {
      screen: LewaRekaCase,
      navigationOptions: {
        headerTitle: 'Ból lewej ręki',
      },
    },
    ObrzekPlucCase: {
      screen: ObrzekPlucCase,
      navigationOptions: {
        headerTitle: 'Obrzęk płuc',
      },
    },
    ZwalczanieBoluP: {
      screen: ZwalczanieBoluP,
      navigationOptions: {
        headerTitle: 'Zwalczanie bólu u dzieci',
      },
    },
    DzieckoSvt: {
      screen: DzieckoSvt,
      navigationOptions: {
        headerTitle: 'Dziecko z częstoskurczem',
      },
    },
    NiemowleCase: {
      screen: NiemowleCase,
      navigationOptions: {
        headerTitle: 'Nieprzytomne niemowlę',
      },
    },
    ZawalCase: {
      screen: ZawalCase,
      navigationOptions: {
        headerTitle: 'Zawał serca',
      },
    },
    AnafilaksjaCase: {
      screen: AnafilaksjaCase,
      navigationOptions: {
        headerTitle: 'Anafilaksja',
      },
    },
    AmiodaronCase: {
      screen: AmiodaronCase,
      navigationOptions: {
        headerTitle: 'Migotanie przedsionków',
      },
    },
    AlsScreen: {
      screen: AlsScreen,
      navigationOptions: {
        headerTitle: 'ALS',
        headerBackTitle: 'ALS',
      },
    },
    ZatruciaScreen: {
      screen: ZatruciaScreen,
      navigationOptions: {
        headerTitle: 'Zatrucia',
        headerBackTitle: 'Zatrucia',
      },
    },
    FarmaScreen: {
      screen: FarmaScreen,
      navigationOptions: {
        headerTitle: 'Farmakologia',
        headerBackTitle: 'Farmakologia',
      },
    },
    Mannitol: {
      screen: Mannitol,
      navigationOptions: {
        headerTitle: 'Mannitol 15%',
      },
    },
    ToolsScreen: {
      screen: ToolsScreen,
      navigationOptions: {
        headerTitle: 'Narzędzia',
        headerBackTitle: 'Narzędzia',
      },
    },
    LekiUDzieciScreen: {
      screen: LekiUDzieciScreen,
      navigationOptions: {
        headerTitle: 'Leki u dzieci',
        headerBackTitle: 'Leki u dzieci',
      },
    },
    Glasgow: {
      screen: Glasgow,
      navigationOptions: {
        headerTitle: 'Skala Glasgow',
      },
    },
    Izosorbid: {
      screen: Izosorbid,
      navigationOptions: {
        headerTitle: 'Izosorbid - Mononit',
      },
    },
    Ketoprofen: {
      screen: Ketoprofen,
      navigationOptions: {
        headerTitle: 'Ketoprofen',
      },
    },
    Nalokson: {
      screen: Nalokson,
      navigationOptions: {
        headerTitle: 'Nalokson',
      },
    },
    Westley: {
      screen: Westley,
      navigationOptions: {
        headerTitle: 'Skala Westley',
      },
    },
    ParacetamolDzieci: {
      screen: ParacetamolDzieci,
      navigationOptions: {
        headerTitle: 'Paracetamol u dzieci',
      },
    },
    CasesScreen: {
      screen: CasesScreen,
      navigationOptions: {
        headerTitle: 'Przypadki',
      },
    },
    Adenozyna: {
      screen: Adenozyna,
      navigationOptions: {
        headerTitle: 'Adenozyna',
      },
    },
    Anestezja: {
      screen: Anestezja,
      navigationOptions: {
        headerTitle: 'Leki anestezjologiczne',
      },
    },
    Natrii: {
      screen: Natrii,
      navigationOptions: {
        headerTitle: 'Natrium Bicarbonicum',
      },
    },
    Torecan: {
      screen: Torecan,
      navigationOptions: {
        headerTitle: 'Torecan',
      },
    },
    KodCiazy: {
      screen: KodCiazy,
      navigationOptions: {
        headerTitle: 'Kody bezpieczeństwa',
      },
    },
    Zelazo: {
      screen: Zelazo,
      navigationOptions: {
        headerTitle: 'Zatrucie żelazem',
      },
    },
    OsrodkiOstrychZatruc: {
      screen: OsrodkiOstrychZatruc,
      navigationOptions: {
        headerTitle: 'Ośrodki Ostrych Zatruć',
      },
    },
    Apgar: {
      screen: Apgar,
      navigationOptions: {
        headerTitle: 'Skala Apgar',
      },
    },
    Catheter: {
      screen: Catheter,
      navigationOptions: {
        headerTitle: 'Cewnik dializacyjny',
      },
    },
    Metoklopramid: {
      screen: Metoklopramid,
      navigationOptions: {
        headerTitle: 'Metoklopramid',
      },
    },
    Pressure: {
      screen: Pressure,
      navigationOptions: {
        headerTitle: 'Średnie ciśnienie tętnicze',
      },
    },
    Fosforany: {
      screen: Fosforany,
      navigationOptions: {
        headerTitle: 'Związki fosfoorganiczne',
      },
    },
    LifeParam: {
      screen: LifeParam,
      navigationOptions: {
        headerTitle: 'Parametry życiowe',
      },
    },
    Cyjanki: {
      screen: Cyjanki,
      navigationOptions: {
        headerTitle: 'Zatrucie cyjankami',
      },
    },

    TLP: {
      screen: TLP,
      navigationOptions: {
        headerTitle: 'Zatrucie trójcyklicznymi lekami przeciwdepresyjnymi',
      },
    },
    Flumazenil: {
      screen: Flumazenil,
      navigationOptions: {
        headerTitle: 'Flumazenil',
      },
    },
    ZatrucieParacetamolem: {
      screen: ZatrucieParacetamolem,
      navigationOptions: {
        headerTitle: 'Zatrucie paracetamolem',
      },
    },
    Opiaty: {
      screen: Opiaty,
      navigationOptions: {
        headerTitle: 'Zatrucie opiatami',
      },
    },
    LSD: {
      screen: LSD,
      navigationOptions: {
        headerTitle: 'Zatrucie LSD',
      },
    },
    Rtec: {
      screen: Rtec,
      navigationOptions: {
        headerTitle: 'Zatrucie rtęci',
      },
    },
    Metanol: {
      screen: Metanol,
      navigationOptions: {
        headerTitle: 'Zatrucie metanolem',
      },
    },
    Digoksyna: {
      screen: Digoksyna,
      navigationOptions: {
        headerTitle: 'Zatrucie digoksyną, glikozydami naparstnicy',
      },
    },
    Ibuprofen: {
      screen: Ibuprofen,
      navigationOptions: {
        headerTitle: 'Ibuprofen',
      },
    },
    Dopalacze: {
      screen: Dopalacze,
      navigationOptions: {
        headerTitle: 'Zatrucie dopalaczami',
      },
    },
    ZdarzenieMasowe: {
      screen: ZdarzenieMasowe,
      navigationOptions: {
        headerTitle: 'Zdarzenie Masowe',
      },
    },
    Toksydrom: {
      screen: Toksydrom,
      navigationOptions: {
        headerTitle: 'Toksydromy',
      },
    },
    Betablokery: {
      screen: Betablokery,
      navigationOptions: {
        headerTitle: 'Betablokery',
      },
    },
    CO: {
      screen: CO,
      navigationOptions: {
        headerTitle: 'Tlenek węgla',
      },
    },
    Amfetamina: {
      screen: Amfetamina,
      navigationOptions: {
        headerTitle: 'Amfetamina, Kokaina',
      },
    },
    Benzodiazepiny: {
      screen: Benzodiazepiny,
      navigationOptions: {
        headerTitle: 'Benzodiazepiny',
      },
    },
    TriageAdult: {
      screen: TriageAdult,
      navigationOptions: {
        headerTitle: 'TRIAGE',
      },
    },
    TriageJump: {
      screen: TriageJump,
      navigationOptions: {
        headerTitle: 'TRIAGE JUMP',
      },
    },
    CentrumUrazowe: {
      screen: CentrumUrazowe,
      navigationOptions: {
        headerTitle: 'Centrum Urazowe - kryteria przyjęć',
      },
    },
    Anafilaksja: {
      screen: Anafilaksja,
      navigationOptions: {
        headerTitle: 'Anafilaksja',
      },
    },
    SaturacjaSwiezorodka: {
      screen: SaturacjaSwiezorodka,
      navigationOptions: {
        headerTitle: 'Saturacja Świeżorodka',
      },
    },
    Hiperwentylacja: {
      screen: Hiperwentylacja,
      navigationOptions: {
        headerTitle: 'Hiperwentylacja',
      },
    },
    Czaszka: {
      screen: Czaszka,
      navigationOptions: {
        headerTitle: 'Uraz czaszkowo - mózgowy',
      },
    },
    NzkP: {
      screen: NzkP,
      navigationOptions: {
        headerTitle: 'Nagłe zatrzymanie krążenia u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=WGQMNBiHPP8')}
          />
        ),
      },
    },
    Swiezorodek: {
      screen: Swiezorodek,
      navigationOptions: {
        headerTitle: 'Resuscytacja świeżorodka',
      },
    },
    BradykardiaP: {
      screen: BradykardiaP,
      navigationOptions: {
        headerTitle: 'Bradykardia u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=1fjmyog37Fo')}
          />
        ),
      },
    },
    CzestoskurczP: {
      screen: CzestoskurczP,
      navigationOptions: {
        headerTitle: 'Częstoskurcz u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=AJY2yoQUqzA')}
          />
        ),
      },
    },
    ZapalenieKrtani: {
      screen: ZapalenieKrtani,
      navigationOptions: {
        headerTitle: 'Zapalenie krtani u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=d8k-4GI429o')}
          />
        ),
      },
    },
    DrgawkiP: {
      screen: DrgawkiP,
      navigationOptions: {
        headerTitle: 'Drgawki u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=Ac17vTPGbl0')}
          />
        ),
      },
    },
    DiazepamDzieci: {
      screen: DiazepamDzieci,
      navigationOptions: {
        headerTitle: 'Diazepam u dzieci',
      },
    },
    LeczenieGoraczki: {
      screen: LeczenieGoraczki,
      navigationOptions: {
        headerTitle: 'Zwalczanie gorączki u dzieci',
      },
    },
    MidazolamDzieci: {
      screen: MidazolamDzieci,
      navigationOptions: {
        headerTitle: 'Midazolam u dzieci',
      },
    },
    AnafilaksjaP: {
      screen: AnafilaksjaP,
      navigationOptions: {
        headerTitle: 'Anafilaksja u dzieci',
      },
    },
    BolBrzuchaP: {
      screen: BolBrzuchaP,
      navigationOptions: {
        headerTitle: 'Ból brzucha u dzieci',
      },
    },
    HipoglikemiaP: {
      screen: HipoglikemiaP,
      navigationOptions: {
        headerTitle: 'Hipoglikemia u dzieci',
      },
    },
    MorfinaDzieci: {
      screen: MorfinaDzieci,
      navigationOptions: {
        headerTitle: 'Morfina u dzieci',
      },
    },
    SkalaBolu: {
      screen: SkalaBolu,
      navigationOptions: {
        headerTitle: 'Obrazkowa skala bólu',
      },
    },
    Budezonid: {
      screen: Budezonid,
      navigationOptions: {
        headerTitle: 'Budezonid',
      },
    },
    AtropinaWskazowki: {
      screen: AtropinaWskazowki,
      navigationOptions: {
        headerTitle: 'Atropina u dzieci',
      },
    },
    SedacjaDzieci: {
      screen: SedacjaDzieci,
      navigationOptions: {
        headerTitle: 'Sedacja u dzieci',
      },
    },
    AstmaP: {
      screen: AstmaP,
      navigationOptions: {
        headerTitle: 'Astma u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=Ac17vTPGbl0')}
          />
        ),
      },
    },
    Goraczka: {
      screen: Goraczka,
      navigationOptions: {
        headerTitle: 'Gorączka u dzieci',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=bj2CNkjvY4o')}
          />
        ),
      },
    },
    Astma: {
      screen: Astma,
      navigationOptions: {
        headerTitle: 'Astma i POCHP',
      },
    },
    AdrenalinaWskazowki: {
      screen: AdrenalinaWskazowki,
      navigationOptions: {
        headerTitle: 'Adrenalina u dzieci',
      },
    },
    NaloksonDzieci: {
      screen: NaloksonDzieci,
      navigationOptions: {
        headerTitle: 'Nalokson u dzieci',
      },
    },
    AmiodaronWskazowki: {
      screen: AmiodaronWskazowki,
      navigationOptions: {
        headerTitle: 'Amiodaron u dzieci',
      },
    },
    Padaczka: {
      screen: Padaczka,
      navigationOptions: {
        headerTitle: 'Drgawki R 56',
      },
    },

    Udar: {
      screen: Udar,
      navigationOptions: {
        headerTitle: 'Udar mózgu',
      },
    },
    ZaburzeniaPsychiczne: {
      screen: ZaburzeniaPsychiczne,
      navigationOptions: {
        headerTitle: 'Zaburzenia psychiczne',
      },
    },
    BolPlecow: {
      screen: BolPlecow,
      navigationOptions: {
        headerTitle: 'Ból pleców',
      },
    },
    Clonazepam: {
      screen: Clonazepam,
      navigationOptions: {
        headerTitle: 'Clonazepam',
      },
    },
    BolBrzucha: {
      screen: BolBrzucha,
      navigationOptions: {
        headerTitle: 'Ból brzucha',
      },
    },
    Hipoglikemia: {
      screen: Hipoglikemia,
      navigationOptions: {
        headerTitle: 'Hipoglikemia',
      },
    },
    Glukoza: {
      screen: Glukoza,
      navigationOptions: {
        headerTitle: 'Glukoza',
      },
    },
    Hiperglikemia: {
      screen: Hiperglikemia,
      navigationOptions: {
        headerTitle: 'Hiperglikemia',
      },
    },
    Hydrokortyzon: {
      screen: Hydrokortyzon,
      navigationOptions: {
        headerTitle: 'Hydrokortyzon',
      },
    },
    Drotaweryna: {
      screen: Drotaweryna,
      navigationOptions: {
        headerTitle: 'Drotaweryna',
      },
    },
    Salbutamol: {
      screen: Salbutamol,
      navigationOptions: {
        headerTitle: 'Salbutamol',
      },
    },
    Glukagon: {
      screen: Glukagon,
      navigationOptions: {
        headerTitle: 'Glukagon',
      },
    },
    Clemastin: {
      screen: Clemastin,
      navigationOptions: {
        headerTitle: 'Clemastin',
      },
    },
    Dexaven: {
      screen: Dexaven,
      navigationOptions: {
        headerTitle: 'Deksametazon',
      },
    },
    BadanieScreen: {
      screen: BadanieScreen,
      navigationOptions: {
        headerTitle: 'Badanie ratunkowe',
      },
    },
    Nadcisnienie: {
      screen: Nadcisnienie,
      navigationOptions: {
        headerTitle: 'Nadciśnienie tętnicze',
      },
    },
    Wstep: {
      screen: Wstep,
      navigationOptions: {
        headerTitle: 'I Wstęp',
      },
    },
    FunkcjeZyciowe: {
      screen: FunkcjeZyciowe,
      navigationOptions: {
        headerTitle: 'II Funkcje Życiowe',
      },
    },
    Zlecenia: {
      screen: Zlecenia,
      navigationOptions: {
        headerTitle: 'III Zlecenia i wywiad',
      },
    },
    BadanieKliniczne: {
      screen: BadanieKliniczne,
      navigationOptions: {
        headerTitle: 'IV Badanie Kliniczne',
      },
    },
    Podsumowanie: {
      screen: Podsumowanie,
      navigationOptions: {
        headerTitle: 'V Podsumowanie',
      },
    },

    NzkAlsScreen: {
      screen: NzkAlsScreen,
      navigationOptions: {
        headerTitle: 'NZK',
      },
    },
    NzkHipotermia: {
      screen: NzkHipotermia,
      navigationOptions: {
        headerTitle: 'NZK w hipotermii',
      },
    },
    Obrzek: {
      screen: Obrzek,
      navigationOptions: {
        headerTitle: 'Niewydolnosc lewokomorowa - Obrzęk płuc',
      },
    },
    ObjawyBrzuszne: {
      screen: ObjawyBrzuszne,
      navigationOptions: {
        headerTitle: 'Objawy patologiczne',
      },
    },
    Ogrzewanie: {
      screen: Ogrzewanie,
      navigationOptions: {
        headerTitle: 'Ogrzewanie pozaszpitalne',
      },
    },
    KlasyfikacjaHipotermii: {
      screen: KlasyfikacjaHipotermii,
      navigationOptions: {
        headerTitle: 'Klasyfikacja hipotermii',
      },
    },
    EnergiaDefibrylacji: {
      screen: EnergiaDefibrylacji,
      navigationOptions: {
        headerTitle: 'Energia defibrylacji',
      },
    },
    Adrenalina: {
      screen: Adrenalina,
      navigationOptions: {
        headerTitle: 'Adrenalina',
      },
    },
    Atropina: {
      screen: Atropina,
      navigationOptions: {
        headerTitle: 'Atropina',
      },
    },
    Captopril: {
      screen: Captopril,
      navigationOptions: {
        headerTitle: 'Captopril',
      },
    },
    ASA: {
      screen: ASA,
      navigationOptions: {
        headerTitle: 'Kwas acetylosalicylowy',
      },
    },
    Paracetamol: {
      screen: Paracetamol,
      navigationOptions: {
        headerTitle: 'Paracetamol',
      },
    },
    Tikagrelor: {
      screen: Tikagrelor,
      navigationOptions: {
        headerTitle: 'Tikagrelor',
      },
    },
    Urapidil: {
      screen: Urapidil,
      navigationOptions: {
        headerTitle: 'Urapidil',
      },
    },
    Klopidogrel: {
      screen: Klopidogrel,
      navigationOptions: {
        headerTitle: 'Klopidogrel',
      },
    },
    Heparyna: {
      screen: Heparyna,
      navigationOptions: {
        headerTitle: 'Heparyna',
      },
    },
    Hydroxyzyna: {
      screen: Hydroxyzyna,
      navigationOptions: {
        headerTitle: 'Hydroxyzyna',
      },
    },
    Amiodaron: {
      screen: Amiodaron,
      navigationOptions: {
        headerTitle: 'Amiodaron',
      },
    },
    Furosemid: {
      screen: Furosemid,
      navigationOptions: {
        headerTitle: 'Furosemid',
      },
    },
    Lignokaina: {
      screen: Lignokaina,
      navigationOptions: {
        headerTitle: 'Lignokaina',
      },
    },
    Fentanyl: {
      screen: Fentanyl,
      navigationOptions: {
        headerTitle: 'Fentanyl',
      },
    },
    Midazolam: {
      screen: Midazolam,
      navigationOptions: {
        headerTitle: 'Midazolam',
      },
    },
    Morfina: {
      screen: Morfina,
      navigationOptions: {
        headerTitle: 'Morfina',
      },
    },
    Diazepam: {
      screen: Diazepam,
      navigationOptions: {
        headerTitle: 'Diazepam',
      },
    },
    Metoprolol: {
      screen: Metoprolol,
      navigationOptions: {
        headerTitle: 'Metoprolol',
      },
    },
    Screen4H4T: {
      screen: Screen4H4T,
      navigationOptions: {
        headerTitle: 'Odwracalne przyczyny NZK',
      },
    },
    Hipotermia: {
      screen: Hipotermia,
      navigationOptions: {
        headerTitle: 'Hipotermia',
      },
    },
    Bradykardia: {
      screen: Bradykardia,
      navigationOptions: {
        headerTitle: 'Bradykardia',
      },
    },
    Czestoskurcz: {
      screen: Czestoskurcz,
      navigationOptions: {
        headerTitle: 'Czestoskurcz',
        headerRight: (
          <FilmsButton
            showMovie={() => Linking.openURL('https://www.youtube.com/watch?v=8DIRiOA_OsA')}
          />
        ),
      },
    },
    Ozw: {
      screen: Ozw,
      navigationOptions: {
        headerTitle: 'Ból w klatce piersiowej',
      },
    },
    Dusznosc: {
      screen: Dusznosc,
      navigationOptions: {
        headerTitle: 'Duszność',
      },
    },
    Kardiowersja: {
      screen: Kardiowersja,
      navigationOptions: {
        headerTitle: 'Energia kardiowersji',
      },
    },
    Magnez: {
      screen: Magnez,
      navigationOptions: {
        headerTitle: 'Magnez',
      },
    },
    Nitrogliceryna: {
      screen: Nitrogliceryna,
      navigationOptions: {
        headerTitle: 'Nitrogliceryna',
      },
    },
    Pyralgina: {
      screen: Pyralgina,
      navigationOptions: {
        headerTitle: 'Metamizol - Pyralgina',
      },
    },
    Papaweryna: {
      screen: Papaweryna,
      navigationOptions: {
        headerTitle: 'Papaweryna',
      },
    },
    Sedacja: {
      screen: Sedacja,
      navigationOptions: {
        headerTitle: 'Sedacja i analgezja',
      },
    },
    PalsScreen: {
      screen: PalsScreen,
      navigationOptions: {
        headerTitle: 'PALS',
        headerBackTitle: 'PALS',
      },
    },
    PoloznictwoScreen: {
      screen: PoloznictwoScreen,
      navigationOptions: {
        headerTitle: 'Położnictwo',
        headerBackTitle: 'Położnictwo',
      },
    },
    UrazyScreen: {
      screen: UrazyScreen,
      navigationOptions: {
        headerTitle: 'Urazy',
        headerBackTitle: 'Urazy',
      },
    },
    Porod: {
      screen: Porod,
      navigationOptions: {
        headerTitle: 'Poród fizjologiczny',
      },
    },
    Pepowina: {
      screen: Pepowina,
      navigationOptions: {
        headerTitle: 'Wypadnięta pępowina',
      },
    },
    PepowinaNaSzyi: {
      screen: PepowinaNaSzyi,
      navigationOptions: {
        headerTitle: 'Pepowina wokół szyi',
      },
    },
    KrwawieniePorod: {
      screen: KrwawieniePorod,
      navigationOptions: {
        headerTitle: 'Masywne krwawienie po porodzie',
      },
    },
    Rzucawka: {
      screen: Rzucawka,
      navigationOptions: {
        headerTitle: 'Rzucawka porodowa',
      },
    },
  },
  {
    // headerMode: 'none',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#ff5353',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);
