import React from 'react';
import { ScrollView, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ScrollContainer from '../../components/container/ScrollContainer';
import { MyTextView, RedBoldText } from '../../components/TextViews';

const styles = EStyleSheet.create({
  image: { height: 300, width: 1000 },
});

const MetoprololEfekt = () => (
  <ScrollContainer>
    <MyTextView>
      <RedBoldText>Powód wezwania</RedBoldText>
    </MyTextView>
    <MyTextView>
      Otrzymano wezwanie do kobiety lat 75 z wysokim ciśnienie tętniczym krwi.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Wywiad</RedBoldText>
    </MyTextView>
    <MyTextView>
      Pacjentka zgłasza wysokie ciśnienie, kołatanie serca i ból w klatce piersiowej od 40 min.
      Choruje na napadowe migotanie przedsionków, nadciśnienie tętnicze. Zażywa tritace oraz
      hydroksyzynę doraźnie.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Badanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Przytomna, w logicznym kontakcie, oddech 14/min, saturacja 97%, tętno niemiarowe 138 -
      198/min, skóra i nawrót włośniczkowy prawidłowe, osłuchowo szmer pęcherzykowy, symetryczny,
      ciśnienie tętnicze 170/100 mmHg. Brak innych objawów patologicznych.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Zapis EKG</RedBoldText>
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/betaloc1.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Postępowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Zdecydowano się na podanie betalocu 1 mg powoli iv. Nie nastąpiła poprawa, więc podano drugą,
      taką samą dawkę leku. Korzystny efekt uzyskano po podaniu trzeciej dawki betalocu. Łącznie
      podano 3 mg. Pacjentka została przetransportowana na SOR w stanie stabilnym, bez dolegliwości
      bólowych. W czasie transportu otrzymała jeszcze 2 g magnezu we wlewie. Drugi zapis EKG:
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/betaloc2.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Podsumowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      W pierwszym zapisie widoczne migotanie przedsionków (brak załamków P, rytm całkowicie
      niemiarowy) z szybką odpowiedzią komór. Po podaniu betablokera doszło do powrotu rytmu
      zatokowego i co najważniejsze, zwolnienia akcji serca. Dodatkowo rozpoznać można niepełny blok
      prawej odnogi pęczka Hisa - QRS powyżej 110, ale poniżej 120 ms, łopatowate załamki S w I i V6
      oraz zespół rSR&apos; w V1.
    </MyTextView>
  </ScrollContainer>
);

export default MetoprololEfekt;
