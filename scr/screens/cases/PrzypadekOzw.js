import React from 'react';
import { ScrollView, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ScrollContainer from '../../components/container/ScrollContainer';
import { MyTextView, RedBoldText } from '../../components/TextViews';

const styles = EStyleSheet.create({
  image: { height: 300, width: 1000 },
});

const PrzypadekOzw = () => (
  <ScrollContainer>
    <MyTextView>
      <RedBoldText>Powód wezwania</RedBoldText>
    </MyTextView>
    <MyTextView>Mężczyzna l.75: silny ból w klatce piersiowej.</MyTextView>
    <MyTextView>
      <RedBoldText>Wywiad</RedBoldText>
    </MyTextView>
    <MyTextView>
      Pacjent zgłasza ból w klatce piersiowej (10 na 10 punktów), bezwysiłkowy, od około 2 godzin o
      charakterze rozpierania i ucisku, promienujący do pleców. Choruje na cukrzycę, astmę
      oskrzelową, nikotynizm. Brak przeszłości kardiologicznej.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Badanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Przytomny, w logicznym kontakcie, oddech 16/min, saturacja 98%. Akcja serca 72/min, ciśnienie
      tętnicze 170/90, skóra wilgotna, glikemia 131 mg/dl, szmer pęcherzykowy nad płucami, brak
      patologicznych objawów neurologicznych.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Zapis EKG</RedBoldText>
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/ozw1.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Postępowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Założono dostęp dożylny, podano 0,4 mg Nitromint s.l. Wykonano teletransmisję. Po około 2
      minutach ból ustąpił, w międzyczasie lekarz pracowni hemodynamiki, zasugerował transport na
      SOR. Podano ASA 300 mg p.o. Po około 5 minutach od wykonania EKG ból powrócił. Wykonano ekg nr
      2:
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/ozw2.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      Powtórzono teletransmisję, w międzyczasie pacjent otrzymał Nitromint 0,4 mg s.l. Lekarz
      oceniający zapis EKG zaproponował, aby pacjent trafił do niego. Podano Morfinę 3 mg i.v.,
      Clopidogrel 600 mg p.o., Heparynę 5000 j.m. i.v. W ambulansie pacjent podał, że ból minął. W
      drodze do pracowni wykonano zapis EKG nr 3:
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/ozw3.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Podsumowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Początkowo brak cech niedokrwienia w ekg, ale w drugim zapisie wyraźne uniesienia ST w I, aVL,
      V2 - V6 oraz obniżki w III i aVF wskazują na świeży zawał ściany przedniej i bocznej. Dlaczego
      zmiany te pojawiają się i znikają po krótkiej chwili?
    </MyTextView>
    <MyTextView>
      Typowo dzieje się tak w tzw. dławicy Prinzmetala. Przyczyną niedokrwienia jest w tych
      przypadkach nagły skurcz tętnicy więńcowej. Nie wiadomo co wywołuje taki skurcz, ale
      czynnikami predysponującymi są m.in. palenie tytoniu, stres, spożycie alkoholu lub niektóre
      leki. Zwykle poziom enzymów sercowych jest w normie. W leczeniu stosuje się nitraty i blokery
      kanału wapniowego.
    </MyTextView>
  </ScrollContainer>
);

export default PrzypadekOzw;
