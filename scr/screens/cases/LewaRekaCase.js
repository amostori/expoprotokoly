import React from 'react';
import { ScrollView, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ScrollContainer from '../../components/container/ScrollContainer';
import { MyTextView, RedBoldText } from '../../components/TextViews';

const styles = EStyleSheet.create({
  image: { height: 300, width: 1000 },
});

const LewaRekaCase = () => (
  <ScrollContainer>
    <MyTextView>
      <RedBoldText>Powód wezwania</RedBoldText>
    </MyTextView>
    <MyTextView>Mężczyzna l. 59 zgłasza ból lewej ręki od 2 h. Jest środek nocy.</MyTextView>
    <MyTextView>
      <RedBoldText>Wywiad</RedBoldText>
    </MyTextView>
    <MyTextView>
      Pacjent leczy się na nadciśnienie, parę miesięcy temu był kilka dni w szpitalu z powodu
      reakcji uczuleniowej na nieznany lek.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Badanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Pacjent początkowo przytomny, w logicznym kontakcie, parametry życiowe prawidłowe, akcja serca
      ok. 84/min, ciśnienie tętnicze 120/80. Chory neguje ból w klatce piersiowej.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Zapis EKG</RedBoldText>
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/ekg_blok1.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Postępowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Wykonano teletransmisję, ale lekarz zalecił transport na SOR i ewentualne powtórzenie ekg
      przed szpitalem. Zespół przekonany, że ma do czynienia z zawałem serca podał pacjentowi
      polopirynę 300 mg. W trakcie spożywania tabletki pacjent stracił przytomność, tętno na
      obwodzie zniknęło, a na monitorze pojawił się obraz bloku serca. Oddech i tętno na szyi
      obecne. Powtórzono ekg:
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/ekg_blok2.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      Kierownik zespołu zlecił podanie Atropiny 0,5 mg iv i rozpoczął przygotowania do
      przeprowadzenia stymulacji przezskórnej. Jednak zanim można było przystąpić do zabiegu
      nastąpił powrót prawidłowego rytmu, a pacjent oddzyskał przytomność. Ciśnienie tętnicze
      wynosiło 110/80, a wykonany zapis ekg był identyczny z pierwszym. Po kolejnym telefonie na
      oddział hemodynamiki lekarz zezwolił na przyjęcie do oddziału, gdzie następnie rozpoznano
      STEMI.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Podsumowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Pacjent od początku skarżył się tylko na ból lewej ręki i osłabienie. W klatce piersiowej nie
      odczuwał absolutnie żadnego bólu. Na szczęście zespół zbadał pacjenta dokładnie, traktując EKG
      jako badanie rutynowe. Obraz, który uzyskali w pierwszym zapisie - obniżki ST w I, aVL oraz
      uniesienia II, III i aVF - uznali za objaw zawału ściany dolnej.
    </MyTextView>
    <MyTextView>
      Warto zwrócić uwagę, że uniesienia ST w odprowadzeniach z nad ściany dolnej u tego pacjenta
      nie są typowe dla zawału (powinny być raczej wypukłe do góry, a tu są wklęsłe), ale zmiany
      przeciwległe (I i aVL) są niepokojące.
    </MyTextView>
    <MyTextView>
      W drugim zapisie widoczny jest blok 2:1 (tylko co drugie pobudzenie przechodzi z przedsionków
      do komór). Akcja serca spadła do 36/min, ciśnienie nieoznaczalne, pojawiły się zaburzenia
      świadomości. Oznacza to konieczność stymulacji serca. Na szczęście blok ustąpił po Atropinie
      lub samoistnie.
    </MyTextView>
  </ScrollContainer>
);

export default LewaRekaCase;
