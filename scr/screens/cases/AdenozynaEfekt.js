import React from 'react';
import { ScrollView, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ScrollContainer from '../../components/container/ScrollContainer';
import { MyTextView, RedBoldText } from '../../components/TextViews';

const styles = EStyleSheet.create({
  image: { height: 300, width: 1000 },
});

const AdenozynaEfekt = () => (
  <ScrollContainer>
    <MyTextView>
      <RedBoldText>Powód wezwania</RedBoldText>
    </MyTextView>
    <MyTextView>
      Kobieta l. 47 zgłasza ból w klatce piersiowej od 30 min. Blada, spocona, zażyła 2x nitromint -
      bez efektu.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Wywiad</RedBoldText>
    </MyTextView>
    <MyTextView>
      Ból w klatce piersiowej o charakterze rozpierania, bez promieniowania, pojawił się w spoczynku
      i trwa od ok. 30 min, uczucie duszności, niepokój, zlewne poty.
    </MyTextView>
    <MyTextView>
      Choroby: przewlekła niewydolność serca II wg NYHA, choroba niedokrwienna serca, cukrzyca typu
      2, niedoczynność tarczycy, wole guzowate, hiperlipidemia mieszana, uszkodzenie wątroby wraz z
      jej stłuszczeniem - etiologia niejasna, otyłość.
    </MyTextView>
    <MyTextView>
      Pacjentka podaje, że miała podobny epizod napadowej tachyarytmii - nie pamięta kiedy to było i
      jak ten epizod się zakończył (brak dokumentacji medycznej).
    </MyTextView>
    <MyTextView>
      <RedBoldText>Badanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Przytomna, w logicznym kontakcie, oddech 18/min, osłuchowo szmer pęcherzykowy, saturacja 96%,
      tętno na tętnicy promieniowej nitkowate, na monitorze akcja serca 220/min, miarowa, skóra
      wilgotna, RR 145/80 mmHg, glikemia 179 mg/dl, pozostałe badania w normie.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Zapis EKG</RedBoldText>
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/adenozyna1.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Postępowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Masaż zatoki szyjnej (po obu stronach) po uprzednim osłuchaniu tętnicy szyjnej - bez efektu.
      Zmodyfikowana próba Valsalvy ze zmianą pozycji ciała - bez efektu. Podano 6 mg i.v. w szybkim
      wstrzyknięciu, które natychmiast zostało przepłukane 20 ml 0,9% NaCl, dodatkowo uniesiono
      kończynę górną. Przed podaniem leku uprzedzono pacjentkę o nieprzyjemnym uczuciu po podaży
      tego leku. Jednocześnie podano tlen w przepływie 6 l/min przez maskę tlenową bez rezerwuaru -
      brak efektu.
    </MyTextView>
    <MyTextView>
      Podano drugą dawkę Adenozyny, 12 mg, która w końcu przyniosła pozytywną odpowiedź. Pacjentka
      zgłosiła zmniejszenie dolegliwości.
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/adenozyna2.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <ScrollView horizontal>
      <Image source={require('./data/adenozyna3.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Podsumowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Miarowy częstoskurcz z wąskimi QRS, brak załamków P wskazuje na nawrotny częstoskurcz węzłowy
      (AVNRT). Mógłby to być częstoskurcz nawrotny w węźle przedsionkowo - komorowym (AVRT), ale
      brak cech preekscytacji (fala delta - zespół WPW) raczej wykluczają tą możliwość.
    </MyTextView>
    <MyTextView>
      Reasumując: adenozynę stosuje się jeśli częstoskurcz jest miarowy, QRS wąski i nie widać
      załamków P (AVRT lub AVNRT). Oczywiście należy również pamiętać o przeciwwskazaniach dla tego
      leku.
    </MyTextView>
  </ScrollContainer>
);

export default AdenozynaEfekt;
