import React from 'react';
import { ScrollView, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ScrollContainer from '../../components/container/ScrollContainer';
import { MyTextView, RedBoldText } from '../../components/TextViews';

const styles = EStyleSheet.create({
  image: { height: 300, width: 1000 },
});

const BetablokerCzestoskurcz = () => (
  <ScrollContainer>
    <MyTextView>
      <RedBoldText>Powód wezwania</RedBoldText>
    </MyTextView>
    <MyTextView>
      Duszność, gniecenie w klatce piersiowej od pół godziny. Mężczyzna lat 77.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Wywiad</RedBoldText>
    </MyTextView>
    <MyTextView>
      Zgłasza ból o charakterze gniecenia w klatce piersiowej od 1 h. Zażył nitroglicerynę 2 dawki
      (aerozol). Na stałe zażywa m.in acenokumarol, metocard 25 mg rano i wieczór. Choruje na
      nadciśnienie, chorobę niedokrwienną serca, stan po zawale serca w 2007 i 2009, po operacji
      tętniaka aorty brzusznej, wszczepiony kardiowerter.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Badanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Przytomny, w logicznym kontakcie, oddech prawidłowy, saturacja 95%, akcja serca 136/min, RR
      125/75 pozostałe badania w normie.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Zapis EKG</RedBoldText>
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/betaloc_efekt.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Postępowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Podano metoprolol 5 mg (Betaloc), rozcieńczony w 10 ml NaCl 0,9% w ciągu 5 min. Po 5 min
      nastąpiło zwolnienie akcji serca do 72/min oraz ustąpienie dolegliwości bólowych. Pacjenta
      przewieziono do szpitala.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Podsumowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      W zapisie ekg widoczny jest częstoskurcz z szerokomi zespołami QRS. Rytm pochodzi ze
      stymulatora. Zastosowano metoprolol ponieważ z wypisu ze szpitala wynikało, że wcześniej ta
      metoda była skuteczna. To jeden z powodów, dla których warto przeczytać dokumentację medyczną.
    </MyTextView>
  </ScrollContainer>
);

export default BetablokerCzestoskurcz;
