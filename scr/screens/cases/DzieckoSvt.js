import React from 'react';
import { ScrollView, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ScrollContainer from '../../components/container/ScrollContainer';
import { MyTextView, RedBoldText } from '../../components/TextViews';

const styles = EStyleSheet.create({
  image: { height: 300, width: 1000 },
});

const DzieckoSvt = () => (
  <ScrollContainer>
    <MyTextView>
      <RedBoldText>Powód wezwania</RedBoldText>
    </MyTextView>
    <MyTextView>
      Dziewczynka 12 l. zgłosiła się do pielęgniarki szkolnej z powodu złego samopoczucia, uczucia
      arytmii serca i duszności.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Wywiad</RedBoldText>
    </MyTextView>
    <MyTextView>
      Pacjentka nie zgłasza uczuleń na leki, zażywa Kalipoz, Magnez, Bisocard. Choruje na napadowy
      SVT. Stan po przebytym zapaleniu mięśnia sercowego. Półtora roku temu podobny epizod - brak
      dokumentacji medycznej. Na miejscu obecna matka dziecka.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Badanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Dziewczynka przytomna, w logicznym kontakcie, oddech 14/min, saturacja 95%, tętno bardzo
      szybkie ok. 200/min, ciśnienie tętnicze 105/70 mmHg, glikemia 128 mg/dl, brak patologicznych
      objawów neurologicznych.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Zapis EKG</RedBoldText>
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/dzieckosvt1.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Postępowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Zastosowano stymulację nerwu błędnego - bez efektu, masaż zatoki szyjnej - bez efektu).
      Podjęto decyzję o podaniu Adenozyny. Dawka 0,1 mg/kg m.c. Dziewczyna o wadze 31 kg, podano 3
      mg i.v. w szybkim wstrzyknięciu, które natychmiast zostało przepłukane 20 ml 0,9% NaCl,
      dodatkowo uniesiono kończynę górną. Po podaniu leku uzyskano pozytywny efekt. Zapis ekg po
      leczeniu:
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/dzieckosvt2.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Podsumowanie</RedBoldText>
    </MyTextView>
    <MyTextView>Klasyczny częstoskurcz nadkomorowy AVNRT.</MyTextView>
  </ScrollContainer>
);

export default DzieckoSvt;
