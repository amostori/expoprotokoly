import React from 'react';
import { ScrollView, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ScrollContainer from '../../components/container/ScrollContainer';
import { MyTextView, RedBoldText } from '../../components/TextViews';

const styles = EStyleSheet.create({
  image: { height: 300, width: 1000 },
});

const AmiodaronCase = () => (
  <ScrollContainer>
    <MyTextView>
      <RedBoldText>Powód wezwania</RedBoldText>
    </MyTextView>
    <MyTextView>Mężczyzna l. 46 zgłasza złe samopoczucie i kołatanie serca od 1,5 h.</MyTextView>
    <MyTextView>
      <RedBoldText>Wywiad</RedBoldText>
    </MyTextView>
    <MyTextView>Wady zastawek serca, nadciśnienie. Uczuleń pacjent nie zgłasza.</MyTextView>
    <MyTextView>
      <RedBoldText>Badanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Przytomny, w logicznym kontakcie, oddech prawidłowy, saturacja 97%, szmer pęcherzykowy nad
      płucami. Akcja serca około 180/min, niemiarowa, ciśnienie 140/80.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Zapis EKG</RedBoldText>
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/af.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Postępowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Zespół karetki rozpoczął wlew 300 mg Amiodaronu. Po kilku minutach w szpitalu doszło do
      powrotu rytmu zatokowego.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Podsumowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Zespół miał do czynienia z pacjentem cierpiącym z powodu migotania przedsionków z szybką
      odpowiedzią komór. Jego stan był stabilny więc nie było wskazań do kardiowersji. Ze względu na
      krótki czas trwania arytmii można było zastosować wlew Amiodaronu.
    </MyTextView>
    <MyTextView>
      W przypadku gdy migotanie przedsionków trwa powyżej 48 h jest ryzyko, że zdążyła wytworzyć się
      skrzeplina w lewym przedsionku. W tej sytuacji leczenie antyarytmiczne nie jest bezpieczne.
      Aby poprawić stan pacjenta można zastosować betabloker (Metoprolol) w celu zwolnienia akcji
      serca.
    </MyTextView>
  </ScrollContainer>
);

export default AmiodaronCase;
