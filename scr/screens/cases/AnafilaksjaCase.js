import React from 'react';
import ScrollContainer from '../../components/container/ScrollContainer';
import { MyTextView, RedBoldText } from '../../components/TextViews';

const AnafilaksjaCase = () => (
  <ScrollContainer>
    <MyTextView>
      <RedBoldText>Powód wezwania</RedBoldText>
    </MyTextView>
    <MyTextView>
      Mężczyzna lat 37 został użądlony przez osę w przedramię. Jest uczulony na jad owadów
      błonkoskrzydłych.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Wywiad</RedBoldText>
    </MyTextView>
    <MyTextView>
      Rok wcześniej również nastąpił wstrząs anafilaktyczny po użądleniu. Poza tym pacjent choruje
      na astmę.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Badanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Blady, zlany potem, nieprzytomny, ciśnienie tętnicze nieoznaczalne. Osłuchowo szmer
      pęcherzykowy.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Postępowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Podano tlen 15 l/min, Adrenalinę 0,5 mg im w udo, klemastin 2 mg rozcieńczone do 10 ml solą
      fizjologiczną, hydrokortyzon 200 mg iv. W drodze do szpitala przetoczono 1000 ml NaCl 0,9%. Po
      dotarciu do szpitala pacjent oddzyskał przytomność, a ciśnienie tętnicze wynosiło 110/80 mmHg.
    </MyTextView>
  </ScrollContainer>
);

export default AnafilaksjaCase;
