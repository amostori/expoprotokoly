import React from 'react';
import { ScrollView, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ScrollContainer from '../../components/container/ScrollContainer';
import { MyTextView, RedBoldText } from '../../components/TextViews';

const styles = EStyleSheet.create({
  image: { height: 300, width: 1000 },
});

const NiemowleCase = () => (
  <ScrollContainer>
    <MyTextView>
      <RedBoldText>Powód wezwania</RedBoldText>
    </MyTextView>
    <MyTextView>
      Dziecko 12 miesięcy życia. Matka zgłasza utrudniony kontakt, gorączkę 38,7.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Wywiad</RedBoldText>
    </MyTextView>
    <MyTextView>
      Zespół Downa, przebyta operacja serca (ubytek w przegrodach przedsionkowej i komorowej) w
      04.2017 r., zapalenie płuc 05.2017 r., obecnie dziecko nie zażywa rzadnych leków.
    </MyTextView>
    <MyTextView>
      Dzień wcześniej wystąpiły wymioty x2, dziś od rana wymioty x1 i luźne stolce x5. Około 13.00
      wizyta lekarz - dziecko w dobrym kontakcie, bez odchyleń od normy. Lekarz zalecił obserwację
      dziecka.
    </MyTextView>
    <MyTextView>
      O 15.00 pojawiają się zaburzenia świadomości, po godzinie gorączka 38,7 - rodzice wzywają
      karetkę.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Badanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Dziecko drobne (waga 6 kg), wiotkie, sinica centralna, oddech 60/min, oczy otwarte (odruch
      rzęskowy zachowany, leniwy), brak reakcji na ból. Saturacja 45% - podano tlen na maskę z
      przepływem 15 l/min. Tętno na tętnicy ramiennej wyraźne, ok. 200/min, ciśnienia tętniczego nie
      udaje się zmierzyć (możliwe, że jest nieoznaczalne).
    </MyTextView>
    <MyTextView>
      Po ok. 5 min saturacja 100%, skóra różowa, dziecko nadal bez kontaktu. Poziom glukozy 128
      mg/dl, skalę GCS oszacowano na 6 punktów (po 1 za reakcję ruchową i werbalną, 4 za oczy).
    </MyTextView>
    <MyTextView>
      <RedBoldText>Zapis EKG</RedBoldText>
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/hipernatremia_ekg.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Postępowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      W karetce podano dziecku wlew 0,9% NaCl w ilości 150 ml oraz 45 mg paracetamolu w 45 ml 0,9%
      NaCl iv (brak czopków na wyposażeniu karetki). Pacjenta ułożono na pedipacu i
      przetransportowano do najbliższego soru (pogoda nielotna dla LPR). Stan dziewczynki nie uległ
      zmianie. Diagnoza ostateczna: odwodnienie i hipernatremia.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Podsumowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Zwraca uwagę jak szybko poprawiło się wysycenie hemoglobiny tlenem po podaniu tlenu.
      Początkowo zespół podejrzewał, że pierwszy odczyt saturacji (45%) był niedokładny, ale gdy w
      szpitalu zdjęto dziecku maskę tlenową saturacja gwałtownie spadła.
    </MyTextView>
    <MyTextView>
      W zapisie ekg widoczna jest znaczna tachykardia z wąskimi QRS. Nie jest to jednak częstoskurcz
      wymagający leczenia kardiowersją ponieważ dość wyraźnie widać załamki P. Jest to więc
      tachykardia zatokowa, spowodowana prawdopodobnie gorączką.
    </MyTextView>
  </ScrollContainer>
);

export default NiemowleCase;
