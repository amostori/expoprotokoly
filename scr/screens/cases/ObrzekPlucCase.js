import React from 'react';
import { ScrollView, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ScrollContainer from '../../components/container/ScrollContainer';
import { MyTextView, RedBoldText } from '../../components/TextViews';

const styles = EStyleSheet.create({
  image: { height: 300, width: 1000 },
});

const ObrzekPlucCase = () => (
  <ScrollContainer>
    <MyTextView>
      <RedBoldText>Powód wezwania</RedBoldText>
    </MyTextView>
    <MyTextView>Mężczyzna l. 71 zgłasza nagłą duszność.</MyTextView>
    <MyTextView>
      <RedBoldText>Wywiad</RedBoldText>
    </MyTextView>
    <MyTextView>
      Pacjent nie jest w stanie mówić z powodu duszności. Rodzina zgłasza nagłą silną duszność i ból
      w klatce piersiowej u pacjenta. Wcześniej wykonywał on prace w ogrodzie. Leczony na
      nadciśnienie, nie był dotychczas hospitalizowany.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Badanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Duszność jest wyraźna. Słychać również głośne rzężenia przy oddychaniu. Pacjent jest blady,
      spocony, skórę ma raczej chłodną, sinica obwodowa. Jest w utrudnionym kontakcie z powodu
      duszności. Częstość oddechu 36/min, saturacja 67%, nie osłuchano pacjenta z powodu wyraźnych
      rzężeń słyszalnych bez słuchawki. Tętno ok 132/min, ciśnienie tętnicze krwi 190/100, brzuch
      miękki, niedowładów nie stwierdzono, cukier 136 mg/dl.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Zapis EKG</RedBoldText>
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/obrzek_pluc.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Postępowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Podano tlen 15 l/min przez maskę, furosemid 40 mg iv, nitromint 0,8 mg pod język, morfinę 6
      mg. Po pięciu minutach brak poprawy więc podano jeszcze 20 mg furosemidu (znaczna nadwaga). Po
      przygotowaniu pacjenta do transportu pacjent w lepszym kontakcie, saturacja 88% - podano
      polopirynę doustnie 300 mg.
    </MyTextView>
    <MyTextView>
      W karetce wykonano teletransmisję, ale lekarz zalecił transport na SOR w celu ustabilizowania
      stanu pacjenta. Wezwano karetkę S na spotkanie z powodu niewydolności oddechowej i możliwej
      konieczności wykonania intubacji (czas dojazdu do szpitala ok. 15 min).
    </MyTextView>
    <MyTextView>
      Ostatecznie zespół rozminął się z karetką S i dotarł na SOR gdzie przekazano pacjenta w stanie
      ciężkim, ale nie pogarszającym się od początku transportu. W ekg wykonanym na sorze wyraźne
      cechy świeżego zawału serca. Po 2 h stabilizowania stanu pacjenta został on przewieziony na
      oddział hemodynamiki serca w dobrym stanie, bez duszności.
    </MyTextView>
  </ScrollContainer>
);

export default ObrzekPlucCase;
