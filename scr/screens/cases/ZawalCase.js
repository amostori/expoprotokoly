import React from 'react';
import { ScrollView, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ScrollContainer from '../../components/container/ScrollContainer';
import { MyTextView, RedBoldText } from '../../components/TextViews';

const styles = EStyleSheet.create({
  image: { height: 300, width: 1000 },
});

const ZawalCase = () => (
  <ScrollContainer>
    <MyTextView>
      <RedBoldText>Powód wezwania</RedBoldText>
    </MyTextView>
    <MyTextView>Mężczyzna lat 73 - zaburzenia mowy, drętwienie lewej ręki.</MyTextView>
    <MyTextView>
      <RedBoldText>Wywiad</RedBoldText>
    </MyTextView>
    <MyTextView>
      Pacjent zgłasza ucisk w klatce piersiowej od 3h, duszność po spacerze, drętwienie lewej ręki.
      Stan po 2 zawałach serca przed rokiem 2000, obecnie na nic się nie leczy, uczuleń nie zgłasza.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Badanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Blady, spocony, oddech 12/min, saturacja 90%, osłuchowo bez zmian patologicznych nad płucami,
      ciśnienie tętnicze krwi 80/60 mmHg, siła symetryczna.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Zapis EKG</RedBoldText>
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/small_stemi1.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Postępowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Rozpoczęto wlew 500 ml 0,9% NaCl. Wykonano teletransmisję ekg do oddziału hemodynamiki serca.
      Po dłuższym czasie zalecono podanie heparyny 5000 j.m., polopiryny 300 mg p.o. i transport na
      oddział. Lekarz zabronił podania clopidogrelu i tikagreloru. Przed przekazaniem pacjent na
      oddział hemodynamiki wykonano drugie ekg:
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/small_stemi2.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Podsumowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      W pierwszym ekg jedynie odprowadzenie III wygląda niepokojąco (poziome uniesienie odcinka ST,
      szeroki załamek Q, ujemny T). Z tego powodu zespół lekarzy z oddziału hemodynamiki nie był
      pewien czy pacjent powinien do nich trafić.
    </MyTextView>
    <MyTextView>
      W drugim zapisie wątpliwości nie było. Zmiany uległy nasileniu i pojawiły się lustrzane
      obniżki w I i aVL, oraz V3-V6.
    </MyTextView>
  </ScrollContainer>
);

export default ZawalCase;
