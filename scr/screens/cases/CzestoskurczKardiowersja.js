import React from 'react';
import { ScrollView, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ScrollContainer from '../../components/container/ScrollContainer';
import { MyTextView, RedBoldText } from '../../components/TextViews';

const styles = EStyleSheet.create({
  image: { height: 300, width: 1000 },
});

const CzestoskurczKardiowersja = () => (
  <ScrollContainer>
    <MyTextView>
      <RedBoldText>Powód wezwania</RedBoldText>
    </MyTextView>
    <MyTextView>
      Utrudniony kontakt, mężczyzna 38 lat. Wcześniej karetka pogotowia wybudziła pacjenta ze
      śpiączki hipoglikemicznej. Pacjent nie zgodził się na przewóz do szpitala.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Wywiad</RedBoldText>
    </MyTextView>
    <MyTextView>
      Pacjent zgłasza osłabienie. Choruje na marskość wątroby, cukrzycę insulinozależną. HBS i HCV
      dodatni, stan po operacji wady serca, ostatni pobyt w szpitalu związany był z wysiękiem w
      jamie opłucnowej.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Badanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Pacjent podsypiający, oddech 36/min, tętno słabo wyczuwalne, nitkowate, skóra normalna.
      Saturacja 84%, cukier 56 mg/dl, ciśnienie 80/40, temperatura 38,0. Osłuchowo brak szmeru
      oddechowego po lewej stronie - wypuk stłumiony, tony serca słabo słyszalne. Obwód brzucha
      powiększony, obrzęki obwodowe.
    </MyTextView>
    <MyTextView>
      <RedBoldText>Zapis EKG</RedBoldText>
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/kardio_efekt1.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Postępowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Tlen 15 l/min, wlew 500 ml NaCl iv, glukoza 20% 100 ml iv. W trakcie transportu epizod
      częstoskurczu z szerokimi QRS - podpięto elektrody do defibrylacji, wkrótce po tym nastąpił
      ponowny epizod.
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/kardio_efekt2.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      Podano midazolam 5 mg bez rozcieńczenia, fentanyl 0,1 mg bez rozcieńczenia i ponownie
      midazolam 5 mg, po którym pacjent stracił przytomność. Wykonano kardiowersję energią 200 J. Po
      wyładowaniu powrócił rytm poprzedni. Pacjenta przekazano na SOR gdzie otrzymał amiodaron 150
      mg w bolusie i został przetransportowany na OIOM.
    </MyTextView>
    <ScrollView horizontal>
      <Image source={require('./data/kardio_efekt3.jpg')} style={styles.image} />
    </ScrollView>
    <MyTextView />
    <MyTextView>
      <RedBoldText>Podsumowanie</RedBoldText>
    </MyTextView>
    <MyTextView>
      Początkowo widoczne jest migotanie przedsionków z szybką odpowiedzią komór. Zespoły QRS są
      szerokie i niemiarowe. Szerokość QRS spowodowana jest prawdopodobnie blokiem prawej odnogi
      pęczka Hisa (łopatowate załamki S w V6, zespół rsR&apos; w V1). Zwraca uwagę prawogram (ujemny
      QRS w I, dodatni w II i avF). Przyczyną może być przeciążenie prawej komory w przebiegu
      przewlekłej niewydolności oddchowej.
    </MyTextView>
    <MyTextView>
      Na drugim zapisie widać znaczny częstoskurcz z szerokimi QRS (akcja serca wynosiła początkowo
      240/min) oraz efekt kardiowersji - zwolnienie akcji serca. Na uwagę zasługuje dawka energii
      kardiowersji (zaleca się 120 - 150 J).
    </MyTextView>
  </ScrollContainer>
);

export default CzestoskurczKardiowersja;
