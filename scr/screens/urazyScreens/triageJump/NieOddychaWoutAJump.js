import React from 'react';
import { View } from 'react-native';
import { ButtonsYesNo, ButtonTriageAdult } from '../../../components/Buttons';
import { MyTextView, AskTextView } from '../../../components/TextViews';
import NieOddychaWithA from './NieOddychaWithA';

export default class TetnoObecne extends React.Component {
  state = {
    divy: null,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const Czerwony = (
      <ButtonTriageAdult kolor="red" tekstKolor="white">
        CZERWONY
      </ButtonTriageAdult>
    );

    return (
      <View>
        <MyTextView>Udrożnij drogi oddechowe.</MyTextView>
        <AskTextView>Oddycha?</AskTextView>

        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Czerwony)}
          onPressNo={() => this.handleOnClick(<NieOddychaWithA />)}
        />
        {divy}
        <MyTextView />
      </View>
    );
  }
}
