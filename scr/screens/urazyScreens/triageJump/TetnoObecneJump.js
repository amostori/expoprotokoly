import React from 'react';
import { View } from 'react-native';
import { ButtonsYesNo, ButtonTriageAdult } from '../../../components/Buttons';
import { MyTextView, AskTextView } from '../../../components/TextViews';

export default class TetnoObecne extends React.Component {
  state = {
    divy: null,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const Czerwony = (
      <ButtonTriageAdult kolor="red" tekstKolor="white">
        CZERWONY
      </ButtonTriageAdult>
    );
    const Czarny = (
      <ButtonTriageAdult kolor="black" tekstKolor="white">
        CZARNY
      </ButtonTriageAdult>
    );

    return (
      <View>
        <MyTextView>Wykonaj 5 wdmuchnięć.</MyTextView>
        <AskTextView>Oddycha?</AskTextView>

        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Czerwony)}
          onPressNo={() => this.handleOnClick(Czarny)}
        />
        <MyTextView />
        {divy}
        <MyTextView />
      </View>
    );
  }
}
