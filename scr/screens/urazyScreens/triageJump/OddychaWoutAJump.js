import React from 'react';
import { View } from 'react-native';
import { ButtonsYesNo, ButtonTriageAdult } from '../../../components/Buttons';
import { MyTextView, AskTextView } from '../../../components/TextViews';
import OddechOk from '../triageAdult/OddechOk';

export default class OddychaWoutAJump extends React.Component {
  state = {
    divy: null,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const Czerwony = (
      <ButtonTriageAdult kolor="red" tekstKolor="white">
        CZERWONY
      </ButtonTriageAdult>
    );

    return (
      <View>
        <AskTextView>Oddech &gt;45/min lub &lt;15/min?</AskTextView>

        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Czerwony)}
          onPressNo={() => this.handleOnClick(<OddechOk />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
