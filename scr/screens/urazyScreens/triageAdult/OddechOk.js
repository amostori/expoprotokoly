import React from 'react';
import { View } from 'react-native';
import { ButtonsYesNo, ButtonTriageAdult } from '../../../components/Buttons';
import { MyTextView, AskTextView } from '../../../components/TextViews';
import TetnoObecne from './TetnoObecne';

export default class OddechOk extends React.Component {
  state = {
    divy: null,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const Czerwony = (
      <ButtonTriageAdult kolor="red" tekstKolor="white">
        CZERWONY
      </ButtonTriageAdult>
    );

    return (
      <View>
        <AskTextView>Tętno obecne na tętnicy promieniowej?</AskTextView>

        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<TetnoObecne />)}
          onPressNo={() => this.handleOnClick(Czerwony)}
        />
        <MyTextView />
        {divy}
        <MyTextView />
      </View>
    );
  }
}
