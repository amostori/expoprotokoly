import React from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../../components/container';
import { Hypertekst, MyTextView, VersionTextView } from '../../../components/TextViews';

export default class BolUrazowy extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleMorfina = () => {
    const { navigation } = this.props;
    navigation.navigate('Morfina');
  };
  handleMorfinaP = () => {
    const { navigation } = this.props;
    navigation.navigate('MorfinaDzieci');
  };

  handleFentanyl = () => {
    const { navigation } = this.props;
    navigation.navigate('Fentanyl');
  };

  handleFentanylP = () => {
    const { navigation } = this.props;
    navigation.navigate('FentanylDzieci');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          Zastosuj techniki niefarmakologiczne zmniejszające ból (unieruchomienie złamań, wygodna
          pozycja, schładzanie oparzeń itp.)
        </MyTextView>
        <MyTextView>Oceń siłę bólu w skali od 0 do 10.</MyTextView>
        <MyTextView>
          Podaj
          <Hypertekst onPress={this.handleFentanyl}>Fentanyl</Hypertekst>
w dawce 1 &micro;g/kg u
          dorosłych 
          <Hypertekst onPress={this.handleFentanylP}>(1 - 3 &micro;g/kg u dzieci)</Hypertekst>
           iv lub io. (Ostrożnie w przypadku urazu klatki
          piersiowej). U dorosłych kolejne dawki fentanylu można podawać co 15 min do istotnego
          zmniejszenia bólu, wystąpienia sedacji lub jakościowych zaburzeń świadomości.
        </MyTextView>
        <MyTextView>lub</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleMorfina}>Morfinę</Hypertekst>
w dawce 0,1 - 0,2 mg/kg u
          dorosłych 
          <Hypertekst onPress={this.handleMorfinaP}>(0,1 mg/kg u dzieci)</Hypertekst>
           iv lub io. U dorosłych kolejne dawki morfiny można podawać
          co 5 min do istotnego zmniejszenia bólu, wystąpienia sedacji lub jakościowych zaburzeń
          świadomości.
        </MyTextView>
        <VersionTextView>
          Na podstawie Dobrych Praktyk Leczenia Bólu Ministerstwa Zdrowia.
        </VersionTextView>
      </ScrollContainer>
    );
  }
}
