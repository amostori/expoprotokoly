import ZdarzenieMasowe from './ZdarzenieMasowe';
import TriageAdult from './triageAdult/TriageAdult';
import TriageJump from './triageJump/TriageJump';
import CentrumUrazowe from './CentrumUrazowe';
import Czaszka from './Czaszka';
import Oparzenia from './oparzenia/Oparzenia';
import TestOparzen from './oparzenia/TestOparzen';
import BolUrazowy from './bolUrazowy/BolUrazowy';

export {
  ZdarzenieMasowe, TriageAdult, TriageJump, CentrumUrazowe, Czaszka, Oparzenia, TestOparzen, BolUrazowy,
};
