import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView, BoldText, VersionTextView, Hypertekst,
} from '../../components/TextViews';

export default class CentrumUrazowe extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleGlasgow = () => {
    const { navigation } = this.props;
    navigation.navigate('Glasgow');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <BoldText>Kryterium fizjologiczne</BoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleGlasgow}>Skala Glasgow</Hypertekst>
          mniejsza lub równa 13.
        </MyTextView>
        <MyTextView>RR &lt;90 mmHg</MyTextView>
        <MyTextView>Oddech &lt;10/min lub &gt;29/min (niemowlęta &lt;20/min).</MyTextView>
        <MyTextView>Konieczność wspomagania oddechu.</MyTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Kryterium anatomiczne</BoldText>
        </MyTextView>
        <MyTextView>
          Wszystkie urazy przenikające głowy, szyi, tułowia, kończyn proksymalnie (bliżej serca) od
          łokcia lub kolana.
        </MyTextView>
        <MyTextView>Niestabilna lub zniekształcona klatka piersiowa.</MyTextView>
        <MyTextView>Dwa lub więcej złamań kości długich.</MyTextView>
        <MyTextView>
          Zmiażdżenie, zerwanie powłok, poszarpanie kończyn lub brak tętna w okolicy uszkodzonej
          kończyny.
        </MyTextView>
        <MyTextView>Amputacja proksymalnie od nadgarstka lub stawu skokowego.</MyTextView>
        <MyTextView>Złamanie miednicy.</MyTextView>
        <MyTextView>Złamanie kości czaszki otwarte lub połączone z wgłobieniem.</MyTextView>
        <MyTextView>Niedowład, porażenie.</MyTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Mechanizm urazu</BoldText>
        </MyTextView>
        <MyTextView>
          Upadek dorosłej osoby z wysokości &gt;6 m lub dziecka &gt;3 m (lub 2 - 3 krotność wzrostu
          dziecka).
        </MyTextView>
        <MyTextView>
          Wypadek samochodowy: wgniecenie &gt;30 cm w okolicy pacjenta, wgniecenie gdziekolwiek
          (również dachu) &gt;45 cm, wypadnięcie całkowite lub częściowe z pojazdu, śmierć
          współpasażera, telemetryczne dane pojazdu skłaniające do groźnych urazów (np małe auto vs
          TIR).
        </MyTextView>
        <MyTextView>
          Uderzenie, przejechanie pieszego lub rowerzysty przez pojazd jadący &gt;35 km/h.
        </MyTextView>
        <MyTextView>Wypadek motocyklisty przy prędkości &gt; 35 km/h.</MyTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Sytuacje szczególne</BoldText>
        </MyTextView>
        <MyTextView>Osoby &gt;55 r.ż. mają zwiększone ryzyko śmierci po urazie.</MyTextView>
        <MyTextView>
          RR skurczowe &lt;110 mmHg u osób po 65 r.ż. może być objawem wstrząsu.
        </MyTextView>
        <MyTextView>
          Ryzyko zgonu jest większe nawet po upadku z niskiej wysokości u osób starszych.
        </MyTextView>
        <MyTextView>
          Dzieci staraj się transportować do Centrum Urazowego z pediatrycznym zapleczem.
        </MyTextView>
        <MyTextView>
          Osoby zażywające leki przeciwkrzepliwe (kwas acetylosalicylowy, clopidogrel, acenokumarol)
          z urazem głowy mogą się szybko pogorszyć mimo dobrej początkowej punktacji w skali
          Glasgow.
        </MyTextView>
        <MyTextView>
          Pacjenci z oparzeniami bez innych urazów powinni być transportowani do Centrum Leczenia
          Oparzeń natomiast z oparzeniami i innymi urazami do Centrum Urazowego.
        </MyTextView>
        <MyTextView>Pacjentki z ciążą &gt;20 tydzień rozważyć Centrum Urazowe.</MyTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Jeśli masz wątpliwości transportuj do Centrum Urazowego.</BoldText>
        </MyTextView>
        <VersionTextView>
          Opracowano na podstawie wytycznych Amerykańskiego Ośrodka Zwalczania i Zapobiegania
          Chorobom 2011.
        </VersionTextView>
      </ScrollContainer>
    );
  }
}
