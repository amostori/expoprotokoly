import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, TouchableWithoutFeedback } from 'react-native';

export default class fotoComponent extends Component {
  static propTypes = {
    onHandlePress: PropTypes.func,
    width: PropTypes.number,
    height: PropTypes.number,
    image: PropTypes.any,
    burnColor: PropTypes.string,
  };

  handle = () => null;

  render() {
    const {
      onHandlePress, width, height, image, burnColor,
    } = this.props;
    const divider = 1;
    return (
      <TouchableWithoutFeedback onPress={onHandlePress}>
        <Image
          resizeMode="contain"
          style={{ width: width / divider, height: height / divider, tintColor: burnColor }}
          source={image}
        />
      </TouchableWithoutFeedback>
    );
  }
}
