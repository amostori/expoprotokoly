import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  degreeContainer: {},
  textDegree: {
    fontSize: 18,
    marginRight: 34,
  },
  perpendicular: {
    flexDirection: 'column',
  },
  plus: {
    fontSize: 34,
    marginBottom: 20,
    marginLeft: 5,
  },
  minus: {
    fontSize: 54,
    marginLeft: 5,
  },
});

const DegreeComponent = ({
  degreeText, onPlus, onMinus, onReset,
}) => (
  <View style={styles.container}>
    <View style={styles.degreeContainer}>
      <Text style={styles.textDegree}>{degreeText}</Text>
    </View>
    <View style={styles.perpendicular}>
      <TouchableOpacity onPress={onPlus}>
        <Text style={styles.plus}>+</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={onReset}>
        <MaterialCommunityIcons name="rewind" size={32} color="#343434" />
      </TouchableOpacity>
      <TouchableOpacity onPress={onMinus}>
        <Text style={styles.minus}>-</Text>
      </TouchableOpacity>
    </View>
  </View>
);

export default DegreeComponent;
DegreeComponent.propTypes = {
  degreeText: PropTypes.string,
  onPlus: PropTypes.func,
  onMinus: PropTypes.func,
  onReset: PropTypes.func,
};
