import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity } from 'react-native';
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import { Input } from 'react-native-elements';
import { MyTextView, BoldText, ListaTextView } from '../../../components/TextViews';

const Parkland = ({ waga, onChangeText, onPressCountParkland }) => (
  <View>
    <MyTextView />
    <BoldText>Reguła Parkland</BoldText>
    <MyTextView />
    <ListaTextView>
      Służy do obliczenia utraty płynów w oparzeniach. Obliczoną ilość należy przetoczyć w ciągu 24
      h. Połowę w ciągu pierwszych 8 h, resztę w ciągu pozostałych 16.
    </ListaTextView>
    <ListaTextView>Metoda wyliczania: waga * powierzchnia * 4</ListaTextView>
    <MyTextView />
    <Input
      keyboardType="numeric"
      placeholder=" Podaj wagę pacjenta w kg"
      value={waga}
      onChangeText={onChangeText}
      leftIcon={<MaterialCommunityIcons name="weight-kilogram" size={24} color="#bababa" />}
      rightIcon={(
        <TouchableOpacity onPress={onPressCountParkland}>
          <Ionicons name="ios-arrow-round-forward" size={48} color="#343434" />
        </TouchableOpacity>
)}
    />
    <MyTextView />
  </View>
);

export default Parkland;

Parkland.propTypes = {
  waga: PropTypes.number,
  onChangeText: PropTypes.func,
  onPressCountParkland: PropTypes.func,
};
