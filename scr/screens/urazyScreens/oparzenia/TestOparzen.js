/* eslint-disable linebreak-style */
/* eslint-disable no-shadow */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, KeyboardAvoidingView } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
// import { MyTextView } from '../../../components/TextViews';
import FotoComponent from './fotoComponent';
import PlecyComponent from './PlecyComponent';
import Parkland from './Parkland';
import { ScrollContainer } from '../../../components/container';
import {
  glowa,
  klatka,
  brzuch,
  ramiePrawe,
  ramieLewe,
  przedramiePrawe,
  przedramieLewe,
  dlonR,
  dlonL,
  posladki,
  stopaR,
  stopaL,
  udoPrawe,
  udoLewe,
  podudzieR,
  podudzieL,
} from './data';
import DegreeComponent from './DegreeComponent';
import {
  onPlusClick,
  onMinusClick,
  onResetClick,
  onHeadPress,
  onKlataPress,
  onBellyPress,
  onRamieRPress,
  onRamieLPress,
  onPrzedRamieRPress,
  onPrzedRamieLPress,
  onDlonRPress,
  onDlonLPress,
  onPosladkiPress,
  onStopaLPress,
  onStopaRPress,
  onUdoRPress,
  onUdoLPress,
  onPodudzieRPress,
  onPodudzieLPress,
  onKroczePress,
  onSzyjaPressed,
  onPlecyGoraPressed,
  onPlecyDolPressed,
  setParkland,
} from '../../../redux/actions';
import { VersionTextView, MyTextView } from '../../../components/TextViews';

const styles = EStyleSheet.create({
  container: { alignItems: 'center' },
  image: { flex: 1, width: undefined, height: undefined },
  horizontal: { flexDirection: 'row' },
  degree: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  degreeText: {
    fontSize: 24,
  },
});

class TestOparzen extends Component {
  static navigationOptions = ({ navigation }) => {
    const wiek = navigation.getParam('age', 5);
    if (wiek === 1) {
      return {
        title: 'Dziecko 0 - 1 roku życia',
      };
    }
    if (wiek === 2) {
      return {
        title: 'Dziecko 1 - 4 roku życia',
      };
    }
    if (wiek === 3) {
      return {
        title: 'Dziecko 5 - 9 roku życia',
      };
    }
    if (wiek === 4) {
      return {
        title: 'Dziecko 10 - 14 roku życia',
      };
    }
    return {
      title: 'Pacjent powyżej 14 roku życia',
    };
  };

  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
    onPlusClick: PropTypes.func,
    onMinusClick: PropTypes.func,
    onResetClick: PropTypes.func,
    onHeadPress: PropTypes.func,
    onKlataPress: PropTypes.func,
    onBellyPress: PropTypes.func,
    headPressed: PropTypes.bool,
    klataPressed: PropTypes.bool,
    bellyPressed: PropTypes.bool,
    ramieRPressed: PropTypes.bool,
    ramieLPressed: PropTypes.bool,
    przedramieRPressed: PropTypes.bool,
    przedramieLPressed: PropTypes.bool,
    dlonRPressed: PropTypes.bool,
    dlonLPressed: PropTypes.bool,
    posladkiPressed: PropTypes.bool,
    isUdoRPressed: PropTypes.bool,
    isUdoLPressed: PropTypes.bool,
    stopaRPressed: PropTypes.bool,
    stopaLPressed: PropTypes.bool,
    isPodUdoRPressed: PropTypes.bool,
    isPodUdoLPressed: PropTypes.bool,
    onRamieRPress: PropTypes.func,
    onRamieLPress: PropTypes.func,
    onPrzedRamieRPress: PropTypes.func,
    onPrzedRamieLPress: PropTypes.func,
    onDlonRPress: PropTypes.func,
    onDlonLPress: PropTypes.func,
    onPosladkiPress: PropTypes.func,
    onStopaRPress: PropTypes.func,
    onStopaLPress: PropTypes.func,
    onUdoRPress: PropTypes.func,
    onUdoLPress: PropTypes.func,
    onPodudzieRPress: PropTypes.func,
    onPodudzieLPress: PropTypes.func,
    onKroczePress: PropTypes.func,
    onSzyjaPressed: PropTypes.func,
    onPlecyGoraPressed: PropTypes.func,
    onPlecyDolPressed: PropTypes.func,
    setParkland: PropTypes.func,
    total: PropTypes.number,
    weight: PropTypes.any,
    isGroinsPressed: PropTypes.bool,
    isSzyjaPressed: PropTypes.bool,
    isPlecyDolPressed: PropTypes.bool,
    isPlecyGoraPressed: PropTypes.bool,
  };

  handleOnPlus = () => {
    // eslint-disable-next-line no-shadow
    const { onPlusClick } = this.props;
    onPlusClick();
  };

  handleOnMinus = () => {
    // eslint-disable-next-line no-shadow
    const { onMinusClick } = this.props;
    onMinusClick();
  };

  onReset = () => {
    const { onResetClick } = this.props;
    onResetClick();
  };

  handleGlowaClick = () => {
    // eslint-disable-next-line no-shadow
    const { onHeadPress, navigation } = this.props;
    const wiek = navigation.getParam('age', 5);
    onHeadPress(wiek);
  };

  handleUdoRClick = () => {
    // eslint-disable-next-line no-shadow
    const { onUdoRPress, navigation } = this.props;
    const wiek = navigation.getParam('age', 5);
    onUdoRPress(wiek);
  };

  handleUdoLClick = () => {
    // eslint-disable-next-line no-shadow
    const { onUdoLPress, navigation } = this.props;
    const wiek = navigation.getParam('age', 5);
    onUdoLPress(wiek);
  };

  handlePodUdoLClick = () => {
    // eslint-disable-next-line no-shadow
    const { onPodudzieLPress, navigation } = this.props;
    const wiek = navigation.getParam('age', 5);
    onPodudzieLPress(wiek);
  };

  handlePodUdoRClick = () => {
    // eslint-disable-next-line no-shadow
    const { onPodudzieRPress, navigation } = this.props;
    const wiek = navigation.getParam('age', 5);
    onPodudzieRPress(wiek);
  };

  handleKlataClick = () => {
    // eslint-disable-next-line no-shadow
    const { onKlataPress } = this.props;
    onKlataPress();
  };

  handleBellyClick = () => {
    // eslint-disable-next-line no-shadow
    const { onBellyPress } = this.props;
    onBellyPress();
  };

  handleRamiePrawe = () => {
    // eslint-disable-next-line no-shadow
    const { onRamieRPress } = this.props;
    onRamieRPress();
  };

  handleRamieLewe = () => {
    // eslint-disable-next-line no-shadow
    const { onRamieLPress } = this.props;
    onRamieLPress();
  };

  handlePrzedRamiePrawe = () => {
    // eslint-disable-next-line no-shadow
    const { onPrzedRamieRPress } = this.props;
    onPrzedRamieRPress();
  };

  handlePrzedRamieLewe = () => {
    // eslint-disable-next-line no-shadow
    const { onPrzedRamieLPress } = this.props;
    onPrzedRamieLPress();
  };

  handleDlonRPressed = () => {
    // eslint-disable-next-line no-shadow
    const { onDlonRPress } = this.props;
    onDlonRPress();
  };

  handleDlonLPressed = () => {
    // eslint-disable-next-line no-shadow
    const { onDlonLPress } = this.props;
    onDlonLPress();
  };

  handlePosladkiPress = () => {
    // eslint-disable-next-line no-shadow
    const { onPosladkiPress } = this.props;
    onPosladkiPress();
  };

  handleStopaR = () => {
    // eslint-disable-next-line no-shadow
    const { onStopaRPress } = this.props;
    onStopaRPress();
  };

  handleStopaL = () => {
    // eslint-disable-next-line no-shadow
    const { onStopaLPress } = this.props;
    onStopaLPress();
  };

  handleGroinsPress = () => {
    const { onKroczePress } = this.props;
    onKroczePress();
  };

  handleSzyjaPress = () => {
    const { onSzyjaPressed } = this.props;
    onSzyjaPressed();
  };

  handlePlecyGoraPress = () => {
    const { onPlecyGoraPressed } = this.props;
    onPlecyGoraPressed();
  };

  handlePlecyDolPress = () => {
    const { onPlecyDolPressed } = this.props;
    onPlecyDolPressed();
  };

  handleParklandOnChangeText = (patientWeight) => {
    const { setParkland } = this.props;
    setParkland(patientWeight);
  };

  showParkland = () => {
    /*
  Reguła parkland: 4 ml * waga * procent
  połowa w przeciągu 8 h, połowa w 16h
*/
    const { weight, total } = this.props;
    const parklandPolowa = (weight * 4 * total) / 2;
    const text = `Ilość płynu do przetoczenia przez pierwsze 8h: ${parklandPolowa} ml`;
    const text2 = `Ilość płynu do przetoczenia przez pozostałe 16h: ${parklandPolowa} ml`;
    const regulaParklandDiv = (
      <View>
        <MyTextView>{`Powierzchnia oparzenia ${total}%`}</MyTextView>
        <MyTextView>{text}</MyTextView>
        <MyTextView>{text2}</MyTextView>
      </View>
    );
    this.setState({
      divy: regulaParklandDiv,
    });
  };

  render() {
    const { divy } = this.state;
    const {
      total,
      headPressed,
      klataPressed,
      bellyPressed,
      ramieRPressed,
      przedramieRPressed,
      ramieLPressed,
      przedramieLPressed,
      dlonRPressed,
      dlonLPressed,
      posladkiPressed,
      stopaRPressed,
      stopaLPressed,
      isUdoRPressed,
      isUdoLPressed,
      isPodUdoRPressed,
      isPodUdoLPressed,
      isGroinsPressed,
      isSzyjaPressed,
      isPlecyGoraPressed,
      isPlecyDolPressed,
    } = this.props;
    const klata = klataPressed ? 'red' : null;
    const head = headPressed ? 'red' : null;
    const belly = bellyPressed ? 'red' : null;
    const armPrawe = ramieRPressed ? 'red' : null;
    const forearmPrawe = przedramieRPressed ? 'red' : null;
    const armLewe = ramieLPressed ? 'red' : null;
    const forearmLewe = przedramieLPressed ? 'red' : null;
    const dlonPrawa = dlonRPressed ? 'red' : null;
    const dlonLewa = dlonLPressed ? 'red' : null;
    const dupa = posladkiPressed ? 'red' : null;
    const footR = stopaRPressed ? 'red' : null;
    const footL = stopaLPressed ? 'red' : null;
    const udkoPrawe = isUdoRPressed ? 'red' : null;
    const udkoLewe = isUdoLPressed ? 'red' : null;
    const podudkoPrawe = isPodUdoRPressed ? 'red' : null;
    const podudkoLewe = isPodUdoLPressed ? 'red' : null;
    return (
      <ScrollContainer>
        <KeyboardAvoidingView behavior="padding">
          <DegreeComponent
            degreeText={`Powierzchnia oparzenia ${total}%`}
            onPlus={this.handleOnPlus}
            onMinus={this.handleOnMinus}
            onReset={this.onReset}
          />
          <View style={{ alignItems: 'center' }}>
            <FotoComponent
              width={87}
              height={85}
              image={glowa}
              burnColor={head}
              onHandlePress={this.handleGlowaClick}
            />
            <View style={styles.horizontal}>
              <FotoComponent width={26} height={85} image={require('./data/zaRamiePrawe.png')} />
              <FotoComponent
                width={24}
                height={85}
                image={ramiePrawe}
                burnColor={armPrawe}
                onHandlePress={this.handleRamiePrawe}
              />
              <FotoComponent
                width={87}
                height={85}
                image={klatka}
                burnColor={klata}
                onHandlePress={this.handleKlataClick}
              />
              <FotoComponent
                width={24}
                height={85}
                image={ramieLewe}
                burnColor={armLewe}
                onHandlePress={this.handleRamieLewe}
              />
              <FotoComponent width={26} height={85} image={require('./data/zaRamieLewe.png')} />
            </View>
            <View style={styles.horizontal}>
              <FotoComponent
                width={78}
                height={60}
                image={przedramiePrawe}
                burnColor={forearmPrawe}
                onHandlePress={this.handlePrzedRamiePrawe}
              />
              <FotoComponent
                width={87}
                height={60}
                image={brzuch}
                burnColor={belly}
                onHandlePress={this.handleBellyClick}
              />
              <FotoComponent
                width={77}
                height={60}
                image={przedramieLewe}
                burnColor={forearmLewe}
                onHandlePress={this.handlePrzedRamieLewe}
              />
            </View>
            <View style={styles.horizontal}>
              <FotoComponent
                width={78}
                height={42}
                image={dlonR}
                burnColor={dlonPrawa}
                onHandlePress={this.handleDlonRPressed}
              />
              <FotoComponent
                width={87}
                height={42}
                image={posladki}
                burnColor={dupa}
                onHandlePress={this.handlePosladkiPress}
              />
              <FotoComponent
                width={77}
                height={42}
                image={dlonL}
                burnColor={dlonLewa}
                onHandlePress={this.handleDlonLPressed}
              />
            </View>
            <View style={styles.horizontal}>
              <FotoComponent
                width={44}
                height={84}
                image={udoPrawe}
                burnColor={udkoPrawe}
                onHandlePress={this.handleUdoRClick}
              />
              <FotoComponent
                width={43}
                height={84}
                burnColor={udkoLewe}
                image={udoLewe}
                onHandlePress={this.handleUdoLClick}
              />
            </View>
            <View style={styles.horizontal}>
              <FotoComponent
                width={44}
                height={101}
                image={podudzieR}
                burnColor={podudkoPrawe}
                onHandlePress={this.handlePodUdoRClick}
              />
              <FotoComponent
                width={43}
                height={101}
                image={podudzieL}
                burnColor={podudkoLewe}
                onHandlePress={this.handlePodUdoLClick}
              />
            </View>
            <View style={styles.horizontal}>
              <FotoComponent
                width={44}
                height={42}
                image={stopaR}
                burnColor={footR}
                onHandlePress={this.handleStopaR}
              />
              <FotoComponent
                width={43}
                height={42}
                burnColor={footL}
                image={stopaL}
                onHandlePress={this.handleStopaL}
              />
            </View>
          </View>
          <PlecyComponent
            handleGroinsPress={this.handleGroinsPress}
            kroczeChecked={isGroinsPressed}
            handleSzyjaPress={this.handleSzyjaPress}
            szyjaChecked={isSzyjaPressed}
            handlePlecyGoraPress={this.handlePlecyGoraPress}
            handlePlecyDolPress={this.handlePlecyDolPress}
            plecyGoraChecked={isPlecyGoraPressed}
            plecyDolChecked={isPlecyDolPressed}
          />
          <Parkland
            onChangeText={this.handleParklandOnChangeText}
            onPressCountParkland={this.showParkland}
          />
          {divy}
        </KeyboardAvoidingView>
        <VersionTextView>Na podstawie tabeli Lunga i Browdera</VersionTextView>
      </ScrollContainer>
    );
  }
}

const mapStateToProps = ({ burnsReducer }) => {
  const {
    total,
    headPressed,
    klataPressed,
    bellyPressed,
    ramieRPressed,
    ramieLPressed,
    przedramieRPressed,
    przedramieLPressed,
    dlonRPressed,
    dlonLPressed,
    posladkiPressed,
    stopaRPressed,
    stopaLPressed,
    isUdoRPressed,
    isUdoLPressed,
    isPodUdoRPressed,
    isPodUdoLPressed,
    isGroinsPressed,
    isSzyjaPressed,
    isPlecyGoraPressed,
    isPlecyDolPressed,
    weight,
  } = burnsReducer;
  return {
    total,
    headPressed,
    klataPressed,
    bellyPressed,
    ramieRPressed,
    ramieLPressed,
    przedramieRPressed,
    przedramieLPressed,
    dlonRPressed,
    dlonLPressed,
    posladkiPressed,
    stopaRPressed,
    stopaLPressed,
    isUdoRPressed,
    isUdoLPressed,
    isPodUdoRPressed,
    isPodUdoLPressed,
    isGroinsPressed,
    isSzyjaPressed,
    isPlecyGoraPressed,
    isPlecyDolPressed,
    weight,
  };
};

export default connect(
  mapStateToProps,
  {
    onPlusClick,
    onMinusClick,
    onResetClick,
    onHeadPress,
    onKlataPress,
    onBellyPress,
    onRamieRPress,
    onRamieLPress,
    onPrzedRamieRPress,
    onPrzedRamieLPress,
    onDlonRPress,
    onDlonLPress,
    onPosladkiPress,
    onStopaRPress,
    onStopaLPress,
    onUdoRPress,
    onUdoLPress,
    onPodudzieRPress,
    onPodudzieLPress,
    onKroczePress,
    onSzyjaPressed,
    onPlecyGoraPressed,
    onPlecyDolPressed,
    setParkland,
  },
)(TestOparzen);

/*
  Reguła parkland: 4 ml * waga * procent
  połowa w przeciągu 8 h, połowa w 16h
*/
