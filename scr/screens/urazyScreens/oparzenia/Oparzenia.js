import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  MyTextView, Hypertekst, BoldText, ListaTextView,
} from '../../../components/TextViews';
import { ScrollContainer } from '../../../components/container';

export default class Oparzenia extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleMorfina = () => {
    const { navigation } = this.props;
    navigation.navigate('Morfina');
  };

  handleDziecko = (wiek) => {
    const { navigation } = this.props;
    navigation.navigate('TestOparzen', { age: wiek });
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          Oceń bezpieczeństwo. W miarę możliwości usuń odzież i biżuterię z miejsca oparzenia i
          polewaj ranę zimną wodą. Warto schładzać ranę do 30 minut po oparzeniu, a nawet później.
        </MyTextView>
        <MyTextView>
          Skróć czas schładzania u małych dzieci z powodu niebezpieczeństwa wychłodzenia.
        </MyTextView>
        <MyTextView>
          Po zakończeniu schładzania nałóż na ranę opatrunek hydrożelowy. Jeśli konieczne jest
          działanie przeciwbólowe podaj
          {' '}
          <Hypertekst onPress={this.handleMorfina}>Morfinę</Hypertekst>
w dawkach frakcjonowanych po
          2 mg do ustąpienia bólu (u dzieci 0,1 mg/kg).
        </MyTextView>

        <MyTextView>
          Podłącz wlew 500 ml soli fizjologicznej u dorosłych, 20 ml/kg u dzieci. Transport do
          szpitala.
        </MyTextView>
        <MyTextView>
          <BoldText>Kryterium transportu do Centrum Oparzeniowego (wg LPR):</BoldText>
        </MyTextView>
        <ListaTextView>Oparzenie II&deg; przekraczające 20%.</ListaTextView>
        <ListaTextView>
          Oparzenia II&deg; przekraczające 10% u dzieci do 10 r.ż. i powyżej 50 r.ż.
        </ListaTextView>
        <ListaTextView>Oparzenie III&deg;.</ListaTextView>
        <ListaTextView>Oparzenie dróg oddechowych.</ListaTextView>
        <ListaTextView>Oparzenia II&deg; obejmujące twarz, kończyny, krocze.</ListaTextView>
        <ListaTextView>Oparzenia elektryczne.</ListaTextView>
        <ListaTextView>Oparzenia chemiczne.</ListaTextView>
        <MyTextView />
        <BoldText>Powierzchnia oparzenia:</BoldText>
        <MyTextView />
        <MyTextView>
          <Hypertekst onPress={() => this.handleDziecko(1)}>Dziecko 0 - 1 r.ż.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={() => this.handleDziecko(2)}>Dziecko 1 - 4 r.ż.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={() => this.handleDziecko(3)}>Dziecko 5 - 9 r.ż.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={() => this.handleDziecko(4)}>Dziecko 10 - 14 r.ż.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={() => this.handleDziecko(5)}>
            Dziecko &gt;14 r.ż. i dorośli.
          </Hypertekst>
        </MyTextView>
      </ScrollContainer>
    );
  }
}
