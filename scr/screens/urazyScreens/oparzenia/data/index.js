import glowa from './glowa.png';
import klatka from './klatka.png';
import brzuch from './brzuch.png';
import ramiePrawe from './ramiePrawe.png';
import przedramiePrawe from './przedramiePrawe.png';
import ramieLewe from './ramieLewe.png';
import przedramieLewe from './przedramieLewe.png';
import dlonR from './dlonPrawa.png';
import dlonL from './dlonLewa.png';
import posladki from './posladki.png';
import stopaR from './stopaPrawa.png';
import stopaL from './stopaLewa.png';
import udoPrawe from './udoPrawe.png';
import udoLewe from './udoLewe.png';
import podudzieR from './podudziePrawe.png';
import podudzieL from './podudzieLewe.png';

export {
  glowa,
  klatka,
  brzuch,
  ramiePrawe,
  ramieLewe,
  przedramiePrawe,
  przedramieLewe,
  dlonR,
  dlonL,
  posladki,
  stopaR,
  stopaL,
  udoPrawe,
  udoLewe,
  podudzieR,
  podudzieL,
};
