/* eslint-disable no-shadow */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import { ScrollContainer } from '../../components/container';
import { BadanieHipertekst, VersionTextView } from '../../components/TextViews';
import { TriageColorButtons } from '../../components/Buttons';
import masowka from './data/masowka';
import { Separator, ListItemMasowka } from '../../components/ListViews/ALSList';
import {
  totalIncrease,
  greenIncrease,
  yellowIncrease,
  redIncrease,
  blackIncrease,
  greenDecrease,
  yellowDecrease,
  redDecrease,
  blackDecrease,
  resetTotal,
  itemPressed,
} from '../../redux/actions';

class ZdarzenieMasowe extends Component {
  static propTypes = {
    green: PropTypes.number,
    yellow: PropTypes.number,
    red: PropTypes.number,
    black: PropTypes.number,
    total: PropTypes.number,
    itemPressedId: PropTypes.array,
    totalIncrease: PropTypes.func,
    itemPressed: PropTypes.func,
    blackIncrease: PropTypes.func,
    greenIncrease: PropTypes.func,
    greenDecrease: PropTypes.func,
    yellowDecrease: PropTypes.func,
    redDecrease: PropTypes.func,
    blackDecrease: PropTypes.func,
    redIncrease: PropTypes.func,
    yellowIncrease: PropTypes.func,
    resetTotal: PropTypes.func,
  };

  state = {
    needToRerender: false,
  };

  handleTotalIncrease = () => {
    // eslint-disable-next-line no-shadow
    const { totalIncrease } = this.props;
    totalIncrease();
  };

  handleIncreaseBlack = () => {
    // eslint-disable-next-line no-shadow
    const { blackIncrease } = this.props;
    blackIncrease();
  };

  handleIncreaseGreen = () => {
    // eslint-disable-next-line no-shadow
    const { greenIncrease } = this.props;
    greenIncrease();
  };

  handleIncreaseRed = () => {
    // eslint-disable-next-line no-shadow
    const { redIncrease } = this.props;
    redIncrease();
  };

  handleIncreaseYellow = () => {
    // eslint-disable-next-line no-shadow
    const { yellowIncrease } = this.props;
    yellowIncrease();
  };

  handlGreenLongPress = () => {
    const { greenDecrease } = this.props;
    greenDecrease();
  };

  handlYellowLongPress = () => {
    const { yellowDecrease } = this.props;
    yellowDecrease();
  };

  handlRedLongPress = () => {
    const { redDecrease } = this.props;
    redDecrease();
  };

  handlBlackLongPress = () => {
    const { blackDecrease } = this.props;
    blackDecrease();
  };

  handleTotalReset = () => {
    // eslint-disable-next-line no-shadow
    const { resetTotal } = this.props;
    const { needToRerender } = this.state;
    resetTotal();
    this.setState({
      needToRerender: !needToRerender,
    });
  };

  onItemPressed = (item) => {
    // eslint-disable-next-line no-shadow
    const { itemPressedId, itemPressed } = this.props;
    const { needToRerender } = this.state;
    const myArray = itemPressedId;

    if (myArray.includes(item)) {
      const newArray = myArray.filter(value => value !== item);
      // metoda filter to sposób na usunięcie elementu z array. Metoda tworzy nową tablicę
      // zawierającą
      // elementy spełniające warunek value !== item
      itemPressed(newArray);
    } else {
      myArray.push(item);
      itemPressed(myArray);
    }
    this.setState({
      needToRerender: !needToRerender,
    });
  };

  render() {
    const {
      total, black, green, yellow, red, itemPressedId,
    } = this.props;
    const comparisonId = itemPressedId;
    return (
      <ScrollContainer>
        <BadanieHipertekst onLongPress={this.handleTotalReset}>
          Razem:
          {` ${total}`}
        </BadanieHipertekst>
        <TriageColorButtons
          onPressGreen={this.handleIncreaseGreen}
          onGreenLongPress={this.handlGreenLongPress}
          onYellowLongPress={this.handlYellowLongPress}
          onRedLongPress={this.handlRedLongPress}
          onBlackLongPress={this.handlBlackLongPress}
          onPressYellow={this.handleIncreaseYellow}
          onPressRed={this.handleIncreaseRed}
          onPressBlack={this.handleIncreaseBlack}
          greenTekst={green}
          yellowTekst={yellow}
          redTekst={red}
          blackTekst={black}
        />
        <FlatList
          data={masowka}
          extraData={this.state}
          /*
          extraData - przesyłamy state aby FlatList wiedział, że ma się na nowo zrenderować jeśli
          zmieni się state.
          */

          renderItem={({ item }) => (
            <ListItemMasowka
              onPress={() => this.onItemPressed(item)}
              text={item}
              selected={comparisonId.includes(item)}
            />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
        <VersionTextView>
          W oparciu o zalecenia Konsultanta Krajowego w Dziedzinie Medycyny Ratunkowej dotyczące
          procedur postępowanie na wypadek wystąpienia zdarzenia mnogiego/masowego.
        </VersionTextView>
      </ScrollContainer>
    );
  }
}

const mapStateToProps = ({ masowkaReducer }) => {
  const {
    green, yellow, red, black, total, itemPressedId,
  } = masowkaReducer;
  return {
    green,
    yellow,
    red,
    black,
    total,
    itemPressedId,
  };
};

export default connect(
  mapStateToProps,
  {
    totalIncrease,
    greenIncrease,
    yellowIncrease,
    redIncrease,
    blackIncrease,
    resetTotal,
    itemPressed,
    greenDecrease,
    yellowDecrease,
    redDecrease,
    blackDecrease,
  },
)(ZdarzenieMasowe);
