import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, StatusBar } from 'react-native';
import palsList from './data/palsList';
import { ListItem, Separator } from '../../components/ListViews/ALSList';

class PalsScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  onPressHandler = (item) => {
    const { navigation } = this.props;
    switch (item) {
      case 'NZK u dzieci I 46':
        navigation.navigate('NzkP');
        break;
      case 'NZK u świeżorodka I 46':
        navigation.navigate('Swiezorodek');
        break;
      case 'Bradykardia u dzieci R 00.1':
        navigation.navigate('BradykardiaP');
        break;
      case 'Częstoskurcz u dzieci R 00.0':
        navigation.navigate('CzestoskurczP');
        break;
      case 'Anafilaksja u dzieci T 78.2':
        navigation.navigate('AnafilaksjaP');
        break;
      case 'Astma u dzieci J 46':
        navigation.navigate('AstmaP');
        break;
      case 'Zapalenie krtani J 05.0':
        navigation.navigate('ZapalenieKrtani');
        break;
      case 'Ból brzucha u dzieci R 10.4':
        navigation.navigate('BolBrzuchaP');
        break;
      case 'Hipoglikemia u dzieci E 16.2':
        navigation.navigate('HipoglikemiaP');
        break;
      case 'Drgawki u dzieci R 56.8':
        navigation.navigate('DrgawkiP');
        break;
      case 'Dziecko gorączkujące R 50':
        navigation.navigate('Goraczka');
        break;
      case 'Zwalczanie bólu u dzieci':
        navigation.navigate('ZwalczanieBoluP');
        break;
      default:
        alert('navigation error');
        break;
    }
  };

  render() {
    return (
      <View style={{ backgroundColor: '#e9ecf0', flex: 1 }}>
        <StatusBar translucent={false} barStyle="light-content" />
        <FlatList
          data={palsList}
          renderItem={({ item }) => (
            <ListItem text={item} onPress={() => this.onPressHandler(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
      </View>
    );
  }
}

export default PalsScreen;
