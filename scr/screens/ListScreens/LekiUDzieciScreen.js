import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, StatusBar } from 'react-native';
import lekiUdzieci from './data/lekiUdzieci';
import { ListItem, Separator } from '../../components/ListViews/ALSList';

class LekiUDzieciScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  onPressHandler = (item) => {
    const { navigation } = this.props;
    switch (item) {
      case 'Adrenalina u dzieci':
        navigation.navigate('AdrenalinaWskazowki');
        break;
      case 'Atropina u dzieci':
        navigation.navigate('AtropinaWskazowki');
        break;
      case 'Amiodaron u dzieci':
        navigation.navigate('AmiodaronWskazowki');
        break;
      case 'Diazepam u dzieci':
        navigation.navigate('DiazepamDzieci');
        break;
      case 'Midazolam u dzieci':
        navigation.navigate('MidazolamDzieci');
        break;
      case 'Morfina u dzieci':
        navigation.navigate('MorfinaDzieci');
        break;
      case 'Fentanyl u dzieci':
        navigation.navigate('FentanylDzieci');
        break;
      case 'Nalokson u dzieci':
        navigation.navigate('NaloksonDzieci');
        break;
      case 'Sedacja dzieci':
        navigation.navigate('SedacjaDzieci');
        break;
      case 'Paracetamol u dzieci':
        navigation.navigate('ParacetamolDzieci');
        break;
      default:
        alert('dupa');
        break;
    }
  };

  render() {
    return (
      <View style={{ backgroundColor: '#e9ecf0', flex: 1 }}>
        <StatusBar translucent={false} barStyle="light-content" />
        <FlatList
          data={lekiUdzieci}
          renderItem={({ item }) => (
            <ListItem text={item} onPress={() => this.onPressHandler(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
      </View>
    );
  }
}

export default LekiUDzieciScreen;
