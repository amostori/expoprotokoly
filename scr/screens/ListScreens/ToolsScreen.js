import React from 'react';
import { View, FlatList, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import toolsList from './data/tools_list';
import { ListItem, Separator } from '../../components/ListViews/ALSList';

class ToolsScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  onPressHandler = (item) => {
    const { navigation } = this.props;
    switch (item) {
      case 'Asystent RKO':
        navigation.navigate('CPRAssistant');
        break;
      case 'Skala Glasgow':
        navigation.navigate('Glasgow');
        break;
      case 'Parametry życiowe':
        navigation.navigate('LifeParam');
        break;
      case 'Skala Apgar':
        navigation.navigate('Apgar');
        break;
      case 'Skala Westleya':
        navigation.navigate('Westley');
        break;
      case 'Cewnik dializacyjny':
        navigation.navigate('Catheter');
        break;
      case 'Średnie ciśnienie tętnicze':
        navigation.navigate('Pressure');
        break;
      case 'Triage trainer':
        navigation.navigate('TriageTrening');
        break;

      default:
        alert('error');
        break;
    }
  };

  render() {
    return (
      <View style={{ backgroundColor: '#e9ecf0', flex: 1 }}>
        <StatusBar translucent={false} barStyle="light-content" />
        <FlatList
          data={toolsList}
          renderItem={({ item }) => (
            <ListItem text={item} onPress={() => this.onPressHandler(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
      </View>
    );
  }
}

export default ToolsScreen;
