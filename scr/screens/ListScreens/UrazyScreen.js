import React from 'react';
import { View, FlatList, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import urazyList from './data/urazy_list';
import { ListItem, Separator } from '../../components/ListViews/ALSList';

class UrazyScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  onPressHandler = (item) => {
    const { navigation } = this.props;
    switch (item) {
      case 'Zdarzenia masowe':
        navigation.navigate('ZdarzenieMasowe');
        break;
      case 'TRIAGE':
        navigation.navigate('TriageAdult');
        break;
      case 'TRIAGE pediatryczny':
        navigation.navigate('TriageJump');
        break;
      case 'Centrum Urazowe - kryteria przyjęć':
        navigation.navigate('CentrumUrazowe');
        break;
      case 'Oparzenia T 30.0':
        navigation.navigate('Oparzenia');
        break;
      case 'Uraz czaszkowo - mózgowy S 06':
        navigation.navigate('Czaszka');
        break;
      case 'Zwalczanie bólu urazowego':
        navigation.navigate('BolUrazowy');
        break;
      default:
        alert('navigation error');
        break;
    }
  };

  render() {
    return (
      <View style={{ backgroundColor: '#e9ecf0', flex: 1 }}>
        <StatusBar translucent={false} barStyle="light-content" />
        <FlatList
          data={urazyList}
          renderItem={({ item }) => (
            <ListItem text={item} onPress={() => this.onPressHandler(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
      </View>
    );
  }
}

export default UrazyScreen;
