import React from 'react';
import { View, FlatList, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import farmaList from './data/farma_list';
import { ListItem, Separator } from '../../components/ListViews/ALSList';

class FarmaScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  onPressHandler = (item) => {
    const { navigation } = this.props;
    switch (item) {
      case 'Adenozyna':
        navigation.navigate('Adenozyna');
        break;
      case 'Adrenalina':
        navigation.navigate('Adrenalina');
        break;
      case 'Amiodaron':
        navigation.navigate('Amiodaron');
        break;
      case 'Atropina':
        navigation.navigate('Atropina');
        break;
      case 'Budesonidum':
        navigation.navigate('Budezonid');
        break;
      case 'Captopril':
        navigation.navigate('Captopril');
        break;
      case 'Clemastinum':
        navigation.navigate('Clemastin');
        break;
      case 'Clonazepamum':
        navigation.navigate('Clonazepam');
        break;
      case 'Clopidogrel':
        navigation.navigate('Klopidogrel');
        break;
      case 'Deksametazon':
        navigation.navigate('Dexaven');
        break;
      case 'Diazepamum':
        navigation.navigate('Diazepam');
        break;
      case 'Drotaweryna (No-Spa)':
        navigation.navigate('Drotaweryna');
        break;
      case 'Fentanyl':
        navigation.navigate('Fentanyl');
        break;
      case 'Flumazenil':
        navigation.navigate('Flumazenil');
        break;
      case 'Furosemid':
        navigation.navigate('Furosemid');
        break;
      case 'Glukagon':
        navigation.navigate('Glukagon');
        break;
      case 'Glukoza 20%':
        navigation.navigate('Glukoza');
        break;
      case 'Heparyna':
        navigation.navigate('Heparyna');
        break;
      case 'Hydrokortyzon':
        navigation.navigate('Hydrokortyzon');
        break;
      case 'Hydroxyzinum':
        navigation.navigate('Hydroxyzyna');
        break;
      case 'Ibuprofen':
        navigation.navigate('Ibuprofen');
        break;
      case 'Izosorbid - Mononit':
        navigation.navigate('Izosorbid');
        break;
      case 'Ketoprofen':
        navigation.navigate('Ketoprofen');
        break;
      case 'Kwas acetylosalicylowy':
        navigation.navigate('ASA');
        break;
      case 'Leki anestezjologiczne':
        navigation.navigate('Anestezja');
        break;
      case 'Lignokaina':
        navigation.navigate('Lignokaina');
        break;
      case 'Magnez':
        navigation.navigate('Magnez');
        break;
      case 'Mannitol 15%':
        navigation.navigate('Mannitol');
        break;
      case 'Metamizol (Pyralgina)':
        navigation.navigate('Pyralgina');
        break;
      case 'Metoclopramid':
        navigation.navigate('Metoklopramid');
        break;
      case 'Metoprolol':
        navigation.navigate('Metoprolol');
        break;
      case 'Midazolam':
        navigation.navigate('Midazolam');
        break;
      case 'Morfina':
        navigation.navigate('Morfina');
        break;
      case 'Nalokson':
        navigation.navigate('Nalokson');
        break;
      case 'Natrii hydrogenocarbonas 8,4%':
        // todo
        navigation.navigate('Natrii');
        break;
      case 'Nitrogliceryna':
        navigation.navigate('Nitrogliceryna');
        break;
      case 'Papaweryna':
        navigation.navigate('Papaweryna');
        break;
      case 'Paracetamol':
        navigation.navigate('Paracetamol');
        break;
      case 'Salbutamol':
        navigation.navigate('Salbutamol');
        break;
      case 'Thiethylperazinum (Torecan)':
        // todo
        navigation.navigate('Torecan');
        break;
      case 'Ticagrelol':
        navigation.navigate('Tikagrelor');
        break;
      case 'Urapidil (Ebrantil)':
        navigation.navigate('Urapidil');
        break;
      default:
        alert('dupa');
        break;
    }
  };

  render() {
    return (
      <View style={{ backgroundColor: '#e9ecf0', flex: 1 }}>
        <StatusBar translucent={false} barStyle="light-content" />
        <FlatList
          data={farmaList}
          renderItem={({ item }) => (
            <ListItem text={item} onPress={() => this.onPressHandler(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
      </View>
    );
  }
}

export default FarmaScreen;
