import React from 'react';
import { View, FlatList, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import zatruciaList from './data/zatrucia_list';
import { ListItem, Separator } from '../../components/ListViews/ALSList';

class ZatruciaScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  onPressHandler = (item) => {
    const { navigation } = this.props;
    switch (item) {
      case 'Amfetamina i kokaina T 50':
        navigation.navigate('Amfetamina');
        break;
      case 'Benzodiazepiny T 50':
        navigation.navigate('Benzodiazepiny');
        break;
      case 'Betablokery i Ca - blokery T 50':
        navigation.navigate('Betablokery');
        break;
      case 'Cyjanki T 50':
        navigation.navigate('Cyjanki');
        break;
      case 'Digoksyna T 50':
        navigation.navigate('Digoksyna');
        break;
      case 'Dopalacze T 50':
        navigation.navigate('Dopalacze');
        break;
      case 'LSD, grzyby halucynogenne T 50':
        navigation.navigate('LSD');
        break;
      case 'Metanol, Glikol etylenowy T 50':
        navigation.navigate('Metanol');
        break;
      case 'Morfina, Heroina, Kodeina T 50':
        navigation.navigate('Opiaty');
        break;
      case 'Paracetamol T 50':
        navigation.navigate('ZatrucieParacetamolem');
        break;
      case 'Rtęć T 50':
        navigation.navigate('Rtec');
        break;
      case 'Tlenek węgla T 50':
        navigation.navigate('CO');
        break;
      case 'Trójcykliczne leki przeciwdepresyjne T 50':
        navigation.navigate('TLP');
        break;
      case 'Związki fosfoorganiczne T 50':
        navigation.navigate('Fosforany');
        break;
      case 'Żelazo T 50':
        navigation.navigate('Zelazo');
        break;
      default:
        alert('dupa');
        break;
    }
  };

  render() {
    return (
      <View style={{ backgroundColor: '#e9ecf0', flex: 1 }}>
        <StatusBar translucent={false} barStyle="light-content" />
        <FlatList
          data={zatruciaList}
          renderItem={({ item }) => (
            <ListItem text={item} onPress={() => this.onPressHandler(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
      </View>
    );
  }
}

export default ZatruciaScreen;
