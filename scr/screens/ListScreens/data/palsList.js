export default [
  'NZK u dzieci I 46',
  'NZK u świeżorodka I 46',
  'Bradykardia u dzieci R 00.1',
  'Częstoskurcz u dzieci R 00.0',
  'Anafilaksja u dzieci T 78.2',
  'Astma u dzieci J 46',
  'Zapalenie krtani J 05.0',
  'Ból brzucha u dzieci R 10.4',
  'Hipoglikemia u dzieci E 16.2',
  'Drgawki u dzieci R 56.8',
  'Dziecko gorączkujące R 50',
  'Zwalczanie bólu u dzieci',
];
