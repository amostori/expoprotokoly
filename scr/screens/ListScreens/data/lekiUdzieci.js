export default [
  'Adrenalina u dzieci',
  'Atropina u dzieci',
  'Amiodaron u dzieci',
  'Diazepam u dzieci',
  'Midazolam u dzieci',
  'Morfina u dzieci',
  'Nalokson u dzieci',
  'Sedacja dzieci',
  'Paracetamol u dzieci',
  'Fentanyl u dzieci',
];
