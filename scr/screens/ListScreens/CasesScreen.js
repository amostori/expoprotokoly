import React from 'react';
import { View, FlatList, StatusBar } from 'react-native';
import PropTypes from 'prop-types';
import casesList from './data/cases_list';
import { ListItem, Separator } from '../../components/ListViews/ALSList';

class AlsScreen extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  onPressHandler = (item) => {
    const { navigation } = this.props;
    switch (item) {
      case 'Betabloker przerywa częstoskurcz':
        navigation.navigate('BetablokerCzestoskurcz');
        break;
      case 'Częstoskurcz i kardiowersja':
        navigation.navigate('CzestoskurczKardiowersja');
        break;
      case 'Zastosowanie Adenozyny':
        navigation.navigate('AdenozynaEfekt');
        break;
      case 'Przypadek OZW':
        navigation.navigate('PrzypadekOzw');
        break;
      case 'Zastosowanie Metoprololu':
        navigation.navigate('MetoprololEfekt');
        break;
      case 'Obrzęk płuc':
        navigation.navigate('ObrzekPlucCase');
        break;
      case 'Dziecko z częstoskurczem':
        navigation.navigate('DzieckoSvt');
        break;
      case 'Zawał serca':
        navigation.navigate('ZawalCase');
        break;
      case 'Wstrząs anafilaktyczny':
        navigation.navigate('AnafilaksjaCase');
        break;
      case 'Migotanie przedsionków':
        navigation.navigate('AmiodaronCase');
        break;
      case 'Niemowlę z zaburzeniami świadomości':
        navigation.navigate('NiemowleCase');
        break;
      case 'Ból lewej ręki':
        navigation.navigate('LewaRekaCase');
        break;
      default:
        alert('test');
        break;
    }
  };

  render() {
    return (
      <View style={{ backgroundColor: '#e9ecf0', flex: 1 }}>
        <StatusBar translucent={false} barStyle="light-content" />
        <FlatList
          data={casesList}
          renderItem={({ item }) => (
            <ListItem text={item} onPress={() => this.onPressHandler(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
      </View>
    );
  }
}

export default AlsScreen;
