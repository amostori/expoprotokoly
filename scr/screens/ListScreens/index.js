import AlsScreen from './AlsScreen';
import PalsScreen from './PalsScreen';
import PoloznictwoScreen from './PoloznictwoScreen';
import UrazyScreen from './UrazyScreen';
import ZatruciaScreen from './ZatruciaScreen';
import ToolsScreen from './ToolsScreen';
import LekiUDzieciScreen from './LekiUDzieciScreen';
import FarmaScreen from './FarmaScreen';
import CasesScreen from './CasesScreen';

export {
  AlsScreen,
  PalsScreen,
  PoloznictwoScreen,
  UrazyScreen,
  ZatruciaScreen,
  ToolsScreen,
  LekiUDzieciScreen,
  FarmaScreen,
  CasesScreen,
};
