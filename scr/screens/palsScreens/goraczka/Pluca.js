import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import {
  MyTextView,
  Hypertekst,
  BoldText,
  TiM,
  VersionTextView,
} from '../../../components/TextViews';

export default class Pluca extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleGoraczka = () => {
    const { navigation } = this.props;
    navigation.navigate('LeczenieGoraczki');
  };

  render() {
    return (
      <View>
        <MyTextView>
          <BoldText>Podejrzenie zapalenia płuc.</BoldText>
        </MyTextView>
        <MyTextView>Podaj tlen aby utrzymać prawidłową saturację.</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleGoraczka}>Zwalczaj gorączkę.</Hypertekst>
        </MyTextView>
        <TiM />
        <VersionTextView>
          Na podstawie National Institute for Health and Care Excellence &quot;Feverish illness in
          children: assessment and initial management in children younger than 5 years&quot; oraz
          Royal College of Obstetricians and Gynaecologists, London, 2013, David S Stephens, Brian
          Greenwood, Petter Brandtzaeg &quot;Epidemic meningitis, meningococcaemia, and Neisseria
          meningitidis&quot; Lancet 2007 369 2196–210
        </VersionTextView>
      </View>
    );
  }
}
