import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ButtonsYesNo } from '../../../components/Buttons';
import { MyTextView } from '../../../components/TextViews';
import Pluca from './Pluca';
import NiePluca from './NiePluca';

export default class NieOpony extends React.Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const { navigation } = this.props;

    return (
      <View>
        <MyTextView>
          Przyspieszony oddech, zaciąganie międzyżebrzy, trzeszczenia nad płucami, sinica, saturacja
          &lt;94%?
        </MyTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<Pluca navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<NiePluca navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
