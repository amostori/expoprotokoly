import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView,
  Hypertekst,
  BoldText,
  Sol,
  TiM,
  VersionTkaczykZwalczanieBolu,
} from '../../components/TextViews';

export default class ZwalczanieBoluP extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleObjawyBrzuszne = () => {
    const { navigation } = this.props;
    navigation.navigate('ObjawyBrzuszne');
  };

  handleMorfina = () => {
    const { navigation } = this.props;
    navigation.navigate('MorfinaDzieci');
  };

  handleSkalaBolu = () => {
    const { navigation } = this.props;
    navigation.navigate('SkalaBolu');
  };

  handleParacetamol = () => {
    const { navigation } = this.props;
    navigation.navigate('ParacetamolDzieci');
  };

  handleFentanyl = () => {
    const { navigation } = this.props;
    navigation.navigate('FentanylDzieci');
  };

  handleDrotaweryna = () => {
    const { navigation } = this.props;
    navigation.navigate('Drotaweryna');
  };

  handleIbuprofen = () => {
    const { navigation } = this.props;
    navigation.navigate('Ibuprofen');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          Oceń ból używając
          <Hypertekst onPress={this.handleSkalaBolu}>skali bólu.</Hypertekst>
        </MyTextView>
        <MyTextView>
          W przypadku bólu łagodnego (zwykle &lt;5 punktów) podaj doustnie
          <Hypertekst onPress={this.handleParacetamol}>Paracetamol</Hypertekst>
w dawce 15 mg/kg lub
          <Hypertekst onPress={this.handleIbuprofen}>Ibuprofen</Hypertekst>
          (u dzieci &gt;3 m.ż.) w dawce 10 mg/kg.
        </MyTextView>
        <MyTextView>
          Jeśli poziom bólu osiąga wartości 5 - 7 punktów podaj
          <Hypertekst onPress={this.handleParacetamol}>Paracetamol.</Hypertekst>
          <BoldText>Dawkowanie dożylne:</BoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci &lt;10 kg (1 r.ż):</BoldText>
          Pobierz z fiolki 7,5 mg/kg (tj. 0,75 ml/kg) i rozcieńcz w stosunku nie większym niż 1:10 w
          <Sol />
          lub 5% glukozie.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci &gt;10 kg:</BoldText>
          Podaj 15 mg/kg (tj. 1,5 ml/kg) z fiolki.
        </MyTextView>
        <MyTextView>Uwaga, przetaczać w przeciągu 15 min!</MyTextView>
        <MyTextView>
          <BoldText>lub</BoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleIbuprofen}>Ibuprofen</Hypertekst>
- 10 mg/kg p.o.
        </MyTextView>
        <MyTextView />
        <MyTextView>
          W przypadku braku poprawy lub gdy ból jest &gt; 7 punktów podaj
          <Hypertekst onPress={this.handleMorfina}>Morfinę</Hypertekst>
          0,1 mg/kg i.v.
        </MyTextView>
        <MyTextView>Morfiny nie podawaj w przypadku kolki wątrobowej.</MyTextView>
        <MyTextView>
          U dzieci &gt;12 r.ż. zamiast morfiny można podać
          <Hypertekst onPress={this.handleFentanyl}>Fentanyl</Hypertekst>
w dawce 1 - 3 &micro;g/kg
          iv
        </MyTextView>
        <MyTextView>
          W przypadku występowania stanów spastycznych mięśniówki gładkiej przewodu pokarmowego lub
          dróg moczowych dodatkowo podaj:
          <Hypertekst onPress={this.handleDrotaweryna}>Drotawerynę</Hypertekst>
w dawce 40 - 120
          mg/24 h.
        </MyTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Ból urazowy</BoldText>
        </MyTextView>
        <MyTextView>Postępowanie niefarmakologiczne: unieruchomienie złamań, chłodzenie oparzenia itp.</MyTextView>
        <MyTextView>
          Podaj
          <Hypertekst onPress={this.handleMorfina}>Morfinę</Hypertekst>
          w dawce 0,1 mg/kg iv
        </MyTextView>
        <MyTextView>lub</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleFentanyl}>Fentanyl</Hypertekst>
          1 - 3 &micro;g/kg iv.
        </MyTextView>
        <TiM />
        <VersionTkaczykZwalczanieBolu />
      </ScrollContainer>
    );
  }
}
