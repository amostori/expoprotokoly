import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView,
  Hypertekst,
  BoldText,
  Sol,
  TiM,
  VersionTkaczykZwalczanieBolu,
} from '../../components/TextViews';

export default class BolBrzuchaP extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleObjawyBrzuszne = () => {
    const { navigation } = this.props;
    navigation.navigate('ObjawyBrzuszne');
  };

  handleMorfina = () => {
    const { navigation } = this.props;
    navigation.navigate('MorfinaDzieci');
  };

  handleSkalaBolu = () => {
    const { navigation } = this.props;
    navigation.navigate('SkalaBolu');
  };

  handleParacetamol = () => {
    const { navigation } = this.props;
    navigation.navigate('ParacetamolDzieci');
  };

  handleFentanyl = () => {
    const { navigation } = this.props;
    navigation.navigate('FentanylDzieci');
  };

  handleDrotaweryna = () => {
    const { navigation } = this.props;
    navigation.navigate('Drotaweryna');
  };
  

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <Hypertekst onPress={this.handleObjawyBrzuszne}>Objawy patologiczne</Hypertekst>
        </MyTextView>
        <MyTextView>
          Oceń ból używając
          <Hypertekst onPress={this.handleSkalaBolu}>skali bólu.</Hypertekst>
          Jeśli ból wymaga zniesienia podaj
          <Hypertekst onPress={this.handleParacetamol}>Paracetamol.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <BoldText>Dawkowanie dożylne:</BoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci &lt;10 kg (1 r.ż):</BoldText>
          Pobierz z fiolki 7,5 mg/kg (tj. 0,75 ml/kg) i rozcieńcz w stosunku nie większym niż 1:10 w
          <Sol />
          lub 5% glukozie.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci &gt;10 kg:</BoldText>
          Podaj 15 mg/kg (tj. 1,5 ml/kg) z fiolki.
        </MyTextView>
        <MyTextView>
          <BoldText>Uwaga, przetaczać w przeciągu 15 min!</BoldText>
        </MyTextView>
        <MyTextView>
          W przypadku ostrego brzucha, zapalenia otrzewnej, silnej kolki nerkowej ból może nie
          ustąpić. W takim przypadku podaj
          <Hypertekst onPress={this.handleMorfina}>Morfinę</Hypertekst>
          0,1 mg/kg i.v.
        </MyTextView>
        <MyTextView>Morfiny nie podawaj w przypadku kolki wątrobowej.</MyTextView>
        <MyTextView>
          U dzieci &gt;12 r.ż. zamiast morfiny można podać
          <Hypertekst onPress={this.handleFentanyl}>Fentanyl</Hypertekst>
w dawce 1 - 3 &micro;g/kg
          iv
        </MyTextView>
        <MyTextView>
        W przypadku występowania stanów spastycznych mięśniówki gładkiej przewodu pokarmowego lub
          dróg moczowych dodatkowo podaj:
          <Hypertekst onPress={this.handleDrotaweryna}>Drotawerynę</Hypertekst>
          w dawce 40 - 120 mg/24 h.
        </MyTextView>
        <TiM />
        <VersionTkaczykZwalczanieBolu />
      </ScrollContainer>
    );
  }
}
