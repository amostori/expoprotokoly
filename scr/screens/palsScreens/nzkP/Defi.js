import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { MyTextView, Hypertekst, VersionERC } from '../../../components/TextViews';

export default class Defi extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAdrenalina = () => {
    const { navigation } = this.props;
    navigation.navigate('AdrenalinaWskazowki');
  };

  handleAmiodaron = () => {
    const { navigation } = this.props;
    navigation.navigate('AmiodaronWskazowki');
  };

  handleLignokaina = () => {
    const { navigation } = this.props;
    navigation.navigate('Lignokaina');
  };

  handle4H4T = () => {
    const { navigation } = this.props;
    navigation.navigate('Screen4H4T');
  };

  render() {
    return (
      <View>
        <MyTextView>Wykonaj defibrylację energią 4 J/kg.</MyTextView>
        <MyTextView>Kontynuuj resuscytację. Co 2 minuty ponownie oceniaj rytm.</MyTextView>
        <MyTextView>
          Po trzeciej defibrylacji podaj
          <Hypertekst onPress={this.handleAdrenalina}>Adrenalinę</Hypertekst>
          0,01 mg/kg i.v. lub i.o. oraz
          <Hypertekst onPress={this.handleAmiodaron}>Amiodaron</Hypertekst>
5 mg/kg rozcieńczone w 5%
          glukozie.
        </MyTextView>
        <MyTextView>
          Adrenalinę powtarzaj co drugą pętlę. Amiodaron możesz raz powtórzyć w dawce 5 mg/kg.
        </MyTextView>
        <MyTextView>
          W przypadku braku Amiodaronu lub gdy jest przeciwskazany jak w Torsade de Pointes podaj
          <Hypertekst onPress={this.handleLignokaina}>Lignokainę</Hypertekst>
1 mg/kg iv (0,1 ml/kg
          1% roztworu). Druga dawka to 0,5 mg/kg (połowa poprzedniej).
        </MyTextView>
        <MyTextView>
          Rozważ
          <Hypertekst onPress={this.handle4H4T}>4H i 4T</Hypertekst>
        </MyTextView>
        <VersionERC />
      </View>
    );
  }
}
