import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { MyTextView, Hypertekst, VersionERC } from '../../../components/TextViews';

export default class NoDefi extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAdrenalina = () => {
    const { navigation } = this.props;
    navigation.navigate('AdrenalinaWskazowki');
  };

  handle4H4T = () => {
    const { navigation } = this.props;
    navigation.navigate('Screen4H4T');
  };

  render() {
    return (
      <View>
        <MyTextView>
          Kontynuuj resuscytację. Jak najszybciej podaj
          <Hypertekst onPress={this.handleAdrenalina}>Adrenalinę</Hypertekst>
          0,01 mg/kg i.v. lub i.o.
        </MyTextView>
        <MyTextView>Powtarzaj ją co drugą pętlę.</MyTextView>
        <MyTextView>Co 2 minuty oceniaj rytm.</MyTextView>
        <MyTextView>
          Rozważ
          <Hypertekst onPress={this.handle4H4T}>4H i 4T</Hypertekst>
        </MyTextView>
        <MyTextView>
          Pamiętaj, że asystolia wymaga potwierdzenia przez sprawdzenie sprzętu, zmianę
          odprowadzenia, położenia łyżek i zwiększenie cechy.
        </MyTextView>
        <VersionERC />
      </View>
    );
  }
}
