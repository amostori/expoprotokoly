import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView,
  Hypertekst,
  ListaTextView,
  Sol,
  VersionTextView,
} from '../../components/TextViews';

export default class AstmaP extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAdrenalina = () => {
    const { navigation } = this.props;
    navigation.navigate('Adrenalina');
  };

  handleHydrokortyzon = () => {
    const { navigation } = this.props;
    navigation.navigate('Adrenalina');
  };

  handleSalbutamol = () => {
    const { navigation } = this.props;
    navigation.navigate('Salbutamol');
  };

  handleAnafilaksja = () => {
    const { navigation } = this.props;
    navigation.navigate('AnafilaksjaP');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <Hypertekst onPress={this.handleSalbutamol}>Salbutamol w nebulizacji</Hypertekst>
        </MyTextView>
        <ListaTextView>dziecko &lt;20 kg: 2,5 mg</ListaTextView>
        <ListaTextView>dziecko &gt;20 kg: 5 mg</ListaTextView>
        <ListaTextView>Można trzykrotnie powtórzyć co 20 min.</ListaTextView>
        <MyTextView />
        <MyTextView>
          Jeśli brak poprawy podaj
          <Hypertekst onPress={this.handleHydrokortyzon}>Hydrokortyzon</Hypertekst>
5 mg/kg i.v.
        </MyTextView>
        <MyTextView>lub</MyTextView>
        <ListaTextView>dzieci &lt;6 m.ż. 25 mg</ListaTextView>
        <ListaTextView>dzieci 6 m.ż. - 6 r.ż. 50 mg</ListaTextView>
        <ListaTextView>dzieci 6 r.ż. - 12 r.ż. 100 mg</ListaTextView>
        <ListaTextView>dzieci &gt;12 r.ż. 200 mg</ListaTextView>
        <MyTextView />
        <MyTextView>
          Jeśli nadal brak poprawy rozpocznij transport do szpitala i rozpocznij powolny wlew
          <Hypertekst onPress={this.handleMagnez}>Magnezu 20%</Hypertekst>
          25 - 50 mg/kg iv (0,2 ml/kg roztworu dodaj do 100 - 250 ml
          <Sol />
          tj. wagę pomnóż razy 2 i podziel przez 10), podawaj nie szybciej niż w ciągu 20 min.
        </MyTextView>
        <MyTextView>
          Jeśli przyczyną ataku może być reakcja anafilaktyczna podaj Adrenalinę w dawce jak w
          <Hypertekst onPress={this.handleAnafilaksja}>Anafilaksji.</Hypertekst>
        </MyTextView>
        <MyTextView>
          W przypadku niewydolności oddechowej i braku poprawy rozważ wezwanie karetki S lub LPR -
          może być konieczna intubacja.
        </MyTextView>
        <VersionTextView>
          Na podstawie Global Initiative for Asthma 2015 oraz wytycznych ERC 2015.
        </VersionTextView>
      </ScrollContainer>
    );
  }
}
