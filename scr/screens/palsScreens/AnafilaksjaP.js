import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView,
  Hypertekst,
  BoldText,
  VersionERC,
  ListaTextView,
  Sol,
} from '../../components/TextViews';

export default class AnafilaksjaP extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAdrenalina = () => {
    const { navigation } = this.props;
    navigation.navigate('Adrenalina');
  };

  handleHydrokortyzon = () => {
    const { navigation } = this.props;
    navigation.navigate('Hydrokortyzon');
  };

  handleSalbutamol = () => {
    const { navigation } = this.props;
    navigation.navigate('Salbutamol');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          W przypadku ciężkiej reakcji (wstrząs, obrzęk dróg oddechowych lub nasilona duszność)
          podaj
          <Hypertekst onPress={this.handleAdrenalina}>Adrenalinę</Hypertekst>
          domięśniowo (przednio - boczna powierzchnia uda). Powtarzaj co 5 - 10 min do uzyskania
          poprawy stanu pacjenta (rzadko potrzebne więcej niż 2 dawki).
        </MyTextView>
        <MyTextView>
          <BoldText>Dawkowanie wg wieku u dziecka:</BoldText>
        </MyTextView>
        <ListaTextView>0 - 6 r.ż. 150 &micro;g (0,15 ml)</ListaTextView>
        <ListaTextView>6 - 12 r.ż. 300 &micro;g (0,3 ml)</ListaTextView>
        <ListaTextView>&gt;12 r.ż. 500 &micro;g (0,5 ml)</ListaTextView>
        <MyTextView />
        <MyTextView>
          Jeśli objawy wstrząsu nie ustąpiły po Adrenalinie, przetocz
          <Sol />
          20 ml/kg.
        </MyTextView>
        <MyTextView>
          Podaj
          <Hypertekst onPress={this.handleHydrokortyzon}>Hydrokortyzon</Hypertekst>
5 mg/kg i.v.
        </MyTextView>
        <MyTextView>lub</MyTextView>
        <ListaTextView>dzieci &lt;6 m.ż. 25 mg</ListaTextView>
        <ListaTextView>dzieci 6 m.ż. - 6 r.ż. 50 mg</ListaTextView>
        <ListaTextView>dzieci 6 r.ż. - 12 r.ż. 100 mg</ListaTextView>
        <ListaTextView>dzieci &gt;12 r.ż. 200 mg</ListaTextView>
        <MyTextView />
        <MyTextView>
          Skurcz oskrzeli lecz nebulizacją z
          <Hypertekst onPress={this.handleSalbutamol}>Salbutamolu</Hypertekst>
          2,5 mg dziecko &lt;20 kg, 5 mg dziecko &gt;20 kg.
        </MyTextView>
        <MyTextView>
          W przypadku obrzęku języka, ust i chrypki oraz braku poprawy po leczeniu rozważ wezwanie
          karetki S lub LPR na spotkanie. Może być konieczna intubacja.
        </MyTextView>
        <VersionERC />
      </ScrollContainer>
    );
  }
}
