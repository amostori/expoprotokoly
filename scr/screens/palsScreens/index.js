import NzkP from './nzkP/NzkP';
import Swiezorodek from './swiezorodek/Swiezorodek';
import BradykardiaP from './BradykardiaP';
import CzestoskurczP from './czestoskurczP/CzestoskurczP';
import AnafilaksjaP from './AnafilaksjaP';
import AstmaP from './AstmaP';
import ZapalenieKrtani from './ZapalenieKrtani';
import BolBrzuchaP from './BolBrzuchaP';
import HipoglikemiaP from './HipoglikemiaP';
import DrgawkiP from './drgawkiP/DrgawkiP';
import Goraczka from './goraczka/Goraczka';
import ZwalczanieBoluP from './ZwalczanieBoluP';

export {
  NzkP,
  Swiezorodek,
  BradykardiaP,
  CzestoskurczP,
  AnafilaksjaP,
  AstmaP,
  ZapalenieKrtani,
  BolBrzuchaP,
  HipoglikemiaP,
  DrgawkiP,
  Goraczka,
  ZwalczanieBoluP,
};
