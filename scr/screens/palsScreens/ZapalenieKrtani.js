import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView, Hypertekst, VersionTkaczyk, BoldText,
} from '../../components/TextViews';

export default class ZapalenieKrtani extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAdrenalina = () => {
    const { navigation } = this.props;
    navigation.navigate('Adrenalina');
  };

  handleWestley = () => {
    const { navigation } = this.props;
    navigation.navigate('Westley');
  };

  handleDeksametazon = () => {
    const { navigation } = this.props;
    navigation.navigate('Dexaven');
  };

  handleBudezonid = () => {
    const { navigation } = this.props;
    navigation.navigate('Budezonid');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>Objawy: szczekający kaszel, świst wdechowy, utrudniony wdech.</MyTextView>
        <MyTextView>
          Unikaj niepokojenia dziecka. Wykonuj tylko absolutnie konieczne zabiegi i badania.
        </MyTextView>
        <MyTextView>
          Oceń duszność w skali
          <Hypertekst onPress={this.handleWestley}>Westleya</Hypertekst>
        </MyTextView>
        <MyTextView>
          <BoldText>&lt;4 punkty</BoldText>
        </MyTextView>
        <MyTextView>
          Stan kliniczny dobry, podaj
          <Hypertekst onPress={this.handleBudezonid}>Budezonid</Hypertekst>
2 mg w nebulizacji.
        </MyTextView>
        <MyTextView>
          <BoldText>4 - 6 punktów</BoldText>
        </MyTextView>
        <MyTextView>
          Stan umiarkowany. Podaj
          <Hypertekst onPress={this.handleAdrenalina}>Adrenalinę</Hypertekst>
5 mg w nebulizacji
          następnie Budezonid 2 mg w nebulizacji.
        </MyTextView>
        <MyTextView>
          <BoldText>&gt;6 punktów</BoldText>
        </MyTextView>
        <MyTextView>
          Stan ciężki - może wymagać intubacji. Podaj Adrenalinę 5 mg w nebulizacji,
          <Hypertekst onPress={this.handleDeksametazon}>Deksametazon</Hypertekst>
          0,15 - 0,6 mg/kg iv (zwykle 4 mg).
        </MyTextView>
        <MyTextView>
          Rozpocznij transport do szpitala, a w razie niewydolności oddechowej rozważ wezwanie
          karetki S lub LPR na spotkanie.
        </MyTextView>
        <VersionTkaczyk />
      </ScrollContainer>
    );
  }
}
