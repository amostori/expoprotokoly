import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Linking } from 'react-native';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView, Hypertekst, BoldText, VersionERC,
} from '../../components/TextViews';

export default class Bradykardia extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAdrenalina = () => {
    const { navigation } = this.props;
    navigation.navigate('AdrenalinaWskazowki');
  };

  handleAtropina = () => {
    const { navigation } = this.props;
    navigation.navigate('AtropinaWskazowki');
  };

  handleStymulacja = () => {
    Linking.openURL('https://www.youtube.com/watch?v=1fjmyog37Fo');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>Podaj 100% tlen i zapewnij dobrą wentylację.</MyTextView>
        <MyTextView>
          Jeśli bradykardia spowodowana jest pobudzeniem nerwu błędnego lub zespołem chorej zatoki
          (np. blok serca) podaj
          <Hypertekst onPress={this.handleAtropina}>Atropinę</Hypertekst>
          0,02 mg/kg (minimum 0,1 mg) i.v. lub i.o.
        </MyTextView>
        <MyTextView>
          Jeśli brak poprawy rozpocznij
          <Hypertekst onPress={this.handleStymulacja}>stymulację serca</Hypertekst>
        </MyTextView>
        <MyTextView>
          <BoldText>
            Jeśli u dziecka z objawami złej perfuzji częstość rytmu serca wynosi &lt;60/min i nie
            przyspieszy pomimo wentylacji oraz tlenoterapii rozpocznij uciskanie klatki piersiowej i
            podaj
            <Hypertekst onPress={this.handleAdrenalina}>Adrenalinę</Hypertekst>
            0,01 mg/kg i.v. lub i.o.
          </BoldText>
        </MyTextView>
        <VersionERC />
      </ScrollContainer>
    );
  }
}
