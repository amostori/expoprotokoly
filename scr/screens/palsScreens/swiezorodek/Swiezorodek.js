import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ScrollContainer } from '../../../components/container';
import {
  MyTextView, AskTextView, VersionERC, Hypertekst,
} from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import No1 from './No1';

export default class Swiezorodek extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleApgar = () => {
    const { navigation } = this.props;
    navigation.navigate('Apgar');
  };

  handleSaturacja = () => {
    const { navigation } = this.props;
    navigation.navigate('SaturacjaSwiezorodka');
  };

  render() {
    const { navigation } = this.props;
    const { divy } = this.state;

    const AllIsOk = (
      <View>
        <MyTextView>
          Zapewnij dziecku ciepło, nie wcześniej niż po minucie od porodu przetnij i zabezpiecz
          pępowinę.
        </MyTextView>
        <MyTextView>Przekaż dziecko matce, okryj oboje i monitoruj ich stan.</MyTextView>
        <MyTextView>
          Zachęć mamę do podjęcia próby karmienia piersią co powoduje wyrzut oksytocyny.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleApgar}>Skala Apgar</Hypertekst>
        </MyTextView>
        <VersionERC />
      </View>
    );
    return (
      <ScrollContainer>
        <MyTextView>
          Osusz, okryj, oceń oddech i czynność serca (przez osłuchanie tonów serca), podłącz
          pulsoksymetr, monitor EKG.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleSaturacja}>Saturacja u świeżorodka</Hypertekst>
        </MyTextView>
        <AskTextView>Oddech prawidłowy i akcja serca &gt;100/min?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(AllIsOk)}
          onPressNo={() => this.handleOnClick(<No1 navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
