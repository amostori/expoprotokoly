import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import {
  MyTextView,
  AskTextView,
  VersionERC,
  ListaTextView,
  BoldText,
  Hypertekst,
} from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import NieWzrasta from './NieWzrasta';

export default class No1 extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleApgar = () => {
    const { navigation } = this.props;
    navigation.navigate('Apgar');
  };

  render() {
    const { navigation } = this.props;
    const { divy } = this.state;

    const Wzrasta = (
      <View>
        <MyTextView>
          Jeśli oddech jest wydolny przekaż dziecko matce, okryj oboje i monitoruj ich stan.
        </MyTextView>
        <MyTextView>
          <BoldText>Zapewnij dziecku ciepło</BoldText>
        </MyTextView>
        <MyTextView>
          Jeśli oddech jest niewydolny kontynuuj wentylację z częstością 30/min do czasu pojawienia
          się prawidłowego oddechu i akcji serca &gt;100/min.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleApgar}>Skala Apgar</Hypertekst>
        </MyTextView>
        <VersionERC />
      </View>
    );
    return (
      <View>
        <MyTextView>
          Wykonaj 5 wdmuchnięć z dodatnim ciśnieniem (utrzymaj wdech na 2 - 3 sekundy).
        </MyTextView>
        <MyTextView>Wentyluj powietrzem atmosferycznym bez suplementacji tlenem.</MyTextView>
        <ListaTextView>W międzyczasie zleć przecięcie i zabezpieczenie pępowiny.</ListaTextView>
        <MyTextView />
        <AskTextView>Akcje serca wzrasta?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Wzrasta)}
          onPressNo={() => this.handleOnClick(<NieWzrasta navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
