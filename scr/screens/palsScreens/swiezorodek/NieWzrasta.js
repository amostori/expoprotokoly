import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import {
  MyTextView, AskTextView, VersionERC, Hypertekst,
} from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import NieUnosi from './NieUnosi';

export default class NieWzrasta extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleApgar = () => {
    const { navigation } = this.props;
    navigation.navigate('Apgar');
  };

  handleAdrenalinaSwiezorodek = () => {
    const { navigation } = this.props;
    navigation.navigate('AdrenalinaWskazowki');
  };

  render() {
    const { navigation } = this.props;
    const { divy } = this.state;

    const Unosi = (
      <View>
        <MyTextView>
          Jeśli akcja serca jest poniżej 60/min uciskaj klatkę piersiową i wentyluj w stosunku 3
          uciśnięcia na 1 wdech.
        </MyTextView>
        <MyTextView>
          Po 30 sekundach oceń akcję serca. Jeśli pozostaje poniżej 60/min podaj
          <Hypertekst onPress={this.handleAdrenalinaSwiezorodek}>Adrenalinę</Hypertekst>
          0,01 mg/kg i.v. lub i.o.
        </MyTextView>
        <MyTextView>Oceniaj stan dziecka co 30 sekund. Adrenalinę podawaj co 3 - 5 min.</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleApgar}>Skala Apgar</Hypertekst>
        </MyTextView>
        <VersionERC />
      </View>
    );
    return (
      <View>
        <MyTextView>Oceń prawidłowość wentylacji.</MyTextView>
        <AskTextView>Klatka piersiowa się unosi?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Unosi)}
          onPressNo={() => this.handleOnClick(<NieUnosi navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
