import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Metoprolol extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Selektywny betabloker (receptory beta1). Zwalnia akcję serca, obniża ciśnienie tętnicze
          krwi.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zawał serca - wczesne podanie zmniejsza śmiertelność, kontrola częstości akcji serca w
          przypadku migotania przedsionków z szybką odpowiedzią komór.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zdekompensowana niewydolność krążenia, obrzęk płuc, wstrząs kardiogenny, bradykardia (blok
          serca, zespół chorej zatoki). Zawał serca z ciśnieniem &lt;100 mmHg, powikłany
          bradykardią, blokiem serca, znaczącą bradykardią i ciężką niewydolnością krążenia. Nie
          wolno stosować u pacjentów leczonych blokerami kanału wapniowego (werapamil, diltiazem).
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Ze względu na brak odpowiednich badań stosować w ciąży tylko w razie absolutnej
          konieczności. Zachować szczególną ostrożność w czasie karmienia piersią -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 5 mg w 5 ml (1 mg/ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>5 mg powoli iv. Można powtórzyć po 2 minutach dwukrotnie.</MyTextView>
        <MyTextView>
          Leczenie częstoskurczu: 5 mg iv z szybkością 1 mg/1 min. Można powtarzać co 5 min po 5 mg
          do max dawki całkowitej 20 mg.
        </MyTextView>
        <MyTextView>
          Uwaga, monitorować EKG i za każdym razem przed ponownym podaniem leku sprawdzić stan
          pacjenta (RR, akcja serca, zapis EKG). Wstrzymać się z następną dawką jeśli RR &lt;90
          mmHg, akcja serca &lt;40/min lub odcinek PQ dłuższy niż 0,26 s.
        </MyTextView>
        <MyTextView>U dzieci doświadczenie z podawaniem jest niewielkie.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: natychmiastowy</ListaTextView>
        <ListaTextView>Szczyt działania: 20 min</ListaTextView>
        <ListaTextView>Czas działania: 5 - 8 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Objawy niepożądane są zwykle łagodne i przemijające: znaczne obniżenie ciśnienia, zawroty
          i bóle głowy, bradykardia, kołatanie serca, nudności, wymioty, zaburzenia rytmu serca.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Leczenie objawowe i podtrzymujące.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Preparat Betaloc (5 mg/5 ml). Jednoczesne stosowanie z amfetaminą, kokainą, efedryną,
          epinefryną, norepinefryną, lub pseudoefedryną może prowadzić do wzrostu ciśnienia
          tętniczego krwi i bradykardii. W przypadku przedawkowania postępowanie obejmuje podawanie
          adrenaliny, atropiny, dopaminy.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Metoprolol;
