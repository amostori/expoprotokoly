import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Fentanyl extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek przeciwbólowy z grupy opioidów. Około 100 razy silniejszy od morfiny.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Znieczulenie przed zabiegami np. kardiowersja, stymulacja.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Unikać stosowania u pacjenta zażywającego inhibitory MAO w ciągu ostatnich 14 dni - ryzyko
          zgonu. Po podaniu większej dawki (&gt;5 &micro;g/kg) może dojść do sztywności klatki
          piersiowej, którą należy opanować podając nalokson. Może być konieczne podanie leków
          zwiotczających.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Można stosować w ciąży wyłącznie w razie zdecydowanej konieczności -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C.</Hypertekst>
          Po podaniu jednorazowym przerwać karmienie na 24 h.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 0,1 mg w 2 ml (0,05 mg/ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>Zwykle 0,1 mg iv</MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>1 - 3 &micro;g/kg powoli i.v. przez około 1 - 3 min.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 1 - 2 min</ListaTextView>
        <ListaTextView>Szczyt działania: 3 - 5 min.</ListaTextView>
        <ListaTextView>Czas działania: 0,5 - 1 h.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Domięśniowo</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 7 - 15 min</ListaTextView>
        <ListaTextView>Szczyt działania: 20 - 30 min.</ListaTextView>
        <ListaTextView>Czas działania: 1 - 2 h.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>Depresja układu oddechowego, sztywność klatki piersiowej.</MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Nalokson</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Sok z grejpfruta jest inhibitorem CYP3A4 i dlatego nasila działanie fentanylu, zwiększając
          ryzyko depresji oddechowej.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Fentanyl;
