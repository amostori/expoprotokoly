import React from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView, RedBoldText, Hypertekst, VersionDrug,
} from '../../components/TextViews';

class Morfina extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Narkotyczny lek przeciwbólowy, działanie uspokajające, zmniejszające obciążenie serca
          przez rozszerzenie naczyń krwionośnych (wykorzystywane w obrzęku płuc). Uwaga, powoduje
          depresję układu oddechowego.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wkazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Silny ból, obrzęk płuc, analgezja w drobnych zabiegach (np. kardiowersja).
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Uraz wielonarządowy, podwyższone ciśnienie wewnątrzczaszkowe, porfiria, okres karmienia
          piersią.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria C.</Hypertekst>
          Lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          niepożądanego działania u płodu.
          {' '}
          {'\n'}
          {' '}
W okresie okołoporodowym kategoria D.
          {' '}
          {'\n'}
          Nie stosować w okresie karmienia piersią.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 10 mg w 1 ml lub 20 mg w 1 ml</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          dzieci
          {' '}
          {'\n'}
          {' '}
0,1 mg/kg iv
          {' '}
          {'\n'}
          {' '}
          {'\n'}
          {' '}
dorośli
          {' '}
          {'\n'}
          {' '}
          {'\n'}
          {' '}
2 - 5 mg iv następnie dawki
          frakcjonowane po 2 mg do ustąpienia bólu
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          Dożylnie
          {' '}
          {'\n'}
          {' '}
          {'\n'}
          {' '}
Początek działania: 2 - 3 min
          {' '}
          {'\n'}
          {' '}
Szczyt działania: 30 min
          {' '}
          {'\n'}
          Czas działania: 4 - 5 h
          {' '}
          {'\n'}
          {' '}
          {'\n'}
          {' '}
Domięśniowo
          {' '}
          {'\n'}
          {' '}
          {'\n'}
          {' '}
Początek działania: 10 -
          30 min
          {' '}
          {'\n'}
          {' '}
Szczyt działania: 30 - 60 min
          {' '}
          {'\n'}
          {' '}
Czas działania: 4 - 5 h
          {' '}
          {'\n'}
          {' '}
          {'\n'}
          Podskórnie
          {' '}
          {'\n'}
          {' '}
          {'\n'}
          {' '}
Początek działania: 20 min
          {' '}
          {'\n'}
          {' '}
Szczyt działania: 50 - 90 min
          {' '}
          {'\n'}
          {' '}
Czas działania: 4 - 5 h
        </MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Bradykardia, spadek ciśnienia tętniczego, zwiększenie napięcia zwieracza Oddiego, wymioty,
          nudności, zaparcia, zaburzenie sprawności psychicznej, trudności w oddawaniu moczu, objawy
          psychotyczne, nadmierna senność.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Nalokson</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          W razie przedawkowania można podać nalokson, ale działa krócej (ok 45 min) niż sama
          morfina(ok 4h).
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Morfina;
