import React from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView, RedBoldText, Hypertekst, VersionDrug,
} from '../../components/TextViews';

class Nitrogliceryna extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Rozszerzenie naczyń wieńcowych, poszerzenie łożyska naczyniowego, obniżenie ciśnienia
          tętniczego krwi.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Ostry zespół wieńcowy z wykluczeniem zawału ściany dolnej i prawej. Ostra niewydolność
          lewokomorowa.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zażycie inhibitorów fosfodiesterazy stosowanych w leczeniu zaburzeń wzwodu (syldenafil,
          wardenafil i tadalafil np. Viagra) w ciągu ostatnich 24h,
          {' '}
          {'\n'}
          zawał ściany dolnej i prawej komory,
          {'\n'}
          RR &lt; 90 mmHg,
          {'\n'}
          bradykardia lub częstoskurcz.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria B/C.</Hypertekst>
          Lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          niepożądanego działania u płodu.
          {'\n'}
          Ostrożnie stosować w okresie karmienia piersią.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Aerozol: 0,4 mg/dawkę</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>0,4 mg sl</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          Aerozol
          {'\n'}
          {'\n'}
          Początek działania: 1 min
          {'\n'}
          Szczyt działania: nieznany
          {'\n'}
          Czas działania: do 2 h
        </MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożdane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zaczerwienienie skóry, zawroty i bóle głowy, nudności i wymioty, odczyny alergiczne,
          spadek ciśnienia tętniczego, osłabienie, nadmierna potliwość.
          {'\n'}
          {'\n'}
          Przedawkowanie: spadek ciśnienia tętniczego, odruchowa tachykardia.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak. Wlew NaCl może przywrócić ciśnienie tętnicze.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Wcześniej dostępna była postać w tabletkach, obecnie występuje w postaci aerozolu. Uwaga,
          nie wolno wstrząsać buteleczki przed użyciem ponieważ może to spowodować podanie
          wielokrotnie większej dawki.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Nitrogliceryna;
