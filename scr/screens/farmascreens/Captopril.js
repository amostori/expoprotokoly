import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Captopril extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Hamuje działanie enzymu konwertującego angiotensynę I do angiotensyny II co pośrednio
          obniża ciśnienie krwi.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Podwyższone ciśnienie tętnicze krwi.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Stan po przeszczepie nerki, ciąża, obrzęk naczynioruchowy.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Nie stosować w okresie ciąży i karmienia piersią.
          <Hypertekst onPress={this.handleKodCiazy}>kategoria D.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Tabletki: 12,5 mg, 25 mg lub 50 mg.</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>12,5 mg p.o.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Doustnie:</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 15 - 16 min</ListaTextView>
        <ListaTextView>Szczyt działania: 60 - 90 min</ListaTextView>
        <ListaTextView>Czas działania: 6 - 12 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zaburzenia smaku, zawroty głowy, bóle głowy i parestezje, tachykardia lub tachyarytmia,
          dławica piersiowa, kołatanie serca, suchy, uporczywy kaszel, nudności, wymioty, ból w
          klatce piersiowej.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          U pacjentów rasy czarnej mniej skuteczny w obniżaniu ciśnienia krwi niż u pacjentów rasy
          białej. Dodatkowo istnieje zwiększone ryzyko obrzęku naczynioruchowego. Nieskuteczność
          wynika z genetycznej oporności na inhibitory ACE.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Captopril;
