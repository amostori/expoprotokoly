import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Magnez extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Zmniejsza napięcie mięśniowe i działa przeciwdrgawkowo.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Częstoskurcz komorowy, wielokształtny (Torsade de Pointes), rzucawka porodowa, astma,
          zatrucie digoksyną.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Upośledzenie nerek.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek bezpieczny w ciąży.
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria A.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 2 g w 10 ml (200 mg/ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>2 - 4 g iv we wlewie w 5% glukozie lub Optilyte</MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>
          25-50 mg/kg we wlewie iv - czyli 0,2 ml/kg roztworu 20% dodaj do kroplówki (w 5% glukozie
          lub Optilyte) i podaj we wlewie.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: szybki</ListaTextView>
        <ListaTextView>Szczyt działania: nieznany</ListaTextView>
        <ListaTextView>Czas działania: 30 min</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Osłabienie mięśni, zaburzenia OUN, senność, śpiączka, zmniejszenie odruchów, niedowład,
          potliwość, zaburzenia rytmu serca, bradykardia, niedociśnienie, depresja oddechowa,
          zaburzenia czynności nerek, zaczerwienienie twarzy, hipotermia.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Sole wapnia i.v. oraz sztuczna wentylacja płuc.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Najlepiej rozpuszczać w Optilyte badz 5% glukozie, bo z NaCl moze powstać niezgodność. Po
          szybkim podaniu dożylnym powoduje nieprzyjemne uczucie gorąca dlatego należy podawać go
          powoli.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Magnez;
