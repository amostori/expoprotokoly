import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Hydrokortyzon extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Przeciwzapalne, przeciwobrzękowe.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Wstrząs anafilaktyczny, astma, POCHP.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Astma aspirynowa. Nie podawaj w szybkim wstrzyknięciu.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          dla płodu
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka z rozpuszczalnikiem po 25 mg lub 100 mg.</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>200 mg iv</MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>Astma: 5 mg/kg</MyTextView>
        <MyTextView>Anafilaksja - zależności od wieku:</MyTextView>
        <ListaTextView>0 - 6 r.ż. 50 mg iv</ListaTextView>
        <ListaTextView>6 - 12 r.ż. 100 mg iv</ListaTextView>
        <ListaTextView>&gt;12 r.ż. 200 mg iv</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <MyTextView>Efekt działania pojawi się po 4 - 6 h.</MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>Przy okazjonalnym podawaniu zwykle brak.</MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          U pacjentów z astmą aspirynową zastosowanie tego leku może spowodować paradoksalne
          nasilenie dolegliwości.
        </MyTextView>
        <MyTextView>
          Należy pamiętać, że rozpuszczalnik wstrzykujemy jedną strzykawką, a gotowy lek naciągamy i
          podajemy drugą. Minimalizujemy przez to możliwość kontaminacji roztworu i ryzyko
          wystąpienia powikłań septycznych.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Hydrokortyzon;
