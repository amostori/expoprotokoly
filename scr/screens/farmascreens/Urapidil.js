import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  Hypertekst,
} from '../../components/TextViews';

class Urapidil extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek obniżający ciśnienie tętnicze krwi - antagonista receptora adrenergicznego alfa.
          Hamuje zwężające naczynia krwionośne działanie katecholamin (działanie obwodowe),
          zapobiega odruchowemu pobudzeniu lub hamowaniu układu współczulnego (działanie ośrodkowe).
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Stany nagłe w przebiegu nadciśnienia tętniczego np. przełom nadciśnieniowy.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          U pacjentów ze zwężeniem cieśni aorty lub przetoką tętniczo - żylną (z wyjątkiem
          nieczynnej hemodynamicznie przetoki do dializy).
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek bezpieczny, nigdy nie badany w ciąży (bezpieczne jest jednorazowe podanie).
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria B.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 25 mg w 5 ml (5 mg/ml) lub 50 mg w 10 ml (5 mg/ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>
          W pozycji leżącej: 10 - 50 mg powoli iv - zacznij od 10 mg (ampułka zawiera 25 mg w 5 ml).
          W razie potrzeby dawkę można powtórzyć po 5 min.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>Brak doświadzeń w stosowaniu u dzieci (stosować jedynie we wlewie).</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>Efekt pojawia się po około 1 - 5 min. Utrzymuje się do 1 - 2 h.</MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zawroty i bóle głowy, nudności. Kołatanie serca, tachy lub bradykardia, ucisk za mostkiem,
          duszność, wymioty. W przypadku przedawkowania: omdlenie ortostatyczne, zmniejszona
          szybkość reakcji.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Preparat Ebrantil (25 mg/5 ml). W trakcie dożylnego podawania pacjent musi znajdować się w
          pozycji leżącej bo właśnie wtedy lek najsilniej działa. Nie zostawiaj pacjenta w domu
          jeśli podałeś Urapidil. Pacjent powinien być pod obserwacją przez dłuższy czas ponieważ
          jego działanie może być z jednej strony bardzo silne, a z drugiej może być konieczne
          powtórzenie dawki, a nawet wlew.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Urapidil;
