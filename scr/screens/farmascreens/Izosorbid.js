import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Izosorbid extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek o silnym działaniu rozszerzającym naczynia krwionośne. Zmniejsza obciążenie lewej
          komory serca oraz zapotrzebowanie m. sercowego na tlen co poprawia przepływ krwi i
          utlenowanie mięśnia. Prowadzi to do przywrócenia równowagi tlenowej m. sercowego i
          zmniejszenia dolegliwości dławicowych.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Leczenie dławicy piersiowej i zapobieganie jej napadom.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Ostre zaburzenia krążenia (wstrząs, zapaść naczyniowa), krwotok mózgowy, uraz głowy, ostry
          zawał mięśnia sercowego z niskim ciśnieniem (w tym świeży zawał ściany dolnej), ciśnienie
          skurczowe poniżej 90 mmHg, stosowanie inhibitorów 5-fosfodiesterazy (sildenafil,
          tadalafil, wardenafil) w przeciągu ostatnich 24 h.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          dla płodu. Nie stosować w czasie porodu.
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Tabletki po 10, 20, 40, 50, 60, 75 lub 100 mg</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>10 - 40 mg p.o.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Doustnie:</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 30 - 60 min.</ListaTextView>
        <ListaTextView>Szczyt działania: nieznany</ListaTextView>
        <ListaTextView>Czas działania: 7 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>Bóle głowy, napadowa tachykardia, spadek ciśnienia.</MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Leczenie objawowe i podtrzymujące.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Preparat Mononit 10 (10 mg/1 tabletka), Effox 10. Inne nazwy: monoazotan izosorbidu,
          isosorbidi mononitras, isosorbide mononitrate (ang.)
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Izosorbid;
