import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Atropina extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Blokowanie nerwu błędnego, przyspieszenie akcji serca, rozszerzenie źrenic, zmniejszenie
          sekrecji w układzie oddechowym.
        </MyTextView>
        <MyTextView>
          W stanach spastycznych mięśniówki gładkiej w jamie brzusznej działa rozkurczowo.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Bradykardia objawowa, zatrzymanie krążenia w mechaniźmie asystolii gdy przyczyną jest
          choroba węzła zatokowego, kolka żółciowa, nerkowa, zatrucie związkami fosfoorganicznymi
          (środki ochrony roślin, gazy bojowe).
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>W stanach zagrożenia życia brak.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Można stosować w ciąży wyłącznie w razie zdecydowanej konieczności -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C.</Hypertekst>
          Brak danych dotyczących szkodliwości atropina w okresie karmienia.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 0,5 mg w 1 ml lub 1 mg w 1 ml</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>
          asystolia 3 mg iv, bradykardia 0,5 mg powtarzane do całkowitej dawki 3 mg, zatrucie 2 - 5
          mg iv powtarzane co 3 - 15 min do osiągnięcia efektu atropinizacji.
        </MyTextView>
        <MyTextView>
          Pomocniczo w stanach spastycznych mięśniówki gładkiej w jamie brzusznej 0,5 - 1 mg im lub
          iv.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>
          asystolia 0,02 mg/kg, bradykardia 0,02 mg/kg, zatrucie 0,02 mg/kg powtarzane co 3 - 15 min
          do osiągnięcia efektu atropinizacji.
        </MyTextView>
        <MyTextView>
          <BoldText>Uwaga! Minimalna dawka 0,1 mg.</BoldText>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: szybki</ListaTextView>
        <ListaTextView>Szczyt działania: 2 - 4 min.</ListaTextView>
        <ListaTextView>Czas działania: 4 - 6 h.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Przyspieszenie akcji serca, zaburzenia rytmu serca, wzrost ciśnienia tętniczego krwi.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Fizostygmina w dawce 1 - 4 mg.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>Dawka poniżej 0,1 mg może wywołać paradoksalną bradykardię.</MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Atropina;
