import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Lignokaina extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Lek przeciwarytmiczny.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Częstoskurcz komorowy, migotanie komór gdy niedostępny jest amiodaron lub gdy jest
          przeciwwskazany, np. Torsade de Pointes, zatrucie trójcyklicznymi lekami
          przeciwdepresyjnymi.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Wstrząs kardiogenny i hipowolemiczny.</MyTextView>
        <MyTextView>Nie używaj gdy podano już Amiodaron.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          niepożądanego działania u płodu -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria B/C.</Hypertekst>
          Ostrożnie w czasie karmienia piersią.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Fiolka 1 %: 10 mg w 1 ml</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>
          1 mg/kg iv czyli 0,01 ml/kg 1% roztworu. Zwykle podaje się 10 ml 1% roztworu.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>
          1 mg/kg iv czyli 0,01 ml/kg 1% roztworu. W 1 ml dawka na 10 kg wagi ciała.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: natychmiastowy</ListaTextView>
        <ListaTextView>Szczyt działania: natychmiastowy</ListaTextView>
        <ListaTextView>Czas działania: 10 - 20 min</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zawroty głowy, drętwienie języka, oszołomienie, zaburzenia widzenia, drgawki, senność,
          utrata przytomności, zatrzymanie oddechu, depresja m. sercowego, zmniejszenie rzutu,
          zaburzenia przewodzenia, hipotensja, bradykardia, ekstrasystolie komorowe, migotanie
          komór, zatrzymanie krążenia.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>Nie używaj gdy podano już Amiodaron.</MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Lignokaina;
