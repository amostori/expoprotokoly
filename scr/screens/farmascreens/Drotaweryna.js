import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  Hypertekst,
  Sol,
} from '../../components/TextViews';

class Drotaweryna extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Rozkurczowe na mięśnie gładkie.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Stany skurczowe mięśni gładkich: kamica nerkowa, kamica pęcherzyka żółciowego, bolesne
          miesiączki.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Ciężka niewydolność krążenia.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek można stosować gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          niepożądanego działania u płodu -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C</Hypertekst>
          Nie zaleca się stosować w okresie karmienia piersią.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 40 mg w 2 ml (20 mg/ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>
          40 - 80 mg w 20 ml
          <Sol />
          powoli i.v.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>Biodostępność trudna do określenia. Okres półtrwania 7 - 12 h.</MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Nieregularna akcja serca, bóle i zawroty głowy, kołatanie serca, nudności, spadek
          ciśnienia tętniczego.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Szybkie podanie powoduje spadek ciśnienia tętniczego i zawroty głowy.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Drotaweryna;
