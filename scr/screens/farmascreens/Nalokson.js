import React from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView, RedBoldText, Hypertekst, VersionDrug,
} from '../../components/TextViews';

class Nalokson extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Odwrócenie działania opioidów, odtrutka na morfinę.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Zatrucie morfiną i innymi opioidami.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>W stanach zagrożenia życia brak.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria B.</Hypertekst>
W ciąży stosować
          jedynie w razie zdecydowanej konieczności. Zachować ostrożność w okresie karmienia
          piersią.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 0,4 mg w 1 ml</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Dorośli: 0,4 mg iv powtarzać co 2 min do maksymalnej dawki 2 mg
          {'\n'}
          {'\n'}
          Dzieci 0,01 - 0,04 mg/kg.
          {'\n'}
          {'\n'}
          {' '}
W praktyce rozcieńcz 1 ampułkę (0,4 mg) do 4 ml soli fizjologicznej i podaj 0,1
          ml/kg (0,01 mg/kg).
          {'\n'}
          {'\n'}
W przypadku przedawkowania opioidów i wystąpienia depresji oddechowej podawaj co
          minutę dawkę 0,005 mg/kg do momentu poprawy oddychania. Dzięki zastosowaniu tak małych
          dawek uniknąć można zniesienia działania przeciwbólowego.
          {'\n'}
          {'\n'}
          {' '}
U świeżorodka z depresją oddechową spowodowaną stosowaniem opioidów przez matkę
          dawka naloksonu wynosi 0,1 mg/kg (rozcieńczyć 1 ampułka 0,4 mg do 4 ml i podać 1 ml/kg).
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          Dożylnie
          {'\n'}
          {'\n'}
          {' '}
Początek działania: 1 - 2 min
          {'\n'}
          {' '}
Szczyt działania: nieznany
          {'\n'}
          Czas działania: 45 min
          {'\n'}
          {'\n'}
          {' '}
Domięśniowo
          {'\n'}
          {'\n'}
          {' '}
Początek działania: 2 - 5 min
          {'\n'}
          Szczyt działania: nieznany min
          {'\n'}
          {' '}
Czas działania: &gt;45 min
        </MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Nudności, wymioty, tachykardia, wzrost ciśnienia krwi, ostra lewokomorowa niewydolność
          serca, obrzęk płuc, zaburzenia rytmu serca do migotania komór włącznie, pobudzenie po
          odwróceniu analgetycznego działania opioidów.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutki</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>Można podać donosowo.</MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}
export default Nalokson;
