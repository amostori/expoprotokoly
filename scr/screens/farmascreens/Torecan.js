import React from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView, RedBoldText, Hypertekst, VersionDrug,
} from '../../components/TextViews';

class Toceran extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Działanie przeciwwymiotne.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Nudności i wymioty po zabiegach chirurgicznych, chemioterapii przeciwnowotworowej,
          radioterapii lub leczeniu lekami o działaniu wymiotnym, (opiaty, alkaloidy sporyszu,
          teofilina), urazach czaszki i mózgu, w nadciśnieniu wewnątrzczaszkowym, migrenie, w
          mocznicy oraz zaburzeniach żołądkowo-jelitowych, wątroby i dróg żółciowych. Preparat
          Torecan stosuje się w leczeniu zawrotów głowy w chorobie Meniere&apos;a i innych
          zaburzeniach błędnikowych.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Ciężka depresja ośrodkowego układu nerwowego lub zaburzenia świadomości oraz klinicznie
          istotna hipotensja.
          {' '}
          {'\n'}
          {'\n'}
          Dzieci poniżej 15 r.ż.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria C:</Hypertekst>
          Lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          niepożądanego działania u płodu.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>
          Czopki: 6,5 mg
          {'\n'}
          {'\n'}
          Tabletki: 6,5 mg
          {'\n'}
          {'\n'}
          Ampułka: 6,5 mg w 1 ml
        </MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Dorośli: 6,5 mg pr, im lub wyjątkowo dożylnie (w 20 ml 0,9% NaCl, max 3 x na dobę).
          {'\n'}
          {'\n'}
U dzieci poniżej 15 lat nie stosować.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          Doustnie
          {'\n'}
          {'\n'}
          {' '}
Początek działania: 30 min
          {'\n'}
          {' '}
Szczyt działania: nieznany
          {'\n'}
          Czas działania: 4 h
        </MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepoądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Przyspieszenie akcji serca, obniżenie ciśnienia. U osób młodych mogą wystąpić objawy
          pozapiramidowe: trudności w mówieniu i połykaniu, skurcze mięśniowe, drżenia, dyskinezy
          (nieskoordynowane i niezależne od woli ruchy kończyn lub całego ciała, wyginanie i
          prężenie, mimowolne ruchy warg, wysuwanie i chowanie języka), akatyzje (pobudzenie ruchowe
          i przymus bycia w ciągłym ruchu, lęk, rozdrażnienie).
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>
          Leki przeciw chorobie Parkinsona, leki działające ośrodkowo cholinolitycznie (atropina).
          {' '}
          {'\n'}
          {'\n'}
          Nie wolno stosować Adrenaliny do leczenia spadku ciśnienia tętniczego.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Preparaty: Torecan czopki, tabletki, roztwór do wstrzykiwań po 6,5 mg.
          {'\n'}
          {'\n'}
          {' '}
Przy podaży iv należy rozcieńczyć do 20 ml 0,9% NaCl z powodu ryzyka spadku
          ciśnienia.
          {'\n'}
          {'\n'}
          Lek może spowodować wystąpienie złośliwego syndromu neuroleptycznego, który manifestuje
          się klinicznie objawami bardzo wysokiej gorączki, sztywnością mięśni, zmianą stanu
          psychicznego i oznakami niestabilności autonomicznego układu nerwowego.
          {'\n'}
          {'\n'}
          Nie wolno stosować Adrenaliny do leczenia spadku ciśnienia tętniczego po podaniu Torecanu
          bo lek ten jest antagonistą adrenaliny.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Toceran;
