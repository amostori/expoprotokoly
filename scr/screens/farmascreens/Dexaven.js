import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  Hypertekst,
} from '../../components/TextViews';

class Dexaven extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Syntetyczny glikokortykosteroid, fluorowana pochodna prednizonu o długotrwałym i silnym
          działaniu przeciwzapalnym, przeciwalergicznym, immunosupresyjnym. Przeciwzapalnie działa
          6,5 razy silniej niż prednizon i 30 razy silniej niż hydrokortyzon.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Astma, wstrząs anafilaktyczny, zapalenie krtani u dzieci.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Nadwrażliwość na składniki leku.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek można stosować gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          niepożądanego działania u płodu -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C</Hypertekst>
          Uwaga, w pierwszym trymestrze kategoria D.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 4 mg w 1 ml lub 8 mg w 2 ml</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>8 mg i.v.</MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>Zapalenie krtani: 0,15 - 0,6 mg/kg iv lub im</MyTextView>
        <MyTextView>Astma, anafilaksja: 4 - 8 mg iv lub im</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <MyTextView>
          Po podaniu dożylnym maksymalne stężenie występuje po 10 - 30 minutach, a po podaniu
          domięśniowym czas ten wynosi do 60 minut, ale efekt działania pojawi się po 4 - 6
          godzinach.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>Przy okazjonalnym podawaniu zwykle brak.</MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Preparat Dexaven (4 lub 8 mg). Przy podawaniu drogą dożylną nierozcięńczony może powodować
          silnie odczuwany świąd.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Dexaven;
