import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  Hypertekst,
} from '../../components/TextViews';

class Budezonid extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Syntetyczny glikokortykosteroid niehalogenowy o bardo silnym miejscowym działaniu
          przeciwalergicznym i przeciwzapalnym.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Zespół krupu u dzieci.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Nadwrażliwość na którykolwiek składnik leku.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Stosować w ciąży wyłącznie wtedy, gdy korzyści ze stosowania przewyższają ryzyko dla płodu
          -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria B/C.</Hypertekst>
          Brak danych dotyczących przenikania leku do mleka matki.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>
          Roztwór do nebulizacji: 0,25 mg w 2 ml, 0,5 mg w 2 ml lub 1 mg w 2 ml
        </MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>2 mg w nebulizacji.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>Działanie lecznicze preparatu rozpoczyna się po około 6 h.</MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>Podrażnienie błony śluzowej.</MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>Preparaty: Pulmicort, Nebbud.</MyTextView>
        <MyTextView>
          Budezonid nie działa rozkurczowo na oskrzela, w związku z czym nie należy go stosować w
          celu przerwania napadu astmy oskrzelowej.
        </MyTextView>
        <MyTextView>
          W badaniach klinicznych potwierdzono, że budezonid w nebulizacji w dawce 2 mg cechuje się
          równą skutecznością terapeutyczną jak deksametazon podawny dożylnie.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Budezonid;
