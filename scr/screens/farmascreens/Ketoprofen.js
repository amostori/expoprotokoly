import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
  Sol,
} from '../../components/TextViews';

class Ketoprofen extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Niesterydowy lek przeciwbólowy, przeciwzapalny.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Bóle o umiarkowanym nasileniu, bóle po zabiegach chirurgicznych, w schorzeniach
          reumatycznych.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Jakiekolwiek czynne krwawienie. Astma. Nie stosować u dzieci &lt;15 r.ż.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek bezpieczny, nigdy nie badany w ciąży (bezpieczne jednorazowe podanie) -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria B.</Hypertekst>
          Ostrożnie w czasie karmienia piersią.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 100 mg w 2 ml (50 mg/ml), tabletki: 50 mg, 100 mg</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>
          100 - 200 mg w 100 ml
          <Sol />
          iv przez 30 min lub 100 mg p.o. Maksymalna dawka wynosi 200 mg na dobę.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>Nie podawać u dzieci poniżej 15 r.ż.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 5 min</ListaTextView>
        <ListaTextView>Szczyt działania: 40 min</ListaTextView>
        <ListaTextView>Czas działania: 4 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Nudności, wymioty, bóle brzucha, krwawienie z przewodu pokarmowego, odczyny alergiczne.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Preparaty: Ketonal ampułki po 100 mg w 2 ml, Refastin tabletki po 100 mg.
        </MyTextView>
        <MyTextView>
          Lek jest wrażliwy na światło więc podając go we wlewie okryj butelkę folią aluminiową lub
          czarnym papierem.
        </MyTextView>
        <MyTextView>
          Wg charakterystyki produktu leczniczego Ketoprofen w infuzji dożylnej można podawać
          jedynie w warunkach szpitalnych. Roztwór można zmieszać w tej samej butelce z morfiną
          natomiast nie wolno z tramadolem (wytrąci się osad).
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Ketoprofen;
