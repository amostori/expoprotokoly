import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Hydroxyzyna extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Uspokajające, przeciwlękowe, przeciwświądowe, nasenne.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Leczenie lęku, objawowe leczenie świądu, premedykacja przed zabiegami chirurgicznymi.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Porfiria o przewlekłym, zaostrzającym się przebiegu.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          dla płodu. Nie stosować w czasie porodu.
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 100 mg w 2 ml (50 mg/ml)</MyTextView>
        <MyTextView>Tabletki po 10 lub 25 mg.</MyTextView>
        <MyTextView>Syrop 2 mg/ml.</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>
          Objawowe leczenie lęku: 10 - 25 mg p.o., 50 mg i.m. jednorazowo (Nie podawać dożylnie!).
        </MyTextView>
        <MyTextView>U osób w podeszłym wieku stosować połowę dawki.</MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <ListaTextView>12 m.ż - 6 r.ż: 1 - 2,5 mg/kg/dobę w dawkach podzielonych.</ListaTextView>
        <ListaTextView>&gt;6 r.ż: 1 - 2 mg/kg/dobę w dawkach podzielonych.</ListaTextView>
        <ListaTextView>Ampułki 0,6 mg/kg i.m. jednorazowo.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Doustnie i domięśniowo</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 15 - 30 min.</ListaTextView>
        <ListaTextView>Szczyt działania: 2 - 4 h</ListaTextView>
        <ListaTextView>Czas działania: 4 - 6 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Wymioty, tachykardia, gorączka, nadmierne uspokojenie, suchość błony śluzowej,
          upośledzenie koordynacji ruchowej, halucynacje.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak.</MyTextView>
        <MyTextView>
          W przypadku spadku ciśnienia tętniczego podaje się norepinefrynę we wlewie i płyny.
          Epinefryna (Adrenalina) jest nieskuteczna ponieważ hydroksyzyna znosi jej działanie
          presyjne.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>Nie podawać dożylnie.</MyTextView>
        <MyTextView>
          Ampułka zawiera alkohol benzylowy, który może wywołać potencjalnie groźne dla życia
          zaburzenia oddychania u noworodków.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Hydroxyzyna;
