import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Amiodaron extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Lek antyarytmiczny z grupy III według podziału Vaughana-Williamsa</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Czestoskurcz komorowy, zatrzymanie krążenia w mechaniźmie migotania komór i częstoskurczu
          komorowego bez tętna
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Częstoskurcz wielokształtny (Torsade de Pointes), zatrucie trójcyklicznymi lekami
          przeciwdepresyjnymi
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria D.</Hypertekst>
          Nie stosować w okresie ciąży z wyjątkiem sytuacji gdy korzyść z zastosowania przewyższa
          ryzyko. Nie stosować w okresie karmienia.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 150 mg w 3 ml (50 mg/ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>300 mg iv, można podać drugą dawkę 150 mg.</MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>5 mg/kg iv, można raz powtórzyć. Rozcieńczać tylko 5% glukozą.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: może być znacznie opóźniony, nawet 2h</ListaTextView>
        <ListaTextView>Szczyt działania: 3 - 7 min.</ListaTextView>
        <ListaTextView>Czas działania: nieznany.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Bradykardia, spadek ciśnienia tętniczego krwi, zaburzenia łaknienia, zawroty głowy, blok
          serca.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>Zawiera jod, więc powoduje wzrost jego poziomu we krwi.</MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Amiodaron;
