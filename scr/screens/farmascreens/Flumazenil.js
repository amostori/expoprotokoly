import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Flumazenil extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Antagonista receptorów benzodiazepinowych.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Ciężkie zatrucie benzodiazepinami.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Unikaj stosowania tego leku jeśli benzodiazepiny były podane w celu opanowania stanu
          zagrożenia życia (drgawki). Istnieje niebezpieczeństwo, że drgawki wrócą
        </MyTextView>
        <MyTextView>
          Jeśli pacjent zażywa benzodiazepiny od dłuższego czasu podanie flumazenilu może spowodować
          uszkodzenie OUN i zgon.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Można stosować w ciąży wyłącznie w razie zdecydowanej konieczności -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C.</Hypertekst>
          Po podaniu jednorazowym przerwać karmienie na 24 h.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 0,5 mg w 5 ml (0,1 mg/ml).</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>0,2 mg powoli iv powtarzać co 1 minutę do dawki całkowitej 1 mg.</MyTextView>
        <MyTextView>W ampułce jest 0,5 mg w 5 ml.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 1 - 2 min</ListaTextView>
        <ListaTextView>Szczyt działania: 6 - 10 min.</ListaTextView>
        <ListaTextView>Czas działania: 1 - 2 h.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Nudności i wymioty, przejściowy wzrost ciśnienia tętniczego i częstotliwości rytmu serca.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>Preparat: Anexate</MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Flumazenil;
