import React from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  Hypertekst,
  BoldText,
  VersionDrug,
} from '../../components/TextViews';

class Salbutamol extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek z grupy beta2 mimetyków, rozszerza oskrzela, obniża poziom potasu we krwi
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>Astma, POCHP, hiperkaliemia.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>W stanach zagrożenia życia brak.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria C.</Hypertekst>
          Lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          niepożądanego działania u płodu.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>
          Ampułka: 0,5 mg w 1 ml
          {'\n'}
          {'\n'}
          Roztwór do nebulizacji: 2,5 mg w 2,5 ml
          {'\n'}
          lub
          {'\n'}
5 mg w 2,5 ml
        </MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Nebulizacja</BoldText>
          {'\n'}
          {'\n'}
          {' '}
U dzieci &gt;20 kg i dorosłych 5 mg w nebulizacji
          {'\n'}
          {'\n'}
U dzieci &lt;20 kg 2,5 mg - w razie konieczności dawkę można powtarzać co 20 min.
          {'\n'}
          {'\n'}
          <BoldText>Postać dożylna leku:</BoldText>
          {'\n'}
          {'\n'}
          Dorośli: 0,25 - 0,5 mg iv rozcieńczone w 10 ml 0,9% NaCl lub 5% Glukozie w ciągu 5 min lub
          we wlewie kroplowym z szybkością 5 &micro;/min.
          {'\n'}
          {'\n'}
          {' '}
Przy podawaniu iv kontroluj tętno i ciśnienie tętnicze.
          {'\n'}
          {'\n'}
          {' '}
Przy podawaniu i.m lub s.c dawka to 0,5 mg.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          Nebulizacja
          {'\n'}
          {'\n'}
          Początek działania: 5 - 15 min
          {'\n'}
          Szczyt działania: 60 - 90 min
          {'\n'}
          Czas działania: 3 - 6 h
        </MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Bóle głowy, pobudzenie, zaburzenia smaku, drżen ia mięśniowe, przemijające rozszerzenie
          naczyń obwodowych, spadek lub wzrost ciśnienia tętniczego krwi, częstoskurcz,
          tachyarytmie, zaburzenia metaboliczne. Po podaniu wziewnym może powodować podrażnienie
          błony śluzowej jamy ustnej i gardła, a także paradoksalny skurcz oskrzeli. Rzadko
          występuje odczyn anafilaktyczny (pokrzywka, obrzęk naczynioruchowy, zapaść naczyniowa).
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Preparat do neb.: Steri Neb Salamol, Ventolin (żółty: 2,5 mg, zielony: 5 mg). Preparat iv:
          Salbutamol 0,5 mg. Do nebulizacji stosować postać leku do tego przeznaczoną, np. Ventolin.
          Wlać do pojemnika nebulizatora i ustawić przepływ tlenu zgodnie z zaleceniami producenta
          nebulizatora (zwykle 6 do 8 l/min) - nie wymaga rozcieńczenia.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Salbutamol;
