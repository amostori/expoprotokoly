import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  Sol,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Adenozyna extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Czasowo blokuje przewodzenie w węźle AV przerywając nawrotny częstoskurcz węzłowy i
          częstoskurcz przedsionkowo - komorowy. Lek nie wpływa na migotanie i trzepotanie
          przedsionków.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Częstoskurcz nadkomorowy typu AVRT i AVNRT (miarowy, z wąskimi zespołami QRS) jeśli
          stymulacja nerwu błędnego nie przyniosła rezultatu.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Astma oskrzelowa, POCHP, zespół wydłużonego odstępu QT, migotanie przedsionków u pacjenta
          z zespołem WPW, stan po przeszczepie serca.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Można stosować w ciąży i w czasie karmienia wyłącznie w razie zdecydowanej konieczności
          <Hypertekst onPress={this.handleKodCiazy}>(kategoria C)</Hypertekst>
.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 6 mg w 2 ml (3 mg/ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
6 mg iv w szybkim bolusie. Można powtórzyć dwukrotnie po 12
          mg.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
          0,2 mg/kg (max 6 mg). Jeśli nieskuteczne po 2 min podaj 0,4 mg/kg iv (max 12 mg).
        </MyTextView>
        <MyTextView>
          Ampułkę zawierającą 6 mg leku rozcieńcz w 3 ml
          <Sol />
i podaj 0,1 ml/kg w pierwszej dawce i 2 razy tyle w drugiej dawce.
        </MyTextView>
        <MyTextView>
          Uwaga, ze względu na krótki okres półtrwania należy podać szybko i natychmiast przepłukać
          20 ml NaCl unosząc kończynę do góry.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: natychmiastowo.</ListaTextView>
        <ListaTextView>Szczyt działania: 20 - 30 sek.</ListaTextView>
        <ListaTextView>Czas działania: ok. 1 min.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Nudności, wymioty, zawroty głowy, zaburzenia widzenia, uczucie kołatania serca, skurcz
          oskrzeli.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Preparat: adenocor 6 mg/2 ml. Nie zaleca się stosowania następnych dawek jeśli pierwsze
          trzy były nieskuteczne.
        </MyTextView>
        <MyTextView>
          Po podaniu pacjentowi z migotaniem przedsionków i zespołem WPW może dojść do zagrażającego
          życiu częstoskurczu z powodu zwiększenia przewodzenia drogą dodatkową.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Adenozyna;
