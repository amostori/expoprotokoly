import React from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView, RedBoldText, Hypertekst, VersionDrug,
} from '../../components/TextViews';

class Tikagrelor extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Zapobieganiu zależnej od ADP aktywacji i agregacji płytek krwi.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Ostry zawał mięśnia sercowego - w celu zapobiegania zdarzeniom sercowo- naczyniowym u
          pacjentów z ostrym zespołem wieńcowym, w tym u pacjentów leczonych farmakologicznie lub za
          pomocą przezskórnej interwencji wieńcowej lub pomostowania aortalno-wieńcowego.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Czynne krwawienie patologiczne, krwotok śródczaszkowy w wywiadzie, jednoczesne stosowanie
          tikagreloru i silnych inhibitorów enzymu CYP3A4 (np. ketokonazol, klarytromycyna,
          nefazodon, rytonawir, atazanawir).
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria C -</Hypertekst>
          lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          niepożądanego działania u płodu.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Tabletki po 90 mg</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Uwaga, ratownik medyczny może podać ten lek tylko po teletransmisji i za zgodą lekarza
          oceniającego ekg.
          {'\n'}
          {'\n'}
          Dawki nasycająca 180 mg (2 tabletki po 90 mg).
          {'\n'}
          {'\n'}
U dzieci nie stosować.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          Doustnie
          {'\n'}
          {'\n'}
          {' '}
Początek działania: w przeciągu 30 min
          {'\n'}
          {' '}
Szczyt działania: 4 h
          {'\n'}
          {' '}
Czas działania:5 dni
        </MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>Krwawienia, duszność.</MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Preparaty: Brilique, 90 mg. Uwaga, ratownik medyczny może podać ten lek tylko po
          teletransmisji i za zgodą lekarza oceniającego ekg.
          {'\n'}
          {'\n'}
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Tikagrelor;
