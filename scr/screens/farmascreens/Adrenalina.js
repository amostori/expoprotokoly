import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Adrenalina extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Skurcz mięśni gładkich naczyń krwionośnych, rozszerzenie oskrzeli, przyspieszenie akcji
          serca, zwiększenie siły skurczu mięśnia sercowego.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zatrzymanie krążenia w każdej postaci, wstrząs anafilaktyczny, ciężka bradykardia, wstrząs
          oporny na leczenie innymi katecholaminami.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>W stanach zagrożenia życia brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Można stosować w ciąży i w czasie karmienia wyłącznie w razie zdecydowanej konieczności
          <Hypertekst onPress={this.handleKodCiazy}>(kategoria C)</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 1 mg w 1 ml</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>
          Resuscytacja: 1 mg iv bez rozcieńczenia. Anafilaksja: 0,5 mg w mięsień udowy. Bradykardia:
          wlew 2 - 10 &micro;/min.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>Resuscytacja: 0,01 mg/kg iv.</MyTextView>
        <MyTextView>Astma:</MyTextView>
        <ListaTextView>0 - 6 r.ż.: 0,15 mg im (bez rozc. 0,15 ml)</ListaTextView>
        <ListaTextView>6 - 12 lat: 0,3 mg im (bez rozc. 0,3 ml)</ListaTextView>
        <ListaTextView>&gt;12 lat: 0,5 mg im (bez rozc. 0,5 ml)</ListaTextView>
        <MyTextView />
        <MyTextView>Krup wirusowy: 5 mg w nebulizacji.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: szybki.</ListaTextView>
        <ListaTextView>Szczyt działania: 20 min.</ListaTextView>
        <ListaTextView>Czas działania: 20 min.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Domięśniowo</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 6 - 12 min.</ListaTextView>
        <ListaTextView>Szczyt działania: nieznany.</ListaTextView>
        <ListaTextView>Czas działania: &lt;1 - 4h.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Podskórnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 5 - 10 min.</ListaTextView>
        <ListaTextView>Szczyt działania: 20 min.</ListaTextView>
        <ListaTextView>Czas działania: &lt;1 - 4h.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Objawy pobudzenia układu współczulnego: tachykardia, zaburzenia rytmu serca, dławica
          piersiowa, bóle i zawroty głowy, bladość, osłabienie, zaburzenia oddawania moczu,
          duszność, pocenie, ślinienie.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Podawanie dotchawicze nie jest obecnie zalecane, ale jeśli używane to:
        </MyTextView>
        <ListaTextView>waga/10 - tyle mg adrenaliny rozcieńcz do 5 ml solą i podaj.</ListaTextView>
        <MyTextView />
        <MyTextView>
          Podając adrenalinę w NZK użyj innego wkłucia dla dwuwęglanu sodu, gdyż Adrenalina ulega
          przyspieszonemu rozkładowi w środowisku zasadowym.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Adrenalina;
