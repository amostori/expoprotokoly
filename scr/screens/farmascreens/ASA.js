import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
  Sol,
} from '../../components/TextViews';

class ASA extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Niesterydowy lek przeciwbólowy, przeciwzapalny i przeciwgorączkowy. Zmniejsza agregację
          płytek krwi.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Zwiększa przeżywalność w zawale serca.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Czynne krwawienie z przewodu pokarmowego.\n Uczulenie na salicylany.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Tabletki: 75 mg lub 300 mg</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>160 - 325 mg po</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Doustnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 15 - 30 min</ListaTextView>
        <ListaTextView>Szczyt działania: 4 - 6 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          U dzieci poniżej 13 r. ż. po podaniu aspiryny może wystąpić niebezpieczny dla życia Zespół
          Reye&apos;a.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default ASA;
