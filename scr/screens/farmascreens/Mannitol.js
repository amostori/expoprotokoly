import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Mannitol extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Diuretyk osmotycznie czynny. Zwiększa ciśnienie osmotyczne płynu pozakomórkowego co
          prowadzi do przedostania się wody z komórek do osocza. Powoduje to zmniejszanie ciśnienia
          wewnątrzczaszkowego.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Pobudzanie diurezy w zapobieganiu i (lub) leczeniu oligurycznej fazy ostrej niewydolności
          nerek, zanim dojdzie do ustalenia nieodwracalnej oligurycznej niewydolności nerek.
          Zmniejszanie ciśnienia śródczaszkowego i obrzęku mózgu, gdy bariera krew-mózg jest
          nieuszkodzona. Zmniejszanie podwyższonego ciśnienia śródgałkowego, kiedy nie można go
          obniżyć innymi metodami leczenia. Pobudzanie eliminacji toksycznych substancji wydalanych
          przez nerki w przypadku zatrucia.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          U pacjentów ze wstrząsem i zaburzeniem czynności nerek nie należy podawać mannitolu do
          czasu uzupełnienia ubytków objętości (płyn, krew) i elektrolitów. Istniejąca uprzednio
          hiperosmolarność osocza, ciężkie odwodnienie, trwały bezmocz, ciężka niewydolność serca,
          ciężki zastój krwi w płucach lub obrzęk płuc, czynne krwawienie śródczaszkowe, z wyjątkiem
          kraniotomii, uszkodzenie bariery krew-mózg, nadwrażliwość na mannitol, brak właściwej
          odpowiedzi na dawkę testową, postępujące uszkodzenie nerek lub dysfunkcję po wdrożeniu
          leczenia mannitolem, w tym nasilenie objawów skąpomoczu i azotemii.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          niepożądanego działania u płodu -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>
          Roztwór do infuzji: 15 g w 100 ml (150 mg/ml) lub 37,5 g w 250 ml (150 mg/ml)
        </MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>
          Uwaga, stosować przy pomocy zestawu do przetaczania z filtrem. Istnieje ryzyko wytrącania
          się kryształków mannitolu. Dawkowanie oraz szybkość wlewu zależy od wieku, masy ciała i
          stanu klinicznego pacjenta oraz od stosowanego jednocześnie leczenia.
        </MyTextView>
        <MyTextView>Zmniejszenie ciśnienia śródczaszkowego:</MyTextView>
        <MyTextView>1,5 do 2 g/kg mc. (10-13 ml/kg) wlew iv 30 - 60 min</MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>0,5 do 1,5 g/kg(3 ml do 10 ml/kg) wlew iv 30 - 60 min.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 30 - 60 min</ListaTextView>
        <ListaTextView>Szczyt działania: 1 h</ListaTextView>
        <ListaTextView>Czas działania: 6 - 8 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Gorączka, zapalenie żyły w miejscu podania, martwica skóry, obrzęk płuc lub zatrucie wodne
          jeśli wydzielanie moczu jest nieadekwatne.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Preparat: Mannitol 15% 150 mg/ml (150 g/l). Ten hipertoniczny roztwór należy podawać do
          dużych żył obwodowych lub najlepiej do żyły centralnej. Szybki wlew do żył obwodowych może
          być szkodliwy. Przed podaniem należy sprawdzić obecność kryształów. Jeżeli kryształy są
          widoczne, należy je rozpuścić przez ogrzanie roztworu do 37 stopni C, a następnie
          delikatne wymieszanie. Roztworów nie należy ogrzewać w wodzie lub kuchence mikrofalowej z
          uwagi na możliwość zanieczyszczenia produktu lub jego uszkodzenie. Należy stosować tylko
          suche źródła ciepła (np. cieplarka). Przed ponowną kontrolą obecności kryształów i
          użyciem, schłodzić roztwór do temperatury pokojowej lub temperatury ciała.
        </MyTextView>
        <MyTextView>
          Mannitol występuje w przyrodzie (np. w niektórych owocach i warzywach) i jest szeroko
          stosowany jako substancja pomocnicza w produktach leczniczych i kosmetykach. W związku z
          tym, pacjenci mogą być uwrażliwieni bez uprzedniego leczenia dożylnego mannitolem.
        </MyTextView>
        <MyTextView>
          Pacjentom ze znaczną oligurią lub podejrzeniem niewłaściwej czynności nerek należy
          najpierw podać dawkę testową około 200 mg mannitolu/ kg (1,3 ml/kg) we wlewie trwającym 3
          do 5 minut. Reakcję na dawkę testową można uznać za odpowiednią, jeżeli przez 2-3 godziny
          wydalane jest przynajmniej 30-50 ml moczu na godzinę. Jeżeli reakcja nie jest odpowiednia,
          można podać kolejną dawkę testową. Jeżeli nie uzyska się odpowiedniej reakcji na drugą
          dawkę testową, należy przerwać podawanie mannitolu i ponownie ocenić stan zdrowia pacjenta
          gdyż mogło dojść do trwałej niewydolności nerek.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Mannitol;
