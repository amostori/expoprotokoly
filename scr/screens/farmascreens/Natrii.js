import React from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView, RedBoldText, Hypertekst, VersionDrug,
} from '../../components/TextViews';

class Natrii extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek alkalizujący. Podwyższa stężenie wodorowęglanów w surowicy krwi i pH surowicy.
          Przyspiesza eliminację niektórych trucizn np. trójcyklicznych leków przeciwdepresyjnych,
          salicylanów.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek nie jest zalecany rutynowo w resuscytacji. Stosuj tylko gdy do zatrzymania krążenia
          doszło z powodu ciężkiej hiperkaliemii. Inne wskazania: zagrażająca życiu hiperkaliemia,
          zatrucie trójcyklicznymi lekami przeciwdepresyjnymi.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Alkaloza metaboliczna, alkaloza oddechowa, hipowentylacja, hipernatremia i stany, w
          których dostarczanie sodu jest przeciwwskazane, np. w zastoinowej niewydolności krążenia,
          obrzękach, chorobie nadciśnieniowej, rzucawce, niewydolności nerek, hipokalcemia, w której
          alkaloza może wywołać tężyczkę, nadmierna utrata chlorków spowodowana m.in. wymiotami, u
          pacjentów z ryzykiem rozwoju alkalozy indukowanej przez leki moczopędne.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria A.</Hypertekst>
          Lek bezpieczny w okresie ciąży.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 1,68 g w 20 ml (1 mmol/ml lub 1 mEq/ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Dorośli: 50 mmol (50 ml) iv Dzieci: 1 mmol/kg (1 ml/kg) iv. U dzieci &lt;2 lata lek wymaga
          rozcieńczenia w stosunki 1:1 i podawania w powolnym wlewie 1 kropla/sek.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Famakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          Dożylnie:
          {'\n'}
          Początek działania: natychmiastowy
          {'\n'}
          Szczyt działania: szybki
          {'\n'}
          Czas działania: nieznany
        </MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>Nadmierna alkaloza.</MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Inne nazwy: natrium bicarbonicum, NaHCO3, wodorowęglan sodu.
          {'\n'}
1 ml roztworu zawiera: 1 mmol Na+ i 1 mmol HCO3ˉ (= 1 mEq wodorowęglanu sodu
          NaHCO3).
          {'\n'}
          Natrium bicarbonicum 8,4% należy podawać dokładnie do żyły, ponieważ po podaniu
          pozanaczyniowym może wystąpić martwica tkanek. Szybkie wstrzyknięcie (10 ml/min.)
          hipertonicznego roztworu Natrium bicarbonicum 8,4% noworodkom i dzieciom poniżej 2 lat
          może wywołać hipernatremię, obniżyć ciśnienie płynu mózgowo-rdzeniowego i wywołać
          krwawienia wewnątrzczaszkowe.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Natrii;
