import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Klopidogrel extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Inhibitor agregacji płytek krwi indukowanej przez ADP oraz aktywację kompleksu
          glikoprotein GPIIb/IIIa.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Ostry zawał mięśnia sercowego z uniesieniem odcinka ST (oraz NSTEMI), w skojarzeniu z
          kwasem acetylosalicylowym.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Czynne krwawienie (wrzód trawienny, krwawienie wewnątrzczaskowe). Nie zaleca się
          stosowania leku w ciągu 7 dni po ostrym udarze niedokrwiennym.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Z uwagi na brak danych nie zaleca się stosowania leku u kobiet w ciąży i w czasie
          karmienia piersią.
          <Hypertekst onPress={this.handleKodCiazy}> - kategoria B.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Tabletki po 75 lub 300 mg</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>Zazwyczaj dawka to 300 - 600 mg p.o.</MyTextView>
        <MyTextView>
          Uwaga, ratownik medyczny może podać ten lek tylko po teletransmisji i za zgodą lekarza
          oceniającego ekg.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Doustnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 24 h</ListaTextView>
        <ListaTextView>Szczyt działania: 3 dzień</ListaTextView>
        <ListaTextView>Czas działania: 5 dni</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>Krwawienie z nosa, ból brzucha, biegunka.</MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak. W razie potrzeby stosuje się przetoczenia masy płytkowej.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>Preparaty: Areplex, Plavix.</MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Klopidogrel;
