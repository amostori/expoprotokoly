import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Glukagon extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zwiększenie poziomu cukru we krwi poprzez uwalnianie glukozy z glikogenu wątrobowego
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Domięśniowo w hipoglikemii. W zatruciu beta i Ca blokerami oraz we wstrząsie
          anafilkatycznym zaleca się podanie dożylne glukagonu w dawce od 1 do 10 mg, ale w Polsce
          brak preparatu, który można podać tą drogą.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Noworodki, insulinoma, guz chromochłonny, przewlekłe choroby wątroby, stan upojenia
          alkoholowego.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek bezpieczny, nigdy nie badany w ciąży (bezpieczne jednorazowe podanie) -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria B.</Hypertekst>
          Ostrożnie w czasie karmienia piersią.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułkostrzykawka: 1 mg</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>1 mg i.m.</MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>0,5 mg i.m u dzieci do 25 kg, 1 mg i.m. u dzieci powyżej 25 kg.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Domięśniowo</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: do 10 min</ListaTextView>
        <ListaTextView>Szczyt działania: 30 min</ListaTextView>
        <ListaTextView>Czas działania: 12 - 27 min</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Odczyn uczuleniowy (pokrzywka, zaburzenia oddychania, spadek ciśnienia), wzrost ciśnienia
          tętniczego, nudności, wymioty, podwyższenie stężenia potasu w surowicy.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          U noworodków lek nieprzydatny bo wymaga zapasów glikogenu w organizmie i enzymów
          rozkładającyh glikogen.
        </MyTextView>
        <MyTextView>
          Zwiększ siłę skurczu mięśnia serca działając na receptory glukagenowe i dlatego stosowany
          jest w zatruciach.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Glukagon;
