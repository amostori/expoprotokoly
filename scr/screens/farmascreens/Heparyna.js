import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
  Sol,
} from '../../components/TextViews';

class Heparyna extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Hamowanie kaskady krzepnięcia.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Ostry zawał mięśnia sercowego STEMI oraz NSTEMI, w skojarzeniu z kwasem
          acetylosalicylowym. Zator tętnicy płucnej.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Niepoddające się kontroli krwawienie, rozwarstwiający się tętniak aorty, skaza krwotoczna,
          immunologiczna małopłytkowość, krwawienie z przewodu pokarmowego, nadciśnienie wrotne,
          udar krwotoczny, pourazowy krwiak śródmózgowy, nowotowory układu nerwowego, ciężkie
          nadciśnienie tętnicze (skurczowe &gt;200 mmHg), ostre zapalenie osierdzia.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          dla płodu
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Fiolka: 25 000 j.m. w 5 ml (5000 j.m./ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>
          dawka wstępna 5000 j.m. (1 ml roztworu) w 20 ml
          <Sol />
          i.v.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>50 j.m./kg i.v.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: natychmiastowo</ListaTextView>
        <ListaTextView>Szczyt działania: 5 - 10 min.</ListaTextView>
        <ListaTextView>Czas działania: 2 - 6 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>Krwawienie, ból i zaczerwienie w miejscu wstrzyknięcia, priaprizm.</MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Siarczan protaminy 1 mg unieczynnia 115 j.m. heparyny.</MyTextView>
        <MyTextView>Dawka maksymalna: 50 mg w powolnym wlewie (10 min).</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>Miesiączka nie stanowi przeciwwskazania do stosowania heparyny.</MyTextView>
        <MyTextView>
          Nitrogliceryna może istotnie zmniejszać skuteczność działania przeciwzakrzepowego
          heparyny.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Heparyna;
