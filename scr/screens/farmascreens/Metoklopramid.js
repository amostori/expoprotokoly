import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Metoklopramid extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Przeciwwymiotne.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Wymioty, migrena.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Niedrożność mechaniczna jelit.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Nie stosować w I trymestrze. W II i III trymestrze stosować wyłącznie w razie absolutnej
          konieczności. Nie stosować w czasie karmienia piersią -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria B.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 10 mg w 2 ml (5 mg/ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>10 mg iv rozcieńczone do 10 ml NaCl 0,9%.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 1 - 3 min</ListaTextView>
        <ListaTextView>Szczyt działania: natychiastowy</ListaTextView>
        <ListaTextView>Czas działania: 1 - 2 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zaburzenia widzenia; objawy pozapiramidowe, w postaci zmian napięcia mięśniowego (np. bóle
          mięśni, skurcze mięśni twarzy, drżenia, szczękościsk, kręcz szyi); również objawy
          parkinsonizmu (spowolnienie ruchów, drżenia, twarz maskowata, sztywność oraz opóźniona
          dyskineza, mimowolne ruchy języka, twarzy, ust lub żuchwy, niekiedy mimowolne ruchy
          tułowia i kończyn). Spadek ciśnienia tętniczego krwi, nadciśnienie, tachykardia,
          bradykardia, nudności, zaburzenia jelitowe. U pacjentów w podeszłym wieku, zwłaszcza u
          kobiet, mogą wystąpić dyskinezy, często nieodwracalne.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Metoclopramid podany dożylnie działa przeciwbólowo w migrenie. Mechanizm tego działania
          jest nieznany.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Metoklopramid;
