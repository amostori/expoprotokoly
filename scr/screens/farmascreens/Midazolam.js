import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Midazolam extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Nasenne, uspokające, przeciwdrgawkowe.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Sedacja przed zabiegami typu kardiowersja.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Upojenie alkoholowe.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Nie zaleca się stosowania w okresie ciąży i karmienia piersią.
          <Hypertekst onPress={this.handleKodCiazy}>kategoria D.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 5 mg/ml, 5 mg w 2 ml, 2 mg w 2 ml, 15 mg w 2 ml</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli i dzieci:</BoldText>
        </MyTextView>
        <MyTextView>0,1 mg/kg iv</MyTextView>
        <MyTextView>
          <BoldText>Podanie między policzek a dziąsło u dzieci:</BoldText>
        </MyTextView>
        <MyTextView>Uwaga, użyj preparatu zawierającego 5 mg leku w 1 ml roztworu.</MyTextView>
        <ListaTextView>&lt;1 r.ż.: 2,5 mg</ListaTextView>
        <ListaTextView>1 - 4 r.ż.: 5 mg</ListaTextView>
        <ListaTextView>5 - 9 r.ż.: 7,5 mg</ListaTextView>
        <ListaTextView>&gt;9 r.ż.: 10 mg</ListaTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 1,5 - 5 min</ListaTextView>
        <ListaTextView>Szczyt działania: szybki</ListaTextView>
        <ListaTextView>Czas działania: 2 - 6 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Domięśniowo</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 15 min</ListaTextView>
        <ListaTextView>Szczyt działania: 30 - 60 min</ListaTextView>
        <ListaTextView>Czas działania: 2 - 6 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Często nasilone zaburzenia pamięci, spadek ciśnienia skurczowego. Rzadko : depresja
          oddechowa, bezdech, zatrzymanie czynności serca (zwłaszcza u pacjentów w podeszłym wieku,
          chorych z niewydolnością oddechową, chorobami serca, szczególnie po podaniu zbyt szybko
          lub w zbyt dużej dawce), reakcje paradoksalne: pobudzenie, nadmierna ruchliwość,
          agresywność, ruchy mimowolne, drgawki toniczno-kloniczne, drżenia mięśniowe, czkawka,
          nudności, wymioty, ból głowy, kaszel, nadmierne uspokojenie, senność, stany spastyczne
          układu oddechowego, zaburzenia widzenia, zawroty głowy.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Flumazenil</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Podany między policzek a dziąsło przerywa drgawki bardziej skutecznie niż wlewka relsed.
          Nazwa handlowa: dormicum, sopodorm, midanium.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Midazolam;
