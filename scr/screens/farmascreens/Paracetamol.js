import React from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  Hypertekst,
  BoldText,
  VersionDrug,
} from '../../components/TextViews';

class Paracetamol extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Lek przeciwbólowy i przeciwgorączkowy.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Niewielki i średni ból, gorączka.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Ciężka niewydolność wątroby</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>
            Kategoria B (postać doustna), kategoria C (postać dożylna).
          </Hypertekst>
          Należy unikać stosowania jednak w razie konieczności w dawkach terapeutycznych wydaje się,
          że jest bezpieczny w krótkotrwałym stosowaniu.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>
          Fiolki: 1 g w 100 ml (10 mg/ml)
          {'\n'}
          lub
          {'\n'}
          500 mg w 50 ml (10 mg/ml)
          {'\n'}
          {'\n'}
          Czopki po 50, 80, 125, 150, 250, 300, 500 mg
          {'\n'}
          {'\n'}
          Tabletki po 300, 500, 1000 mg
          {'\n'}
          {'\n'}
          Syrop: 24 mg/ml, 40 mg/ml, 50 mg/ml, 100 mg/ml
        </MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Doustnie:</BoldText>
          {'\n'}
          {'\n'}
          dorośli i dzieci &gt;12 lat 500 mg.
          {'\n'}
          dzieci &lt;12 lat 10 - 15 mg/kg.
          {'\n'}
          {'\n'}
          <BoldText>Dożylnie:</BoldText>
          {' '}
- wlew 15 min:
          {'\n'}
          {'\n'}
          Uwaga! Istnieje duże ryzyko pomylenia dawki, szczególnie u niemowląt, dlatego fiolki po
          100 ml są przeznaczone wyłącznie do stosowania u pacjentów &gt;33 kg m.c.
          {'\n'}
          {'\n'}
          Dzieci &lt;10 kg:
          {'\n'}
U tych dzieci należy z fiolki pobrać odpowiednią dawkę i rozcieńczyć w stosunku nie
          większym niż 1 do 10 w 0,9% NaCl lub 5% glukozie.
          {'\n'}
          {' '}
Dawka: 7,5 mg/kg (tj. 0,75 ml roztworu/kg)
          {'\n'}
          {'\n'}
          dzieci 10 - 50 kg:
          {'\n'}
          15 mg/kg (tj. 1,5 ml roztworu/kg)
          {'\n'}
          {'\n'}
          pacjenci &gt;50 kg:
          {'\n'}
1 g.
          {'\n'}
          {'\n'}
          {' '}
Przetaczać w ciągu 15 min.
          {'\n'}
          {'\n'}
          <BoldText>Doodbytniczo:</BoldText>
          {'\n'}
          {'\n'}
0 -3 m.ż.: 50 mg
          {'\n'}
3 m.ż. - 1 r.ż.: 80 mg
          {'\n'}
2 r.ż. - 3 r.ż.: 125 mg
          {'\n'}
4 r.ż. - 9 r.ż.: 250 mg
          {'\n'}
          &gt;9 r.ż.: 500 mg
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          Dożylnie - przeciwbólowo
          {'\n'}
          {'\n'}
          Początek działania: 5 - 10 min
          {'\n'}
          Szczyt działania: 1 h
          {'\n'}
          Czas działania: 4 - 6 h
          {'\n'}
          {'\n'}
          Dożylnie - przeciwgorączkowo
          {'\n'}
          {'\n'}
          Początek działania: w przeciągu 30 min
          {'\n'}
          Szczyt działania: 30 min
          {'\n'}
          Czas działania: 4 - 6 h
          {'\n'}
          {'\n'}
          Doustnie, doodbytniczo
          {'\n'}
          {'\n'}
          Początek działania: 0,5 - 1 h
          {'\n'}
          Szczyt działania: 1 - 3 h
          {'\n'}
          Czas działania: 3 - 8 h
          {'\n'}
          {'\n'}
        </MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Uszkodzenie wątroby, żółtaczka przy zatruciu. Już 2 - krotne przekroczenie dawki dobowej
          może powodować objawy uszkodzenia wątroby.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>
          N - acetylocysteina w dawce 150 mg/kg w 15 min następnie wlew 50 mg/kg w ciągu 4 h.
          Następnie 100 mg/kg w ciągu 16 h.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Inna nazwa: acetaminofen. Ampułka (50 ml lub 100 ml) zawiera 10 mg/ml paracetamolu. Czopki
          po 50 mg, 80 mg, 125 mg, 250 mg i 500 mg.
          {'\n'}
          {'\n'}
W odróżnieniu od niesteroidowych leków przeciwzapalnych paracetamol nie wpływa na
          krzepliwość krwi i wykazuje słabe działanie przeciwzapalne.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Paracetamol;
