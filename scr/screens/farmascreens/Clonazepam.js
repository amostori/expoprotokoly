import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  Hypertekst,
} from '../../components/TextViews';

class Clonazepam extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Działanie przeciwdrgawkowe</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Atak padaczki</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>W stanach zagrożenia życia brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          W czasie ciąży lek może być stosowany wyłącznie w przypadku bezwzględnych wskazań (stan
          zagrożenia życia)
          <Hypertekst onPress={this.handleKodCiazy}> - kategoria D.</Hypertekst>
          Lek przenika do mleka matki.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 1 mg w 1 ml</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>1 mg w rozcieńczeniu conajmniej 1:1 iv</MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView> 0,5 mg iv po rozcieńczeniu conajmniej 1:1</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <MyTextView>Brak danych</MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zaburzenia koordynacji ruchowej, ruchy pląsawicopodobne, podwójne widzenie, oczopląs,
          niewyraźna mowa, bóle głowy, zawroty głowy, niedowład połowiczy, osłabienie czynności
          oddechowych, uczucie kołatania serca, spadek ciśnienia tętniczego.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>
          Flumazenil 0,2 mg powoli iv powtarzać co 1 minutę do dawki całkowitej 1 mg.
        </MyTextView>
        <MyTextView>W ampułce jest 0,5 mg w 5 ml.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek podawać zawsze rozcieńczony i powoli. Przy podawaniu domięśniowo i dożylnie
          rozcieńczyć w stosunku 1:1 czyli 1 ml leku + 1 ml wodu do wstrzykiwań.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Clonazepam;
