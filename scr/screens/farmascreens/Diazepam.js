import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  Hypertekst,
  ListaTextView,
} from '../../components/TextViews';

class Diazepam extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Działanie przeciwdrgawkowe, uspokajające.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Epilepsja, drgawki gorączkowe gdy inne metody nie są skuteczne, histeria, sedacja do
          krótkich zabiegów.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Zatrucie alkoholem</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Leku nie należy podawać w okresie ciąży, zwłaszcza w I i III trymestrze, o ile korzyści
          dla matki nie przeważają nad ryzykiem dla dziecka
          <Hypertekst onPress={this.handleKodCiazy}> - kategoria D.</Hypertekst>
          Nie stosować w okresie karmienia piersią.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 10 mg w 2 ml (5 mg/ml)</MyTextView>
        <MyTextView>Wlewka: 5 lub 10 mg w 2,5 ml</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>5 - 10 mg i.v.</MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>Ampułka: 0,1 - 0,3 mg/kg i.v.</MyTextView>
        <MyTextView>Wlewka:</MyTextView>
        <ListaTextView>dzieci &lt;10 kg: 0,5 mg/kg,</ListaTextView>
        <ListaTextView>dzieci 10 - 15 kg: 5 mg,</ListaTextView>
        <ListaTextView>dzieci &gt;15 kg: 10 mg</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 1 - 5 min.</ListaTextView>
        <ListaTextView>Szczyt działania: 15 - 30 min.</ListaTextView>
        <ListaTextView>Czas działania: 15 - 60 min.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Domięśniowo</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: do 20 min.</ListaTextView>
        <ListaTextView>Szczyt działania: 0,5 - 1,5 h.</ListaTextView>
        <ListaTextView>Czas działania: nieznany</ListaTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Doodbytniczo</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 2 - 10 min.</ListaTextView>
        <ListaTextView>Szczyt działania: 1 - 2 h.</ListaTextView>
        <ListaTextView>Czas działania: 4 - 12 h.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zmęczenie, senność, osłabienie siły mięśniowej, zawroty głowy, dezorientacja, spowolniona
          mowa, drżenie rąk.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>
          Flumazenil 0,2 mg powoli iv powtarzać co 1 minutę do dawki całkowitej 1 mg.
        </MyTextView>
        <MyTextView>W ampułce jest 0,5 mg w 5 ml.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>Nie należy go rozcieńczać w strzykawce bo się wytrąca.</MyTextView>
        <MyTextView>Lek należy podawać powoli (1 ml/min).</MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Diazepam;
