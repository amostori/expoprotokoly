import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Ibuprofen extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Ibuprofen jest niesteroidowym lekiem przeciwzapalnym, wykazującym słabe do umiarkowanego
          działanie przeciwbólowe oraz działanie przeciwgorączkowe.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Bóle słabe do umiarkowanych różnego pochodzenia, np. bóle głowy, również migrenowe, bóle
          zębów, mięśni, kości i stawów, bóle pourazowe, nerwobóle. Gorączka różnego pochodzenia.
          Bolesne miesiączkowanie.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Czynna lub występująca w przeszłości choroba wrzodowa żołądka i (lub) dwunastnicy. Ciężka
          niewydolność wątroby, ciężka niewydolność nerek lub ciężka niewydolność serca. Istniejące
          lub występujące w przeszłości objawy alergii w postaci kataru, pokrzywki lub astmy
          oskrzelowej, po przyjęciu kwasu acetylosalicylowego lub innych niesteroidowych leków
          przeciwzapalnych. Trzeci trymestr ciąży.
        </MyTextView>
        <MyTextView>
          Nie stosować u dzieci poniżej 7 kg wagi oraz w przypadku ospy i odwodnienia!
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          dla płodu. Nie stosować w czasie porodu.
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C.</Hypertekst>
W okresie
          okołoporodowym kat. D. W okresie karmienia piersią stosować krótkotrwale.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <ListaTextView>Tabletki po 100, 200, 300, 400, 600 mg</ListaTextView>
        <ListaTextView>Czopki po 60, 125 mg</ListaTextView>
        <ListaTextView>Syrop: 20 mg/ml, 40 mg/ml</ListaTextView>
        <ListaTextView>Tabletki po 10 lub 25 mg.</ListaTextView>
        <ListaTextView>Syrop 2 mg/ml.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>200 – 800 mg p.o., max 3,2 g/24 h.</MyTextView>
        <MyTextView>U osób w podeszłym wieku stosować połowę dawki.</MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <ListaTextView>
          3 m.ż. - 12 lat: 20 - 30 mg/kg/24 h p.o. w 3 - 4 dawkach podzielonych.
        </ListaTextView>
        <ListaTextView>Nie stosować u dzieci poniżej 7 kg wagi ciała.</ListaTextView>
        <ListaTextView>
          Przy gorączce do 39 stopni zalecana dawka 5 mg/kg, zaś powyżej 39 stopni 10 mg/kg m.c.
        </ListaTextView>
        <ListaTextView>
          Maksymalna dawka dobowa ibuprofenu dla dzieci wynosi 20 - 30 mg/kg/dobę i nie powinna
          nigdy przekroczyć 40 mg/kg/dobę.
        </ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Doustnie:</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 30 min.</ListaTextView>
        <ListaTextView>Szczyt działania: 1 - 2 h</ListaTextView>
        <ListaTextView>Czas działania: 4 - 6 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Nudności, wymioty, zgaga, zaostrzenie astmy oskrzelowej, zaburzenia krzepnięcia.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Ratownik może podawać ten lek tylko w postaci tabletek. Działanie ibuprofenu na płód może
          spowodować przedwczesne zamknięcie przewodu tętniczego Botalla, co może prowadzić do
          rozwoju nadciśnienia płucnego u noworodka. Nie należy stosować ibuprofenu w ostatnich
          trzech miesiącach ciąży.
        </MyTextView>
        <MyTextView>
          Ampułka zawiera alkohol benzylowy, który może wywołać potencjalnie groźne dla życia
          zaburzenia oddychania u noworodków.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Ibuprofen;
