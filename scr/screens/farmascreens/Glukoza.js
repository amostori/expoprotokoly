import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Glukoza extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Zwiększenie poziomu cukru we krwi.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Hipoglikemia objawowa.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>W stanach zagrożenia życia brak.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Lek bezpieczny,
          <Hypertekst onPress={this.handleKodCiazy}>kategoria A.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka lub wlew kroplowy: 200 mg/ml (2 g/10 ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>1 ml/kg 20% roztworu czyli 0,2 g/kg iv</MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>2,5 ml/kg 20% roztworu czyli 0,5 g/kg i.v.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: szybki</ListaTextView>
        <ListaTextView>Szczyt działania: szybki</ListaTextView>
        <ListaTextView>Czas działania: krótki</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>Przy przedawkowaniu obrzęk płuc, hiperwolemia, zakrzepica żył.</MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Insulina może zmniejszyć poziom glukozy we krwi.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>2 g glukozy podnoszą poziom cukru o 15 - 20 mg%.</MyTextView>
        <MyTextView>
          Roztwory o stężeniach powyżej 15% działają miejscowo drażniąco, co może być przyczyną
          powstawania zmian zapalnych i bolesności w miejscu wstrzyknięcia.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Glukoza;
