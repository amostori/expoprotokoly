import React from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView, RedBoldText, Hypertekst, VersionDrug,
} from '../../components/TextViews';

class Papaweryna extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Działa rozkurczowo na mięśnie gładkie przewodu pokarmowego, dróg żółciowych oraz dróg
          moczowo-płciowych.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Wskaania</RedBoldText>
        </MyTextView>
        <MyTextView>
          W stanach skurczowych mięśni gładkich:
          {'\n'}
          {'\n'}
· przewodu pokarmowego: kolka żółciowa, stany skurczowe dróg żółciowych, kolka
          jelitowa
          {'\n'}
          {'\n'}
· dróg moczowych: kolka nerkowa, bolesne parcie na mocz.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Zaburzenia przewodnictwa w mięśniu sercowym. Nie należy stosować preparatu u dzieci.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria C.</Hypertekst>
          Lek można stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko
          niepożądanego działania u płodu.
          {'\n'}
          Ze względu na brak badań nie stosuj w okresie karmienia piersią.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 40 mg w 2 ml (20 mg/ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          Dorośli: od 40 mg do 120 mg (od 2 ml do 6 ml) im lub sc bez rozcieńczenia.
          {'\n'}
          {'\n'}
W razie konieczności dawka może być powtórzona po 3 godzinach, maksymalnie 4 razy na
          dobę.
          {'\n'}
          {'\n'}
U dzieci nie stosować.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>Działanie lecznicze preparatu trwa około 8 godzin.</MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>Podana dożylnie może wywołać zaburzenia rytmu serca.</MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Preparaty: 20 mg/ml (40 mg/2 ml) tylko do podawania domięśniowego.
          {' '}
          {'\n'}
          {'\n'}
          Papawerynę, alkaloid izochinolinowy opium, otrzymywano dawniej z maku ogrodowego, ale nie
          działa przeciwbólowo i nie prowadzi do rozwoju uzależnienia.
          {' '}
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Papaweryna;
