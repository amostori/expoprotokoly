import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Anestezja extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>I Leki nasenne</RedBoldText>
        </MyTextView>
        <BoldText>Tiopental</BoldText>
        <MyTextView>Lek nasenny, działający 5 - 10 min. Nie działa przeciwbólowo.</MyTextView>
        <BoldText>Przygotowanie fiolki 1g:</BoldText>
        <MyTextView>
          rozpuścić w 20 ml NaCl 0,9%. Powstanie roztwór zawierający 50 mg/ml leku. Nabrać wyliczoną
          dawkę do strzykawki i rozcieńczyć do 20 ml.
        </MyTextView>
        <BoldText>Przygotowanie fiolki 0,5 g:</BoldText>
        <MyTextView>
          rozpuścić w 10 ml NaCl 0,9%. Powstanie roztwór zawierający 50 mg/ml leku. Nabrać wyliczoną
          dawkę do strzykawki i rozcieńczyć do 20 ml.
        </MyTextView>
        <BoldText>Dawkowanie:</BoldText>
        <MyTextView>3 - 5 mg/kg</MyTextView>
        <BoldText>Uwagi:</BoldText>
        <MyTextView>
          Obniża ciśnienie tętnicze i śródczaszkowe dlatego szczególnie polecany w urazach czaszkowo
          - mózgowych, ale przeciwwskazany we wstrząsie. Podany pozanaczyniowo powoduje martwicę
          tkanki z powodu mocno zasadowego odczynu.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria C stosowania w ciąży.</Hypertekst>
        </MyTextView>
        <BoldText>Propofol</BoldText>
        <MyTextView>lek nasenny, działający 5 - 10 min. Nie działa przeciwbólowo.</MyTextView>
        <BoldText>Postać leku:</BoldText>
        <MyTextView>200 mg/20 ml</MyTextView>
        <BoldText>Przygotowanie</BoldText>
        <MyTextView>Bez rozcieńczenia.</MyTextView>
        <BoldText>Dawkowanie:</BoldText>
        <MyTextView>2 - 3 mg/kg</MyTextView>
        <BoldText>Uwagi:</BoldText>
        <MyTextView>
          Podobnie jak tiopental obniża ciśnienie tętnicze i śródczaszkowe dlatego szczególnie
          polecany w urazach czaszkowo - mózgowych, ale przeciwwskazany we wstrząsie. Iniekcja
          dożylna jest bolesna.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria B stosowania w ciąży.</Hypertekst>
        </MyTextView>
        <BoldText>Etomidat</BoldText>
        <MyTextView>Lek nasenny działający 4 - 6 minut. Nie działa przeciwbólowo.</MyTextView>
        <BoldText>Postać:</BoldText>
        <MyTextView>ampułka 20 mg/10 ml</MyTextView>
        <BoldText>Przygotowanie:</BoldText>
        <MyTextView>bez rozcieńczenia.</MyTextView>
        <BoldText>Dawkowanie:</BoldText>
        <MyTextView>0,3 mg/kg</MyTextView>
        <BoldText>Uwagi:</BoldText>
        <MyTextView>
          W przeciwieństwie do Propofolu i Tiopentalu nie obniża ciśnienia tętniczego, więc można go
          stosować u pacjenta we wstrząsie. Przeciwwskazany u dzieci &lt;10 r. ż. oraz u pacjentów
          uczulonych na soję.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria D stosowania w ciąży.</Hypertekst>
        </MyTextView>
        <BoldText>Ketamina</BoldText>
        <MyTextView>Lek nasenny i przeciwbólowy, działający do 20 min.</MyTextView>
        <BoldText>Postać:</BoldText>
        <MyTextView>fiolka 500 mg/10 ml lub 200 mg/20 ml</MyTextView>
        <BoldText>Przygotowanie:</BoldText>
        <MyTextView>bez rozcieńczenia.</MyTextView>
        <BoldText>Dawkowanie:</BoldText>
        <MyTextView>1 - 2 mg/kg</MyTextView>
        <BoldText>Uwagi:</BoldText>
        <MyTextView>
          Szczególnie zalecany w astmie (rozszerza oskrzela). Przeciwwskazany w urazach czaszkowo -
          mózgowych (zwiększa ciśnienie śródczaszkowe).
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria B stosowania w ciąży.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>II Leki zwiotczające</RedBoldText>
        </MyTextView>
        <BoldText>Sukcynylocholina (Chlorsuccillin)</BoldText>
        <BoldText>Postać:</BoldText>
        <MyTextView>fiolka 200 mg</MyTextView>
        <BoldText>Przygotowanie:</BoldText>
        <MyTextView>rozcieńczyć w 20 ml NaCl 0,9%, wtedy otrzymujemy 10 mg w 1 ml</MyTextView>
        <BoldText>Dawkowanie:</BoldText>
        <MyTextView>1 - 2 mg/kg</MyTextView>
        <BoldText>Uwagi:</BoldText>
        <MyTextView>
          Nie zalecany u dzieci i w sytuacjach, w których występuje hiperkaliemia (oparzenia,
          porażenia prądem). Działa około 5 min.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria C stosowania w ciąży.</Hypertekst>
        </MyTextView>
        <BoldText>Wekuronium (Norcuron)</BoldText>
        <BoldText>Postać:</BoldText>
        <MyTextView>fiolka 4 mg</MyTextView>
        <BoldText>Przygotowanie:</BoldText>
        <MyTextView>
          rozpuścić w dołączonym rozpuszczalniku ew. w wodzie do iniekcji w 1 ml. Otrzymujemy
          roztwór 4 mg/ml.
        </MyTextView>
        <BoldText>Dawkowanie:</BoldText>
        <MyTextView>0,1 - 0,2 mg/kg</MyTextView>
        <BoldText>Uwagi:</BoldText>
        <MyTextView>działa 20 - 40 min.</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria C stosowania w ciąży.</Hypertekst>
        </MyTextView>
      </ScrollContainer>
    );
  }
}

export default Anestezja;
