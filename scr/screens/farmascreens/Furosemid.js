import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Furosemid extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Zmniejsza żylny, obwodowy opór naczyniowy.</MyTextView>
        <MyTextView>Działa moczopędnie (diuretyk pętlowy).</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Niewydolność lewokomorowa - obrzęk płuc.</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Bezmocz, noworodki.</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Można stosować w ciąży wyłącznie w razie zdecydowanej konieczności -
          <Hypertekst onPress={this.handleKodCiazy}>kategoria C.</Hypertekst>
          Przenika do mleka matki, stosować jedynie w razie wyraźniej konieczności.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 20 mg w 2 ml (10 mg/ml).</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>
          0,5 - 1 mg/kg i.v. Ewentualnie dawka 2,5 razy większa niż stosowana przewlekle.
        </MyTextView>
        <MyTextView>W obrzęku płuc zwykle pierwsza dawka 40 mg iv.</MyTextView>
        <MyTextView>
          Uwaga, wg CHPL lek należy podawać z szybkością nie większą niż 4 mg/min (40 mg/10 min).
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>1 - 2 mg/kg i.v.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 5 min</ListaTextView>
        <ListaTextView>Szczyt działania: 20 - 60 min.</ListaTextView>
        <ListaTextView>Czas działania: 2 h.</ListaTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Domięśniowo</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 10 - 30 min</ListaTextView>
        <ListaTextView>Szczyt działania: nieznany</ListaTextView>
        <ListaTextView>Czas działania: 4 - 8 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Długotrwałe stosowanie powoduje hipokaliemię, hipomagnezemię, zasadowicę metaboliczną,
          nudności, wymioty, reakcje alergiczne, ostre zapalenie trzustki, żółtaczkę.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Podczas łącznego stosowania furosemidu i roztworów o obniżonym pH (np. roztworów glukozy)
          może wystąpić wytrącenie furosemidu.
        </MyTextView>
        <MyTextView>Lek należy podawać z szybkością nie większą niż 4 mg/min.</MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Furosemid;
