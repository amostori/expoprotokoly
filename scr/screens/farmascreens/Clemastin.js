import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Clemastin extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Lek przeciwhistaminowy. Działanie przeciwalergiczne.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Wstrząs anafilaktyczny</MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>Nie stosuj u dzieci</MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Stosować w okresie ciąży wyłącznie w przypadku zdecydowanej konieczności
          <Hypertekst onPress={this.handleKodCiazy}> - kategoria B.</Hypertekst>
          Nie należy karmić piersią podczas stosowania leku.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułka: 2 mg w 2 ml (1 mg/ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli:</BoldText>
        </MyTextView>
        <MyTextView>2 mg w 10 ml iv</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Doustnie</BoldText>
        </MyTextView>
        <ListaTextView>Początek działania: 15 - 60 min</ListaTextView>
        <ListaTextView>Szczyt działania: 1 - 2 h</ListaTextView>
        <ListaTextView>Czas działania: 8 - 16 h</ListaTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Dożylnie</BoldText>
        </MyTextView>
        <MyTextView>Brak danych</MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Spowolnienie psychoruchowe, zawroty i ból głowy, nudności, wymioty, suchość błony śluzowej
          jamy ustnej, skurcz oskrzeli, zaburzenia widzenia, nadwrażliwość na światło, osutka,
          rumień.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Ampułka clemastinum zawiera 96% etanolu i dlatego podawanie u dzieci nie jest zalecane.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Clemastin;
