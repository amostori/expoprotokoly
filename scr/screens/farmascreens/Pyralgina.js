import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollContainer from '../../components/container/ScrollContainer';
import {
  MyTextView,
  RedBoldText,
  BoldText,
  VersionDrug,
  ListaTextView,
  Hypertekst,
} from '../../components/TextViews';

class Pyralgina extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKodCiazy = () => {
    const { navigation } = this.props;
    navigation.navigate('KodCiazy');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <RedBoldText>Działanie</RedBoldText>
        </MyTextView>
        <MyTextView>Niesterydowy lek przeciwbólowy, przeciwzapalny i przeciwgorączkowy.</MyTextView>
        <MyTextView>
          <RedBoldText>Wskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Bóle różnego pochodzenia o dużym nasileniu. Gorączka, gdy inne metody leczenia są
          nieskuteczne.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Przeciwwskazania</RedBoldText>
        </MyTextView>
        <MyTextView>
          Ciąża i karmienie piersią. Niedociśnienie i zaburzenia krążenia. Pacjenci z rozpoznanym
          zespołem astmy analgetycznej lub znaną nietolerancją na leki przeciwbólowe objawiające się
          pokrzywką, obrzękiem naczynioworuchowym lub inną reakcją anafilaktoidalną na salicylany,
          paracetamol lub inne nieopioidowe leki przeciwbólowe, w tym niesteroidowe leki
          przeciwzapalne (NLZP), takie jak: diklofenak, ibuprofen, indometacyna lub naproksen.
          Zaburzenia czynności szpiku kostnego (np. po leczeniu cytostatykami) oraz zmiany w obrazie
          morfologicznym krwi (agranulocytoza, leukopenia, niedokrwistość). Ostra niewydolność nerek
          lub wątroby, ostra porfiria wątrobowa. Wrodzony niedobór dehydrogenazy
          glukozo-6-fosforanowej. Stosowanie leków z grupy pochodnych pirazolonu i pirazolidyny (np.
          fenylobutazon, propyfenazon).
        </MyTextView>
        <MyTextView>
          <RedBoldText>Ciąża i karmienie piersią</RedBoldText>
        </MyTextView>
        <MyTextView>
          Nie stosować w okresie ciąży i karmienia piersią. Istnieją doniesienia o zwiększonym
          ryzyku wystąpienia guza Wilmsa u potomstwa kobiet przyjmujących metamizol w ciąży.
          <Hypertekst onPress={this.handleKodCiazy}>Kategoria C.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <RedBoldText>Postać</RedBoldText>
        </MyTextView>
        <MyTextView>Ampułki: 2,5 g w 5 ml (500 mg/ml) lub 1 g w 2 ml (500 mg/ml)</MyTextView>
        <MyTextView>
          <RedBoldText>Dawkowanie</RedBoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Dorośli i młodzież &gt;15 lat (o masie ciała &gt;53 kg):</BoldText>
        </MyTextView>
        <MyTextView>
          Jednorazowa dawka od 0,5 g do 1 g (1 ml-2 ml) iv lub im. W razie konieczności można podać
          dawkę jednorazową wynoszącą 2,5 g (5 ml). Z powodu ryzyka spadku ciśnienia zachować
          ostrożność. Dożylne podawanie nie szybciej niż 500 mg/1 min. Przed podaniem dożylnym
          zaleca się wcześniejsze rozcieńczenie w stężeniu 1:10 (2 ml leku w 20 ml, 5 ml leku w 50
          ml). Dawkę jednorazową można powtarzać co 6 - 8h.
        </MyTextView>
        <MyTextView>
          <BoldText>Dzieci:</BoldText>
        </MyTextView>
        <MyTextView>Nie podawać.</MyTextView>
        <MyTextView>
          <RedBoldText>Farmakokinetyka</RedBoldText>
        </MyTextView>
        <MyTextView>Po podaniu dożylnym max stężenie po kilku minutach.</MyTextView>
        <MyTextView>
          <RedBoldText>Działania niepożądane</RedBoldText>
        </MyTextView>
        <MyTextView>
          Reakcja z nadwrażliwością (wykwity skórne, wstrząs), spadek ciśnienia.
        </MyTextView>
        <MyTextView>
          <RedBoldText>Odtrutka</RedBoldText>
        </MyTextView>
        <MyTextView>Brak</MyTextView>
        <MyTextView>
          <RedBoldText>Ciekawostki</RedBoldText>
        </MyTextView>
        <MyTextView>
          Preparat: Pyralgin 2,5 g/5 ml lub 1 g/2 ml. Po rozcieńczeniu może nastąpić zmiana i (lub)
          pogłębienie zabarwienia w kierunku barwy żółtej, bez zmiany właściwości produktu. Po
          przyjęciu bardzo dużych dawek, wydalanie kwasu rubazonowego może powodować czerwone
          zabarwienie moczu. Maksymalna dawka dobowa wynosi 5 g/dobę dla wstrzyknięć domięśniowych i
          dożylnych. Preparat został wycofany m.in. w Australii, Norwegii, Stanach Zjednoczonych,
          Danii, Szwecji, Irlandii.
        </MyTextView>
        <VersionDrug />
      </ScrollContainer>
    );
  }
}

export default Pyralgina;
