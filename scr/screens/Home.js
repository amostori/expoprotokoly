import React from 'react';
import {
  StatusBar, Linking, TouchableOpacity, View, ScrollView,
} from 'react-native';
import PropTypes from 'prop-types';
import { FloatingAction } from 'react-native-floating-action';
import { VersionTextView } from '../components/TextViews';
import styles from '../components/TextViews/styles';
import AllImagesPage from '../components/Images/HomeImageButton/AllImagesPage';
import FabImage from './image/FabImage';

const Home = ({ navigation }) => (
  // navigation przesyłamy jako props do AllImagesPage
  <View style={styles.containermainmenu}>
    <StatusBar backgroundColor="red" translucent={false} barStyle="light-content" />
    <ScrollView style={styles.flex}>
      <AllImagesPage navigation={navigation} />
      <TouchableOpacity
        onPress={() => Linking.openURL('mailto:amostori@op.pl?subject=Aplikacja Protokoły (22.0)')}
      >
        <VersionTextView>amostori@op.pl</VersionTextView>
      </TouchableOpacity>
    </ScrollView>
    <FloatingAction
      onPressMain={() => {
        navigation.navigate('CPRAssistant');
      }}
      // floatingIcon={<MaterialCommunityIcons name="needle" size={32} color="white" />}
      floatingIcon={<FabImage />}
      color="#ffc107"
      showBackground={false}
    />
  </View>
);

Home.propTypes = {
  navigation: PropTypes.object,
};
export default Home;
