import { Image } from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  MyTextView, BoldText, Hypertekst, VersionSzczeklikERC,
} from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class Benzodiazepiny extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleToksydrom = () => {
    const { navigation } = this.props;
    navigation.navigate('Toksydrom');
  };

  handleFlumazenil = () => {
    const { navigation } = this.props;
    navigation.navigate('Flumazenil');
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>
          Objawy zbliżone do
          <Hypertekst onPress={this.handleToksydrom}>toksydromu opioidowego:</Hypertekst>
          senność, wąskie źrenice, depresja oddechowa, zaburzenia równowagi.
        </MyTextView>
        <MyTextView>Leczenie objawowe.</MyTextView>
        <MyTextView>
          W skrajnie ciężkim zatruciu odtrutką jest
          <Hypertekst onPress={this.handleFlumazenil}>Flumazenil</Hypertekst>
w dawce 0,2 mg powoli
          iv powtarzać co 1 minutę do dawki całkowitej 1 mg. W ampułce jest 0,5 mg w 5 ml.
        </MyTextView>
        <MyTextView>
          Unikaj stosowania tego leku jeśli Benzodiazepiny były podane w celu opanowania drgawek.
          Istnieje niebezpieczeństwo, że drgawki wrócą.
        </MyTextView>
        <MyTextView>
          <BoldText>Uwaga!</BoldText>
          Nie stosować betablokerów! Może dojść do znacznej bradykardii i hipotensji (spadku
          ciśnienia tętniczego).
        </MyTextView>
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}

export default Benzodiazepiny;
