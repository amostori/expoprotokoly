import { Image, TouchableOpacity, Linking } from 'react-native';
import React from 'react';

import { MyTextView, BoldText } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const OsrodkiOstrychZatruc = () => (
  <WskazowkiContainer>
    <Image
      source={require('../wskazowkiScreens/images/stetoskop.png')}
      style={styles.image}
      resizeMode="contain"
    />
    <TouchableOpacity onPress={() => Linking.openURL('tel: +48586820404')}>
      <MyTextView>
        <BoldText>Gdańsk:</BoldText>
        (58) 682 04 04
      </MyTextView>
    </TouchableOpacity>
    <TouchableOpacity onPress={() => Linking.openURL('tel: +48124231122')}>
      <MyTextView>
        <BoldText>Kraków:</BoldText>
        (12) 423 11 22
      </MyTextView>
    </TouchableOpacity>
    <TouchableOpacity onPress={() => Linking.openURL('tel: +48817408983')}>
      <MyTextView>
        <BoldText>Lublin:</BoldText>
        (81) 740 89 83
      </MyTextView>
    </TouchableOpacity>
    <TouchableOpacity onPress={() => Linking.openURL('tel: +48426579900')}>
      <MyTextView>
        <BoldText>Łódź:</BoldText>
        (42) 657 99 00
      </MyTextView>
    </TouchableOpacity>
    <TouchableOpacity onPress={() => Linking.openURL('tel: +48618476946')}>
      <MyTextView>
        <BoldText>Poznań:</BoldText>
        (61) 847 69 46
      </MyTextView>
    </TouchableOpacity>
    <TouchableOpacity onPress={() => Linking.openURL('tel: +48178664025')}>
      <MyTextView>
        <BoldText>Rzeszów:</BoldText>
        (17) 866 40 25
      </MyTextView>
    </TouchableOpacity>
    <TouchableOpacity onPress={() => Linking.openURL('tel: +48323682116')}>
      <MyTextView>
        <BoldText>Sosnowiec - WSS nr 5:</BoldText>
        (32) 368 21 16
      </MyTextView>
    </TouchableOpacity>
    <TouchableOpacity onPress={() => Linking.openURL('tel: +48226196654')}>
      <MyTextView>
        <BoldText>Warszawa:</BoldText>
        (22) 619 66 54
      </MyTextView>
    </TouchableOpacity>
    <TouchableOpacity onPress={() => Linking.openURL('tel: +48713064841')}>
      <MyTextView>
        <BoldText>Wrocław:</BoldText>
        (71) 306 48 41
      </MyTextView>
    </TouchableOpacity>
  </WskazowkiContainer>
);

export default OsrodkiOstrychZatruc;
