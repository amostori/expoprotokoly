import { Image } from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';
import {
  MyTextView, Hypertekst, BoldText, VersionSzczeklikERC,
} from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class ZatrucieParacetamolem extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>
          Objawy: żółtaczka i nudności pojawiające się po kilku - kilkunastu godzinach.
        </MyTextView>
        <MyTextView>Dawka toksyczna: 150 mg/kg.</MyTextView>
        <MyTextView>
          <BoldText>Odtrutka:</BoldText>
N - Acetylocysteina (preparat ACC)
        </MyTextView>
        <MyTextView>Lek niedostępny w karetce.</MyTextView>
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}

export default ZatrucieParacetamolem;
