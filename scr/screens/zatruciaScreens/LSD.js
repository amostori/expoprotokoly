import { Image } from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  MyTextView, BoldText, Hypertekst, VersionSzczeklikERC,
} from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class LSD extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleDiazepam = () => {
    const { navigation } = this.props;
    navigation.navigate('Diazepam');
  };

  handleToksydrom = () => {
    const { navigation } = this.props;
    navigation.navigate('Toksydrom');
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <Image
          source={require('./images/lysiczka.jpg')}
          style={styles.bigImage}
          resizeMode="contain"
        />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>
          <BoldText>Łysiczka lancetowata</BoldText>
        </MyTextView>
        <MyTextView>
          LSD, podobnie jak substancje psychoaktywne zawarte w grzybkach halucynogennych (psylocyna,
          psylocybina) pobudzają receptory serotoninowe wywołując objawy jak w
          <Hypertekst onPress={this.handleToksydrom}>toksydromie cholinolitycznym.</Hypertekst>
        </MyTextView>
        <MyTextView>
          W przypadku LSD typowe dawki to około 300 &micro; (lub 100 z dodatkiem amfetaminy). Czas
          działania to 8 - 12 h.
        </MyTextView>
        <MyTextView>
          Halucynacje po grzybkach (rosnących w Polsce) występują po spożyciu około 30 sztuk.
        </MyTextView>
        <MyTextView>Leczenie objawowe.</MyTextView>
        <MyTextView>
          Leczenie pobudzenia psychoruchowego:
          <Hypertekst onPress={this.handleDiazepam}>Diazepam</Hypertekst>
5 - 10 mg iv. Można
          powtórzyć.
        </MyTextView>
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}

export default LSD;
