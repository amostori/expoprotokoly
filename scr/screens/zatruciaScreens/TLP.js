import { Image } from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { MyTextView, Hypertekst, VersionSzczeklikERC } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class TLP extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleNatrii = () => {
    const { navigation } = this.props;
    navigation.navigate('Natrii');
  };

  handleToksydrom = () => {
    const { navigation } = this.props;
    navigation.navigate('Toksydrom');
  };

  onHandleLignokaina = () => {
    const { navigation } = this.props;
    navigation.navigate('Lignokaina');
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>
          Objawy jak w
          <Hypertekst onPress={this.handleToksydrom}>toksydromie cholinolitycznym.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleNatrii}>Natrium Bicarbonicum</Hypertekst>
          0,5 ml/kg 8,4% roztworu i.v. przyspiesza eliminację tych leków z krwi.
        </MyTextView>
        <MyTextView>
          Komorowe zaburzenia rytmu leczyć
          <Hypertekst onPress={this.onHandleLignokaina}>Lignokainą</Hypertekst>
1 mg/kg i.v.
        </MyTextView>
        <MyTextView>
          Amiodaron jest przeciwwskazany ponieważ wydłuża odstęp QT (podobnie jak TLP).
        </MyTextView>
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}

export default TLP;
