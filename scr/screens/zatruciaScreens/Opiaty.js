import { Image } from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { MyTextView, Hypertekst, VersionSzczeklikERC } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class Opiaty extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleNalokson = () => {
    const { navigation } = this.props;
    navigation.navigate('Nalokson');
  };

  handleNaloksonDzieci = () => {
    const { navigation } = this.props;
    navigation.navigate('NaloksonDzieci');
  };

  handleToksydrom = () => {
    const { navigation } = this.props;
    navigation.navigate('Toksydrom');
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>
          Objawy jak w
          <Hypertekst onPress={this.handleToksydrom}>toksydromie opioidowym.</Hypertekst>
        </MyTextView>
        <MyTextView>
          Jeśli pacjent jest wydolny krążeniowo - oddechowo przetransportuj go na SOR.
        </MyTextView>
        <MyTextView>
          W przeciwnym wypadku podaj
          <Hypertekst onPress={this.handleNalokson}>Nalokson</Hypertekst>
          0,4 mg i.v. lub i.m. Można powtórzyć do dawki całkowitej 1,2 mg.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleNaloksonDzieci}>U dzieci dawka</Hypertekst>
          wynosi 0,01 mg/kg. Można raz powtórzyć w dawce 0,1 mg/kg i.v lub i.m.
        </MyTextView>
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}

export default Opiaty;
