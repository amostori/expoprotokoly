import { Image } from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { MyTextView, Hypertekst, VersionSzczeklikERC } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class Dopalacze extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleDiazepam = () => {
    const { navigation } = this.props;
    navigation.navigate('Diazepam');
  };

  handleToksydrom = () => {
    const { navigation } = this.props;
    navigation.navigate('Toksydrom');
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };


  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>
          Dopalacze to uzależniające środki psychoaktywne pochodzenia syntetycznego lub roślinnego.
        </MyTextView>
        <MyTextView>
          Objawy jak w
          <Hypertekst onPress={this.handleToksydrom}>toksydromie sympatykomimetycznym.</Hypertekst>
          pobudzenie lub senność, halucynacje, wymioty, biegunka, zaburzenia świadomości,
          oddychania, niebezpieczny wzrost lub spadek ciśnienia tętniczego krwi, hipoglikemia,
          drgawki.
        </MyTextView>
        <MyTextView>Leczenie objawowe.</MyTextView>
        <MyTextView>
          Leczenie pobudzenia psychoruchowego:
          <Hypertekst onPress={this.handleDiazepam}>Diazepam</Hypertekst>
5 - 10 mg iv. Można
          powtórzyć.
        </MyTextView>
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}

export default Dopalacze;
