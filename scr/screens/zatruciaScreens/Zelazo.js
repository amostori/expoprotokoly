import { Image } from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';
import { MyTextView, Hypertekst, VersionSzczeklikERC } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class Zelazo extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>10 mg/kg żelaza jest nietoksyczne.</MyTextView>
        <MyTextView>180 mg/kg dawka śmiertelna.</MyTextView>
        <MyTextView>Odtrutka: Deferoksamina, lek niedostępny w karetce.</MyTextView>
        <MyTextView>Rozważ transport do odpowiedniego szpitala.</MyTextView>
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}

export default Zelazo;
