import { Image } from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  MyTextView, BoldText, Hypertekst, VersionSzczeklikERC,
} from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class Amfetamina extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleDiazepam = () => {
    const { navigation } = this.props;
    navigation.navigate('Diazepam');
  };

  handleToksydrom = () => {
    const { navigation } = this.props;
    navigation.navigate('Toksydrom');
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>
          Objawy jak w
          <Hypertekst onPress={this.handleToksydrom}>toksydromie sympatykomimetycznym.</Hypertekst>
        </MyTextView>
        <MyTextView>
          Leczenie pobudzenia psychoruchowego:
          <Hypertekst onPress={this.handleDiazepam}>Diazepam</Hypertekst>
5 - 10 mg iv. Można
          powtórzyć.
        </MyTextView>
        <MyTextView>
          <BoldText>Uwaga!</BoldText>
          Nie stosować betablokerów! Może dojść do znacznej bradykardii i hipotensji (spadku
          ciśnienia tętniczego).
        </MyTextView>
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}

export default Amfetamina;
