import Amfetamina from './Amfetamina';
import Benzodiazepiny from './Benzodiazepiny';
import Betablokery from './Betablokery';
import CO from './CO';
import Cyjanki from './Cyjanki';
import Dopalacze from './Dopalacze';
import Digoksyna from './Digoksyna';
import LSD from './LSD';
import Metanol from './Metanol';
import Opiaty from './Opiaty';
import Rtec from './Rtec';
import TLP from './TLP';
import ZatrucieParacetamolem from './ZatrucieParacetamolem';
import Fosforany from './Fosforany';
import Zelazo from './Zelazo';
import OsrodkiOstrychZatruc from './OsrodkiOstrychZatruc';

export {
  Amfetamina,
  Benzodiazepiny,
  Betablokery,
  CO,
  Cyjanki,
  Digoksyna,
  Dopalacze,
  LSD,
  Metanol,
  Opiaty,
  Rtec,
  TLP,
  ZatrucieParacetamolem,
  Fosforany,
  Zelazo,
  OsrodkiOstrychZatruc,
};
