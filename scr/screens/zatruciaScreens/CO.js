import React from 'react';
import { Image } from 'react-native';
import PropTypes from 'prop-types';

import {
  MyTextView,
  VersionSzczeklikERC,
  BoldText,
  Sol,
  Hypertekst,
  ListaTextView,
} from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class CO extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>
          <BoldText>Uwaga, niebezpieczeństwo zatrucia ratowników!</BoldText>
        </MyTextView>
        <MyTextView>Podaj 100% tlen.</MyTextView>
        <MyTextView>
          Konieczny wlew 500 ml
          <Sol />
          (u dzieci 20 ml/kg).
        </MyTextView>
        <MyTextView>
          Rozważ transport bezpośrednio do komory hiperbarycznej, jeśli chory jest nieprzytomny.
        </MyTextView>
        <MyTextView>
          <BoldText>Objawy w zależności od stężenia CO w powietrzu:</BoldText>
        </MyTextView>
        <ListaTextView>
          0,01 - 0,02% (100 - 200 ppm): niewielki ból głowy przy ekspozycji przez 2 - 3 h.
        </ListaTextView>
        <ListaTextView>
          0,04% (400 ppm): silny ból głowy zaczynający się po około 1 h wdychania.
        </ListaTextView>
        <ListaTextView>
          0,08% (800 ppm): zawroty głowy, wymioty i drgawki po 45 min wdychania; po dwóch h trwała
          śpiączka.
        </ListaTextView>
        <ListaTextView>
          0,16% (1600 ppm): silny ból głowy, wymioty, drgawki po 20 min; zgon po 2 h.
        </ListaTextView>
        <ListaTextView>
          0,34% (3200 ppm): intensywny ból głowy i wymioty po 5 - 10 min; zgon po 30 min.
        </ListaTextView>
        <ListaTextView>
          0,64% (6400 ppm): ból głowy i wymioty po 1 - 2 min: zgon w niecałe 20 min.
        </ListaTextView>
        <ListaTextView>
          1,28% (12800 ppm): utrata przytomności po 2 - 3 wdechach; śmierć po 3 min.
        </ListaTextView>
        <MyTextView />
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}

export default CO;
