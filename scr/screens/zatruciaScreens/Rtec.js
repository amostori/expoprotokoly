import { Image } from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';
import { MyTextView, Hypertekst, VersionSzczeklikERC } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class Rtec extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>
          Doustne spożycie rtęci jest nieszkodliwe. Prowokacja wymiotów czy płukanie żołądka nie
          jest konieczne.
        </MyTextView>
        <MyTextView>Szkodliwe natomiast jest wdychanie rtęci.</MyTextView>
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}
export default Rtec;
