import { Image } from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { MyTextView, Hypertekst, VersionSzczeklikERC } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class Betablokery extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleToksydrom = () => {
    const { navigation } = this.props;
    navigation.navigate('Toksydrom');
  };

  handleAtropina = () => {
    const { navigation } = this.props;
    navigation.navigate('Atropina');
  };

  handleAdrenalina = () => {
    const { navigation } = this.props;
    navigation.navigate('Adrenalina');
  };

  handleGlukagon = () => {
    const { navigation } = this.props;
    navigation.navigate('Glukagon');
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>
          Objawy: ciężka bradykardia, oporna na leczenie, wysokie ryzyko zgonu.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleAtropina}>Atropina</Hypertekst>
          0,5 mg iv powtarzane do dawki całkowitej 3 mg. U dzieci dawka: 0,02 mg/kg iv
        </MyTextView>
        <MyTextView>
          W ciężkiej niewydolności krążenia: wlew
          <Hypertekst onPress={this.handleAdrenalina}>Adrenaliny</Hypertekst>
2 - 10 &micro;/min.
        </MyTextView>
        <MyTextView>
          Uwaga, zalecany jest
          <Hypertekst onPress={this.handleGlukagon}>Glukagon</Hypertekst>
          dożylnie, ale w Polsce brak preparatu, który można podać drogą dożylną.
        </MyTextView>
        <MyTextView>
          W przypadku zatrucia Ca - blokerami należy podać 10 ml 10% CaCl iv (na wyposażeniu karetki
          S lub LPR).
        </MyTextView>
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}

export default Betablokery;
