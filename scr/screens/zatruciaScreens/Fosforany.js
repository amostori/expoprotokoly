import { Image } from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  MyTextView, Hypertekst, VersionSzczeklikERC, BoldText,
} from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class Fosforany extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAtropina = () => {
    const { navigation } = this.props;
    navigation.navigate('Atropina');
  };

  handleAtropinaDzieci = () => {
    const { navigation } = this.props;
    navigation.navigate('AtropinaWskazowki');
  };

  handleAnafilaksja = () => {
    const { navigation } = this.props;
    navigation.navigate('Anafilaksja');
  };

  handleToksydrom = () => {
    const { navigation } = this.props;
    navigation.navigate('Toksydrom');
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>
          Objawy jak w
          <Hypertekst onPress={this.handleToksydrom}>toksydromie cholinergicznym.</Hypertekst>
          Znaczna ilość wydzieliny w drzewie oskrzelowym przypomina obraz obrzęku płuc.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleAtropina}>Atropina 2 mg iv lub io</Hypertekst>
        </MyTextView>
        <MyTextView>
          Powtarzaj do poprawy stanu pacjenta czyli tak, by nie było konieczności odsysania częściej
          niż raz na godzinę, a akcja serca pozostawała &gt;80/min.
        </MyTextView>
        <MyTextView>Zwykle wystarczy mniej niż 20 - 30 mg/dobę.</MyTextView>
        <MyTextView>
          Nadmierna suchość błon śluzowych i akcja serca &gt;120/min świadczy o podaniu zbyt dużej
          dawki atropiny.
        </MyTextView>
        <MyTextView>
          U dzieci powtarzać dawkę po
          <Hypertekst onPress={this.handleAtropinaDzieci}>0,02 mg/kg.</Hypertekst>
        </MyTextView>
        <MyTextView>
          Jeśli brak efektu rozważ
          <BoldText>Obidoksym</BoldText>
          {' '}
w dawce 1 - 6 mg/kg iv, zawsze po wstępnej dawce atropiny.
          (preparaty Toxobidin, Toxogonine)
        </MyTextView>
        <MyTextView>
          Lek jest niedostępny w karetce - konieczny szybki transport do odpowiedniego szpitala.
        </MyTextView>
        <MyTextView>
          W razie znacznego pobudzenia, drgawek podaj diazepam 5 mg iv, a w razie znacznego skurczu
          oskrzeli adrenalinę domięśniowo, jak w
          <Hypertekst onPress={this.handleAnafilaksja}>Anafilaksji.</Hypertekst>
        </MyTextView>
        <MyTextView>
          <BoldText>Uwaga! Nie podawaj morfiny, furosemidu, hydrokortyzonu i teofiliny.</BoldText>
        </MyTextView>
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}

export default Fosforany;
