import React from 'react';
import { Image } from 'react-native';
import PropTypes from 'prop-types';
import {
  MyTextView, BoldText, VersionSzczeklikERC, Hypertekst,
} from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class Cyjanki extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOOZ = () => {
    const { navigation } = this.props;
    navigation.navigate('OsrodkiOstrychZatruc');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image source={require('./images/toxic.png')} style={styles.image} resizeMode="contain" />
        <MyTextView>
          <Hypertekst onPress={this.handleOOZ}>Ośrodek Ostrych Zatruć</Hypertekst>
        </MyTextView>
        <MyTextView>
          Zatrucie cyjankami może zdarzyć się w czasie pożaru w pomieszczeniach. Tworzywa sztuczne
          paląc się wydzielają cyjanek.
        </MyTextView>
        <MyTextView>
          <BoldText>Odtrutka:</BoldText>
          Hydroksykobalamina (preparat Cyanokit).
        </MyTextView>
        <MyTextView>Lek niedostępny w karetce.</MyTextView>
        <VersionSzczeklikERC />
      </WskazowkiContainer>
    );
  }
}

export default Cyjanki;
