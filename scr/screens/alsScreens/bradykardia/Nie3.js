import React from 'react';
import { View } from 'react-native';
import { MyTextView, VersionERC } from '../../../components/TextViews';

const Nie3 = () => (
  <View>
    <MyTextView>Transport na SOR</MyTextView>
    <VersionERC />
  </View>
);

export default Nie3;
