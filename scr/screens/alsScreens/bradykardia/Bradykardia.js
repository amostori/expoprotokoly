import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ScrollContainer } from '../../../components/container';
import {
  AskTextView, MyTextView, ListaTextView, VersionERC,
} from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import Tak from './Tak';

export default class Bradykardia extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { navigation } = this.props;
    const { divy } = this.state;
    const Nie = (
      <View>
        <MyTextView>Transport na SOR</MyTextView>
        <VersionERC />
      </View>
    );
    return (
      <ScrollContainer>
        <AskTextView>Występują objawy niepokojące?</AskTextView>
        <ListaTextView>1. Wstrząs?</ListaTextView>
        <ListaTextView>2. Omdlenie?</ListaTextView>
        <ListaTextView>3. Niedokrwienie mięśnia sercowego?</ListaTextView>
        <ListaTextView>4. Niewydolność serca?</ListaTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<Tak navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(Nie)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
