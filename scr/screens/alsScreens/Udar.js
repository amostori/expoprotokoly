import React from 'react';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView,
  BoldText,
  TiM,
  ListaTextView,
  VersionTextView,
} from '../../components/TextViews';

const Udar = () => (
  <ScrollContainer>
    <MyTextView>Jeśli saturacja &lt;94% podaj tlen.</MyTextView>
    <MyTextView>
      Jeśli występują zaburzenia oddychania rozważ wezwanie karetki S lub LPR - może być konieczna
      intubacja.
    </MyTextView>
    <MyTextView>Kontroluj poziom glukozy.</MyTextView>
    <MyTextView>Rozpocznij powolny wlew krystaloidów w celu utrzymania wkłucia.</MyTextView>
    <MyTextView>
      W przypadku udaru mózgu obniżanie ciśnienia nie zawsze jest korzystne. Wymagana jest
      wcześniejsza konsultacja z lekarzem oddziału udarowego.
    </MyTextView>
    <TiM />
    <BoldText>
      Uwaga! Jeśli od początku objawów nie upłynęło 6 h należy transportować pacjenta na sygnałach i
      powiadomić szpital docelowy o sytuacji.
    </BoldText>

    <MyTextView />
    <VersionTextView>
      Na podstawie dokumentu Ministerstwa Zdrowia Udar mózgu: dobre praktyki postępowania z
      pacjentem.
    </VersionTextView>
  </ScrollContainer>
);

export default Udar;
