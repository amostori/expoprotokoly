import BadanieScreen from './BadanieScreen';
import NzkAlsScreen from './nzk/NzkAlsScreen';
import NzkHipotermia from './nzkHipotermia/NzkHipotermia';
import Hipotermia from './hipotermia/Hipotermia';
import Bradykardia from './bradykardia/Bradykardia';
import Czestoskurcz from './czestoskurcz/Czestoskurcz';
import Ozw from './ozw/Ozw';
import Nadcisnienie from './nadcisnienie/Nadcisnienie';
import Dusznosc from './dusznosc/Dusznosc';
import Obrzek from './obrzek/Obrzek';
import Anafilaksja from './Anafilaksja';
import Astma from './Astma';
import BolBrzucha from './BolBrzucha';
import Hipoglikemia from './Hipoglikemia';
import Hiperglikemia from './Hiperglikemia';
import Padaczka from './padaczka/Padaczka';
import Udar from './Udar';
import BolPlecow from './BolPlecow';
import ZaburzeniaPsychiczne from './psychika/ZaburzeniaPsychiczne';
import BolGlowy from './BolGlowy';

export {
  BadanieScreen,
  NzkAlsScreen,
  NzkHipotermia,
  Hipotermia,
  Bradykardia,
  Czestoskurcz,
  Ozw,
  Nadcisnienie,
  Dusznosc,
  Obrzek,
  Anafilaksja,
  Astma,
  BolBrzucha,
  Hipoglikemia,
  Hiperglikemia,
  Padaczka,
  Udar,
  BolPlecow,
  ZaburzeniaPsychiczne,
  BolGlowy,
};
