import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import {
  MyTextView, Hypertekst, VersionERC, BoldText,
} from '../../../components/TextViews';

export default class WaskieNiemiarowy extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleMetoprolol = () => {
    const { navigation } = this.props;
    navigation.navigate('Metoprolol');
  };

  handleAmiodaron = () => {
    const { navigation } = this.props;
    navigation.navigate('Amiodaron');
  };

  render() {
    return (
      <View>
        <MyTextView>Możliwe przyczyny:</MyTextView>

        <MyTextView>
          <BoldText>Migotanie przedsionków</BoldText>
- jeśli trwa dłużej niż 48 h wymagana
          konsultacja specjalistyczna.
        </MyTextView>
        <MyTextView>
          W celu poprawy stanu pacjenta i zwolnienia akcji serca rozważ
          <Hypertekst onPress={this.handleMetoprolol}>Metoprolol</Hypertekst>
5 mg iv z szybkością 1
          mg/min. Można powtarzać co 5 min po 5 mg do dawki całkowitej 20 mg.
        </MyTextView>
        <MyTextView>
          Uwaga, monitorować EKG i wstrzymać się przed kolejną dawką jeśli RR poniżej 90 mmHg, akcja
          serca poniżej 40/min, lub odcinek PQ dłuższy niż 0,26 s.
        </MyTextView>
        <MyTextView>
          Jeśli migotanie przedsionków trwa krócej niż 48 h podaj 300 mg
          <Hypertekst onPress={this.handleAmiodaron}>Amiodaronu</Hypertekst>
w 5% glukozie we wlewie
          trwającym 20 min.
        </MyTextView>
        <MyTextView>
          <BoldText>Trzepotanie przedsionków ze zmiennym blokiem - </BoldText>
          wymagana konsultacja specjalistyczna. W celu poprawy stanu pacjenta i zwolnienia akcji
          serca rozważ Metoprolol.
        </MyTextView>

        <VersionERC />
      </View>
    );
  }
}
