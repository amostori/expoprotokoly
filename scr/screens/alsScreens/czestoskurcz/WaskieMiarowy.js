import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { MyTextView, Hypertekst, VersionERC } from '../../../components/TextViews';

export default class WaskieMiarowy extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleVagal = () => {
    const { navigation } = this.props;
    navigation.navigate('Vagal');
  };

  handleAdenozyna = () => {
    const { navigation } = this.props;
    navigation.navigate('Adenozyna');
  };

  handleMetoprolol = () => {
    const { navigation } = this.props;
    navigation.navigate('Metoprolol');
  };

  render() {
    return (
      <View>
        <MyTextView>
          <Hypertekst onPress={this.handleVagal}>Stymulacja nerwu błędnego</Hypertekst>
        </MyTextView>

        <MyTextView>
          Jeśli brak poprawy
          <Hypertekst onPress={this.handleAdenozyna}>Adenozyna</Hypertekst>
6 mg w szybkim bolusie.
          Jeśli nieskuteczna można dwukrotnie powtórzyć po 12 mg.
        </MyTextView>
        <MyTextView>
          W przypadku powrotu rytmu zatokowego prawdopodobnie był to nawrotny częstoskurcz węzłowy
          (AVNRT) lub nawrotny częstoskurcz przedsionkowo - komorowy (AVRT).
        </MyTextView>
        <MyTextView>
          W przeciwnym razie przyczyną jest prawdopodobnie trzepotanie przedsionków. Konieczna
          konsultacja specjalistyczna.
        </MyTextView>
        <MyTextView>
          W celu poprawy stanu pacjenta rozważ
          <Hypertekst onPress={this.handleMetoprolol}>Metoprolol</Hypertekst>
5 mg iv z szybkością 1
          mg/min. Można powtarzać co 5 min po 5 mg do dawki całkowitej 20 mg.
        </MyTextView>
        <MyTextView>
          Uwaga, monitorować EKG i wstrzymać się przed kolejną dawką jeśli RR poniżej 90 mmHg, akcja
          serca poniżej 40/min, lub odcinek PQ dłuższy niż 0,26 s.
        </MyTextView>
        <VersionERC />
      </View>
    );
  }
}
