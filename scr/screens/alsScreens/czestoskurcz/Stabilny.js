import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ButtonsYesNo } from '../../../components/Buttons';
import { AskTextView, MyTextView, BoldText } from '../../../components/TextViews';
import Szerokie from './Szerokie';
import Waskie from './Waskie';

export default class Stabilny extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { navigation } = this.props;
    const { divy } = this.state;

    return (
      <View>
        <BoldText>STABILNY</BoldText>
        <MyTextView />
        <MyTextView>Konsultacja specjalistyczna - monitoring i transport na SOR.</MyTextView>
        <MyTextView>Rozważ dalsze postępowanie.</MyTextView>
        <AskTextView>
          Szerokość QRS powyżej 110 ms (3 mm na papierze o przesuwie 25 mm/s)?
        </AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<Szerokie navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<Waskie navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
