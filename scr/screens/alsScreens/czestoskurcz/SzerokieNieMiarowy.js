import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import {
  MyTextView, BoldText, Hypertekst, VersionERC,
} from '../../../components/TextViews';

export default class SzerokieNieMiarowy extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAdenozyna = () => {
    const { navigation } = this.props;
    navigation.navigate('Adenozyna');
  };

  handleMagnez = () => {
    const { navigation } = this.props;
    navigation.navigate('Magnez');
  };

  handleLignokaina = () => {
    const { navigation } = this.props;
    navigation.navigate('Lignokaina');
  };

  render() {
    return (
      <View>
        <MyTextView>Możliwe przyczyny:</MyTextView>
        <MyTextView>
          <BoldText>Częstoskurcz komorowy wielokształtny</BoldText>
          (Torsade de Pointes) - podaj
          <Hypertekst onPress={this.handleMagnez}>Siarczan Magnezu</Hypertekst>
2 g iv przez 10 min
          i/lub
          <Hypertekst onPress={this.handleLignokaina}>Lignokaina</Hypertekst>
          <BoldText>Uwaga!</BoldText>
          Amiodaron przeciwwskazany.
        </MyTextView>
        <MyTextView>
          <BoldText>Migotanie przedsionków z blokiem odnogi - </BoldText>
          konsultacja specjalistyczna.
        </MyTextView>

        <MyTextView>
          <BoldText>Migotanie przedsionków z zespołem preekscytacji -</BoldText>
          konsultacja specjalistyczna.
          <BoldText>Uwaga!</BoldText>
          unikaj
          <Hypertekst onPress={this.handleAdenozyna}>Adenozyny,</Hypertekst>
          Werapamilu, Diltiazemu i Digoksyny - ryzyko wystąpienia groźnego dla życia częstoskurczu.
        </MyTextView>
        <VersionERC />
      </View>
    );
  }
}
