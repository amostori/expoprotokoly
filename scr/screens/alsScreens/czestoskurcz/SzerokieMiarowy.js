import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import {
  MyTextView, BoldText, Hypertekst, VersionERC,
} from '../../../components/TextViews';

export default class SzerokieMiarowy extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleVagal = () => {
    const { navigation } = this.props;
    navigation.navigate('Vagal');
  };

  handleAdenozyna = () => {
    const { navigation } = this.props;
    navigation.navigate('Adenozyna');
  };

  render() {
    return (
      <View>
        <MyTextView>Możliwe przyczyny:</MyTextView>
        <MyTextView>
          <BoldText>Częstoskurz komorowy (VT)</BoldText>
- podaj amiodaron 300 mg w 250 ml 5% glukozy
          w przeciągu 20 - 60 min.
        </MyTextView>
        <MyTextView>
          <BoldText>Częstoskurcz nadkomorowy z blokiem odnogi - </BoldText>
          <Hypertekst onPress={this.handleVagal}>stymulacja nerwu błędnego.</Hypertekst>
        </MyTextView>
        <MyTextView>
          Rozważ
          <Hypertekst onPress={this.handleAdenozyna}>Adenozynę</Hypertekst>
6 mg, kolejne dawki 12
          mg, 12 mg.
        </MyTextView>
        <VersionERC />
      </View>
    );
  }
}
