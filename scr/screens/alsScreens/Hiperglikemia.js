import React from 'react';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView,
  TiM,
  ListaTextView,
  Sol,
  BoldText,
  VersionSzczeklikERC,
} from '../../components/TextViews';

const Hiperglikemia = () => (
  <ScrollContainer>
    <MyTextView>Podaj tlen jeśli saturacja &lt;90%.</MyTextView>
    <MyTextView>
      Rozpocznij wlew
      <Sol />
      iv
    </MyTextView>
    <TiM />
    <MyTextView>
      <BoldText>Postacie ostrych powikłań cukrzycy w przebiegu hiperglikemii</BoldText>
    </MyTextView>
    <MyTextView>
      <ListaTextView>
        1. Cukrzycowa kwasica ketonowa: glikemia &gt;250 mg/dl, obecność ciał ketonowych w moczu lub
        w surowicy. Często jest to pierwsza manifestacja cukrzycy typu I.
      </ListaTextView>
    </MyTextView>
    <MyTextView>
      <ListaTextView>
        2. Stan hiperglikemiczno - hipermolalny: glikemia &gt;600 mg/dl, brak ciał ketonowych w
        surowicy. Zwykle występuje w cukrzycy typu II, w przypadku jej opóźnionego rozpoznania.
      </ListaTextView>
    </MyTextView>
    <MyTextView>
      <ListaTextView>
        3. Kwasica mleczanowa: glikemia nieznacznie podwyższona, czasami prawidłowa, stężenie kwasu
        mlekowego w surowicy &gt;5 mmol/l. Pojawia się u chorych na cukrzycę pod wpływem różnych
        czynników (np. wstrząs, spożycie alkoholu).
      </ListaTextView>
    </MyTextView>
    <MyTextView>
      <BoldText>Typowe objawy</BoldText>
    </MyTextView>
    <MyTextView>
      Zaburzenia świadomości, aż do śpiączki, tachykardia, przyspieszony i płytki oddech,
      odwodnienie, często hipotensja.
    </MyTextView>
    <MyTextView>W wywiadzie wielomocz, nadmierne pragnienie, osłabienie.</MyTextView>
    <VersionSzczeklikERC />
  </ScrollContainer>
);

export default Hiperglikemia;
