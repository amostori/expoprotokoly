import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { MyTextView, Hypertekst, VersionERC } from '../../../components/TextViews';

export default class Tak extends React.Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleAdrenalina = () => {
    const { navigation } = this.props;
    // destrukturyzacja props przesłanych z NzkAlsScreen. Jednym z elementów musi być "navigation"
    navigation.navigate('Adrenalina');
  };

  handleEnergiaDefi = () => {
    const { navigation } = this.props;
    navigation.navigate('EnergiaDefibrylacji');
  };

  handleAmiodaron = () => {
    const { navigation } = this.props;
    navigation.navigate('Amiodaron');
  };

  handleLignokaina = () => {
    const { navigation } = this.props;
    navigation.navigate('Lignokaina');
  };

  handle4H4T = () => {
    const { navigation } = this.props;
    navigation.navigate('Screen4H4T');
  };

  render() {
    return (
      <View>
        <MyTextView>
          Wykonaj defibrylację odpowiednią
          <Hypertekst onPress={this.handleEnergiaDefi}>energią.</Hypertekst>
          Następnie kontynuuj resuscytację. Co 2 minuty ponownie oceniaj rytm.
        </MyTextView>
        <MyTextView>
          Po trzeciej defibrylacji podaj 1 mg
          <Hypertekst onPress={this.handleAdrenalina}>Adrenaliny</Hypertekst>
          iv lub io i 300 mg
          <Hypertekst onPress={this.handleAmiodaron}>Amiodaronu</Hypertekst>
w 20 ml 5% glukozy.
        </MyTextView>
        <MyTextView>
          Adrenalinę powtarzaj co drugą pętlę. Amiodaron możesz raz powtórzyć w dawce 150 mg
        </MyTextView>
        <MyTextView>
          W przypadku braku Amiodaronu lub gdy jest przeciwskazany jak w Torsade de Pointes podaj
          <Hypertekst onPress={this.handleLignokaina}>Lignokainę</Hypertekst>
          100 mg iv (10 ml 1% roztworu). Druga dawka 50 mg (5 ml 1% roztworu).
        </MyTextView>
        <MyTextView>
          Rozważ
          <Hypertekst onPress={this.handle4H4T}>4H i 4T</Hypertekst>
        </MyTextView>
        <VersionERC />
      </View>
    );
  }
}
