import React from 'react';
import PropTypes from 'prop-types';
import { ButtonsYesNo } from '../../../components/Buttons';
import { MyTextView, AskTextView } from '../../../components/TextViews';
import { ScrollContainer } from '../../../components/container';
import Nie from './Nie';
import Tak from './Tak';

class NzkAlsScreen extends React.Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnYesClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleOnNoClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleAdrenalina = () => {
    const { navigation } = this.props;
    navigation.navigate('NzkPediatricScreen');
  };

  render() {
    const { divy } = this.state;
    const { navigation } = this.props;

    return (
      <ScrollContainer>
        <MyTextView>Oceń rytm serca najszybciej jak to możliwe.</MyTextView>
        <MyTextView>
          Do czasu przyłożenia łyżek defibrylatora prowadź resuscytację krążeniowo oddechową.
        </MyTextView>
        <AskTextView>Rytm do defibrylacji?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnYesClick(<Tak navigation={navigation} />)}
          onPressNo={() => this.handleOnNoClick(<Nie navigation={navigation} />)}
          // trzeba przesłać jako props "navigation" !!!! inaczej błąd w <Nie/> że "navigation"
          // jest niezdefiniowany
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}

export default NzkAlsScreen;
