import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../../components/container';
import { MyTextView, AskTextView } from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import Ponizej180 from './Ponizej180';
import Powyzej180 from './Powyzej180';

export default class Nadcisnienie extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleASA = () => {
    const { navigation } = this.props;
    navigation.navigate('ASA');
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const { navigation } = this.props;
    return (
      <ScrollContainer>
        <AskTextView>RR skurczowe &gt;180 mmHg?</AskTextView>
        <MyTextView>lub</MyTextView>
        <AskTextView>
          RR skurczowe &gt;140 mmHg i cechy uszkodzenia narządów (krwawienie wewnątrzaszkowe, obrzęk
          płuc, OZW, rozwarstwienie aorty, rzucawka)?
        </AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<Powyzej180 navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<Ponizej180 navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
