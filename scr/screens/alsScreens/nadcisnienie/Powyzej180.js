import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../../components/container';
import {
  MyTextView,
  Hypertekst,
  VersionTextView,
  BoldText,
  ListaTextView,
} from '../../../components/TextViews';

export default class Powyzej180 extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleUrapidil = () => {
    const { navigation } = this.props;
    navigation.navigate('Urapidil');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <BoldText>STAN NAGLĄCY</BoldText>
        </MyTextView>
        <MyTextView>Ułóż pacjenta w pozycji półleżącej.</MyTextView>
        <MyTextView>
          Podaj
          <Hypertekst onPress={this.handleUrapidil}>Urapidil (Ebrantil, Tachyben)</Hypertekst>
          10 - 50 mg powoli iv - zacznij od 10 mg (ampułka zawiera 25 mg w 5 ml).
        </MyTextView>
        <MyTextView>W razie potrzeby dawkę można powtórzyć po 5 min.</MyTextView>
        <MyTextView>Pełen monitoring i transport do szpitala</MyTextView>
        <ListaTextView>
          W przypadku gdy podejrzewasz udar mózgu obniżanie ciśnienia nie zawsze jest korzystne.
          Postępowanie uzależnione jest od rodzaju udaru:
        </ListaTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Udar niedokrwienny:</BoldText>
          <ListaTextView>
            Ostrożnie obniżaj ciśnienie jeśli wartość skurczowego przekracza 220 mmHg lub
            rozkurczowego 120 mmHg.
          </ListaTextView>
        </MyTextView>
        <MyTextView>
          <BoldText>Udar krwotoczny:</BoldText>
          <ListaTextView>
            Ostrożnie obniżaj gdy ciśnienie skurczowe przekracza 180 mmHg lub rozkurczowe 105 mmHg.
          </ListaTextView>
        </MyTextView>
        <MyTextView>
          W przypadku podejrzenia zatoru jednej z tętnic kończyn górnych lub dolnych (ból,
          osłabienie kończyny, chłodna, marmurkowata skóra) ciśnienie tętnicze rośnie by umożliwić
          przedostanie się krwi przez miejsce zwężenia. Obniżanie ciśnienia w takiej sytuacji może
          być niekorzystne!
        </MyTextView>
        <VersionTextView>
          Na podstawie &quot;Interny Szczeklika&quot; pod redakcją Piotra Gajewskiego.
        </VersionTextView>
      </ScrollContainer>
    );
  }
}
