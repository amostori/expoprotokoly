import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../../components/container';
import {
  MyTextView,
  Hypertekst,
  VersionTextView,
  BoldText,
  ListaTextView,
} from '../../../components/TextViews';

export default class Ponizej180 extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleCaptopril = () => {
    const { navigation } = this.props;
    navigation.navigate('Captopril');
  };

  handleHydroxyzyna = () => {
    const { navigation } = this.props;
    navigation.navigate('Hydroxyzyna');
  };

  handleFurosemid = () => {
    const { navigation } = this.props;
    navigation.navigate('Furosemid');
  };

  handleDiazepam = () => {
    const { navigation } = this.props;
    navigation.navigate('Diazepam');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <BoldText>STAN PILNY</BoldText>
        </MyTextView>
        <MyTextView>
          Charakteryzuje się podwyższonym ciśnieniem tętniczym, bez towarzyszących, postępujących
          powikłań narządowych.
        </MyTextView>
        <MyTextView>
          <BoldText>Typowe objawy:</BoldText>
          ciśnienie skurczowe 141 - 180 mmHg, ból głowy, niepokój, krwawienie z nosa.
        </MyTextView>
        <MyTextView>
          Podaj
          <Hypertekst onPress={this.handleCaptopril}>Captopril</Hypertekst>
          12,5 mg sl oraz
          <Hypertekst onPress={this.handleDiazepam}>Diazepam</Hypertekst>
5 mg iv lub
          <Hypertekst onPress={this.handleHydroxyzyna}>Hydroksyzynę</Hypertekst>
          10 - 25 mg p.o. lub 50 mg i.m.
        </MyTextView>
        <MyTextView>
          W razie potrzeby dodatkowo podaj
          <Hypertekst onPress={this.handleFurosemid}>Furosemid</Hypertekst>
          20 mg iv
        </MyTextView>
        <MyTextView>Pełen monitoring i transport do szpitala</MyTextView>
        <ListaTextView>
          W przypadku gdy podejrzewasz udar mózgu obniżanie ciśnienia nie zawsze jest korzystne.
          Konieczna jest konsultacja z lekarzem z oddziału udarowego. Postępowanie uzależnione jest
          od rodzaju udaru:
        </ListaTextView>
        <MyTextView />
        <MyTextView>
          <BoldText>Udar niedokrwienny:</BoldText>
          <ListaTextView>
            Ostrożnie obniżaj ciśnienie jeśli wartość skurczowego przekracza 220 mmHg lub
            rozkurczowego 120 mmHg.
          </ListaTextView>
        </MyTextView>
        <MyTextView>
          <BoldText>Udar krwotoczny:</BoldText>
          <ListaTextView>
            Ostrożnie obniżaj gdy ciśnienie skurczowe przekracza 180 mmHg lub rozkurczowe 105 mmHg.
          </ListaTextView>
        </MyTextView>
        <MyTextView>
          W przypadku podejrzenia zatoru jednej z tętnic kończyn górnych lub dolnych (ból,
          osłabienie kończyny, chłodna, marmurkowata skóra) ciśnienie tętnicze rośnie by umożliwić
          przedostanie się krwi przez miejsce zwężenia. Obniżanie ciśnienia w takiej sytuacji może
          być niekorzystne!
        </MyTextView>
        <VersionTextView>
          Na podstawie &quot;Interny Szczeklika&quot; pod redakcją Piotra Gajewskiego.
        </VersionTextView>
      </ScrollContainer>
    );
  }
}
