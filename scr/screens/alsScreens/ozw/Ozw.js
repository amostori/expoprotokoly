/* eslint-disable lines-between-class-members */
/* eslint-disable linebreak-style */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../../components/container';
import {
  MyTextView, Hypertekst, AskTextView, ListaTextView,
} from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import Nitro from './Nitro';
import NoNitro from './NoNitro';

export default class Ozw extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleASA = () => {
    const { navigation } = this.props;
    navigation.navigate('ASA');
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const { navigation } = this.props;
    return (
      <ScrollContainer>
        <MyTextView>
          Podaj tlen jeśli saturacja jest poniżej 94% (88% w POCHP) lub pacjent odczuwa duszność lub
          występuje niewydolność krążenia.
        </MyTextView>
        <MyTextView>Wykonaj teletransmisję EKG.</MyTextView>
        <MyTextView>
          Jeśli pacjent nie jest uczulony na salicylany i nie zażywa przewlekle podaj 300 mg
          <Hypertekst onPress={this.handleASA}>kwasu acetylosalicylowego</Hypertekst>
          do pogryzienia.
        </MyTextView>
        <AskTextView>Występują przeciwwskazania do podania nitrogliceryny?</AskTextView>
        <MyTextView />
        <ListaTextView>1. RR poniżej 90 mmHg?</ListaTextView>
        <ListaTextView>2. Zawał ściany dolnej i prawej?</ListaTextView>
        <ListaTextView>3. Bradykardia lub częstoskurcz?</ListaTextView>
        <ListaTextView>
          4. Pacjent zażywał inhibitory fosfodiesterazy stosowane w leczeniu zaburzeń wzwodu
          (syldenafil, wardenafil i tadalafil np. viagra) w ciągu ostatnich 24 h?
        </ListaTextView>
        <MyTextView />
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<NoNitro navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<Nitro navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
