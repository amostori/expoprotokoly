/* eslint-disable linebreak-style */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../../components/container';
import { MyTextView, Hypertekst, VersionTextView } from '../../../components/TextViews';

export default class NoNitro extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handlePyralgina = () => {
    const { navigation } = this.props;
    navigation.navigate('Pyralgina');
  };

  handleNitro = () => {
    const { navigation } = this.props;
    navigation.navigate('Nitrogliceryna');
  };

  handleMorfina = () => {
    const { navigation } = this.props;
    navigation.navigate('Morfina');
  };

  handleHeparyna = () => {
    const { navigation } = this.props;
    navigation.navigate('Heparyna');
  };

  handleBrylic = () => {
    const { navigation } = this.props;
    navigation.navigate('Tikagrelor');
  };

  handleKlopidogrel = () => {
    const { navigation } = this.props;
    navigation.navigate('Klopidogrel');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>

          Nie podawaj
          <Hypertekst onPress={this.handleNitro}>Nitrogliceryny</Hypertekst>
        </MyTextView>
        <MyTextView>

          Jeśli ból wymaga zniesienia podaj
          <Hypertekst onPress={this.handlePyralgina}>Metamizol (Pyralgina)</Hypertekst>

          2,5 g w 100 ml 0,9% NaCl iv w ciągu 5 min.
          <MyTextView>i/lub</MyTextView>
          <Hypertekst onPress={this.handleMorfina}>Morfinę</Hypertekst>

          0,1 - 0,2 mg/kg iv. Kolejne dawki co 5 min do istotnego zmniejszenia bólu, wystąpienia
          sedacji lub jakościowych zaburzeń świadomości.
        </MyTextView>
        <MyTextView>

          Tylko za zgodą lekarza oceniającego EKG podaj
          <Hypertekst onPress={this.handleBrylic}>Tikagrelor</Hypertekst>

          180 mg p.o. Rozważ podanie
          <Hypertekst onPress={this.handleHeparyna}>Heparyny</Hypertekst>

          5000 UI iv.
        </MyTextView>
        <MyTextView>

          W przypadku chorych przyjmujących acenokumarol lub inny doustny lek przeciwkrzepliwy oraz
          w przypadku chorych po przebytym krwawieniu śródczaszkowym zamiast Tikagreloru podaj
          <Hypertekst onPress={this.handleKlopidogrel}>Klopidogrel</Hypertekst>

          600 mg p.o.
        </MyTextView>
        <VersionTextView>

          Na podstawie wytycznych Europejskiego Towarzystwa Kardiologicznego, podręcznika
          &quot;Ostre Stany Wieńcowe&quot; Adama Stępki wyd. 2015 oraz wytycznych ERC 2015. Leczenie
          bólu na podstawie Dobrych Praktyk Leczenia Bólu Ministerstwa Zdrowia.
        </VersionTextView>
      </ScrollContainer>
    );
  }
}
