import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ScrollContainer } from '../../../components/container';
import {
  MyTextView, Hypertekst, BoldText, VersionTextView,
} from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import NieHisteria from './NieHisteria';

export default class Dusznosc extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleDiazepam = () => {
    const { navigation } = this.props;
    navigation.navigate('Diazepam');
  };

  render() {
    const Histeria = (
      <View>
        <MyTextView>
          <BoldText>Atak histerii.</BoldText>
        </MyTextView>
        <MyTextView>
          Zwykle związany z sytuacją stresową (śmierć bliskiej osoby, kłótnia w rodzinie, zawód
          miłosny).
        </MyTextView>
        <MyTextView>
          Podaj
          <Hypertekst onPress={this.handleDiazepam}>Diazepam</Hypertekst>
5 mg iv powoli, w razie
          potrzeby powtórz dawkę.
        </MyTextView>
        <MyTextView>
          Transport na SOR w celu wykluczenia innych stanów zagrożenia życia o podobnym przebiegu
          (np. zator tętnicy płucnej).
        </MyTextView>
        <VersionTextView>
          Na podstawie wytycznych ERC 2015 i &quot;Interny Szczeklika&quot; pod redakcją Piotra
          Gajewskiego.
        </VersionTextView>
      </View>
    );
    const { divy } = this.state;
    const { navigation } = this.props;
    return (
      <ScrollContainer>
        <MyTextView>
          Hiperwentylacja, drętwienie rąk, parametry życiowe prawidłowe, brak patologicznych zmian
          osłuchowych nad płucami, brak poważnych obrażeń ciała?
        </MyTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Histeria)}
          onPressNo={() => this.handleOnClick(<NieHisteria navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
