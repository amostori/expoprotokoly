import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { MyTextView, Hypertekst, VersionTextView } from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import NieObrzek from './NieObrzek';

export default class NieAstma extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleObrzek = () => {
    const { navigation } = this.props;
    navigation.navigate('Obrzek');
  };

  render() {
    const Obrzek = (
      <View>
        <MyTextView>
          <Hypertekst onPress={this.handleObrzek}>Ostra niewydolność lewokomorowa -</Hypertekst>
          obrzęk płuc.
        </MyTextView>
        <VersionTextView>
          Na podstawie wytycznych ERC 2015 i &quot;Interny Szczeklika&quot; pod redakcją Piotra
          Gajewskiego.
        </VersionTextView>
      </View>
    );
    const { divy } = this.state;
    const { navigation } = this.props;
    return (
      <View>
        <MyTextView>
          Trzeszczenia nad całymi płucami, sinica obwodowa lub centralna, niestabilne zmiany w EKG
          (częstoskurcze, bradykardie, zmiany niedokrwienne, przeciążenie lewej komory?
        </MyTextView>
        <MyTextView>
          Odpluwanie pienistej, różowej wydzieliny, ból w klatce piersiowej, w wywiadzie
          zaawansowana niewydolność krążenia?
        </MyTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Obrzek)}
          onPressNo={() => this.handleOnClick(<NieObrzek navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
