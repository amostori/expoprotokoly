import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { MyTextView, Hypertekst, VersionTextView } from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import NieAstma from './NieAstma';

export default class NieHisteria extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleAstma = () => {
    const { navigation } = this.props;
    navigation.navigate('Astma');
  };

  render() {
    const Astma = (
      <View>
        <MyTextView>
          <Hypertekst onPress={this.handleAstma}>Astma lub POCHP</Hypertekst>
        </MyTextView>
        <VersionTextView>
          Na podstawie wytycznych ERC 2015 i &quot;Interny Szczeklika&quot; pod redakcją Piotra
          Gajewskiego.
        </VersionTextView>
      </View>
    );
    const { divy } = this.state;
    const { navigation } = this.props;
    return (
      <View>
        <MyTextView>
          Duszność wdechowa, świsty nad płucami, szczególnie na wydechu lub &quot;cicha&quot; klatka
          piersiowa, pacjent w pozycji siedziącej, saturacja obniżona, w wywiadzie astma lub POCHP?
        </MyTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Astma)}
          onPressNo={() => this.handleOnClick(<NieAstma navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
