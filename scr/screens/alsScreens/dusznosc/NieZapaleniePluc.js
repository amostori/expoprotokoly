import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { MyTextView, VersionTextView, BoldText } from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import NieOdma from './NieOdma';

export default class NieZapaleniePluc extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const Odma = (
      <View>
        <MyTextView>
          <BoldText>Odma opłucnowa</BoldText>
        </MyTextView>
        <MyTextView>
          Jeśli towarzyszą temu objawy niepokojące (wstrząs, zaburzenia świadomości) wykonaj
          odbarczenie odmy przez nakłucie drugiej przestrzeni międzyżebrowej, nad trzecim żebrem, w
          linii środkowoobojczykowej.
        </MyTextView>
        <MyTextView>Transport do szpitala.</MyTextView>
        <VersionTextView>
          Na podstawie wytycznych ERC 2015 i &quot;Interny Szczeklika&quot; pod redakcją Piotra
          Gajewskiego.
        </VersionTextView>
      </View>
    );
    const { divy } = this.state;
    const { navigation } = this.props;
    return (
      <View>
        <MyTextView>
          Nagła, silna duszność w wyniku urazu lub kaszlu, brak szmeru pęcherzykowego po jednej
          stronie i odgłos opukowy bębenkowy po tej samej stronie?
        </MyTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Odma)}
          onPressNo={() => this.handleOnClick(<NieOdma navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
