import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { MyTextView, VersionTextView, BoldText } from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import NieZapaleniePluc from './NieZapaleniePluc';

export default class NieObrzek extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const ZapaleniePluc = (
      <View>
        <MyTextView>
          <BoldText>Zapalenie płuc</BoldText>
        </MyTextView>
        <MyTextView>Podaj tlen jeśli saturacja &lt;94%.</MyTextView>
        <MyTextView>Transport do szpitala.</MyTextView>
        <VersionTextView>
          Na podstawie wytycznych ERC 2015 i &quot;Interny Szczeklika&quot; pod redakcją Piotra
          Gajewskiego.
        </VersionTextView>
      </View>
    );
    const { divy } = this.state;
    const { navigation } = this.props;
    return (
      <View>
        <MyTextView>
          Trzeszczenia nad niektórymi rejonami płuc, gorączka, odpluwanie ropnej wydzieliny?
        </MyTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(ZapaleniePluc)}
          onPressNo={() => this.handleOnClick(<NieZapaleniePluc navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
