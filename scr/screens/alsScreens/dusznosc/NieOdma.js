import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { MyTextView, VersionTextView, BoldText } from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import NieZator from './NieZator';

export default class NieOdma extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const Zator = (
      <View>
        <MyTextView>
          <BoldText>Zator tętnicy płucnej</BoldText>
        </MyTextView>
        <MyTextView>
          <BoldText>Zero wysiłku dla pacjenta!</BoldText>
        </MyTextView>
        <MyTextView>Podaj tlen 15 l/min jeśli saturacja &lt;94%.</MyTextView>
        <MyTextView>Transport do szpitala.</MyTextView>
        <VersionTextView>
          Na podstawie wytycznych ERC 2015 i &quot;Interny Szczeklika&quot; pod redakcją Piotra
          Gajewskiego.
        </VersionTextView>
      </View>
    );
    const { divy } = this.state;
    const { navigation } = this.props;
    return (
      <View>
        <MyTextView>
          Duszność wysiłkowa, tachykardia zatokowa, zespół S1Q3T3 (głęboki S w I, głębokie Q w III i
          ujemne T w III), RBBB, obraz przeciążenia prawej komory w EKG?
        </MyTextView>
        <MyTextView>
          W wywiadzie długi pobyt w szpitalu, zażywanie środków antykoncepcyjnych lub przypadki
          zatorowości płucnej w rodzinie?
        </MyTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(Zator)}
          onPressNo={() => this.handleOnClick(<NieZator />)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
