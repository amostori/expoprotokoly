import React from 'react';
import { View } from 'react-native';
import { MyTextView, VersionTextView } from '../../../components/TextViews';

const NieZator = () => (
  <View>
    <MyTextView>Podaj tlen 15 l/min jeśli saturacja &lt;94%</MyTextView>
    <MyTextView>Transport na SOR</MyTextView>
    <VersionTextView>
      Na podstawie wytycznych ERC 2015 i &quot;Interny Szczeklika&quot; pod redakcją Piotra
      Gajewskiego.
    </VersionTextView>
  </View>
);

export default NieZator;
