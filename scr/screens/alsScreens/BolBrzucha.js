/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable linebreak-style */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView,
  Hypertekst,
  BoldText,
  TiM,
  VersionSzczeklikERC,
} from '../../components/TextViews';
import { VersionSzczeklikERCLeczenieBolu } from '../../components/TextViews/VersionERC';

export default class BolBrzucha extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleMorfina = () => {
    const { navigation } = this.props;
    navigation.navigate('Morfina');
  };

  handleParacetamol = () => {
    const { navigation } = this.props;
    navigation.navigate('Paracetamol');
  };

  handlePyralgina = () => {
    const { navigation } = this.props;
    navigation.navigate('Pyralgina');
  };

  handleDrotaweryna = () => {
    const { navigation } = this.props;
    navigation.navigate('Drotaweryna');
  };
  
  handleFentanyl = () => {
    const { navigation } = this.props;
    navigation.navigate('Fentanyl');
  };

  handleObjawyBrzuszne = () => {
    const { navigation } = this.props;
    navigation.navigate('ObjawyBrzuszne');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          <Hypertekst onPress={this.handleObjawyBrzuszne}>Objawy patologiczne</Hypertekst>
        </MyTextView>
        <MyTextView>
          <BoldText>Pamiętaj</BoldText>
          by wykonać EKG.Ból brzucha może być objawem m.in. zawału serca.
        </MyTextView>
        <MyTextView>
          Jeśli ból wymaga zniesienia podaj
          <Hypertekst onPress={this.handlePyralgina}>Metamizol</Hypertekst>
          (Pyralgina) 2,5 g w 100 ml 0,9% NaCl iv w ciągu 5 min.
        </MyTextView>
        <MyTextView>i/lub</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleParacetamol}>Paracetamol</Hypertekst>1 g we wlewie iv (15
          min).
        </MyTextView>
        <MyTextView>
          Jeśli ból nie ustąpił podaj
          <Hypertekst onPress={this.handleFentanyl}>Fentanyl</Hypertekst>
          0,5 - 0,1 &micro;/kg iv lub
          <Hypertekst onPress={this.handleMorfina}>Morfinę</Hypertekst>
          0,1 - 0,2 mg/kg iv. Kolejne dawki morfiny można podawać co 5 min do istotnego zmniejszenia
          bólu, wystąpienia sedacji lub jakościowych zaburzeń świadomości.
        </MyTextView>
        <MyTextView>W przypadku kolki wątrobowej nie podawaj Morfiny.</MyTextView>
        <MyTextView>
          W przypadku występowania stanów spastycznych mięśniówki gładkiej przewodu pokarmowego lub
          dróg moczowych dodatkowo podaj:
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleDrotaweryna}>Drotawerynę</Hypertekst>
          (No - Spa) 80 mg w 20 ml 0,9% NaCl powoli iv
        </MyTextView>
        <TiM />
        <VersionSzczeklikERCLeczenieBolu />
      </ScrollContainer>
    );
  }
}
