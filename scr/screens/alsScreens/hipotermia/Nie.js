import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { MyTextView, Hypertekst, VersionERC } from '../../../components/TextViews';

class Nie extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleKlasyfikacja = () => {
    const { navigation } = this.props;
    navigation.navigate('KlasyfikacjaHipotermii');
  };

  render() {
    return (
      <View>
        <MyTextView>
          <Hypertekst onPress={this.handleKlasyfikacja}>Hipotermia I stopnia</Hypertekst>
        </MyTextView>

        <MyTextView>
          Ciepłe otoczenie, suche ubranie, ciepłe płyny doustnie, okrycie kocami, nakrycie głowy,
          aktywność fizyczna.
        </MyTextView>
        <MyTextView>Transport na SOR.</MyTextView>
        <VersionERC />
      </View>
    );
  }
}

export default Nie;
