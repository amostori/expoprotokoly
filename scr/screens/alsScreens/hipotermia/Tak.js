import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { ButtonsYesNo } from '../../../components/Buttons';
import {
  MyTextView, AskTextView, Hypertekst, VersionERC,
} from '../../../components/TextViews';

class Tak extends React.Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleKlasyfikacja = () => {
    const { navigation } = this.props;
    navigation.navigate('KlasyfikacjaHipotermii');
  };

  handleOgrzewanie = () => {
    const { navigation } = this.props;
    navigation.navigate('Ogrzewanie');
  };

  render() {
    const { divy } = this.state;
    const stabilny = (
      <View>
        <MyTextView>Transport na SOR</MyTextView>
        <VersionERC />
      </View>
    );
    const niestabilny = (
      <View>
        <MyTextView>Transport do Centrum Leczenia Hipotermii Głębokiej.</MyTextView>
        <VersionERC />
      </View>
    );
    return (
      // musi być View bo Container zawiera wyśrodkowanie, a te elementy
      // już są wyśrodkowane bo znajdują się w ScrollContainer z pliku Hipotermia.js
      // dublowanie wyśrodkowania znosi je.
      <View>
        <MyTextView>
        <Hypertekst onPress={this.handleKlasyfikacja}>Hipotermia 2 lub 3 stopnia</Hypertekst>
        </MyTextView>
        
        <MyTextView>
          Unikanie ruchów pacjenta, pozycja leżąca, szczelne, warstwowe okrycie.
        </MyTextView>
        <MyTextView>
          Ostrożne
          <Hypertekst onPress={this.handleOgrzewanie}>ogrzewanie zewnętrzne</Hypertekst>
          czyli ogrzewacze w okolicy tułowia.
        </MyTextView>
        <AskTextView>Stabilny krążeniowo-oddechowo?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(stabilny)}
          onPressNo={() => this.handleOnClick(niestabilny)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}

export default Tak;
