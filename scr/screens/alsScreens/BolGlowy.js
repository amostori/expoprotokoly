import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView, Hypertekst, TiM, VersionTextView,
} from '../../components/TextViews';

export default class BolGlowy extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleParacetamol = () => {
    const { navigation } = this.props;
    navigation.navigate('Paracetamol');
  };

  handleKetoprofen = () => {
    const { navigation } = this.props;
    navigation.navigate('Ketoprofen');
  };

  handlePyralgina = () => {
    const { navigation } = this.props;
    navigation.navigate('Pyralgina');
  };

  handleFentanyl = () => {
    const { navigation } = this.props;
    navigation.navigate('Fentanyl');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          Oceń stan pacjenta, zwróć szczególną uwagę na obecność dodatnich objawów oponowych
          (sztywność karku).
        </MyTextView>
        <MyTextView>Oceń siłę bólu w skali 0 - 10.</MyTextView>
        <MyTextView>
          W przypadku bólu lekkiego (&lt;5 punktów) podaj
          <Hypertekst onPress={this.handleParacetamol}>Paracetamol</Hypertekst>
1 g iv lub po.
        </MyTextView>
        <MyTextView>
          Jeśli ból jest umiarkowany (5 - 7 punktów) podaj
          <Hypertekst onPress={this.handlePyralgina}>Metamizol (Pyralgina)</Hypertekst>
          2,5 g iv lub
          {' '}
          <Hypertekst onPress={this.handleKetoprofen}>Ketonal</Hypertekst>
          100 mg we wlewie iv w 100 ml 0,9% NaCl przez 30 min lub
          <Hypertekst>Ibuprofen</Hypertekst>
          800 mg p.o.
        </MyTextView>
        <MyTextView>
          W przypadku podejrzenia krwawienia do Ośrodkowego Układu Nerwowego podaj
          <Hypertekst onPress={this.handleFentanyl}>Fentanyl</Hypertekst>
          0,5 - 1 &micro;g/kg iv
        </MyTextView>
        <MyTextView>
          Jeśli ból jest silny (oceniany na &gt;7 puntów) podaj
          {' '}
          <Hypertekst onPress={this.handleFentanyl}>Fentanyl</Hypertekst>
          0,5 - 1 &micro;g/kg iv.
        </MyTextView>
        <MyTextView>
          Ponownie oceń natężenie bólu i zanotuj. Celem jest redukcja dolegliwości co najmniej o
          połowę.
        </MyTextView>
        <TiM />
        <VersionTextView>
          Na podstawie Dobrych Praktyk Leczenia Bólu Ministerstwa Zdrowia.
        </VersionTextView>
      </ScrollContainer>
    );
  }
}
