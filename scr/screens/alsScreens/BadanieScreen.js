import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BadanieHipertekst, MyTextView } from '../../components/TextViews';
import { ScrollContainer } from '../../components/container';

class BadanieScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleI = () => {
    const { navigation } = this.props;
    navigation.navigate('Wstep');
  };

  handleII = () => {
    const { navigation } = this.props;
    navigation.navigate('FunkcjeZyciowe');
  };

  handleIII = () => {
    const { navigation } = this.props;
    navigation.navigate('Zlecenia');
  };

  handleIV = () => {
    const { navigation } = this.props;
    navigation.navigate('BadanieKliniczne');
  };

  handleV = () => {
    const { navigation } = this.props;
    navigation.navigate('Podsumowanie');
  };

  render() {
    return (
      <ScrollContainer>
        <BadanieHipertekst onPress={this.handleI}>I Badanie wstępne</BadanieHipertekst>
        <MyTextView>1. Bezpieczeństwo</MyTextView>
        <MyTextView>2. Liczba poszkodowanych</MyTextView>
        <MyTextView>3. Siły i środki</MyTextView>
        <BadanieHipertekst onPress={this.handleII}>II Funkcje życiowe</BadanieHipertekst>
        <MyTextView>1. Przytomność</MyTextView>
        <MyTextView>2. Oddech</MyTextView>
        <MyTextView>3. Krążenie</MyTextView>
        <BadanieHipertekst onPress={this.handleIII}>III Zlecenia i wywiad</BadanieHipertekst>
        <BadanieHipertekst onPress={this.handleIV}>IV Badanie kliniczne</BadanieHipertekst>
        <MyTextView>1. Głowa i szyja</MyTextView>
        <MyTextView>2. Klatka piersiowa</MyTextView>
        <MyTextView>3. Brzuch</MyTextView>
        <MyTextView>4. Krocze</MyTextView>
        <MyTextView>5. Kończyny</MyTextView>
        <BadanieHipertekst onPress={this.handleV}>V Podsumowanie</BadanieHipertekst>
        <MyTextView>1. Zebranie wyników badań</MyTextView>
        <MyTextView>2. Decyzje terapeutyczne</MyTextView>
        <MyTextView>3. Przewóz do szpitala</MyTextView>
      </ScrollContainer>
    );
  }
}

export default BadanieScreen;
