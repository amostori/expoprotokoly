import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { MyTextView, Hypertekst, VersionERC } from '../../../components/TextViews';

export default class Tak extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOgrzewanie = () => {
    const { navigation } = this.props;
    navigation.navigate('Ogrzewanie');
  };

  handleKlasyfikacjaHipotermii = () => {
    const { navigation } = this.props;
    navigation.navigate('KlasyfikacjaHipotermii');
  };

  render() {
    return (
      <View>
        <MyTextView>Resuscytacja bez leków.</MyTextView>
        <MyTextView>
          Jeśli rytm jest do defibrylacji wykonaj nie więcej niż 3 wyładowania. Ewentualne następne
          dopiero po ogrzaniu pacjent powyżej 30 stopni C.
        </MyTextView>
        <MyTextView>
          Nie opóźniaj transportu.
          <Hypertekst onPress={this.handleOgrzewanie}>Ogrzewanie pozaszpitalne</Hypertekst>
          jest mało skuteczne.
        </MyTextView>
        <MyTextView>
          Rozważ transport do ośrodka posiadającego możliwość ogrzewania pozaustrojowego. Okryj
          szczelnie pacjenta.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKlasyfikacjaHipotermii}>
            Klasyfikacja hipotermii
          </Hypertekst>
        </MyTextView>
        <VersionERC />
      </View>
    );
  }
}
