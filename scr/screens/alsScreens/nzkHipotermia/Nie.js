import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { MyTextView, Hypertekst, VersionERC } from '../../../components/TextViews';

export default class Nie extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOgrzewanie = () => {
    const { navigation } = this.props;
    navigation.navigate('Ogrzewanie');
  };

  handleKlasyfikacjaHipotermii = () => {
    const { navigation } = this.props;
    navigation.navigate('KlasyfikacjaHipotermii');
  };

  render() {
    return (
      <View>
        <MyTextView>Leki podawaj w odstępie dwukrotnie większym niż zwykle.</MyTextView>
        <MyTextView>Defibrylacje wykonuj standardowo</MyTextView>
        <MyTextView>
          Nie opóźniaj transportu.
          <Hypertekst onPress={this.handleOgrzewanie}>Ogrzewanie pozaszpitalne</Hypertekst>
          jest mało skuteczne.
        </MyTextView>
        <MyTextView>
          Rozważ transport do ośrodka posiadającego możliwość ogrzewania pozaustrojowego. Okryj
          szczelnie pacjenta.
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleKlasyfikacjaHipotermii}>
            Klasyfikacja hipotermii
          </Hypertekst>
        </MyTextView>
        <VersionERC />
      </View>
    );
  }
}
