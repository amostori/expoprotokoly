import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { ScrollContainer } from '../../../components/container';
import {
  MyTextView,
  Hypertekst,
  AskTextView,
  Sol,
  VersionSzczeklik,
} from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';

export default class Padaczka extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleMidazolam = () => {
    const { navigation } = this.props;
    navigation.navigate('Midazolam');
  };

  handleClonazepam = () => {
    const { navigation } = this.props;
    navigation.navigate('Clonazepam');
  };

  handleDiazepam = () => {
    const { navigation } = this.props;
    navigation.navigate('Diazepam');
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const DoZyly = (
      <View>
        <MyTextView>
          <Hypertekst onPress={this.handleDiazepam}>Diazepam</Hypertekst>
          (Relanium) 10 mg iv
        </MyTextView>
        <MyTextView>lub</MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleClonazepam}>Clonazepam</Hypertekst>
          1 mg rozcieńczony do 5 ml
          <Sol />
          {' '}
iv
        </MyTextView>
        <MyTextView>Jeśli drgawki nie ustąpiły po 5 minutach powtórz ten sam lek.</MyTextView>
        <VersionSzczeklik />
      </View>
    );

    const NieDoZyly = (
      <View>
        <MyTextView>
          <Hypertekst onPress={this.handleMidazolam}>Midazolam</Hypertekst>
          10 mg i.m. jednorazowo.
        </MyTextView>
        <MyTextView>
          Jeśli drgawki nie ustąpiły rozważ wezwanie karetki S lub LPR w celu zastosowania innych
          leków lub zastosowania znieczulenia ogólnego (Propofol).
        </MyTextView>
        <MyTextView>
          Transport na SOR w celu wykluczenia udaru mózgu lub krwawienia śródczaszkowego.
        </MyTextView>
        <VersionSzczeklik />
      </View>
    );
    return (
      <ScrollContainer>
        <MyTextView>Wyklucz hipoglikemię</MyTextView>
        <AskTextView>Dojście dożylne obecne?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(DoZyly)}
          onPressNo={() => this.handleOnClick(NieDoZyly)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
