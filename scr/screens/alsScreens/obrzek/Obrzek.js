import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../../components/container';
import { MyTextView, Hypertekst, AskTextView } from '../../../components/TextViews';
import { ButtonsYesNo } from '../../../components/Buttons';
import Poprawa from './Poprawa';
import BrakPoprawy from './BrakPoprawy';

export default class Obrzek extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleFurosemid = () => {
    const { navigation } = this.props;
    navigation.navigate('Furosemid');
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  render() {
    const { divy } = this.state;
    const { navigation } = this.props;
    return (
      <ScrollContainer>
        <MyTextView>
          Podaj
          <Hypertekst onPress={this.handleFurosemid}>Furosemid</Hypertekst>
          40 mg iv
        </MyTextView>
        <MyTextView>Podaj tlen jeśli saturacja &lt;90%</MyTextView>
        <AskTextView>Poprawa stanu pacjenta?</AskTextView>
        <ButtonsYesNo
          onPressYes={() => this.handleOnClick(<Poprawa navigation={navigation} />)}
          onPressNo={() => this.handleOnClick(<BrakPoprawy navigation={navigation} />)}
        />
        <MyTextView />
        {divy}
      </ScrollContainer>
    );
  }
}
