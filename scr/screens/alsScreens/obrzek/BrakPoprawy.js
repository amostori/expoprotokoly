import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import {
  MyTextView,
  Hypertekst,
  AskTextView,
  VersionTextView,
} from '../../../components/TextViews';
import { CisnienieButtons } from '../../../components/Buttons';

export default class BrakPoprawy extends Component {
  state = {
    divy: null,
  };

  static propTypes = {
    navigation: PropTypes.object,
  };

  handleOnClick = (div) => {
    this.setState({
      divy: div,
    });
  };

  handleNitro = () => {
    const { navigation } = this.props;
    navigation.navigate('Nitrogliceryna');
  };

  handleMorfina = () => {
    const { navigation } = this.props;
    navigation.navigate('Morfina');
  };

  render() {
    const { divy } = this.state;

    const Below = (
      <View>
        <MyTextView>
          Rozważ wezwanie karetki S lub LPR na spotkanie. Konieczne jest podanie katecholamin we
          wlewie.
        </MyTextView>
        <VersionTextView>
          Na podstawie wytycznych Europejskiego Towarzystwa Kardiologicznego i podręcznika
          &quot;Ostre Stany Wieńcowe&quot; Adama Stępki wyd. 2015
        </VersionTextView>
      </View>
    );
    const Between = (
      <View>
        <MyTextView>Monitoring i transport na SOR.</MyTextView>
        <VersionTextView>
          Na podstawie wytycznych Europejskiego Towarzystwa Kardiologicznego i podręcznika
          &quot;Ostre Stany Wieńcowe&quot; Adama Stępki wyd. 2015
        </VersionTextView>
      </View>
    );
    const Over = (
      <View>
        <MyTextView>
          <Hypertekst onPress={this.handleNitro}>Nitrogliceryna</Hypertekst>
          0,4 mg s.l., powtarzaj co 10 min pod kontrolą ciśnienia.
        </MyTextView>
        <MyTextView>Transport na SOR.</MyTextView>
        <VersionTextView>
          Na podstawie wytycznych Europejskiego Towarzystwa Kardiologicznego i podręcznika
          &quot;Ostre Stany Wieńcowe&quot; Adama Stępki wyd. 2015
        </VersionTextView>
      </View>
    );
    return (
      <View>
        <MyTextView>Rozważ wezwanie karetki na S lub LPR na spotkanie.</MyTextView>
        <MyTextView>Rozważ wspomaganie oddechu.</MyTextView>
        <AskTextView>Ciśnienie skurczowe?</AskTextView>
        <CisnienieButtons
          onPressBelow={() => this.handleOnClick(Below)}
          onPressBetween={() => this.handleOnClick(Between)}
          onPressOver={() => this.handleOnClick(Over)}
        />
        <MyTextView />
        {divy}
      </View>
    );
  }
}
