/* eslint-disable camelcase */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { ScrollContainer } from '../components/container';
import { Separator, ListItem } from '../components/ListViews/ALSList';
import all_list from './ListScreens/data/AllList';

class SearchScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      data: all_list,
      search: '',
    };
    this.myArray = all_list.sort((a, b) => {
      if (a < b) return -1;
      if (a > b) return 1;
      return 0;
    });
  }

  searchFilterFunction = (text) => {
    const newData = this.myArray.filter((item) => {
      const itemData = `${item.toUpperCase()}`;

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({ data: newData, search: text });
  };

  renderHeader = () => {
    const { search } = this.state;
    return (
      <SearchBar
        placeholder="Tu wpisz..."
        lightTheme
        value={search}
        round
        onChangeText={text => this.searchFilterFunction(text)}
        autoCorrect={false}
      />
    );
  };

  onPressHandler = (item) => {
    const { navigation } = this.props;
    switch (item) {
      case 'Badanie ratunkowe':
        navigation.navigate('BadanieScreen');
        break;
      case 'Zatrzymanie krążenia I 46':
        navigation.navigate('NzkAlsScreen');
        break;
      case 'NZK w hipotermii I 46':
        navigation.navigate('NzkHipotermia');
        break;
      case 'Hipotermia R 68':
        navigation.navigate('Hipotermia');
        break;
      case 'Bradykardia R 00.0':
        navigation.navigate('Bradykardia');
        break;
      case 'Częstoskurcz R 00.0':
        navigation.navigate('Czestoskurcz');
        break;
      case 'Ból w klatce piersiowej R 07.4':
        navigation.navigate('Ozw');
        break;
      case 'Zawał serca':
        navigation.navigate('Ozw');
        break;
      case 'Nadciśnienie tętnicze R 03.0':
        navigation.navigate('Nadcisnienie');
        break;
      case 'Duszność R 06':
        navigation.navigate('Dusznosc');
        break;
      case 'Obrzęk płuc J 81':
        navigation.navigate('Obrzek');
        break;
      case 'Anafilaksja T 78.2':
        navigation.navigate('Anafilaksja');
        break;
      case 'Astma i POCHP (J 46, J 43)':
        navigation.navigate('Astma');
        break;
      case 'Ból brzucha R 10.4':
        navigation.navigate('BolBrzucha');
        break;
      case 'Hipoglikemia E 16.2':
        navigation.navigate('Hipoglikemia');
        break;
      case 'Hiperglikemia E14.8':
        navigation.navigate('Hiperglikemia');
        break;
      case 'Padaczka G 40':
        navigation.navigate('Padaczka');
        break;
      case 'Udar mózgu I 64':
        navigation.navigate('Udar');
        break;
      case 'Ból pleców M 54':
        navigation.navigate('BolPlecow');
        break;
      case 'Zaburzenia psychiczne F 99':
        navigation.navigate('ZaburzeniaPsychiczne');
        break;
      case 'NZK u dzieci I 46':
        navigation.navigate('NzkP');
        break;
      case 'NZK u świeżorodka I 46':
        navigation.navigate('Swiezorodek');
        break;
      case 'Bradykardia u dzieci R 00.1':
        navigation.navigate('BradykardiaP');
        break;
      case 'Częstoskurcz u dzieci R 00.0':
        navigation.navigate('CzestoskurczP');
        break;
      case 'Anafilaksja u dzieci T 78.2':
        navigation.navigate('AnafilaksjaP');
        break;
      case 'Astma u dzieci J 46':
        navigation.navigate('AstmaP');
        break;
      case 'Zapalenie krtani J 05.0':
        navigation.navigate('ZapalenieKrtani');
        break;
      case 'Ból brzucha u dzieci R 10.4':
        navigation.navigate('BolBrzuchaP');
        break;
      case 'Hipoglikemia u dzieci E 16.2':
        navigation.navigate('HipoglikemiaP');
        break;
      case 'Drgawki u dzieci R 56.8':
        navigation.navigate('DrgawkiP');
        break;
      case 'Dziecko gorączkujące R 50':
        navigation.navigate('Goraczka');
        break;
      case 'Poród fizjologiczny O 26':
        navigation.navigate('Porod');
        break;
      case 'NZK świeżorodka I 46':
        navigation.navigate('Swiezorodek');
        break;
      case 'Wypadnięta pępowina O 26':
        navigation.navigate('Pepowina');
        break;
      case 'Pępowina wokół szyi O 26':
        navigation.navigate('PepowinaNaSzyi');
        break;
      case 'Krwawienie po porodzie O 26':
        navigation.navigate('KrwawieniePorod');
        break;
      case 'Rzucawka porodowa O 26':
        navigation.navigate('Rzucawka');
        break;
      case 'Zdarzenia masowe':
        navigation.navigate('ZdarzenieMasowe');
        break;
      case 'TRIAGE':
        navigation.navigate('TriageAdult');
        break;
      case 'TRIAGE pediatryczny':
        navigation.navigate('TriageJump');
        break;
      case 'Centrum Urazowe - kryteria przyjęć':
        navigation.navigate('CentrumUrazowe');
        break;
      case 'Oparzenia T 30.0':
        navigation.navigate('Oparzenia');
        break;
      case 'Reguła dziewiątek':
        navigation.navigate('Oparzenia');
        break;
      case 'Reguła Parkland':
        navigation.navigate('Oparzenia');
        break;
      case 'Uraz czaszkowo - mózgowy':
        navigation.navigate('Czaszka');
        break;
      case 'Asystent RKO':
        navigation.navigate('CPRAssistant');
        break;
      case 'Reanimacja':
        navigation.navigate('CPRAssistant');
        break;
      case 'Skala Glasgow':
        navigation.navigate('Glasgow');
        break;
      case 'Parametry życiowe':
        navigation.navigate('LifeParam');
        break;
      case 'Skala Apgar':
        navigation.navigate('Apgar');
        break;
      case 'Skala Westleya':
        navigation.navigate('Westley');
        break;
      case 'Cewnik dializacyjny':
        navigation.navigate('Catheter');
        break;
      case 'Średnie ciśnienie tętnicze':
        navigation.navigate('Pressure');
        break;
      case 'Adrenalina u dzieci':
        navigation.navigate('AdrenalinaWskazowki');
        break;
      case 'Atropina u dzieci':
        navigation.navigate('AtropinaWskazowki');
        break;
      case 'Amiodaron u dzieci':
        navigation.navigate('AmiodaronWskazowki');
        break;
      case 'Diazepam u dzieci':
        navigation.navigate('DiazepamDzieci');
        break;
      case 'Relanium u dzieci':
        navigation.navigate('DiazepamDzieci');
        break;
      case 'Midazolam u dzieci':
        navigation.navigate('MidazolamDzieci');
        break;
      case 'Morfina u dzieci':
        navigation.navigate('MorfinaDzieci');
        break;
      case 'Nalokson u dzieci':
        navigation.navigate('NaloksonDzieci');
        break;
      case 'Sedacja dzieci':
        navigation.navigate('SedacjaDzieci');
        break;
      case 'Paracetamol u dzieci':
        navigation.navigate('ParacetamolDzieci');
        break;
      case 'Amfetamina i kokaina T 50':
        navigation.navigate('Amfetamina');
        break;
      case 'Benzodiazepiny T 50':
        navigation.navigate('Benzodiazepiny');
        break;
      case 'Betablokery i Ca - blokery T 50':
        navigation.navigate('Betablokery');
        break;
      case 'Cyjanki T 50':
        navigation.navigate('Cyjanki');
        break;
      case 'Digoksyna T 50':
        navigation.navigate('Digoksyna');
        break;
      case 'Dopalacze T 50':
        navigation.navigate('Dopalacze');
        break;
      case 'LSD, grzyby halucynogenne T 50':
        navigation.navigate('LSD');
        break;
      case 'Metanol, Glikol etylenowy T 50':
        navigation.navigate('Metanol');
        break;
      case 'Morfina, Heroina, Kodeina T 50':
        navigation.navigate('Opiaty');
        break;
      case 'Paracetamol T 50':
        navigation.navigate('ZatrucieParacetamolem');
        break;
      case 'Rtęć T 50':
        navigation.navigate('Rtec');
        break;
      case 'Tlenek węgla T 50':
        navigation.navigate('CO');
        break;
      case 'Trójcykliczne leki przeciwdepresyjne T 50':
        navigation.navigate('TLP');
        break;
      case 'Związki fosfoorganiczne T 50':
        navigation.navigate('Fosforany');
        break;
      case 'Żelazo T 50':
        navigation.navigate('Zelazo');
        break;
      case 'Adenozyna':
        navigation.navigate('Adenozyna');
        break;
      case 'Adrenalina':
        navigation.navigate('Adrenalina');
        break;
      case 'Amiodaron':
        navigation.navigate('Amiodaron');
        break;
      case 'Atropina':
        navigation.navigate('Atropina');
        break;
      case 'Budesonidum':
        navigation.navigate('Budezonid');
        break;
      case 'Captopril':
        navigation.navigate('Captopril');
        break;
      case 'Clemastinum':
        navigation.navigate('Clemastin');
        break;
      case 'Clonazepamum':
        navigation.navigate('Clonazepam');
        break;
      case 'Clopidogrel':
        navigation.navigate('Klopidogrel');
        break;
      case 'Deksametazon':
        navigation.navigate('Dexaven');
        break;
      case 'Diazepamum':
        navigation.navigate('Diazepam');
        break;
      case 'Relanium':
        navigation.navigate('Diazepam');
        break;
      case 'Drotaweryna (No-Spa)':
        navigation.navigate('Drotaweryna');
        break;
      case 'Fentanyl':
        navigation.navigate('Fentanyl');
        break;
      case 'Flumazenil':
        navigation.navigate('Flumazenil');
        break;
      case 'Furosemid':
        navigation.navigate('Furosemid');
        break;
      case 'Glukagon':
        navigation.navigate('Glukagon');
        break;
      case 'Glukoza 20%':
        navigation.navigate('Glukoza');
        break;
      case 'Heparyna':
        navigation.navigate('Heparyna');
        break;
      case 'Hydrokortyzon':
        navigation.navigate('Hydrokortyzon');
        break;
      case 'Hydroxyzinum':
        navigation.navigate('Hydroxyzyna');
        break;
      case 'Ibuprofen':
        navigation.navigate('Ibuprofen');
        break;
      case 'Izosorbid - Mononit':
        navigation.navigate('Izosorbid');
        break;
      case 'Ketoprofen':
        navigation.navigate('Ketoprofen');
        break;
      case 'Kwas acetylosalicylowy':
        navigation.navigate('ASA');
        break;
      case 'Lignokaina':
        navigation.navigate('Lignokaina');
        break;
      case 'Magnez':
        navigation.navigate('Magnez');
        break;
      case 'Mannitol 15%':
        navigation.navigate('Mannitol');
        break;
      case 'Metamizol (Pyralgina)':
        navigation.navigate('Pyralgina');
        break;
      case 'Metoclopramid':
        navigation.navigate('Metoklopramid');
        break;
      case 'Metoprolol':
        navigation.navigate('Metoprolol');
        break;
      case 'Midazolam':
        navigation.navigate('Midazolam');
        break;
      case 'Morfina':
        navigation.navigate('Morfina');
        break;
      case 'Nalokson':
        navigation.navigate('Nalokson');
        break;
      case 'Natrii hydrogenocarbonas 8,4%':
        // todo
        navigation.navigate('Natrii');
        break;
      case 'Nitrogliceryna':
        navigation.navigate('Nitrogliceryna');
        break;
      case 'Papaweryna':
        navigation.navigate('Papaweryna');
        break;
      case 'Paracetamol':
        navigation.navigate('Paracetamol');
        break;
      case 'Salbutamol':
        navigation.navigate('Salbutamol');
        break;
      case 'Thiethylperazinum (Torecan)':
        // todo
        navigation.navigate('Torecan');
        break;
      case 'Ticagrelol':
        navigation.navigate('Tikagrelor');
        break;
      case 'Urapidil (Ebrantil)':
        navigation.navigate('Urapidil');
        break;
      case 'Leki anestezjologiczne':
        navigation.navigate('Anestezja');
        break;
      case 'Tiopental':
        navigation.navigate('Anestezja');
        break;
      case 'Propofol':
        navigation.navigate('Anestezja');
        break;
      case 'Etomidat':
        navigation.navigate('Anestezja');
        break;
      case 'Ketamina':
        navigation.navigate('Anestezja');
        break;
      case 'Skolina':
        navigation.navigate('Anestezja');
        break;
      case 'Sukcynylocholina':
        navigation.navigate('Anestezja');
        break;
      case 'Wekuronium':
        navigation.navigate('Anestezja');
        break;
      case 'Norcuron':
        navigation.navigate('Anestezja');
        break;
      case 'Triage trainer':
        navigation.navigate('TriageTrening');
        break;
      case 'Ośrodki Ostrych Zatruć':
        navigation.navigate('OsrodkiOstrychZatruc');
        break;
      case 'Ból urazowy':
        navigation.navigate('BolUrazowy');
        break;
      case 'Zwalczanie bólu urazowego':
        navigation.navigate('BolUrazowy');
        break;
      case 'Ból głowy':
        navigation.navigate('BolGlowy');
        break;
      case 'Zwalczanie bólu u dzieci':
        navigation.navigate('ZwalczanieBoluP');
        break;
      default:
        alert('test');
        break;
    }
  };

  render() {
    const { data } = this.state;
    return (
      <ScrollContainer>
        <FlatList
          data={data}
          renderItem={({ item }) => (
            <ListItem text={item} onPress={() => this.onPressHandler(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
          ListHeaderComponent={this.renderHeader}
        />
      </ScrollContainer>
    );
  }
}

export default SearchScreen;
