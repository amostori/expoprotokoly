import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    // flex: 1,
    //   marginTop: 20,
    //   marginHorizontal: 20,
    //   justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 20,
    alignItems: 'center',
    height: 80,
    width: 80,
  },
  bigImage: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 20,
    alignItems: 'center',
    height: 150,
    width: 150,
  },
  skalaBolu: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
