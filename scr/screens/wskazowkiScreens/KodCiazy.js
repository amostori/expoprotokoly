import { Image } from 'react-native';
import React from 'react';

import { MyTextView, BoldText } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const KodCiazy = () => (
  <WskazowkiContainer>
    <Image source={require('./images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      <BoldText>Kategorie bezpieczeństwa podawania leków w okresie ciąży:</BoldText>
    </MyTextView>
    <MyTextView>
      <BoldText>Kategoria A:</BoldText>
      lek bezpieczny w ciąży.
    </MyTextView>
    <MyTextView>
      <BoldText>Kategoria B:</BoldText>
      Lek bezpieczny, nigdy nie badany na ludziach, a na zwierzętach nie wykazywał działania
      teratogennego. Bezpieczne jest jednorazowe podanie.
    </MyTextView>
    <MyTextView>
      <BoldText>Kategoria C:</BoldText>
      lek jest toksyczny u zwierząt cieżarnych, ale nigdy nie był badany u ludzi. Lek można
      stosować, gdy korzyści wynikające z ich stosowania u matki przewyższają ryzyko niepożądanego
      działania u płodu.
    </MyTextView>
    <MyTextView>
      <BoldText>Kategoria D:</BoldText>
      Lek powoduje wady wrodzone u płodu, lekarz decyduje o podaniu leku w stanie zagrożenia życia.
    </MyTextView>
    <MyTextView>
      <BoldText>Kategoria X:</BoldText>
      lek nie może być podawany w okresie ciąży. Udokumentowane działania szkodliwe na płód u ludzi
      i ryzyko stosowania u kobiet w ciąży przewyższa wszelkie możliwe korzyści. Lek bezwzględnie
      przeciwwskazany dla cieżarnej.
    </MyTextView>
    <MyTextView>
      <BoldText>Kategorie bezpieczeństwa podawania leków w okresie karmienia piersią:</BoldText>
    </MyTextView>
    <MyTextView>1. Możliwe jest karmienie piersią. Nie ma ryzyka dla dziecka.</MyTextView>
    <MyTextView>2. Możliwe jest karmienie piersią, należy jednak obserwować dziecko.</MyTextView>
    <MyTextView>
      3. Możliwe jest jednorazowe lub krótkotrwałe stosowanie leku. Przy długotrwałym stosowaniu
      leku należy unikać kariemienia.
    </MyTextView>
    <MyTextView>4. Nie można karmić piersią przy stosowaniu tych leków.</MyTextView>
  </WskazowkiContainer>
);

export default KodCiazy;
