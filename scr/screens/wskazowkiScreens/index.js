import EnergiaDefibrylacji from './EnergiaDefibrylacji';
import Screen4H4T from './Screen4H4T';
import Ogrzewanie from './Ogrzewanie';
import KlasyfikacjaHipotermii from './KlasyfikacjaHipotermii';
import Sedacja from './Sedacja';
import Kardiowersja from './Kardiowersja';
import AdrenalinaWskazowki from './AdrenalinaWskazowki';
import AmiodaronWskazowki from './AmiodaronWskazowki';
import SaturacjaSwiezorodka from './SaturacjaSwiezorodka';
import AtropinaWskazowki from './AtropinaWskazowki';
import SedacjaDzieci from './SedacjaDzieci';
import MorfinaDzieci from './MorfinaDzieci';
import FentanylDzieci from './FentanylDzieci';
import SkalaBolu from './SkalaBolu';
import DiazepamDzieci from './DiazepamDzieci';
import MidazolamDzieci from './MidazolamDzieci';
import LeczenieGoraczki from './LeczenieGoraczki';
import Hiperwentylacja from './Hiperwentylacja';
import Toksydrom from './Toksydrom';
import ObjawyBrzuszne from './ObjawyBrzuszne';
import ParacetamolDzieci from './ParacetamolDzieci';
import NaloksonDzieci from './NaloksonDzieci';
import Vagal from './Vagal';
import KodCiazy from './KodCiazy';

export {
  EnergiaDefibrylacji,
  Screen4H4T,
  Ogrzewanie,
  KlasyfikacjaHipotermii,
  Sedacja,
  Kardiowersja,
  AdrenalinaWskazowki,
  AmiodaronWskazowki,
  SaturacjaSwiezorodka,
  AtropinaWskazowki,
  ParacetamolDzieci,
  SedacjaDzieci,
  MorfinaDzieci,
  SkalaBolu,
  DiazepamDzieci,
  MidazolamDzieci,
  LeczenieGoraczki,
  Hiperwentylacja,
  Toksydrom,
  ObjawyBrzuszne,
  NaloksonDzieci,
  KodCiazy,
  Vagal,
  FentanylDzieci,
};
