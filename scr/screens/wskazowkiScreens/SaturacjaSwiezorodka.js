import React from 'react';
import { Image } from 'react-native';
import { MyTextView } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const SaturacjaSwiezorodka = () => (
  <WskazowkiContainer>
    <Image source={require('./images/kid.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>po 2 min: 60%</MyTextView>
    <MyTextView>po 3 min: 70%</MyTextView>
    <MyTextView>po 4 min: 80%</MyTextView>
    <MyTextView>po 5 min: 85%</MyTextView>
    <MyTextView>po 10 min: 90%</MyTextView>
  </WskazowkiContainer>
);

export default SaturacjaSwiezorodka;
