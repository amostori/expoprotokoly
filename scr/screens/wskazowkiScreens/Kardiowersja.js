import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const Kardiowersja = () => (
  <WskazowkiContainer>
    <Image source={require('./images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      <BoldText>Częstoskurcz z szerokimi QRS lub migotanie przedsionków:</BoldText>
      120 - 150 J defibrylator dwufazowy, 200 J defibrylator jednofazowy.
    </MyTextView>
    <MyTextView>
      <BoldText>Trzepotanie przedsionków i częstoskurcz nadkomorowy:</BoldText>
      70 - 120 J defibrylator dwufazowy, 100 J defibrylator jednofazowy.
    </MyTextView>
    <MyTextView>Energię kardiowersji stopniowo zwiększaj.</MyTextView>
    <MyTextView>
      U dzieci energia kardiowersji wynosi 1 J/kg. Kolejne wyładowania po 2 J/kg.
    </MyTextView>
  </WskazowkiContainer>
);

export default Kardiowersja;
