import { Image } from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  MyTextView, BoldText, Hypertekst, ListaTextView,
} from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

class Sedacja extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleFentanyl = () => {
    const { navigation } = this.props;
    navigation.navigate('Fentanyl');
  };

  handleMidazolm = () => {
    const { navigation } = this.props;
    navigation.navigate('Midazolam');
  };

  handleMorfina = () => {
    const { navigation } = this.props;
    navigation.navigate('Morfina');
  };

  handleDiazepam = () => {
    const { navigation } = this.props;
    navigation.navigate('Diazepam');
  };

  render() {
    return (
      <WskazowkiContainer>
        <Image
          source={require('./images/stetoskop.png')}
          style={styles.image}
          resizeMode="contain"
        />
        <MyTextView>Kardiowersja i stymulacja wymaga analgosedacji.</MyTextView>
        <MyTextView>
          Fentanyl i Midazolam zapewniają krótkotrwałe, bezpieczne znieczulenie trwające do
          kilkudziesięciu minut.
        </MyTextView>
        <MyTextView>
          <BoldText>Niewielka sedacja:</BoldText>
        </MyTextView>
        <MyTextView>
          <Hypertekst onPress={this.handleFentanyl}>Fentanyl</Hypertekst>
          0,05 mg (1 ml) powoli iv.
          <Hypertekst onPress={this.handleMidazolm}>Midazolam</Hypertekst>
1 - 3, mg iv.
        </MyTextView>
        <MyTextView>
          <BoldText>Głębsza sedacja</BoldText>
        </MyTextView>
        <MyTextView>Fentanyl 0,1 mg (2 ml) powoli iv, Midazolam 5 mg iv.</MyTextView>
        <MyTextView>
          Alternatywą jest podanie
          <Hypertekst onPress={this.handleMorfina}>Morfiny</Hypertekst>
5 - 10 mg iv oraz
          <Hypertekst onPress={this.handleDiazepam}>Diazepamu</Hypertekst>
5 - 10 mg iv
          {' '}
        </MyTextView>
        <MyTextView>
          <BoldText>Procedura:</BoldText>
        </MyTextView>
        <ListaTextView>
          1. W miarę możliwości uzyskaj świadomą zgodę pacjenta - poinformuj o ryzyku.
        </ListaTextView>
        <ListaTextView>2. Podaj 40 - 60% tlen</ListaTextView>
        <ListaTextView>
          3. Przygotuj pełen monitoring: oddech, tętno, ciśnienie, saturacja, ekg.
        </ListaTextView>
        <ListaTextView>
          4. Przygotuj sprzęt do udrażniania dróg oddechowych i prowadzenia wentylacji.
        </ListaTextView>
        <ListaTextView>5. Podaj Fentanyl powoli dożylnie</ListaTextView>
        <ListaTextView>6. Podaj Midazolam</ListaTextView>
        <ListaTextView>
          7. Oceń efekt, w razie potrzeby zwiększ dawkę leków (zastosuj głębszą sedację).
        </ListaTextView>
        <ListaTextView>8. Wykonaj zabieg.</ListaTextView>
        <MyTextView />
      </WskazowkiContainer>
    );
  }
}

export default Sedacja;
