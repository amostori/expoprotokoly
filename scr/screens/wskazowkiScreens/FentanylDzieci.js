import React from 'react';
import { Image } from 'react-native';
import { MyTextView, Sol } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const FentanylDzieci = () => (
  <WskazowkiContainer>
    <Image source={require('./images/kid.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>Dawka Fentanylu u dzieci to 1 - 3 &micro;g/kg.</MyTextView>
    <MyTextView>
      Aby osiągnąć dawkę najmniejszą tj. 1 &micro;g/kg rozcieńcz ampułkę (0,1 mg) do 10 ml
      <Sol />
i podawaj 0,1 ml/kg, czyli w 1 ml jest dawka na 10 kg wagi ciała.
    </MyTextView>
    <MyTextView>
      W przypadku braku efektu może być konieczna dawka podwójna (2 &micro;g/kg) lub potrójna (3
      &micro;g/kg).
    </MyTextView>
  </WskazowkiContainer>
);

export default FentanylDzieci;
