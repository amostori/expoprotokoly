import React from 'react';
import { Image } from 'react-native';
import { MyTextView } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const Ogrzewanie = () => (
  <WskazowkiContainer>
    <Image source={require('./images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      W okresie przedszpitalnym możliwości ogrzewania i monitorowania są mocna ograniczone.
    </MyTextView>
    <MyTextView>
      Podstawą postępowania jest odpowiednia termoizolacja. Zdejmij mokre ubranie przez jego
      rozcięcie, ułóż chorego płasko na folii termicznej (w pozycji na boku, jeśli drogi oddechowe
      nie są zabezpieczone), zawiń tą folią i okryj dodatkowo zwykłym kocem. Całość zabezpiecz
      jeszcze jedną folią.
    </MyTextView>
    <MyTextView>
      Dodatkowo w okolice tułowia, pod pachami ułóż ogrzewacze chemiczne lub termofory (maksymalnie
      3 sztuki).
    </MyTextView>
    <MyTextView>
      Ogrzewaczy nie możesz kłaść bezpośrednio na skórze bo dojdzie do oparzeń.
    </MyTextView>
    <MyTextView>Skuteczne ogrzewanie wewnętrzne można przeprowadzić dopiero w szpitalu.</MyTextView>
  </WskazowkiContainer>
);

export default Ogrzewanie;
