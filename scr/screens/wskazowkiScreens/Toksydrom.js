import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText, ListaTextView } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const Toksydrom = () => (
  <WskazowkiContainer>
    <Image
      source={require('../zatruciaScreens/images/toxic.png')}
      style={styles.image}
      resizeMode="contain"
    />
    <MyTextView>
      <BoldText>Toksydrom cholinolityczny</BoldText>
    </MyTextView>
    <ListaTextView>1. &quot;Czerwony jak cegła&quot; - zaczerwienienie skóry.</ListaTextView>
    <ListaTextView>2. &quot;Rozgrzany jak piec&quot; - hipertermia.</ListaTextView>
    <ListaTextView>3. &quot;Szalony jak kapelusznik&quot; - pobudzenie.</ListaTextView>
    <ListaTextView>4. &quot;Suchy jak wiór&quot; - sucha skóra.</ListaTextView>
    <ListaTextView>5. &quot;Ślepy jak nietoperz&quot; - szerokie źrenice.</ListaTextView>
    <MyTextView />
    <ListaTextView>
      Typowe substancje: trójcykliczne leki przeciwdepresyjne, haloperidol, hydroksyzyna, atropina,
      wilcza jagoda.
    </ListaTextView>
    <MyTextView />
    <MyTextView>
      <BoldText>Toksydrom sympatykomimetyczny</BoldText>
    </MyTextView>
    <ListaTextView>1. Tachykardia.</ListaTextView>
    <ListaTextView>2. Wysokie ciśnienie tętnicze krwi.</ListaTextView>
    <ListaTextView>3. Pobudzenie.</ListaTextView>
    <ListaTextView>4. Szerokie źrenice.</ListaTextView>
    <ListaTextView>5. Wilgotna skóra.</ListaTextView>
    <MyTextView />
    <ListaTextView>Typowe substancje: amfetamina, kokaina.</ListaTextView>
    <MyTextView />
    <MyTextView>
      Toksydrom cholinolityczny i sympatykomimetyczny różnią się wilgotnością skóry: w pierwszym
      jest ona sucha, w drugim wilgotna.
    </MyTextView>
    <MyTextView>
      <BoldText>Toksydrom opioidowy</BoldText>
    </MyTextView>
    <ListaTextView>1. Źrenice szpilkowate.</ListaTextView>
    <ListaTextView>2. Depresja układu oddechowego.</ListaTextView>
    <ListaTextView>3. Śpiączka.</ListaTextView>
    <MyTextView />
    <ListaTextView>Typowe substancje: morfina, fentanyl, dolargan, kodeina, heroina.</ListaTextView>
    <MyTextView />
    <MyTextView>
      <BoldText>Toksydrom cholinergiczny</BoldText>
    </MyTextView>
    <ListaTextView>1. Bradykardia.</ListaTextView>
    <ListaTextView>2. Pobudzony.</ListaTextView>
    <ListaTextView>3. Dreszcze, drgawki.</ListaTextView>
    <ListaTextView>4. Szpilkowate źrenice.</ListaTextView>
    <ListaTextView>5. Ślinotok</ListaTextView>
    <ListaTextView>6. Łzawienie</ListaTextView>
    <ListaTextView>7. Bezwiedne oddanie moczu i stolca</ListaTextView>
    <ListaTextView>8. Wzmożona perystaltyka jelit.</ListaTextView>
    <ListaTextView>9. Biegunka</ListaTextView>
    <MyTextView />
    <ListaTextView>
      Typowe substancje: gazy bojowe: np. sarin, środki fosfoorganiczne, środki ochrony roślin.
    </ListaTextView>
    <MyTextView />
  </WskazowkiContainer>
);

export default Toksydrom;
