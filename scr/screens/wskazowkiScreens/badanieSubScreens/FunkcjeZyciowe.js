import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText } from '../../../components/TextViews';
import { WskazowkiContainer } from '../../../components/container';
import styles from '../styles';

const FunkcjeZyciowe = () => (
  <WskazowkiContainer>
    <Image source={require('../images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      Badanie pacjenta zacznij od oceny funkcji życiowych. Sprawdź przytomność wg skali
      <BoldText>AVPU</BoldText>
      lub
      <BoldText>Glasgow,</BoldText>
      oceń częstość i charakter oddechu, a następnie zbadaj tętno na tętnicy promieniowej i/lub
      szyjnej.
    </MyTextView>
    <MyTextView>
      Po zbadaniu tętna oceń kolor, temperaturę, wilgotność skóry i nawrót włośniczkowy.
    </MyTextView>
    <MyTextView>
      Jeśli w badaniu funkcji życiowych obecne są objawy niepokojące zleć podanie tlenu pacjentowi.
    </MyTextView>
    <MyTextView>
      Jeśli pacjent mógł doznać urazu unikaj zbędnego poruszania pacjentem i zleć ręczną
      stabilizację odcinka szyjnego kręgosłupa.
    </MyTextView>
  </WskazowkiContainer>
);

export default FunkcjeZyciowe;
