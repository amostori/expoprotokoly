import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText } from '../../../components/TextViews';
import { WskazowkiContainer } from '../../../components/container';
import styles from '../styles';

const BadanieKliniczne = () => (
  <WskazowkiContainer>
    <Image source={require('../images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>Po zebraniu wywiadu zbadaj całego pacjenta, od głowy do kończyn.</MyTextView>
    <MyTextView>
      <BoldText>Głowa:</BoldText>
      oceń źrenice, symetrię twarzy, obecność urazów
    </MyTextView>
    <MyTextView>
      <BoldText>Szyja:</BoldText>
      żyły szyjne, mowa, jeśli brak urazu oceń objawy oponowe, w przyciwnym wypadku załóż kołnierz
      ortopedyczny.
    </MyTextView>
    <MyTextView>
      <BoldText>Klatka piersiowa:</BoldText>
      oceń obecność urazów, osłuchaj płuca, tony serca i perystaltykę jelit. Nie zapomnij obejrzeć i
      zbadać plecy.
    </MyTextView>
    <MyTextView>
      <BoldText>Brzuch:</BoldText>
      zanotuj obecność obrony mięśniowej i innych patologicznych objawów, np. Blumberga,
      Chełmońskiego, Goldflama, Rovsinga. Poszukaj urazów brzucha.
    </MyTextView>
    <MyTextView>
      <BoldText>Krocze:</BoldText>
      zapytaj o wydalanie (biegunka, częstomocz, wymioty). W razie potrzeby obejrzyj krocze. Zapytaj
      o ciążę.
    </MyTextView>
    <MyTextView>
      <BoldText>Kończyny dolne:</BoldText>
      obecność obrzęków, żylaków, siła mięśniowa, urazy.
    </MyTextView>
    <MyTextView>
      <BoldText>Kończyny górne:</BoldText>
      siła mięśniowa, zborność ruchów, urazy, tętno na obu tętnicach promieniowych.
    </MyTextView>
  </WskazowkiContainer>
);

export default BadanieKliniczne;
