import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText } from '../../../components/TextViews';
import { WskazowkiContainer } from '../../../components/container';
import styles from '../styles';

const Zlecenia = () => (
  <WskazowkiContainer>
    <Image source={require('../images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      Po ocenie funkcji życiowych poproś o założenie czujnika pulsoksymetru, pomiar cukru, wykonanie
      dojścia dożylnego, ocenę ciśnienia tętniczego krwi, temperatury ciała i monitorowanie EKG.
    </MyTextView>
    <MyTextView>
      Pamiętaj, że u pacjenta dializowanego z obecną tzw. przetoką żylno - tętniczą pomiar ciśnienia
      wykonuje się na ręce bez przetoki!
    </MyTextView>
    <MyTextView>
      W czasie wykonywania zleceń przez zespół przeprowadź wywiad z pacjentem wg
      <BoldText>SAMPLE i OPQRST</BoldText>
    </MyTextView>
    <MyTextView>
      <BoldText>S -</BoldText>
      symptomy
    </MyTextView>
    <MyTextView>
      <BoldText>A -</BoldText>
      alergie
    </MyTextView>
    <MyTextView>
      <BoldText>M -</BoldText>
      medykamenty
    </MyTextView>
    <MyTextView>
      <BoldText>P -</BoldText>
      przeszłość chorobowa
    </MyTextView>
    <MyTextView>
      <BoldText>L -</BoldText>
      lunch
    </MyTextView>
    <MyTextView>
      <BoldText>E -</BoldText>
      events - wydarzenia poprzedzające wezwanie
    </MyTextView>
    <MyTextView />
    <MyTextView>
      <BoldText>OPQRST</BoldText>
    </MyTextView>
    <MyTextView>
      <BoldText>O -</BoldText>
      od kiedy?
    </MyTextView>
    <MyTextView>
      <BoldText>P -</BoldText>
      prowokacja (co wywołuje objawy)
    </MyTextView>
    <MyTextView>
      <BoldText>Q -</BoldText>
      jakość objawu (kłuje, piecze)
    </MyTextView>
    <MyTextView>
      <BoldText>R -</BoldText>
      radiacja (promieniowanie objawu)
    </MyTextView>
    <MyTextView>
      <BoldText>S -</BoldText>
      siła w skali 1 - 10
    </MyTextView>
    <MyTextView>
      <BoldText>T -</BoldText>
      trwanie objawu (stały, kolkowy, czy kiedyś już wystąpił)
    </MyTextView>
  </WskazowkiContainer>
);

export default Zlecenia;
