import React from 'react';
import { Image } from 'react-native';
import { MyTextView, Sol } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const MorfinaDzieci = () => (
  <WskazowkiContainer>
    <Image source={require('./images/kid.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>Dawka Morfiny u dzieci to 0,1 mg/kg.</MyTextView>
    <MyTextView>
      Rozcieńcz 10 mg morfiny do 10 ml
      <Sol />
    </MyTextView>
    <MyTextView>Teraz podaj 0,1 ml/kg, czyli w 1 ml jest dawka na 10 kg wagi ciała.</MyTextView>
    <MyTextView>
      Pamiętaj, że lek ten może wymagać powtórzenia dawki do momentu uzyskania efektu!
    </MyTextView>
  </WskazowkiContainer>
);

export default MorfinaDzieci;
