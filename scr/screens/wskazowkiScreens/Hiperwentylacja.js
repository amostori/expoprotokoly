import { Image } from 'react-native';
import React from 'react';

import { MyTextView, BoldText } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const Hiperwentylacja = () => (
  <WskazowkiContainer>
    <Image source={require('./images/stetoskop.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      <BoldText>Dorośli:</BoldText>
      20 oddechów/min (wdech co 3 sek).
    </MyTextView>
    <MyTextView>
      <BoldText>Dzieci:</BoldText>
      25 oddechów/min (wdech co 2,5 sek).
    </MyTextView>
    <MyTextView>
      <BoldText>Niemowlęta:</BoldText>
      30 oddechów/min (wdech co 2 sek).
    </MyTextView>
    <MyTextView>
      W celu oceny czy hiperwentylacja jest wykonywana prawidłowo skorzystaj z kapnometru. Utrzymuj
      EtCO2 na poziomie 30 - 35 mmHg.
    </MyTextView>
    <MyTextView>Jeśli objawy wgłobienia ustępują przerwij hiperwentylację.</MyTextView>
  </WskazowkiContainer>
);

export default Hiperwentylacja;
