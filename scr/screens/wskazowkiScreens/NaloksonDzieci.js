import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const NaloksonDzieci = () => (
  <WskazowkiContainer>
    <Image source={require('./images/kid.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      W przypadku zatrucia opioidami dawka Naloksonu u dzieci to:
      <BoldText>0,01 mg/kg:</BoldText>
    </MyTextView>
    <MyTextView>
      Nabierz ampułkę (0,4 mg) do strzykawki &quot;dziesiątki&quot;, ale
      <BoldText>UWAGA,</BoldText>
      rozcieńcz do 4 ml. Teraz podawaj w dawce 0,1 ml/kg, czyli w 1 ml jest dawka na 10 kg wagi
      ciała.
    </MyTextView>
  </WskazowkiContainer>
);

export default NaloksonDzieci;
