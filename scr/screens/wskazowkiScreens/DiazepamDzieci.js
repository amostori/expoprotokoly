import React from 'react';
import { Image } from 'react-native';
import { MyTextView, BoldText, ListaTextView } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const DiazepamDzieci = () => (
  <WskazowkiContainer>
    <Image source={require('./images/kid.png')} style={styles.image} resizeMode="contain" />
    <MyTextView>
      W przypadku drgawek dawka diazepamu to
      <BoldText>0,25 mg/kg:</BoldText>
    </MyTextView>
    <MyTextView>
      Nabierz ampułkę (10 mg) do strzykawki &quot;dwójki&quot;. Teraz w 0,1 ml (jedna
      &quot;kreseczka&quot;) jest dawka na
      <BoldText>2 kg wagi ciała.</BoldText>
      Najłatwiej podzielić wagę dziecka przez
      <BoldText>2</BoldText>
i tyle &quot;kreseczek&quot; leku podać.
    </MyTextView>
    <MyTextView>
      <BoldText>Przykład</BoldText>
    </MyTextView>
    <ListaTextView>Dziecko waży 10 kg.</ListaTextView>
    <ListaTextView>10/2 = 5</ListaTextView>
    <ListaTextView>Podajemy 5 &quot;kreseczek&quot;, czyli 0,5 ml leku.</ListaTextView>
    <MyTextView />
    <MyTextView>
      Działanie uspokajające:
      <BoldText>0,1 mg/kg</BoldText>
    </MyTextView>
    <MyTextView>
      Nabierz ampułkę (10 mg) do strzykawki &quot;dwójki&quot;. Teraz w 0,1 ml (jedna
      &quot;kreseczka&quot;) jest dawka na
      <BoldText>5 kg wagi ciała.</BoldText>
      Najłatwiej podzielić wagę dziecka przez
      <BoldText>5</BoldText>
i tyle &quot;kreseczek&quot; leku podać.
    </MyTextView>
    <MyTextView>
      <BoldText>Przykład</BoldText>
    </MyTextView>
    <ListaTextView>Dziecko waży 10 kg.</ListaTextView>
    <ListaTextView>10/5 = 2</ListaTextView>
    <ListaTextView>Podajemy 2 &quot;kreseczki&quot;, czyli 0,2 ml leku.</ListaTextView>
    <MyTextView />
  </WskazowkiContainer>
);

export default DiazepamDzieci;
