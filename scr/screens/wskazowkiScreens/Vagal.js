import { Image } from 'react-native';
import React from 'react';

import { MyTextView } from '../../components/TextViews';
import { WskazowkiContainer } from '../../components/container';
import styles from './styles';

const Vagal = () => (
  <WskazowkiContainer>
    <Image source={require('./images/jozef.jpg')} style={styles.bigImage} resizeMode="contain" />
    <MyTextView>
      Pobudzanie nerwu błędnego można wykonać przez próbę Valsavy, masaż zatoki szyjnej czy okład z
      lodu na twarz. Ucisk na gałki oczne jest przeciwwskazany.
    </MyTextView>
    <MyTextView>
      Zdecydowanie najbezpieczniej jest wykonać próbę Valsalvy z nagłą zmianą pozycji ciała.
    </MyTextView>
    <MyTextView>
      Ułóż pacjenta w pozycji półleżącej i monitoruj jego ekg. Weź dren od maski do podawania tlenu
      i nałóż jeden koniec na manometr do pomiaru ciśnienia krwi. Drugi koniec wręcz pacjentowi.
    </MyTextView>
    <MyTextView>
      Teraz poproś go by dmuchał tak by ciśnienie osiągnęło około 40 mmHg (nie więcej niż 50) przez
      10 - 30 sekund. Kiedy skończy natychmiast położ go płasko i unieś jego nogi pod kątem 45
      stopni. Rytm zatokowy powinien powrócić w przeciągu 1 min.
    </MyTextView>
    <MyTextView>
      Skuteczność tego zabiegu w SVT to około 43% (bez modyfikacji pozycji ciała 10 - 20%).
    </MyTextView>
    <MyTextView>
      Masaż zatoki szyjnej wykonuje się masując przez 5 sekund okolicę szyi na wysokości chrząstki
      pierścieniowatej i kąta żuchwy. Wcześniej osłuchaj to miejsce. Jeśli słyszalny jest szmer nie
      wykonuj tego zabiegu bo może oderwać się blaszka miażdżycowa i spowodować udar mózgu.
    </MyTextView>
    <MyTextView>
      Pamiętaj by pacjent był stale monitorowany w czasie stymulowania nerwu błędnego.
    </MyTextView>
  </WskazowkiContainer>
);

export default Vagal;
