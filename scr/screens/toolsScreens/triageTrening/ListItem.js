import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity } from 'react-native';
import { MyTextView } from '../../../components/TextViews';

const ListItem = ({ text, handleOnPress }) => (
  <TouchableOpacity onPress={handleOnPress}>
    <View>
      <MyTextView>{text}</MyTextView>
    </View>
  </TouchableOpacity>
);

ListItem.propTypes = {
  text: PropTypes.string,
  handleOnPress: PropTypes.func,
};

export default ListItem;
