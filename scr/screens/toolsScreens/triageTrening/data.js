export default [
  'Oddycha po udrożnieniu dróg oddechowych?',
  'Spełnia polecenia?',
  'Udrożnij drogi oddechowe',
  'Poszkodowany chodzi?',
  'Oddycha bez udrożnienia dróg oddechowych?',
  'Obecne tętno na tętnicy promieniowej?',
  'Częstość oddechu?',
];
