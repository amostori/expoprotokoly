import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, View, Text } from 'react-native';

const StartButton = ({ onPress, tekst, disabled }) => (
  <TouchableOpacity onPress={onPress} disabled={disabled}>
    <View style={{}}>
      <Text
        style={{
          color: 'red',
          fontSize: 32,
          textAlign: 'center',
          fontWeight: '900',
        }}
      >
        {tekst}
      </Text>
    </View>
  </TouchableOpacity>
);

StartButton.propTypes = {
  onPress: PropTypes.func,
  tekst: PropTypes.any,
  disabled: PropTypes.bool,
};

export default StartButton;
