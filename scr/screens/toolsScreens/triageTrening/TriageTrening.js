/* eslint-disable no-shadow */
import React, { Component } from 'react';
import { FlatList } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ScrollContainer } from '../../../components/container';
import { TriageColorButtons } from '../../../components/Buttons';
import { MyTextView, BoldText } from '../../../components/TextViews';
import StartButton from './StartButton';
import {
  startTheGame,
  diminishTime,
  runOutOfTime,
  handleErrorBadDirection,
  setInstructionText,
  setGreen,
  setRed,
  setYellow,
  setBlack,
  blackPlusOne,
  redPlusOne,
  yellowPlusOne,
  greenPlusOne,
  addVictim,
} from '../../../redux/actions';
import { Separator } from '../../../components/ListViews/ALSList';
import ListItem from './ListItem';
import questions from './data';

class TriageTrening extends Component {
  static propTypes = {
    tekst: PropTypes.any,
    seconds: PropTypes.any,
    startTheGame: PropTypes.func,
    diminishTime: PropTypes.func,
    runOutOfTime: PropTypes.func,
    disabled: PropTypes.bool,
    canGreenBePressed: PropTypes.bool,
    canYellowBePressed: PropTypes.bool,
    canRedBePressed: PropTypes.bool,
    canBlackBePressed: PropTypes.bool,
    nextQuestionAllowed: PropTypes.bool,
    instruction: PropTypes.string,
    questionNumber: PropTypes.number,
    blacks: PropTypes.number,
    reds: PropTypes.number,
    yellows: PropTypes.number,
    greens: PropTypes.number,
    totalVictimNumber: PropTypes.number,
    handleErrorBadDirection: PropTypes.func,
    setInstructionText: PropTypes.func,
    setGreen: PropTypes.func,
    setRed: PropTypes.func,
    setYellow: PropTypes.func,
    setBlack: PropTypes.func,
    blackPlusOne: PropTypes.func,
    redPlusOne: PropTypes.func,
    yellowPlusOne: PropTypes.func,
    greenPlusOne: PropTypes.func,
    addVictim: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.timer = null;
  }

  state = {
    // eslint-disable-next-line react/no-unused-state
    needToRerender: false,
  };

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  endOfTime = () => {
    const { runOutOfTime } = this.props;
    runOutOfTime();
    clearInterval(this.timer);
    this.timer = null;
  };

  doEverySecond = () => {
    const { seconds, diminishTime } = this.props;
    if (seconds === 0) {
      clearInterval(this.timer);
      this.endOfTime();
    } else {
      diminishTime();
    }
  };

  handleOnStartButton = () => {
    if (this.timer !== null) {
      clearInterval(this.timer);
      this.timer = null;
    }
    const { startTheGame } = this.props;
    startTheGame();
    this.timer = setInterval(this.doEverySecond, 1000);
  };

  badDirection = (errorText) => {
    const { handleErrorBadDirection } = this.props;
    handleErrorBadDirection(errorText);
    clearInterval(this.timer);
    this.timer = null;
  };

  greenAllowed = () => {
    const { setInstructionText, setGreen } = this.props;
    setInstructionText('Poszkodowany chodzący.');
    setGreen();
  };

  yellowAllowed = () => {
    const { setInstructionText, setYellow } = this.props;
    setInstructionText('Poszkodowany spełnia polecenia.');
    setYellow();
  };

  redAllowed = (tekst) => {
    const { setInstructionText, setRed } = this.props;
    setInstructionText(tekst);
    setRed();
  };

  blackAllowed = () => {
    const { setInstructionText, setBlack } = this.props;
    setInstructionText('Poszkodowany nie oddycha po udrożnieniu.');
    setBlack();
  };

  nextQuestionNotAllowed = () => {
    const { handleErrorBadDirection } = this.props;
    handleErrorBadDirection(
      'Powinieneś już wcześniej przyznać kolor. To pytanie jest nie potrzebne.',
    );
    clearInterval(this.timer);
    this.timer = null;
  };

  handleOnPress = (item) => {
    const {
      questionNumber, setInstructionText, instruction, nextQuestionAllowed,
    } = this.props;
    switch (item) {
      case 'Poszkodowany chodzi?':
        if (questionNumber === 0) {
          this.handleOnStartButton();
          const randomChodzi = Math.floor(Math.random() * 10 + 1);
          if (randomChodzi > 7) {
            this.greenAllowed();
          } else {
            setInstructionText('Poszkodowany nie może chodzić.');
          }
        } else {
          this.badDirection(
            'Zła kolejność. Zacznij od pytania o to czy poszkodowany jest chodzący.',
          );
        }
        break;
      case 'Oddycha bez udrożnienia dróg oddechowych?':
        if (nextQuestionAllowed) {
          if (instruction === 'Poszkodowany nie może chodzić.') {
            const randomValue = Math.floor(Math.random() * 10 + 1);
            if (randomValue > 3) {
              setInstructionText('Oddycha.');
            } else {
              setInstructionText('Nie oddycha.');
            }
          } else {
            this.badDirection('Zła kolejność.');
          }
        } else {
          this.nextQuestionNotAllowed();
        }
        break;
      case 'Udrożnij drogi oddechowe':
        if (nextQuestionAllowed) {
          if (instruction === 'Nie oddycha.') {
            setInstructionText('Drogi oddechowe drożne.');
          } else {
            this.badDirection('Zła kolejność.');
          }
        } else {
          this.nextQuestionNotAllowed();
        }
        break;
      case 'Oddycha po udrożnieniu dróg oddechowych?':
        if (nextQuestionAllowed) {
          if (instruction === 'Drogi oddechowe drożne.') {
            const randomValue = Math.floor(Math.random() * 10 + 1);
            if (randomValue > 5) {
              this.redAllowed('Poszkodowany oddycha po udrożnieniu dróg oddechowych.');
            } else {
              this.blackAllowed();
            }
          } else {
            this.badDirection('Zła kolejność.');
          }
        } else {
          this.nextQuestionNotAllowed();
        }
        break;
      case 'Częstość oddechu?':
        if (nextQuestionAllowed) {
          if (instruction === 'Oddycha.') {
            const randomValue = Math.floor(Math.random() * 10 + 1);
            if (randomValue > 7) {
              this.redAllowed('Częstość oddechu powyżej 30/min.');
            } else {
              setInstructionText('Częstość oddechu poniżej 30/min.');
            }
          } else {
            this.badDirection('Zła kolejność postępowania.');
          }
        } else {
          this.nextQuestionNotAllowed();
        }
        break;
      case 'Obecne tętno na tętnicy promieniowej?':
        if (nextQuestionAllowed) {
          if (instruction === 'Częstość oddechu poniżej 30/min.') {
            const randomValue = Math.floor(Math.random() * 10 + 1);
            if (randomValue > 7) {
              this.redAllowed('Brak tętna na tętnicy promieniowej.');
            } else {
              setInstructionText('Tętno na tętnicy promieniowej obecne.');
            }
          } else {
            this.badDirection('Zła kolejność postępowania.');
          }
        } else {
          this.nextQuestionNotAllowed();
        }
        break;
      case 'Spełnia polecenia?':
        if (nextQuestionAllowed) {
          if (instruction === 'Tętno na tętnicy promieniowej obecne.') {
            const randomValue = Math.floor(Math.random() * 10 + 1);
            if (randomValue > 6) {
              this.redAllowed('Poszkodowany nie spełnia poleceń.');
            } else {
              setInstructionText('Poszkodowany spełnia polecenia.');
              this.yellowAllowed();
            }
          } else {
            this.badDirection('Zła kolejność postępowania.');
          }
        } else {
          this.nextQuestionNotAllowed();
        }
        break;
      default:
        alert('error');
    }
  };

  handleIncreaseGreen = () => {
    const { canGreenBePressed, greenPlusOne, addVictim } = this.props;
    if (canGreenBePressed) {
      greenPlusOne();
      addVictim();
      clearInterval(this.timer);
      this.timer = null;
    } else {
      this.badDirection('Popełniłeś błąd. Spróbuj jeszcze raz.');
    }
  };

  handleYellowPress = () => {
    const { canYellowBePressed, yellowPlusOne, addVictim } = this.props;
    if (canYellowBePressed) {
      yellowPlusOne();
      addVictim();
      clearInterval(this.timer);
      this.timer = null;
    } else {
      this.badDirection('Popełniłeś błąd. Spróbuj jeszcze raz.');
    }
  };

  handleRedPress = () => {
    const { canRedBePressed, redPlusOne, addVictim } = this.props;
    if (canRedBePressed) {
      redPlusOne();
      addVictim();
      clearInterval(this.timer);
      this.timer = null;
    } else {
      this.badDirection('Popełniłeś błąd. Spróbuj jeszcze raz.');
    }
  };

  handleBlackPress = () => {
    const { canBlackBePressed, blackPlusOne, addVictim } = this.props;
    if (canBlackBePressed) {
      blackPlusOne();
      addVictim();
      clearInterval(this.timer);
      this.timer = null;
    } else {
      this.badDirection('Popełniłeś błąd. Spróbuj jeszcze raz.');
    }
  };

  render() {
    const {
      tekst, disabled, instruction, blacks, reds, yellows, greens, totalVictimNumber,
    } = this.props;
    return (
      <ScrollContainer>
        <BoldText>{`Total: ${totalVictimNumber}`}</BoldText>
        <TriageColorButtons
          onPressGreen={this.handleIncreaseGreen}
          onPressYellow={this.handleYellowPress}
          onPressRed={this.handleRedPress}
          onPressBlack={this.handleBlackPress}
          greenTekst={greens}
          yellowTekst={yellows}
          redTekst={reds}
          blackTekst={blacks}
        />
        <BoldText>{instruction}</BoldText>
        <StartButton onPress={this.handleOnStartButton} tekst={tekst} disabled={disabled} />
        <MyTextView />
        <FlatList
          data={questions}
          extraData={this.state}
          renderItem={({ item }) => (
            <ListItem text={item} handleOnPress={() => this.handleOnPress(item)} />
          )}
          keyExtractor={item => item}
          ItemSeparatorComponent={Separator}
        />
      </ScrollContainer>
    );
  }
}

const mapStateToProps = ({ triageTrainingReducer }) => {
  const {
    tekst,
    seconds,
    disabled,
    instruction,
    questionNumber,
    canGreenBePressed,
    canYellowBePressed,
    canRedBePressed,
    canBlackBePressed,
    nextQuestionAllowed,
    blacks,
    reds,
    yellows,
    greens,
    totalVictimNumber,
  } = triageTrainingReducer;
  return {
    tekst,
    seconds,
    disabled,
    instruction,
    questionNumber,
    canGreenBePressed,
    canYellowBePressed,
    canRedBePressed,
    canBlackBePressed,
    nextQuestionAllowed,
    blacks,
    reds,
    yellows,
    greens,
    totalVictimNumber,
  };
};

export default connect(
  mapStateToProps,
  {
    startTheGame,
    diminishTime,
    runOutOfTime,
    handleErrorBadDirection,
    setInstructionText,
    setGreen,
    setRed,
    setYellow,
    setBlack,
    blackPlusOne,
    redPlusOne,
    yellowPlusOne,
    greenPlusOne,
    addVictim,
  },
)(TriageTrening);
