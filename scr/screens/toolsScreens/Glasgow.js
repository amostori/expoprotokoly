import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { ToolsContainer } from '../../components/container';
import { MyTextView, RedBoldText, BoldText } from '../../components/TextViews';

const styles = StyleSheet.create({
  container: {
    minHeight: '100%',
    backgroundColor: '#E9ECF0',
    alignItems: 'center',
  },
  picker: {
    alignItems: 'center',
    backgroundColor: '#d1d4d8',
  },
  text: {
    textAlign: 'center',
    marginTop: 20,
  },
  pickerChoose: {
    marginVertical: 10,
    marginHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: '#d1d4d8',
    textAlign: 'center',
  },
  bigText: {
    fontWeight: 'bold',
    fontSize: 15,
    color: '#FF5353',
    marginVertical: 20,
  },
});

export default class Glasgow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pointsLabel: '3',
      points1: 1,
      points2: 1,
      points3: 1,
      points1Options: [
        { label: '1 Brak reakcji', value: '1' },
        { label: '2 Otwiera na ból', value: '2' },
        { label: '3 Otwiera na głos', value: '3' },
        { label: '4 Otwiera spontanicznie', value: '4' },
      ],
      points2Options: [
        { label: '1 Brak reakcji', value: '1' },
        { label: '2 Niezrozumiałe dźwięki', value: '2' },
        { label: '3 Nieprawidłowe słowa', value: '3' },
        { label: '4 Splątany', value: '4' },
        { label: '5 Zorientowany', value: '5' },
      ],
      points3Options: [
        { label: '1 Brak reakcji', value: '1' },
        { label: '2 Reakcja wyrpostna', value: '2' },
        { label: '3 Reakcja zgięciowa', value: '3' },
        { label: '4 Ucieczka przed bólem', value: '4' },
        { label: '5 Lokalizacja bólu', value: '5' },
        { label: '6 Spełnia polecenia', value: '6' },
      ],
    };
  }

  calculate = () => {
    this.setState({
      pointsLabel:
        Number(this.state.points1) + Number(this.state.points2) + Number(this.state.points3),
    });
  };

  renderPunkty = () => {
    const { pointsLabel } = this.state;
    if (pointsLabel < 5) {
      return (
        <BoldText style={{ fontSize: 25 }}>
          Razem:
          {` ${pointsLabel}`}
          {' '}
punkty
        </BoldText>
      );
    }
    return (
      <BoldText style={{ fontSize: 25 }}>
        Razem:
        {` ${pointsLabel}`}
        {' '}
punktów
      </BoldText>
    );
  };

  render() {
    return (
      <ToolsContainer>
        <MyTextView>Służy do oceny głębokości śpiączki.</MyTextView>
        <MyTextView>
          Warto pamiętać, że pacjent z wynikiem 8 i mniej powinien być zaintubowany.
        </MyTextView>
        <MyTextView>
          Skala Glasgow nie daje miarodajnych wyników w następujących przypadkach: zatrucie (także
          alkoholem), pacjent zaintubowany, po podaniu leków zwiotczających, pacjent we wstrząsie.
        </MyTextView>
        <MyTextView />

        <RedBoldText>I Otwieranie oczu</RedBoldText>
        <MyTextView />
        <RNPickerSelect
          items={this.state.points1Options}
          placeholder={{}}
          onValueChange={(value) => {
            this.setState({ points1: value }, () => {
              this.calculate();
            });
          }}
        >
          <Text style={styles.pickerChoose}>
            {this.state.points1Options[Number(this.state.points1) - 1].label}
          </Text>
        </RNPickerSelect>
        <MyTextView />

        <RedBoldText>II Reakcja werbalna</RedBoldText>
        <MyTextView />
        <View>
          <RNPickerSelect
            items={this.state.points2Options}
            style={styles.picker}
            placeholder={{}}
            onValueChange={(value) => {
              this.setState({ points2: value }, () => {
                this.calculate();
              });
            }}
          >
            <Text style={styles.pickerChoose}>
              {this.state.points2Options[Number(this.state.points2) - 1].label}
            </Text>
          </RNPickerSelect>
        </View>
        <MyTextView />

        <RedBoldText>III Reakcja motoryczna</RedBoldText>
        <MyTextView />

        <RNPickerSelect
          items={this.state.points3Options}
          placeholder={{}}
          onValueChange={(value) => {
            this.setState({ points3: value }, () => {
              this.calculate();
            });
          }}
        >
          <Text style={styles.pickerChoose}>
            {this.state.points3Options[Number(this.state.points3) - 1].label}
          </Text>
        </RNPickerSelect>
        <MyTextView />
        {this.renderPunkty()}
        <MyTextView />
        <MyTextView />
        <MyTextView />
      </ToolsContainer>
    );
  }
}
