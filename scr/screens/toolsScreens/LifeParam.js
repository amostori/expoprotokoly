import React from 'react';
import {
  ScrollView, StyleSheet, View, Text,
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    minHeight: '100%',
    alignItems: 'center',
    marginHorizontal: 20,
    marginBottom: 20,
  },
  text: {
    textAlign: 'center',
    marginTop: 20,
  },
});

const LifeParam = () => (
  <ScrollView contentContainerStyle={styles.contentContainer}>
    <View style={styles.container}>
      <Text style={styles.text}>
        &lt;1 roku życia: 30 - 40/min
        {' '}
        {'\n'}
        {' '}
2 - 5 lat: 24 - 30/min
        {' '}
        {'\n'}
        {' '}
5 - 12 lat: 20 - 24/min
        {' '}
        {'\n'}
        {' '}
&gt;12 lat: 12 - 20/min
        {'\n'}
        {'\n'}
        <Text style={{ fontWeight: 'bold' }}>Tachypnoe</Text>
        {'\n'}
        {'\n'}
        {' '}
Noworodek: oddech &gt;60/min
        {'\n'}
        {' '}
Niemowlę: oddech &gt;50/min
        {'\n'}
        {' '}
2 - 5 r.ż: oddech &gt;40/min
        {'\n'}
        {' '}
&gt;5 r.ż: oddech &gt;30/min
        {'\n'}
        {'\n'}
        <Text style={{ fontStyle: 'italic' }}>
          {' '}
          Objętość oddechowa u wszystkich dzieci: 5 - 7 ml/kg
        </Text>
        {'\n'}
        {'\n'}
        {'\n'}
        <Text style={{ fontWeight: 'bold' }}>Akcja serca</Text>
        {'\n'}
        {'\n'}
        {' '}
Wcześniak: 100 - 160/min
        {'\n'}
        {' '}
Noworodek: 100 - 180/min
        {'\n'}
        {' '}
1 - 6 m.ż. 110 - 180/min
        {'\n'}
        {' '}
6 - 12 m.ż. 110 - 170/min
        {'\n'}
        {' '}
12 - 24 m.ż. 90 - 150/min
        {'\n'}
        {' '}
2 - 6 lat: 70 - 140/min
        {'\n'}
        {' '}
6 - 12 lat: 60 - 130/min
        {'\n'}
        {'\n'}
        {'\n'}
        <Text style={{ fontWeight: 'bold' }}>Ciśnienie tętnicze</Text>
        {'\n'}
        {'\n'}
        {' '}
Wcześniak: skurczowe 40 - 60 mmHg
        {'\n'}
        {' '}
Noworodek: 70/50 mmHg
        {'\n'}
1 - 6 m.ż. 80/50 mmHg
        {'\n'}
        {' '}
6 - 12 m.ż. 90/65 mmHg
        {'\n'}
        {' '}
12 - 24 m.ż. 95/65 mmHg
        {'\n'}
        {' '}
2 - 6 lat: 100/60 mmHg
        {'\n'}
        {' '}
6 - 12 lat: 110/60 mmHg
        {'\n'}
        {'\n'}
        <Text style={{ fontStyle: 'italic' }}>
          Minimalne ciśnienie skurczowe u dzieci:
          {' '}
          {'\n'}
          {'\n'}
          {' '}
0 - 28 dni życia: &gt;60 mmHg
          {'\n'}
          {' '}
1 - 12 m.ż.: &gt;70 mmHg
          {'\n'}
          {' '}
1 - 10 lat: &gt;70 + (2x wiek)
          {'\n'}
          {' '}
&gt;10 lat: &gt;90 mmHg
        </Text>
        {'\n'}
        {'\n'}
        {'\n'}
        <Text style={{ fontWeight: 'bold' }}>Saturacja świeżorodka:</Text>
        {'\n'}
        {'\n'}
        {' '}
po 2 min: 60%
        {'\n'}
        {' '}
po 3 min: 70%
        {'\n'}
        {' '}
po 4 min: 80%
        {'\n'}
        {' '}
po 5 min: 85%
        {'\n'}
        {' '}
po 10 min: 90%
        {'\n'}
        {'\n'}
        {'\n'}
        <Text style={{ fontWeight: 'bold' }}>Poziom glukozy</Text>
        {'\n'}
        {'\n'}
        {' '}
Wcześniak: &gt;50 mg/dl
        {'\n'}
        {' '}
Noworodek: &gt;40 mg/dl
        {'\n'}
        {' '}
Dziecko starsze: &gt;60 mg/dl
        {'\n'}
        {'\n'}
        <Text style={{ fontStyle: 'italic' }}>
          Wg definicji hipoglikemia to stan gdy poziom poziom cukru we krwi wynosi &lt;70 mg/dl.
        </Text>
        {'\n'}
        {'\n'}
        <Text style={{ fontWeight: 'bold' }}>Waga dziecka</Text>
        {'\n'}
        {'\n'}
        Noworodek: 3,5 kg
        {' '}
        {'\n'}
        {' '}
2 m.ż.: 5 kg
        {' '}
        {'\n'}
        {' '}
6 m.ż.: 8 kg
        {' '}
        {'\n'}
        {' '}
9 m.ż.: 9 kg
        {' '}
        {'\n'}
        {' '}
1 rok:
        10 kg
        {' '}
        {'\n'}
        {' '}
2 lata: 12 kg
        {' '}
        {'\n'}
        {' '}
3 lata: 14 kg
        {' '}
        {'\n'}
        {' '}
4 lata: 16 kg
        {' '}
        {'\n'}
        {' '}
5 lat: 18 kg
        {' '}
        {'\n'}
        {' '}
6 lat: 20 kg
        {'\n'}
        {' '}
7 lat: 22 kg
        {'\n'}
8 lat: 25 kg
        {'\n'}
        {' '}
9 lat: 28 kg
        {'\n'}
        {' '}
10 lat: 33 kg
        {'\n'}
        {' '}
11 lat: 36 kg
        {'\n'}
        {' '}
12 lat: 40 kg
      </Text>
    </View>
  </ScrollView>
);

export default LifeParam;
