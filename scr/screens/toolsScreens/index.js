import CPRAssistant from './CPRAssistant';
import Glasgow from './Glasgow';
import LifeParam from './LifeParam';
import Apgar from './Apgar';
import Westley from './Westley';
import Catheter from './Catheter';
import Pressure from './Pressure';
import TriageTrening from './triageTrening/TriageTrening';

export {
  CPRAssistant, Glasgow, LifeParam, Apgar, Westley, Catheter, Pressure, TriageTrening,
};
