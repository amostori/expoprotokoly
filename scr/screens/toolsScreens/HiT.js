import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { CheckBox } from 'react-native-elements';

const HiT = ({
  hipoksjaChecked,
  hipoksjaOnPress,
  hipowolemiaChecked,
  hipowolemiaOnPress,
  hipotermiaOnPress,
  hipotermiaChecked,
  elektrolityOnPress,
  elektrolityChecked,
  zakrzepOnPress,
  zakrzepChecked,
  odmaOnPress,
  odmaChecked,
  zatruciaOnPress,
  zatruciaChecked,
  tamponadaOnPress,
  tamponadaChecked,
  kwasicaOnPress,
  kwasicaChecked,
}) => (
  <View>
    <CheckBox title="Hipoksja" checked={hipoksjaChecked} onPress={hipoksjaOnPress} />
    <CheckBox title="Hipowolemia" checked={hipowolemiaChecked} onPress={hipowolemiaOnPress} />
    <CheckBox title="Hipotermia" checked={hipotermiaChecked} onPress={hipotermiaOnPress} />
    <CheckBox
      title="Zaburzenia elektrolitowe"
      checked={elektrolityChecked}
      onPress={elektrolityOnPress}
    />
    <CheckBox
      title="Zaburzenia zakrzepowo-zatorowe"
      checked={zakrzepChecked}
      onPress={zakrzepOnPress}
    />
    <CheckBox title="Odma prężna" checked={odmaChecked} onPress={odmaOnPress} />
    <CheckBox title="Zatrucia" checked={zatruciaChecked} onPress={zatruciaOnPress} />
    <CheckBox title="Tamponada osierdzia" checked={tamponadaChecked} onPress={tamponadaOnPress} />
    <CheckBox title="Kwasica" checked={kwasicaChecked} onPress={kwasicaOnPress} />
  </View>
);

export default HiT;

HiT.propTypes = {
  hipoksjaChecked: PropTypes.bool,
  hipowolemiaChecked: PropTypes.bool,
  hipotermiaChecked: PropTypes.bool,
  elektrolityChecked: PropTypes.bool,
  zakrzepChecked: PropTypes.bool,
  odmaChecked: PropTypes.bool,
  zatruciaChecked: PropTypes.bool,
  tamponadaChecked: PropTypes.bool,
  kwasicaChecked: PropTypes.bool,
  hipoksjaOnPress: PropTypes.func,
  hipowolemiaOnPress: PropTypes.func,
  hipotermiaOnPress: PropTypes.func,
  elektrolityOnPress: PropTypes.func,
  zakrzepOnPress: PropTypes.func,
  odmaOnPress: PropTypes.func,
  zatruciaOnPress: PropTypes.func,
  tamponadaOnPress: PropTypes.func,
  kwasicaOnPress: PropTypes.func,
};
