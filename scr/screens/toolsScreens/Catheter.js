import React from 'react';
import { Image, View } from 'react-native';
import { ToolsContainer } from '../../components/container';
import { MyTextView, BoldText } from '../../components/TextViews';

const styles = {
  bigImage: {
    // flex: 1,
    // justifyContent: 'center',
    marginBottom: 20,
    height: 300,
    width: 300,
  },
  imageContainer: {
    flex: 1,
    alignItems: 'center',
  },
};

const Catheter = () => (
  <ToolsContainer>
    <View style={styles.imageContainer}>
      <Image source={require('./data/cateter.png')} style={styles.bigImage} resizeMode="contain" />
    </View>
    <MyTextView>
      W wytycznych ERC 2015 czytamy, że jeśli pacjent posiada dostęp dializacyjny to w sytuacji
      stanu zagrożenia życia lub zatrzymania krążenia należy z niego korzystać. Cewnik dializacyjny
      jest specjalnym rodzajem dostępu dożylnego i traktowany jest jako wkłucie centralne, więc
      ratownik medyczny, w świetle ustawy o Państwowym Ratownictwie Medycznym, nie może z niego
      korzystać.
      {'\n'}
      {'\n'}
      {' '}
Niemniej warto znać sposób postępowania z tego typu wkłuciem:
      {'\n'}
      {'\n'}
    </MyTextView>
    <BoldText>Przygotowanie do użycia:</BoldText>
    <MyTextView>
      {'\n'}
      {'\n'}
      {' '}
Cewniki dializacyjne są specjalnym dostępem. Duża średnica i dwa światła (choć są i
      trzyświatłowe kaniule dializacyjne, ale tylko tzw. ostre, w których to trzecie światło służy
      do podawania leków). Światła tychże kaniul są zawsze (a przynajmniej powinny być zawsze)
      wypełnione (jeżeli nie są używane) heparyną, cytrynianem, albo taurolidyną, które zapobiegają
      powstawaniu w nich skrzepów. Z tego też powodu przed przystąpieniem do podawania leków należy:
      {'\n'}
      {'\n'}
      {' '}
1. Spryskać korek i część proksymalną światła kaniuli na którą jest on nakręcony
      środkiem dezynfekującym.
      {'\n'}
      {'\n'}
      {' '}
2. Odczekać ok. 60-90 sek., żeby środek dezynfekujący rozwinął swoje działanie
      przeciwdrobnoustrojowe.
      {'\n'}
      {'\n'}
      {' '}
3. Odkręcić korek i podłączyć &quot;pustą&quot; jałową strzykawkę.
      {'\n'}
      {'\n'}
      4. Zwolnić zacisk (Halkey&apos;a-Robertsa) na świetle proksymalnym kaniuli.
      {'\n'}
      {'\n'}
      {' '}
5. Odciągnąć zawartość światła kaniuli (na jej świetle jest podana objętość
      wypełnienia, zwykle 1,4-2,1 ml).
      {'\n'}
      {'\n'}
      {' '}
6. Przepłukać światło 0,9% NaCl i zacząć podawać leki, przepłykując 0,9% NaCl, po
      każdym podaniu leku.
      {'\n'}
      {'\n'}
    </MyTextView>
    <BoldText>Postępowanie po użyciu:</BoldText>
    <MyTextView>
      {'\n'}
      {'\n'}
      {' '}
1. Wypełnić światło kaniuli objętością podaną na świetle kaniuli (ok. 1,4- 2,1 ml),
      najlepiej taurolidyną, albo cytrynianem (Citrolock), albo heparyną, a ostatecznie 0,9% NaCl
      lub
      {'\n'}
      {'\n'}
      {' '}
2. Podłączyć powolny wlew kroplowy (kvo - na utrzymanie wkłucia) - uwaga na
      przewodnienie!
      {'\n'}
      {'\n'}
      {' '}
3. Zamknąć zacisk Halkey&apos;a-Robertsa.
      {'\n'}
      {'\n'}
      {' '}
4. Zabezpieczyć końcówkę proksymalną kaniuli nowym koreczkiem.
      {'\n'}
      {'\n'}
    </MyTextView>
  </ToolsContainer>
);

export default Catheter;
