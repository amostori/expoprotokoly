import React from 'react';
import {
  StyleSheet, View, Text, ScrollView,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { ToolsContainer } from '../../components/container';
import {
  MyTextView, RedBoldText, BoldText, ListaTextView,
} from '../../components/TextViews';

export default class Westley extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pointsLabel: '0',
      points1: 0,
      points2: 0,
      points3: 0,
      points4: 0,
      points5: 0,
      points1Options: [
        { value: 0, label: '0 Brak', points: 0 },
        { value: 1, label: '1 Tylko przy pobudzeniu', points: 1 },
        { value: 2, label: '2 W spoczynku', points: 2 },
      ],
      points2Options: [
        { value: 0, label: '0 Brak', points: 0 },
        { value: 1, label: '1 Niewielkie', points: 1 },
        { value: 2, label: '2 Umiarkowane', points: 2 },
        { value: 3, label: '2 Nasilone', points: 3 },
      ],
      points3Options: [
        { value: 0, label: '0 Prawidłowy', points: 0 },
        { value: 1, label: '1 Nieznacznie utrudniony', points: 1 },
        { value: 2, label: '2 Wyraźnie utrudniony', points: 2 },
      ],
      points4Options: [
        { value: 0, label: '0 Brak', points: 0 },
        { value: 1, label: '4 Przy pobudzeniu', points: 4 },
        { value: 2, label: '5 W spoczynu', points: 4 },
      ],
      points5Options: [
        { value: 0, label: '0 Prawidłowy', points: 0 },
        { value: 1, label: '5 dziecko podsypiające', points: 5 },
      ],
    };
  }

  calculate = () => {
    console.log(this.state.points5);
    this.setState({
      pointsLabel:
        this.state.points1Options[this.state.points1].points
        + this.state.points2Options[this.state.points2].points
        + this.state.points3Options[this.state.points3].points
        + this.state.points4Options[this.state.points4].points
        + this.state.points5Options[this.state.points5].points,
    });
  };

  renderPunkty = () => {
    const { pointsLabel } = this.state;
    switch (pointsLabel) {
      case 1:
        return (
          <BoldText style={{ fontSize: 25 }}>
            Razem:
            {` ${pointsLabel}`}
            {' '}
punkt
          </BoldText>
        );

      case 2:
        return (
          <BoldText style={{ fontSize: 25 }}>
            Razem:
            {` ${pointsLabel}`}
            {' '}
punkty
          </BoldText>
        );
      case 3:
        return (
          <BoldText style={{ fontSize: 25 }}>
            Razem:
            {` ${pointsLabel}`}
            {' '}
punkty
          </BoldText>
        );
      case 4:
        return (
          <BoldText style={{ fontSize: 25 }}>
            Razem:
            {` ${pointsLabel}`}
            {' '}
punkty
          </BoldText>
        );
      default:
        return (
          <BoldText style={{ fontSize: 25 }}>
            Razem:
            {` ${pointsLabel}`}
            {' '}
punktów
          </BoldText>
        );
    }
  };

  render() {
    return (
      <ToolsContainer>
        <RedBoldText>Stridor (świst krtaniowy)</RedBoldText>
        <MyTextView />
        <RNPickerSelect
          items={this.state.points1Options}
          placeholder={{}}
          onValueChange={(value) => {
            this.setState({ points1: value }, () => {
              this.calculate();
            });
          }}
        >
          <Text style={styles.pickerChoose}>
            {this.state.points1Options[this.state.points1].label}
          </Text>
        </RNPickerSelect>
        <MyTextView />

        <RedBoldText>Wciąganie międzyżebrzy</RedBoldText>
        <MyTextView />

        <RNPickerSelect
          items={this.state.points2Options}
          style={styles.picker}
          placeholder={{}}
          onValueChange={(value) => {
            this.setState({ points2: value }, () => {
              this.calculate();
            });
          }}
        >
          <Text style={styles.pickerChoose}>
            {this.state.points2Options[this.state.points2].label}
          </Text>
        </RNPickerSelect>
        <MyTextView />

        <RedBoldText>Wdech</RedBoldText>
        <MyTextView />

        <RNPickerSelect
          items={this.state.points3Options}
          placeholder={{}}
          onValueChange={(value) => {
            this.setState({ points3: value }, () => {
              this.calculate();
            });
          }}
        >
          <Text style={styles.pickerChoose}>
            {this.state.points3Options[this.state.points3].label}
          </Text>
        </RNPickerSelect>
        <MyTextView />

        <RedBoldText>Sinica</RedBoldText>
        <MyTextView />

        <RNPickerSelect
          items={this.state.points4Options}
          placeholder={{}}
          onValueChange={(value) => {
            this.setState({ points4: value }, () => {
              this.calculate();
            });
          }}
        >
          <Text style={styles.pickerChoose}>
            {this.state.points4Options[this.state.points4].label}
          </Text>
        </RNPickerSelect>
        <MyTextView />

        <RedBoldText>Stan przytomności</RedBoldText>
        <MyTextView />

        <RNPickerSelect
          items={this.state.points5Options}
          placeholder={{}}
          onValueChange={(value) => {
            this.setState({ points5: value }, () => {
              this.calculate();
            });
          }}
        >
          <Text style={styles.pickerChoose}>
            {this.state.points5Options[this.state.points5].label}
          </Text>
        </RNPickerSelect>
        <MyTextView />
        {this.renderPunkty()}
        <MyTextView />
      </ToolsContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    minHeight: '100%',
    backgroundColor: '#E9ECF0',
    alignItems: 'center',
  },
  picker: {
    alignItems: 'center',
    backgroundColor: '#d1d4d8',
  },
  text: {
    textAlign: 'center',
    marginTop: 20,
  },
  pickerChoose: {
    marginVertical: 10,
    marginHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: '#d1d4d8',
    textAlign: 'center',
  },
  bigText: {
    fontWeight: 'bold',
    fontSize: 15,
    marginVertical: 20,
    color: '#FF5353',
  },
});
