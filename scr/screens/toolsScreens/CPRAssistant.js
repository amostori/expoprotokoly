/* eslint-disable linebreak-style */
import React from 'react';
import {
  StyleSheet, View, Text, TouchableOpacity,
} from 'react-native';
import { Input } from 'react-native-elements';
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import { activateKeepAwake, deactivateKeepAwake } from 'expo-keep-awake';
import { Audio } from 'expo-av';
import { ToolsContainer } from '../../components/container';
import HiT from './HiT';
import {
  MyTextView, BoldText, SmallText, BigText,
} from '../../components/TextViews';

const styles = StyleSheet.create({
  column: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginLeft: 5,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  leki: {
    alignItems: 'center',

  },
  drugText: {
    fontSize: 22,
    fontWeight: 'bold',
  },
  drugIcon: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  rowHipo: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 15,
  },
  container: {
    minHeight: '100%',
    backgroundColor: '#E9ECF0',
    alignItems: 'center',
  },
  timerContainer: {
    flexDirection: 'row',
  },
  gridStyle: {
    justifyContent: 'center',
  },
  textRow: {},
  mainText: {
    fontWeight: 'bold',
  },
  secondaryText: {},
  refresh: {
    marginTop: 25,
    marginLeft: 15,
  },
  timerText: {
    fontSize: 70,
    marginTop: 25,
    width: 200,
  },
  ocenaRytmuText: {
    fontSize: 20,
    marginTop: 25,
    width: 200,
  },
  icon: {},
});

export default class CPRAssistant extends React.Component {
  constructor(props) {
    super(props);
    this.timeOld = new Date().valueOf();
    this.state = {
      time: 120,
      loops: 0,
      startTime: '00:00:00',
      timePast: 0,
      adrenalineCanUse: false,
      amiodaronCanUse: false,
      playPause: 'play',
      hypothermia: false,
      hipoksjaChecked: false,
      hipowolemiaChecked: false,
      hipotermiaChecked: false,
      elektrolityChecked: false,
      zakrzepChecked: false,
      odmaChecked: false,
      zatruciaChecked: false,
      tamponadaChecked: false,
      kwasicaChecked: false,
      ocenaRytmu: null,
      i: 130,
      dawkaAdrenaliny: '1 mg iv',
      dawkaAmiodaron: 'Podaj 300 mg rozcieńczone do 20 ml w 5% glukozie.',
      dawkaAmiodaronuMg: 0,
      adrenalinaTotal: 0,
      amiodaronTotal: 0,
      dawkaAdrenalinyMg: 0,
      iloscAmiodaronu: 0,
    };
    this.timer = null;
  }

  componentWillMount() {
    activateKeepAwake();
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    deactivateKeepAwake();
  }

  Hypothermia = () => {
    const { hypothermia } = this.state;
    const hipo = !hypothermia;
    this.setState({ hypothermia: hipo });
  };

  ResetCouting = () => {
    this.setState({
      time: 120, i: 130,
    });
    this.timeOld = new Date().valueOf();
  };

  Adrenaline = () => {
    const {
      loops, dawkaAdrenalinyMg, adrenalinaTotal, waga,
    } = this.state;
    const adrenalinaTotalTemp = adrenalinaTotal;
    this.setState({
      adrenalinaTotal: adrenalinaTotalTemp + dawkaAdrenalinyMg,
      adrenalineCanUse: false,
      adrenalineLoops: loops,
    });
    if (typeof waga === 'undefined' || waga === null || waga === 0) {
      this.setState({
        adrenalinaTotal: adrenalinaTotalTemp + 1,
        adrenalineCanUse: false,
        adrenalineLoops: loops,
      });
    }
  };

  handleAmiodaron = () => {
    const {
      loops, iloscAmiodaronu, waga, amiodaronTotal, dawkaAmiodaronuMg,
    } = this.state;
    const iloscAmiodaronuTemp = iloscAmiodaronu;
    const amiodaronTotalTemp = amiodaronTotal;
    if (waga > 59 || typeof waga === 'undefined') {
      if (iloscAmiodaronu === 0) {
        this.setState({
          amiodaronCanUse: false,
          amiodaronLoops: loops,
          iloscAmiodaronu: iloscAmiodaronuTemp + 1,
          dawkaAmiodaron: 'Podaj 150 mg rozcieńczone do 10 ml 5% glukozą',
          amiodaronTotal: 300,
        });
      } else {
        this.setState({
          amiodaronCanUse: false,
          amiodaronLoops: loops,
          iloscAmiodaronu: iloscAmiodaronuTemp + 1,
          dawkaAmiodaron: '',
          amiodaronTotal: 450,
        });
      }
    } else if (waga > 59 || typeof waga === 'undefined') {
      if (iloscAmiodaronu === 1) {
        this.setState({
          amiodaronCanUse: false,
          amiodaronLoops: loops,
          iloscAmiodaronu: iloscAmiodaronuTemp + 1,
          dawkaAmiodaron: 'Podaj 150 mg rozcieńczone do 10 ml 5% glukozą',
          amiodaronTotal: 450,
        });
      }
    } else {
      this.setState({
        amiodaronCanUse: false,
        amiodaronLoops: loops,
        iloscAmiodaronu: iloscAmiodaronuTemp + 1,
        amiodaronTotal: amiodaronTotalTemp + dawkaAmiodaronuMg,
      });
    }
  };

  StartCounting = () => {
    if (this.timer === null) {
      const divy = (
        <View>
          <Text>Ocena rytmu...</Text>
        </View>
      );
      const date = new Date();
      let currentHours = date.getHours();
      currentHours = `0 ${currentHours}`.slice(-2);
      let currentMinutes = date.getMinutes();
      currentMinutes = `0 ${currentMinutes}`.slice(-2);
      let currentSeconds = date.getSeconds();
      currentSeconds = `0 ${currentSeconds}`.slice(-2);
      this.setState({
        startTime: `${currentHours}:${currentMinutes}:${currentSeconds}`,
        time: 120,
        i: 130,
        loops: 0,
        timePast: 0,
        adrenalineCanUse: true,
        amiodaronCanUse: true,
        adrenalinaTotal: 0,
        amiodaronTotal: 0,
        iloscAmiodaronu: 0,
        playPause: 'square',
      });
      this.timeOld = new Date().valueOf();
      if (this.timer !== null) {
        clearInterval(this.timer);
        clearInterval(this.helperTimer);
        this.timer = null;
        this.helperTimer = null;
      }
      this.timer = setInterval(() => {
        const {
          timePast, time, i, hypothermia, adrenalineLoops, amiodaronLoops, loops, iloscAmiodaronu,
        } = this.state;
        const timePastTemp = timePast;
        this.setState({
          time: 120 - Math.floor((new Date().valueOf() - this.timeOld) / 1000),
          timePast: timePastTemp + 1,
          i: i - 1,
        });
        if (time < 5 && time > 3) {
          this.last10Sec();
        }
        if (i < 12 && i > 0) {
          this.setState({
            time: 0,
            ocenaRytmu: divy,
          });
        }
        if (i < 1) {
          let hypoNumber = 1;
          if (hypothermia === true) {
            hypoNumber = 2;
          }
          if (adrenalineLoops + hypoNumber === loops) {
            this.setState({ adrenalineCanUse: true });
          }
          if (amiodaronLoops + hypoNumber === loops && iloscAmiodaronu < 2) {
            this.setState({ amiodaronCanUse: true });
          }
          const tempLoops = loops;
          this.setState({
            time: 120,
            loops: tempLoops + 1,
            i: 130,
            ocenaRytmu: null,
          });

          this.timeOld = new Date().valueOf();
        }
      }, 1000);
    } else {
      clearInterval(this.timer);
      this.timer = null;
      this.setState({ playPause: 'play' });
    }
  };

  last10Sec = async () => {
    try {
      Audio.setAudioModeAsync({
        playsInSilentModeIOS: true,
        allowsRecordingIOS: false,
        interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
        interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
        shouldDuckAndroid: true,
        playThroughEarpieceAndroid: false,
      });
      await Audio.Sound.createAsync(require('./beepPop.mp3'), {
        shouldPlay: true,
        playsInSilentModeIOS: true,
      });
      // Your sound is playing!
    } catch (error) {
      // An error occurred!
    }
  };

  handleHipoksjaOnPress = () => {
    const { hipoksjaChecked } = this.state;
    const hipoksjaCheckedTemp = hipoksjaChecked;
    this.setState({
      hipoksjaChecked: !hipoksjaCheckedTemp,
    });
  };

  hipowolemiaOnPress = () => {
    const { hipowolemiaChecked } = this.state;
    const hipowolemiaCheckedTemp = hipowolemiaChecked;
    this.setState({
      hipowolemiaChecked: !hipowolemiaCheckedTemp,
    });
  };

  hipoterimaOnPress = () => {
    const { hipotermiaChecked } = this.state;
    const hipotermiaCheckedTemp = hipotermiaChecked;
    this.setState({
      hipotermiaChecked: !hipotermiaCheckedTemp,
    });
  };

  elektrolityOnPress = () => {
    const { elektrolityChecked } = this.state;
    const elektrolityCheckedTemp = elektrolityChecked;
    this.setState({
      elektrolityChecked: !elektrolityCheckedTemp,
    });
  };

  zakrzepOnPress = () => {
    const { zakrzepChecked } = this.state;
    const zakrzepCheckedTemp = zakrzepChecked;
    this.setState({
      zakrzepChecked: !zakrzepCheckedTemp,
    });
  };

  odmaOnPress = () => {
    const { odmaChecked } = this.state;
    const odmaCheckedTemp = odmaChecked;
    this.setState({
      odmaChecked: !odmaCheckedTemp,
    });
  };

  zatruciaOnPress = () => {
    const { zatruciaChecked } = this.state;
    const zatruciaCheckedTemp = zatruciaChecked;
    this.setState({
      zatruciaChecked: !zatruciaCheckedTemp,
    });
  };

  tamponadaOnPress = () => {
    const { tamponadaChecked } = this.state;
    const tamponadaCheckedTemp = tamponadaChecked;
    this.setState({
      tamponadaChecked: !tamponadaCheckedTemp,
    });
  };

  kwasicaOnPress = () => {
    const { kwasicaChecked } = this.state;
    const kwasicaCheckedTemp = kwasicaChecked;
    this.setState({
      kwasicaChecked: !kwasicaCheckedTemp,
    });
  };

  handleWagaChange = (input) => {
    const { dawkaAmiodaronuMg } = this.state;
    const dawkaAdr = `Nabierz 1 ampułkę (1 mg) do strzykawki dziesiątki i rozcieńcz do 10 ml. 
    Podaj ${input / 10} ml, czyli ${input / 100} mg.`;
    let dawkaAmiodaronuTemp = dawkaAmiodaronuMg;
    let dawkaAmi = '';
    if (input < 21) {
      dawkaAmi = `Nabierz 100 mg (2 ml leku) do strzykawki dwudziestki i rozcieńcz do 20 ml
      5% glukozą. Podaj ${input} ml roztworu (${input * 5} mg)`;
      dawkaAmiodaronuTemp = input * 5;
    } else if (input > 20 && input < 31) {
      dawkaAmi = `Nabierz 1 ampułkę (150 mg) do strzykawki dwudziestki i rozcieńcz do 15 ml
      5% glukozą. Podaj ${input / 2} ml roztworu (${input * 5} mg)`;
      dawkaAmiodaronuTemp = input * 5;
    } else if (input > 30 && input < 60) {
      dawkaAmi = 'Rozcieńcz 1 ampułkę (150 mg) rozcieńczoną w 5% glukozie.';
      dawkaAmiodaronuTemp = 150;
    } else if (input > 59) {
      dawkaAmi = 'Podaj 300 mg rozcieńczone do 20 ml w 5% glukozie.';
      dawkaAmiodaronuTemp = 300;
    }
    this.setState({
      waga: input,
      dawkaAdrenaliny: dawkaAdr,
      dawkaAdrenalinyMg: input / 100,
      dawkaAmiodaron: dawkaAmi,
      dawkaAmiodaronuMg: dawkaAmiodaronuTemp,
    });
  }

  render() {
    const {
      adrenalineCanUse,
      amiodaronCanUse,
      playPause,
      hypothermia,
      loops,
      timePast,
      startTime,
      time,
      hipoksjaChecked,
      hipowolemiaChecked,
      hipotermiaChecked,
      elektrolityChecked,
      zakrzepChecked,
      odmaChecked,
      zatruciaChecked,
      tamponadaChecked,
      kwasicaChecked,
      ocenaRytmu,
      waga,
      dawkaAdrenaliny,
      dawkaAmiodaron,
      adrenalinaTotal,
      amiodaronTotal,
    } = this.state;
    return (
      <ToolsContainer style={{ alignItems: 'center' }}>
        {ocenaRytmu}
        <Input
          keyboardType="numeric"
          leftIcon={<MaterialCommunityIcons name="weight-kilogram" size={24} color="#bababa" />}
          placeholder="  Waga pacjenta 100 kg"
          onChangeText={this.handleWagaChange}
          value={waga}
        />
        <View style={styles.timerContainer}>
          <Text style={styles.timerText}>
            {`0${Math.floor(time / 60)}`.slice(-2)}
:
            {`0${Math.floor(time % 60)}`.slice(-2)}
          </Text>
          <TouchableOpacity onPress={this.ResetCouting}>
            <Ionicons style={styles.refresh} name="ios-refresh" size={70} color="black" />
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={this.StartCounting}>
          <Ionicons
            style={styles.icon}
            name={`ios-${playPause}`}
            size={100}
            color={playPause === 'play' ? 'black' : 'grey'}
          />
        </TouchableOpacity>
        <TouchableOpacity disabled={!adrenalineCanUse} onPress={this.Adrenaline}>
          <View style={styles.leki}>
            <View style={styles.drugIcon}>
              <MaterialCommunityIcons
                style={styles.icon}
                name="needle"
                size={100}
                color={adrenalineCanUse === true ? 'red' : 'grey'}
              />
              <BigText color={adrenalineCanUse === true ? 'red' : 'grey'}>Adrenalina</BigText>
            </View>
            <SmallText>{dawkaAdrenaliny}</SmallText>
          </View>
        </TouchableOpacity>
        <MyTextView />
        <MyTextView />
        <TouchableOpacity disabled={!amiodaronCanUse} onPress={this.handleAmiodaron}>
          <View style={styles.leki}>
            <View style={styles.drugIcon}>
              <MaterialCommunityIcons
                style={styles.icon}
                name="needle"
                size={100}
                color={amiodaronCanUse === true ? 'red' : 'grey'}
              />
              <BigText color={amiodaronCanUse === true ? 'red' : 'grey'}>Amiodaron</BigText>
            </View>
            <SmallText>{dawkaAmiodaron}</SmallText>
          </View>
        </TouchableOpacity>
        <MyTextView />
        <TouchableOpacity disabled={this.timer !== null} onPress={this.Hypothermia}>
          <View style={styles.rowHipo}>
            <Ionicons
              style={styles.icon}
              name={`ios-${hypothermia === true ? 'checkbox' : 'square'}-outline`}
              size={30}
              color="grey"
            />
            <Text> Hipotermia</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.row}>
          <View style={styles.column}>
            <Text style={styles.mainText}>Ilość Adrenaliny:</Text>
            <Text style={styles.mainText}>Ilość Amiodaronu:</Text>
            <Text style={styles.mainText}>Ilość pętli RKO:</Text>
            <Text style={styles.mainText}>Czas trwania resuscytacji:</Text>
            <Text style={styles.mainText}>Godz. rozpoczęcia:</Text>
          </View>
          <View style={styles.column}>
            <Text style={styles.secondaryText}>{`${adrenalinaTotal} mg`}</Text>
            <Text style={styles.secondaryText}>{`${amiodaronTotal} mg`}</Text>

            <Text style={styles.secondaryText}>{loops}</Text>
            <Text style={styles.secondaryText}>
              {`0${Math.floor(timePast / 60)}`.slice(-2)}
:
              {`0${Math.floor(timePast % 60)}`.slice(-2)}
            </Text>
            <Text style={styles.secondaryText}>{startTime}</Text>
          </View>
        </View>
        <MyTextView />
        <BoldText>Odwracalne przyczyny NZK</BoldText>
        <MyTextView />
        <HiT
          hipoksjaChecked={hipoksjaChecked}
          hipoksjaOnPress={this.handleHipoksjaOnPress}
          hipowolemiaChecked={hipowolemiaChecked}
          hipowolemiaOnPress={this.hipowolemiaOnPress}
          hipotermiaChecked={hipotermiaChecked}
          hipotermiaOnPress={this.hipoterimaOnPress}
          elektrolityChecked={elektrolityChecked}
          elektrolityOnPress={this.elektrolityOnPress}
          zakrzepChecked={zakrzepChecked}
          zakrzepOnPress={this.zakrzepOnPress}
          odmaChecked={odmaChecked}
          odmaOnPress={this.odmaOnPress}
          zatruciaChecked={zatruciaChecked}
          zatruciaOnPress={this.zatruciaOnPress}
          tamponadaChecked={tamponadaChecked}
          tamponadaOnPress={this.tamponadaOnPress}
          kwasicaChecked={kwasicaChecked}
          kwasicaOnPress={this.kwasicaOnPress}
        />
      </ToolsContainer>
    );
  }
}
