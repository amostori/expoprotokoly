import React from 'react';
import {
  StyleSheet, View, Text, TextInput,
} from 'react-native';
import { ToolsContainer } from '../../components/container';

export default class Pressure extends React.Component {
  constructor(props) {
    super(props);
    this.state = { systolic: '', diastolic: '', pressure: 0 };
  }

  calculate = () => {
    if (this.state.diastolic >= 1 && this.state.systolic >= 1) {
      const mag = Math.floor(
        Number(this.state.diastolic)
          + (Number(this.state.systolic) - Number(this.state.diastolic)) / 3,
      );

      this.setState({ pressure: mag });
    }
  };

  render() {
    return (
      <ToolsContainer>
        <View style={styles.row}>
          <TextInput
            onChangeText={(systolic) => {
              this.setState(
                {
                  systolic,
                },
                () => {
                  this.calculate();
                },
              );
            }}
            value={this.state.systolic}
            maxLength={3}
            style={styles.textInput}
            placeholder="Skurczowe"
            textAlign="center"
            keyboardType="numeric"
          />
          <Text> / </Text>
          <TextInput
            onChangeText={(diastolic) => {
              this.setState({ diastolic }, () => {
                this.calculate();
              });
            }}
            value={this.state.diastolic}
            maxLength={3}
            style={styles.textInput}
            placeholder="Rozkurczowe"
            textAlign="center"
            keyboardType="numeric"
          />
        </View>
        <View style={{ alignItems: 'center' }}>
          <Text style={{ marginTop: 15 }}>
            Średnie ciśnienie tętnicze wynosi:
            {' '}
            {this.state.pressure}
            {' '}
mmHg
          </Text>
          <Text style={{ marginTop: 15, fontSize: 10 }}>
            MAP = rozkurczowe + ( skurczowe - rozkurczowe ) / 3
          </Text>
        </View>
      </ToolsContainer>
    );
  }
}

const styles = StyleSheet.create({
  column: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginLeft: 5,
  },
  textInput: {
    height: 40,
    backgroundColor: '#d1d4d8',
    width: 110,
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  container: {
    minHeight: '100%',
    backgroundColor: '#E9ECF0',
    alignItems: 'center',
  },
  row: {
    marginTop: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
