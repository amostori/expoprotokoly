import React from 'react';
import {
  StyleSheet, Text,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { ToolsContainer } from '../../components/container';
import {
  MyTextView, RedBoldText, BoldText, ListaTextView,
} from '../../components/TextViews';

export default class Apgar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pointsLabel: '0',
      points1: 0,
      points2: 0,
      points3: 0,
      points4: 0,
      points5: 0,
      points1Options: [
        { label: '0 Sinica całego ciała', value: '0' },
        { label: '1 Sinica obwodowa', value: '1' },
        { label: '2 Całe różowe', value: '2' },
      ],
      points2Options: [
        { label: '0 Brak tętna', value: '0' },
        { label: '1 puls <100/min', value: '1' },
        { label: '2 puls >100/min', value: '2' },
      ],
      points3Options: [
        { label: '0 Brak reakcji', value: '0' },
        { label: '1 grymas twarzy', value: '1' },
        { label: '2 kichanie, kaszel', value: '2' },
      ],
      points4Options: [
        { label: '0 dziecko wiotkie', value: '0' },
        { label: '1 napięcie zmniejszone, kończyny zgięte', value: '1' },
        { label: '2 napięcie prawidłowe', value: '2' },
      ],
      points5Options: [
        { label: '0 Brak oddechu', value: '0' },
        { label: '1 oddech wolny, nieregularny', value: '1' },
        { label: '2 dziecko płaczące', value: '2' },
      ],
    };
  }

  calculate = () => {
    this.setState({
      pointsLabel:
        Number(this.state.points1)
        + Number(this.state.points2)
        + Number(this.state.points3)
        + Number(this.state.points4)
        + Number(this.state.points5),
    });
  };

  renderPunkty = () => {
    const { pointsLabel } = this.state;
    switch (pointsLabel) {
      case 1:
        return (
          <BoldText style={{ fontSize: 25 }}>
            Razem:
            {` ${pointsLabel}`}
            {' '}
punkt
          </BoldText>
        );

      case 2:
        return (
          <BoldText style={{ fontSize: 25 }}>
            Razem:
            {` ${pointsLabel}`}
            {' '}
punkty
          </BoldText>
        );
      case 3:
        return (
          <BoldText style={{ fontSize: 25 }}>
            Razem:
            {` ${pointsLabel}`}
            {' '}
punkty
          </BoldText>
        );
      case 4:
        return (
          <BoldText style={{ fontSize: 25 }}>
            Razem:
            {` ${pointsLabel}`}
            {' '}
punkty
          </BoldText>
        );
      default:
        return (
          <BoldText style={{ fontSize: 25 }}>
            Razem:
            {` ${pointsLabel}`}
            {' '}
punktów
          </BoldText>
        );
    }
  };

  render() {
    return (
      <ToolsContainer>
        <MyTextView>
          Służy do oceny głębokości świeżorodka po porodzie. Ocenia się pięć elementów, przyznaje
          punkty od 0 do 2 i sumuje punktację.
        </MyTextView>
        <MyTextView />
        <RedBoldText>I Kolor skóry</RedBoldText>
        <MyTextView />
        <RNPickerSelect
          items={this.state.points1Options}
          placeholder={{}}
          onValueChange={(value) => {
            this.setState({ points1: value }, () => {
              this.calculate();
            });
          }}
        >
          <Text style={styles.pickerChoose}>
            {this.state.points1Options[Number(this.state.points1)].label}
          </Text>
        </RNPickerSelect>
        <MyTextView />

        <RedBoldText>II Akcja serca na minutę</RedBoldText>
        <MyTextView />

        <RNPickerSelect
          items={this.state.points2Options}
          style={styles.picker}
          placeholder={{}}
          onValueChange={(value) => {
            this.setState({ points2: value }, () => {
              this.calculate();
            });
          }}
        >
          <Text style={styles.pickerChoose}>
            {this.state.points2Options[Number(this.state.points2)].label}
          </Text>
        </RNPickerSelect>
        <MyTextView />

        <RedBoldText>III Reakcja na cewnik do odsysania</RedBoldText>
        <MyTextView />

        <RNPickerSelect
          items={this.state.points3Options}
          placeholder={{}}
          onValueChange={(value) => {
            this.setState({ points3: value }, () => {
              this.calculate();
            });
          }}
        >
          <Text style={styles.pickerChoose}>
            {this.state.points3Options[Number(this.state.points3)].label}
          </Text>
        </RNPickerSelect>
        <MyTextView />

        <RedBoldText>IV Napięcie mięśniowe</RedBoldText>
        <MyTextView />

        <RNPickerSelect
          items={this.state.points4Options}
          placeholder={{}}
          onValueChange={(value) => {
            this.setState({ points4: value }, () => {
              this.calculate();
            });
          }}
        >
          <Text style={styles.pickerChoose}>
            {this.state.points4Options[Number(this.state.points4)].label}
          </Text>
        </RNPickerSelect>
        <MyTextView />

        <RedBoldText>V Częstość oddechu</RedBoldText>
        <MyTextView />

        <RNPickerSelect
          items={this.state.points5Options}
          placeholder={{}}
          onValueChange={(value) => {
            this.setState({ points5: value }, () => {
              this.calculate();
            });
          }}
        >
          <Text style={styles.pickerChoose}>
            {this.state.points5Options[Number(this.state.points5)].label}
          </Text>
        </RNPickerSelect>
        <MyTextView />

        {this.renderPunkty()}
        <MyTextView />
        <BoldText style={{ fontSize: 25 }}>Interpretacja</BoldText>
        <MyTextView />
        <ListaTextView>10-9 noworodek w dobrym stanie</ListaTextView>
        <ListaTextView>8 - 7 zmęczenie porodem</ListaTextView>
        <ListaTextView>6 - 4 zamartwica średniego stopnia (sina)</ListaTextView>
        <ListaTextView>3 - 0 zamartwica ciężka (blada)</ListaTextView>
        <MyTextView />
        <MyTextView />
        <MyTextView />
      </ToolsContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    minHeight: '100%',
    backgroundColor: '#E9ECF0',
    alignItems: 'center',
  },
  picker: {
    alignItems: 'center',
    backgroundColor: '#d1d4d8',
  },
  text: {
    textAlign: 'center',
    marginTop: 20,
  },
  pickerChoose: {
    marginVertical: 10,
    marginHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: '#d1d4d8',
    textAlign: 'center',
  },
  bigText: {
    fontWeight: 'bold',
    fontSize: 15,
    marginVertical: 20,
    color: '#FF5353',
  },
});
