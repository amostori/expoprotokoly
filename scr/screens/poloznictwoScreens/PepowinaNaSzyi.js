import React from 'react';
import { ScrollContainer } from '../../components/container';
import { MyTextView, BoldText } from '../../components/TextViews';

const PepowinaNaSzyi = () => (
  <ScrollContainer>
    <MyTextView>Kiedy urodzi się główka dziecka oceń obecność pępowiny wokół szyi.</MyTextView>
    <MyTextView>
      Jeśli jest obecna spróbuj ją usunąć wsuwajac pod nią dwa palce od strony krocza matki.
    </MyTextView>
    <MyTextView>
      W rzadkich przypadkach może się to nie udać. Wtedy załóż dwa zaciski do pępowiny, przetnij ją
      nożyczkami i doprowadź poród do końca jak najszybciej.
    </MyTextView>
    <MyTextView>
      <BoldText>Do cięcia nie używaj skalpela - jest to zbyt niebezpieczne.</BoldText>
    </MyTextView>
  </ScrollContainer>
);

export default PepowinaNaSzyi;
