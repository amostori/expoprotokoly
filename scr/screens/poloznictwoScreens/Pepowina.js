import React from 'react';
import { ScrollContainer } from '../../components/container';
import { MyTextView } from '../../components/TextViews';

const Pepowina = () => (
  <ScrollContainer>
    <MyTextView>Ułóż rodzącą na plecach, z pośladkami uniesionymi.</MyTextView>
    <MyTextView>
      Wsuń dwa palce w sterylnej rękawiczce do pochwy i odsuń główkę dziecka od kanału rodnego.
    </MyTextView>
    <MyTextView>Upewnij się co do obecności tętna na pępowinie.</MyTextView>
    <MyTextView>
      Przetransportuj pacjentkę do szpitala pamiętając o tym, by nadal odpychać dziecko palcami.
    </MyTextView>
    <MyTextView>Poproś rodzącą, aby starała się powstrzymać parcie w czasie skurczu.</MyTextView>
    <MyTextView>Palce można wyjąć dopiero na polecenie personelu oddziału położniczego.</MyTextView>
  </ScrollContainer>
);

export default Pepowina;
