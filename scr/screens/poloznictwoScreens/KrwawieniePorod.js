import React from 'react';
import { ScrollContainer } from '../../components/container';
import { MyTextView, BoldText } from '../../components/TextViews';

const KrwawieniePorod = () => (
  <ScrollContainer>
    <MyTextView>
      <BoldText>Masuj macicę.</BoldText>
    </MyTextView>
    <MyTextView>
      Przystaw noworodka do piersi matki (ssanie piersi przez dziecko zwiększa wydzielanie
      oksytocyny).
    </MyTextView>
    <MyTextView>
      Jeśli łożysko jest uszkodzone przyłóż do krocza opatrunek - nie wkładaj opatrunku do dróg
      rodnych.
    </MyTextView>
  </ScrollContainer>
);

export default KrwawieniePorod;
