import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollContainer } from '../../components/container';
import {
  MyTextView, Hypertekst, VersionERC, Sol, TiM,
} from '../../components/TextViews';

export default class Rzucawka extends Component {
  static propTypes = {
    navigation: PropTypes.object,
  };

  handleMagnez = () => {
    const { navigation } = this.props;
    navigation.navigate('Magnez');
  };

  handleDiazepam = () => {
    const { navigation } = this.props;
    navigation.navigate('Diazepam');
  };

  render() {
    return (
      <ScrollContainer>
        <MyTextView>
          Wlew 4 g
          <Hypertekst onPress={this.handleMagnez}>Magnezu 20%</Hypertekst>
          w 250 ml
          <Sol />
          przez 5 minut, następnie 1 g/24h.
        </MyTextView>
        <MyTextView>
          Jeśli drgawki nawracają podaj kolejną dawkę Magnezu 2 - 4 g przez 5 minut.
        </MyTextView>
        <MyTextView>
          W przypadku braku efektu rozważ
          <Hypertekst onPress={this.handleDiazepam}>Diazepam</Hypertekst>
          (relanium) 5 - 10 mg iv.
        </MyTextView>
        <TiM />
        <VersionERC />
      </ScrollContainer>
    );
  }
}
