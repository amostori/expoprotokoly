import Porod from './Porod';
import Pepowina from './Pepowina';
import PepowinaNaSzyi from './PepowinaNaSzyi';
import KrwawieniePorod from './KrwawieniePorod';
import Rzucawka from './Rzucawka';

export {
  Porod, Pepowina, PepowinaNaSzyi, KrwawieniePorod, Rzucawka,
};
