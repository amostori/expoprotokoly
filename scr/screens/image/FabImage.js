import React from 'react';
import { View, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  image: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80,
    width: 80,
  },
  fabCover: {
    padding: 12,
  },
});

const FabImage = () => (
  <View style={styles.fabCover}>
    <Image source={require('./cpr_fab_icon.png')} style={styles.image} resizeMode="center" />
  </View>
);

export default FabImage;
