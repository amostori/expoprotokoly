import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import Navigator from './config/routesTest';

EStyleSheet.build({
  $primaryBlue: '#4F6D7A',
  $primaryRed: '#ff5353',
  $darkText: '#343434',
  $border: '#e2e2e2',
  $routeBackgroundColor: '#f4511e',
  $routeTintColor: '#fff',
  $badanieButtonBackground: '#ea473f',
  // $outline: 1,
});

export default () => <Navigator />;
