import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import { createAppContainer } from 'react-navigation';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import NavigationService from './scr/config/NavigationService';
import reducers from './scr/redux/reducers';
import Navigator from './scr/config/routesTest';

const AppContainer = createAppContainer(Navigator);

EStyleSheet.build({
  $primaryBackground: '#e9ecf0',
  $primaryBlue: '#4F6D7A',
  $primaryRed: '#ff5353',
  $darkText: '#343434',
  $lightText: '#bababa',
  $border: '#e2e2e2',
  $routeBackgroundColor: '#f4511e',
  $routeTintColor: '#fff',
  $badanieButtonBackground: '#ea473f',
  //  $outline: 1,
  // outline: 1 powoduje widoczne ramki wokół obiektów
});

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['masowkaReducer', 'burnsReducer', 'triageTrainingReducer'],
  stateReconciler: autoMergeLevel2,
};

const pReducer = persistReducer(persistConfig, reducers);
const store = createStore(pReducer);
const persistor = persistStore(store);
const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <AppContainer
        ref={(navigatorRef) => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    </PersistGate>
  </Provider>
);

export default App;
